
#!/usr/bin/env python
import sys
#sys.path.append('/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det')
import pandas as pd
import numpy as np
import os
from pysolar import solar
from PIL import Image
import imageio
import datetime
import fog_logging
import logging
from fog_config import Config, external_info_path , collecting , fog_main_preprocessing , preprocess_info , hdfs_path_class , hdfs_path_class
import requests
from hdfs import InsecureClient
from pathlib import Path
from sklearn.preprocessing import MinMaxScaler
import multiprocessing as mp
import difflib
import json

def norming_features(df2 ,feature_summary):
    df = df2.copy()
    # target = df.pop("target")
    normed_df = pd.DataFrame()
    columns = df.columns.tolist()
    for col in columns:
        min = feature_summary.loc["min" , col]
        max = feature_summary.loc["max" , col]
        normed_data = (df.loc[:,col] - min) / (max - min)
        normed_df[col] = normed_data
    # normed_df["target"] = target
    return normed_df

#global_path를 가져오기위한 환경구성
def set_globvar_to_path(env):
    if env == "local":
        data_path = Config.data_local_path
        root_path = Config.root_local_path
        model_path = Config.model_local_path
    elif env == "platform":
        data_path = Config.data_platform_path
        root_path = Config.root_platform_path
        model_path = Config.total_model_platform_path
    elif env == "dgx_share2":
        data_path = Config.data_dgx_path_nas2
        root_path = Config.root_dgx_path
        model_path = Config.model_dgx_path
    return data_path , root_path , model_path


# 학습할때 출력된 데이터의 평균 ,편차와 변수 순서 정보 갖고오기
# def get_order_and_summary(summary_path):
#     summary = pd.read_csv(summary_path , index_col=0)   ### change path
#     input_columns_order = summary.columns.tolist()
#     if "target" in input_columns_order:
#         input_columns_order.remove("target")
#         summary.pop("target")
#     column_length = len(input_columns_order)
#     return summary, input_columns_order, column_length



def check_temp_col(temp_col):
    if temp_col.split("_")[1] == "ncAlbedo":
        return temp_col.split("_")[0]+"_"+"Albedo"
    else:
        return temp_col


# # 태양천정각과 hsr반사도 값 배열에 추가
# def making_detectable_df_use_zenith(final_array,ticketNo, input_columns_order):
#     latlon_900_900 = pd.read_csv('{}/LATLON_KO_2000.txt'.format(root_path),header  = None, sep = '\t')
#     latlon_900_2d = np.array(latlon_900_900)
#     reshaped_to_3d_900 = np.reshape(latlon_900_2d,(900,900,2))
#     zenith_at_time = get_zenith_900_faster(latlon_900_2d, str(ticketNo))
#     final_array[:,:,input_columns_order.index("zenith")] = zenith_at_time
#
#     get_hsr_start = datetime.datetime.now()
#     final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform(str(ticketNo) )
#     get_hsr_end = datetime.datetime.now()
#     print("loading hsr data Duration : {}".format(get_hsr_end - get_hsr_start))
#
#     final_array[:,:,input_columns_order.index("HSR_value") ] = temp_hsr_reshaped
#     detectable_df = final_array_to_detectable_df(final_array, input_columns_order)
#     return detectable_df


def making_detectable_array_use_hsr(final_array, ticketNo, input_columns_order, data_path,root_path):
    final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform(str(ticketNo), data_path,root_path)
    final_array[:,:,input_columns_order.index("HSR_value") ] = temp_hsr_reshaped
    return final_array

def making_detectable_array_use_hsr_hdfs(final_array, ticketNo, input_columns_order, client , root_path):
    final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform2(str(ticketNo), client, root_path)
    final_array[:,:,input_columns_order.index("HSR_value") ] = temp_hsr_reshaped
    return final_array


def making_detectable_array_use_zenith(final_array,ticketNo,input_columns_order,root_path):
    latlon_900_900 = pd.read_csv('{}/LATLON_KO_2000.txt'.format(root_path),header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)
    zenith_at_time = get_zenith_900_faster(latlon_900_2d, str(ticketNo))
    final_array[:,:,input_columns_order.index("zenith")] = zenith_at_time
    return final_array

# def final_array_to_detectable_df(final_array, input_order):
#     reshaped_number_by_len = np.reshape(final_array,(810000,len(input_order)))
#     detectable_df = pd.DataFrame(reshaped_number_by_len, columns = input_order)
#     return detectable_df

def final_array_to_detectable_df(final_array, input_order, ranking):
    reshaped_number_by_len = np.reshape(final_array,(810000,len(input_order)))
    new_col_names = list(map(lambda x: x  + "_" + str(ranking) ,  input_order ))
    detectable_df = pd.DataFrame(reshaped_number_by_len, columns = new_col_names)
    return detectable_df

# 표준화
def making_normalization_data(detectable_df, summary):
    detection_df = detectable_df.copy()
    cols = summary.columns.tolist()
    for i in cols:
        temp_normaliztion =  (detection_df.loc[:,i] - summary.loc["mean",i]) / summary.loc["std",i]
        detection_df.loc[:,i] = temp_normaliztion
    detect_df = detection_df.copy()

    return detect_df


# 태양 천정각 빠르게 계산
def get_zenith_900_faster(latlon_900_2d, time):
    year = int(time[:4])
    month = int(time[4:6])
    day = int(time[6:8])
    hour = int(time[8:10])
    minute = int(time[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime.datetime(year,month,day,hour,minute, tzinfo =datetime.timezone.utc)  -  datetime.timedelta(hours = 9)
    temp_array = get_altitude_faster(latlon_900_2d, dobj)
    # 900 900 2 차원 배열로 만듬
    whole_array = np.reshape(temp_array, (900,900) )
    final_array = float(90) - whole_array
    return final_array

# 태양 고도 빠르게 계산 함수
def get_altitude_faster(latlon_900_2d, when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(latlon_900_2d[:,0])
    hour_angle = solar.get_hour_angle(when, latlon_900_2d[:,1] )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))





def transform_gk_3600_to_900(array_3600):
    array_list = []
    for i in range(4):
        for j in range(4):
            temp_array = array_3600[i::4 ,j::4]
            array_list.append(temp_array)

    final_array = sum(array_list) / 16
    return final_array





def get_final_gk2a_pathlist_by_time_and_config(each_time , summary , log, data_path):
    summary_cols = summary.columns.tolist()
    if "zenith" in summary_cols: summary_cols.remove("zenith")
    if "HSR_value" in summary_cols: summary_cols.remove("HSR_value")
    if "target" in summary_cols: summary_cols.remove("target")
    summary_channels = list(map(lambda x: x.split("_")[0] , summary_cols ))
    summary_channels = pd.Series(summary_channels).unique().tolist()
    yearmonth = each_time[:6]
    day = each_time[6:8]
    hour = each_time[8:10]
    nas_data_path_dic = fog_config.nas_data_path_dic
    final_path_list = []
    for channel in summary_channels:
        nas_path = nas_data_path_dic[channel].format(data_path, yearmonth , day , hour , each_time)
        if Path(nas_path).is_dir():
            files = list(map(lambda x: os.path.join(nas_path , x) ,  os.listdir(nas_path) ))
            for file in files: final_path_list.append(file)
        else:
            log.info("\n \n there is no directory here \n \n")
    return final_path_list

# 1800 1800 크기의 이미지 강제로 900 900 크기로 만드는 함수
def transform_gk_1800_to_900(array_1800):
    a = array_1800[::2, ::2]
    b = array_1800[::2, 1::2]
    c = array_1800[1::2, ::2]
    d = array_1800[1::2, 1::2]
    sum = a +b+ c+d
    result_array = sum / 4
    return result_array


def transform(data , chanels):
    df = data.copy()
    all_chanels = chanels.copy()
    all_columns = df.columns.tolist()
    transformed_df  = pd.DataFrame()
    for channel in all_chanels:
        sub_channels = [x for x in df.columns.tolist() if channel in x]
        sub_channels.sort(reverse = True)
        for sub_channel in sub_channels: all_columns.remove(sub_channel)
        transformed_df[channel] =  df.loc[:,sub_channels].values.tolist()
    for left_column in all_columns:
        transformed_df[left_column] = df[left_column].values.tolist()
    return transformed_df





def get_hdfs_path_list_by_time(ticketNo, input_columns_order):
    convert_dict = {"Radiance" : "Radiance" , "Effective" : "Effective_Brightness_Temperature" , "Brightness" : "Brightness_Temperature" , "Albedo" :"Albedo"  }
    year_month = ticketNo[:6]
    day = ticketNo[6:8]
    final_path_list = []
    features = input_columns_order.copy()
    if "zenith" in features: features.remove("zenith")
    if "HSR_value" in features: features.remove("HSR_value")
    if "target" in features: features.remove("target")
    for feature in features:
        channel = feature.split("_")[0]
        type = feature.split("_")[1]
        type = convert_dict[type]
        if "vi" in channel:
            if "vi006" in channel:
                distance = "05"
            else:
                distance = "10"
        else:
            distance = "20"
        final_path_list.append(collecting.data_collection_path_hdfs_gk2a.format(channel.upper(),year_month,day,channel,distance,ticketNo,type))
    return final_path_list



# 학습할때 출력된 데이터의 평균 ,편차와 변수 순서 정보 갖고오기




def get_hsr_and_transform2(time, client,root_path):
    year_month = time[0:6]
    day = time[6:8]
    # file_path = '/share/3_데이터/HSR/result/' +year_month  +"/" + day + "/result/RDR_CMP_HSR_PUB_" + time +".bin/RDR_CMP_HSR_PUB_" + time + ".txt"
    #file_path = '/share/3_데이터/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'.format(year_month,day,time,time)
    file_path =collecting.data_collection_path_hdfs_hsr.format(year_month,day,time)

    status = client.status(file_path , strict = False)

    if status == None:
        wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )
        wrong_array2 = wrong_array.copy()
        print("no hsr value inserting wrong arrays")
        return wrong_array , wrong_array2

    with client.read(file_path) as file:
        hsr_df = pd.read_csv(file, header = None)
    hsr_np = hsr_df.values
    # mapping_table = pd.read_csv("hsr_processing/gk2a_hsr_int_pixel.csv")
    mapping_table = pd.read_csv("{}/gk2a_hsr_int_pixel.csv".format(root_path))
    # gk2a 영역이 더 넓기 때문에 필요 없는 데이터 제거
    for_merge_table = mapping_table.loc[(mapping_table['hsr_y'] >= 0 ) & (mapping_table['hsr_y'] <= 2880) & (mapping_table['hsr_x'] >= 0 ) & (mapping_table['hsr_x'] <= 2304 ),: ]
    # hsr 반사도 값 갖고 오기
    for_merge_table['hsr_values'] =  list(map(lambda x,y: hsr_np[ int(y) ][ int(x) ], for_merge_table['hsr_x'], for_merge_table['hsr_y'] ))
    # hsr 반사도와 gk2a 픽셀 정보 나란히 갖고오기
    for_merge_table_modified = for_merge_table.loc[:,['gk2a', 'hsr_values']]
    # hsr과 위치정보 테이블 융합 후 hsr value 만 따로 분리
    hsr_transformed_np = pd.merge(mapping_table,for_merge_table_modified, on = 'gk2a', how = 'outer')['hsr_values'].values
    # nan 값 모드 -50000으로 변경
    hsr_transformed_np_modified =  np.nan_to_num(hsr_transformed_np, nan = -50000)
    # 900 900 크기로 변경
    final_hsr_reshaped = np.reshape(hsr_transformed_np_modified, (900,900))
    temp_hsr_reshaped = final_hsr_reshaped.copy()
    # 최소값 0으로 지정
    temp_hsr_reshaped =temp_hsr_reshaped.clip(min = 0)
    return final_hsr_reshaped , temp_hsr_reshaped



def transform(data , chanels):
    df = data.copy()
    all_chanels = chanels.copy()
    all_columns = df.columns.tolist()
    transformed_df  = pd.DataFrame()
    for channel in all_chanels:
        sub_channels = [x for x in df.columns.tolist() if channel in x]
        sub_channels.sort(reverse = True)
        for sub_channel in sub_channels: all_columns.remove(sub_channel)
        transformed_df[channel] =  df.loc[:,sub_channels].values.tolist()
    for left_column in all_columns:
        transformed_df[left_column] = df[left_column].values.tolist()
    return transformed_df



def make_3_elements(data , cols):
    df = data.copy()
    columns = cols.copy()
    for col in columns:
        if type(df[col].iloc[0]) == list:
            if len(df[col].iloc[0]) == 2:
                df[col] = list(map(lambda x: [x[0] , (x[0] + x[1]) / 2, x[1] ] , df[col] ))
            elif len(df[col].iloc[0]) == 3:
                df[col]  = df[col].tolist()
            else:
                print("wrong element : {}".format(col))
        else:
            df[col] = list(map(lambda x: [x, x, x] , df[col]  ))
    return df



def making_detectable_df_from_nas2(arg_list):
    rank, final_folder_path, latlon_900_2d, current_time , total_model_dic  = arg_list


    gk2a_using_chanels = total_model_dic["gk2a_using_chanels"]
    input_order = total_model_dic["input_columns_order"]
    column_length = total_model_dic["column_length"]

    log = fog_logging.get_log_view(1, "platform", False, 'fog_inferencing_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_inferencing_error_log')

    ambari_host = total_model_dic["ambari_host"]
    auth_pass = total_model_dic["auth_pass"]
    ambari_cluster_name  = total_model_dic["ambari_cluster_name"]
    auth_user = total_model_dic["auth_user"]

    hdfs_port = total_model_dic["hdfs_port"]
    full_array = np.full((900,900,column_length), -5000.0 , dtype = np.float32)

    for i in range(len(final_folder_path)):
        hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user)

        if hdfs_status  !=  200:
            log.info("hdfs status : {}".format(hdfs_status))
            continue
        else:
            working_hdfs_path = "http://{}:{}".format(active_host , hdfs_port)

        client = InsecureClient(working_hdfs_path, user="hdfs")

        path = final_folder_path[i]
        x= path.split("/")
        temp_col = x[6].lower() + "_" + x[9].split(".")[2].split("_")[0]
        final_pos = input_order.index(temp_col)
        try:
            file_status = client.status(path , strict = False)
        except Exception as e:
            error_log.error("error find status : {}".format(e))
            continue

        if file_status == None:
            error_log.error("critical error - no file : {}".format(path))
            error_log.error("array  : {} - {}".format(current_time , temp_col))
            wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )
            full_array[:,:,final_pos] = wrong_array
            error_log.error("inserting wrong array")
            continue
        try:
            with client.read(path) as file:
                temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
            log.info('loaded success')
        except Exception as e:
            error_log.error("critical error - no file : {}".format(e))
            error_log.error("array  : {} - {}".format(current_time , temp_col))
            wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )

            full_array[:,:,final_pos] = wrong_array
            error_log.error("inserting wrong array")
            continue
        log.info('loaded : {}'.format(temp_col))
        try:
            print(temp_array[:5, -1])
            if '_vi' in path:
                if "_vi006" in path:
                    try:
                        modified_array = transform_gk_3600_to_900(temp_array)
                        full_array[:,:,final_pos] = modified_array
                    except:
                        error_log.error("string data in the array")
                        error_log.error("extra preprocessing")
                        temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
                        error_log.error("flatten and numeric")
                        temp_array = np.reshape(temp_array , (3600 , 3600))
                        error_log.error("reshaping done")
                        modified_array = transform_gk_3600_to_900(temp_array)
                        error_log.error("transform done")
                        full_array[:,:,final_pos] = modified_array
                        error_log.error("inserting done")
                else:
                    try:
                        modified_array = transform_gk_1800_to_900(temp_array)
                        full_array[:,:,final_pos] = modified_array
                    except:
                        error_log.error("string data in the array : {} - {}".format(current_time , temp_col))
                        error_log.error("extra preprocessing")
                        temp_array = pd.to_numeric(temp_array.flatten() , errors = 'coerce')
                        error_log.error("flatten numeric")
                        temp_array = np.reshape(temp_array , (1800, 1800))
                        error_log.error("reshaping done")
                        modified_array = transform_gk_1800_to_900(temp_array)
                        error_log.error("transforming done")
                        full_array[:,:,final_pos] = modified_array
                        error_log.error("inserting done")
            else:
                try:
                    full_array[:,:,final_pos] = temp_array
                except:
                    error_log.error("string data in the array :{} - {}".format(current_time , temp_col))
                    error_log.error("extra preprocessing")
                    temp_array = pd.to_numeric(temp_array.flatten() , errors = 'coerce' )
                    error_log.error("temp_array done")
                    temp_array = np.reshape(temp_array , (900 , 900 ))
                    error_log.error("reshaping done")
                    full_array[:,:,final_pos] = temp_array
                    error_log.error("insering done")
        except Exception as e:
            error_log.error("critical issue : {}".format(e) )
            error_log.error("array  : {} - {}".format(current_time , temp_col))
            wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )
            error_log.error("inserting wrong array")
            full_array[:,:,final_pos] = wrong_array


    root_path = external_info_path.root_path

    modified_real =  datetime.datetime.strptime(current_time ,"%Y%m%d%H%M" ) + datetime.timedelta(hours  = 9 )
    modified_time = datetime.datetime.strftime(modified_real ,"%Y%m%d%H%M" )
    log.info(modified_time)

    #if rank == 5 or rank == 0:
    try:
        final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform2(modified_time , client , root_path)
        log.info("success loading hsr")
        log.info("success transform hsr shape")
        try:
            full_array[:,:,input_order.index("HSR_value") ] = temp_hsr_reshaped
            log.info("success inserting hsr")
        except:
            error_log.error("string data in the hsr array :{}".format(modified_time))
            error_log.error("extra preprocessing")
            temp_hsr_reshaped = pd.to_numeric(temp_hsr_reshaped.flatten() , errors = 'coerce' )
            error_log.error("temp_array done")
            temp_hsr_reshaped = np.reshape(temp_hsr_reshped , (900 , 900 ))
            error_log.error("reshaping done")
            full_array[:,:,input_order.index("HSR_value") ] = temp_hsr_reshaped
            error_log.error("insering done")
    except Exception as e:
        error_log.error("error loading hsr : {}".format(e))
        error_log.error("hsr array at : {}".format(modified_time))
        wrong_array = np.full((900,900), -5000.0  ,dtype = np.float32)
        error_log.error("inserting wrong array")
        full_array[:,:,input_order.index("HSR_value") ] = wrong_array

    zenith_at_time = get_zenith_900_faster(latlon_900_2d, modified_time )
    full_array[:,:,input_order.index("zenith")] = zenith_at_time
    return full_array , rank





def make_final_seq_array(final_conv_shape_input , normed_whole_df , final_seq_array):
    dimension_len = len(final_conv_shape_input.shape)
    if dimension_len == 3:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0] , indices[1] , indices[2]) )
            for loc in loc_list:
                seq_num = loc[0]
                y_num = loc[1]
                x_num = loc[2]
                final_seq_array[:, seq_num , y_num , x_num] = normed_whole_df[col]
    elif dimension_len == 2:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0] , indices[1]))
            for loc in loc_list:
                y_num = loc[0]
                x_num = loc[1]
                final_seq_array[:,y_num , x_num] = normed_whole_df[col]
    elif dimension_len ==1:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0]))
            for loc in loc_list:
                x_num = loc[0]
                final_seq_array[:, x_num] = normed_whole_df[col]
    return final_seq_array









#
#

#
# def make_final_seq_array(df , final_conv_shape_format , final_seq_array , sequence_len):
#     final_conv_shape_format_shape = np.array(final_conv_shape_format).shape
#     if sequence_len > 1:
#         if len(final_conv_shape_format_shape) == 2:
#             for depth_index in range(0,sequence_len):
#                 for y_index , listing in enumerate(final_conv_shape_format):
#                     for x_index , col in enumerate(listing):
#                         # print(col , ": {} , {}".format(y_index, x_index))
#                         if ":" not in col:
#                             current_col = col + "_{}".format(str(depth_index))
#                             current_data = df.loc[:, current_col]
#                             final_seq_array[:, depth_index , y_index , x_index] = current_data
#                         else:
#                             first_col =  col.split(":")[0] + "_{}".format(str(depth_index))
#                             second_col = col.split(":")[1] + "_{}".format(str(depth_index))
#                             current_data_first = df.loc[:,first_col]
#                             current_data_second = df.loc[:,second_col]
#                             current_data = (current_data_first + current_data_second) / 2
#                             final_seq_array[:, depth_index , y_index , x_index] = current_data
#             return final_seq_array
#         elif len(inal_conv_shape_format_shape) == 1:
#             for depth_index in range(0 , sequence_len):
#                 for x_index , col in enumerate(final_conv_shape_format):
#                     if ":" not in col:
#                         current_col = col + "_{}".format(str(depth_index))
#                         current_data = df.loc[:, current_col]
#                         final_seq_array[:, depth_index , x_index] = current_data
#                     else:
#                         first_col =  col.split(":")[0] + "_{}".format(str(depth_index))
#                         second_col = col.split(":")[1] + "_{}".format(str(depth_index))
#                         current_data_first = df.loc[:,first_col]
#                         current_data_second = df.loc[:,second_col]
#                         current_data = (current_data_first + current_data_second) / 2
#                         final_seq_array[:,depth_index, x_index] = current_data
#             return final_seq_array
#     else:
#         depth_index = 0
#         if len(final_conv_shape_format_shape) == 3:
#             for depth_index_index , depth_cols in enumerate(final_conv_shape_format):
#                 for y_index, y_cols in enumerate(depth_cols):
#                     for x_index , col in enumerate(y_cols):
#                         if ":" not in col:
#                             current_col = col + "_{}".format((str_depth_index))
#                             current_data  = df.loc[:,current_col]
#                             final_seq_array[:,depth_index_index , y_index , x_index] = current_data
#                         else:
#                             first_col = col.split(":")[0] + "_{}".format(str(depth_index))
#                             second_col = col.split(":")[1] + "_{}".format(str(depth_index))
#                             current_data_first  = df.loc[:,first_col]
#                             current_data_second = df.loc[:,second_col]
#                             current_data = (current_data_first + current_data_second) / 2
#                             final_seq_array[:,depth_index_index , y_index ,  x_index] = current_data
#             return final_seq_array
#         elif len(final_conv_shape_format_shape) == 2:
#             for y_index, y_cols in enumerate(final_conv_shape_format):
#                 for x_index, col in enumerate(y_cols):
#                     if ":" not in col:
#                         current_col = col + "_{}".format((str_depth_index))
#                         current_data  = df.loc[:, current_col]
#                         final_seq_array[:,y_index, x_index] = current_data
#                     else:
#                         first_col = col.split(":")[0] + "_{}".format(str(depth_index))
#                         second_col = col.split(":")[1] + "_{}".format(str(depth_index))
#                         current_data_first = df.loc[:,first_col]
#                         current_data_second = df.loc[: , second_col]
#                         current_data = (current_data_first + current_data_second) /2
#                         final_seq_array[:,y_index, x_index] = current_data
#         elif len(final_conv_shape_format_shape) == 1:
#             for


##@@@###






def norming_features_deep(df2 ,feature_summary):
    df = df2.copy()
    # target = df.pop("target")
    normed_df = pd.DataFrame()
    columns = df.columns.tolist()
    for col in columns:
        summary_col = col[:-2]
        min = feature_summary.loc["min" , summary_col]
        max = feature_summary.loc["max" , summary_col]

        if min == max:
            normed_df[col] = 0
        else:
            normed_data = (df.loc[:,col] - min) / (max - min)
            normed_df[col] = normed_data

    # normed_df["target"] = target
    return normed_df

# main_second_multi(ticketNo, env , summary_path , log , error_log)



def single_process(final_path_total_list , hdfs_nas):
    dfl = []
    if hdfs_nas =="hdfs":
        for pars in final_path_total_list:
            df = making_detectable_df_from_nas2(pars)
            dfl.append(df)
        return dfl
    elif hdfs_nas =="nas":
        for pars in final_path_total_list:
            df = making_detectable_df_from_nas(pars)
            dfl.append(df)
        return dfl


def missing_data_processing(whole_detect_df , total_model_class , summary):
    check_dic = whole_detect_df.iloc[0,:]
    missing_data_cols = [col_name for col_name , value in check_dic.items() if value == -5000]
    missing_common_cols = list( dict.fromkeys(list(map(lambda x: x[:-2] , missing_data_cols))) )
    for col in missing_common_cols:
        ranking_list = list(range(total_model_class.sequence_len))
        if sum(list(map(lambda x: col in x ,  missing_data_cols  ))) <= int(total_model_class.sequence_len - 1):
            missing_cols = [x for x in missing_data_cols if col in x ]
            missing_ranking = list(map(lambda x: int(x.split("_")[-1]) , missing_cols   ))
            for rank in missing_ranking: ranking_list.remove(rank)
            first_step = ranking_list[0]
            last_step = ranking_list[-1]
            if first_step != last_step:
                first_col = col+ "_" + str(first_step)
                first_value_list = whole_detect_df[first_col]
                last_col = col + "_" + str(last_step)
                last_value_list = whole_detect_df[last_col]
                step_dif = last_step - first_step
                a  = (last_value_list - first_value_list) / step_dif
                for missing_rank in missing_ranking:
                    x = missing_rank - first_step
                    y = (a * x) + first_value_list
                    whole_detect_df[col + "_" + str(missing_rank)] = y
            else:
                only_remain = whole_detect_df[col+ "_" + str(first_step)]
                for missing_rank in missing_ranking:
                    whole_detect_df[col + "_" + str(missing_rank)] = only_remain

        elif sum(list(map(lambda x: col in x ,  missing_data_cols  ))) ==  int(total_model_class.sequence_len):
            if "HSR" not in col:
                all_cols = whole_detect_df.columns.tolist()


                for rank in ranking_list:
                    current_col = col + "_{}".format(str(rank))
                    key_chr = current_col[:2]
                    same_time_cols = [x for x in all_cols if x.endswith("_{}".format(str(rank)))]
                    similar_col_list= [x for x in same_time_cols if x.startswith(key_chr)]
                    choosing_list = [x for x in similar_col_list if x not in missing_data_cols]
                    if len(choosing_list) > 0:
                        final_copying_col  = difflib.get_close_matches(current_col, choosing_list , 1)[0]
                        whole_detect_df[current_col] = whole_detect_df[final_copying_col]
                    else:
                        print("rare case found : too many missing data --> inserting mean values at {}".format(col))
                        avg_number = summary.loc["mean" ,  current_col[:-2]]
                        whole_detect_df[current_col] = avg_number
            else:
                for rank in ranking_list:
                    whole_detect_df[col+"_{}".format(str(rank) )] = 0
    return whole_detect_df

#
# def main(ticketNo , env , summary_path , log , error_log):
#     start = datetime.datetime.now()
#
#     data_path , root_path , model_path = set_globvar_to_path(env)
#     summary, input_columns_order,column_length  = get_order_and_summary(summary_path)
#
#
#     real_time = datetime.datetime.strptime(ticketNo, "%Y%m%d%H%M")
#     time_8 =  real_time - datetime.timedelta(minutes = 2)
#     time_6 =  real_time - datetime.timedelta(minutes = 4)
#     time_4 =  real_time - datetime.timedelta(minutes = 6)
#     time_2 =  real_time - datetime.timedelta(minutes = 8)
#     time_0 =  real_time - datetime.timedelta(minutes = 10)
#
#     before_8 = time_8.strftime("%Y%m%d%H%M")
#     before_6 = time_6.strftime("%Y%m%d%H%M")
#     before_4 = time_4.strftime("%Y%m%d%H%M")
#     before_2 = time_2.strftime("%Y%m%d%H%M")
#     before_0 = time_0.strftime("%Y%m%d%H%M")
#
#     latlon_900_900 = pd.read_csv('{}/LATLON_KO_2000.txt'.format(root_path),header  = None, sep = '\t')
#     latlon_900_2d = np.array(latlon_900_900)
#
#     ## 10 = now
#     ## 0 = 10 minutes ago ago
#     final_path_list_10 = (5 , get_hdfs_path_list_by_time(str(ticketNo),summary),input_columns_order,column_length, latlon_900_2d, str(ticketNo))
#     final_path_list_8 = (4 , get_hdfs_path_list_by_time(before_8 ,summary) , input_columns_order , column_length , latlon_900_2d, before_8)
#     final_path_list_6 = (3 , get_hdfs_path_list_by_time(before_6 ,summary) , input_columns_order , column_length , latlon_900_2d, before_6)
#     final_path_list_4 = (2 , get_hdfs_path_list_by_time(before_4 ,summary) , input_columns_order , column_length , latlon_900_2d, before_4)
#     final_path_list_2 = (1 , get_hdfs_path_list_by_time(before_2 ,summary) , input_columns_order , column_length , latlon_900_2d, before_2)
#     final_path_list_0 = (0 , get_hdfs_path_list_by_time(before_0 ,summary) , input_columns_order , column_length , latlon_900_2d, before_0)
#
#
#     final_path_total_list = [ final_path_list_10,final_path_list_8,final_path_list_6,final_path_list_4,final_path_list_2 , final_path_list_0 ]
#     log.info("starting")
#
#     dfl = multi_process(final_path_total_list, log)
#
#     array_list = [x[0] for x in dfl]
#     ranking_list = [x[1] for x in dfl ]
#     sorted_array_list =  [y for x, y in sorted(zip(ranking_list, array_list))]
#
#     detect_df_0 = final_array_to_detectable_df(sorted_array_list[0] , input_columns_order , 0  )
#     detect_df_1 = final_array_to_detectable_df(sorted_array_list[1] , input_columns_order , 1  )
#     detect_df_2 = final_array_to_detectable_df(sorted_array_list[2] , input_columns_order , 2  )
#     detect_df_3 = final_array_to_detectable_df(sorted_array_list[3] , input_columns_order , 3  )
#     detect_df_4 = final_array_to_detectable_df(sorted_array_list[4] , input_columns_order , 4  )
#     detect_df_5 = final_array_to_detectable_df(sorted_array_list[5] , input_columns_order , 5  )
#
#     whole_detect_df = pd.concat([detect_df_0 , detect_df_1 , detect_df_2 , detect_df_3 , detect_df_4 ,detect_df_5  ] , axis = 1)
#
#     whole_detect_df_modified = missing_data_processing(whole_detect_df)
#
#     normed_whole_df = norming_features_deep(whole_detect_df_modified , summary)
#
#     final_conv_shape_format = fog_config.final_conv_shape_format
#
#     final_seq_array = np.zeros((810000 , 6, 18 , 3))
#
#     final_seq_data= make_final_seq_array(normed_whole_df , final_conv_shape_format , final_seq_array)
#     final_seq_data = final_seq_data[..., np.newaxis]
#     end = datetime.datetime.now()
#     log.info("preprocessing done : " +  str(end - start) )
#
#     return final_seq_data


def get_nas_folder_path_by_time(ticketNo , input_columns_order):

    final_folder_path = []
    each_time = ticketNo
    base_gk2a_path = collecting.data_collection_path_nas_gk2a
    yearmonth = ticketNo[:6]
    day = ticketNo[6:8]

    hour = ticketNo[8:10]

    features = input_columns_order.copy()
    if "zenith" in features: features.remove("zenith")
    if "target" in features: features.remove("target")
    if "HSR_value" in features: features.remove("HSR_value")

    channels = pd.Series(list(map(lambda x: x.split("_")[0] , features))).unique().tolist()

    for channel in channels:

        if "vi" in channel:
            if "vi006" in channel:
                distance = "05"
            else:
                distance = "10"
        else:
            distance = "20"

        folder_path = base_gk2a_path.format(yearmonth , day , hour , channel , distance , each_time )


        final_folder_path.append(folder_path)
    return final_folder_path





def multi_process(final_path_total_list , log , hdfs_nas , number_of_cpus):

    log.info("starting")

    mp.set_start_method('forkserver', force=True)
    # number_of_cpus = mp.cpu_count() - 10
    pool = mp.Pool(number_of_cpus)

    if hdfs_nas =="hdfs":
        func = making_detectable_df_from_nas2
    elif hdfs_nas =="nas":
        func = making_detectable_df_from_nas

    dfl = pool.map(func ,  final_path_total_list )
    pool.close()
    pool.join()
    return dfl


def data_load(path):
    final_file_path = path
    file_name = final_file_path.split("/")[-1]
    if file_name.endswith("npz"):
        temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
    else:
        print("alright reading")
        temp_df = pd.read_csv(final_file_path ,header = None ,chunksize = 500)
        print("chunk done")
        temp_df = temp_df.read()

        print("reading done")
        temp_array = temp_df.values
    if temp_array.shape[1] == 3601 or temp_array.shape[1] == 1801 or temp_array.shape[1] == 901:
        print("extra line at the end --> deleting last line")
        temp_array = temp_array[: , : -1]


    # if (float(temp_array[temp_array.shape[0] - 1][temp_array.shape[0] - 1])  > 0) == False:
    #     print("there is something wrong in the last column")
    #     temp_array[:,temp_array.shape[0] - 1] = temp_array[:,temp_array.shape[0] - 2]
    #

    print(temp_array[-5: , -1])
    if temp_array[0 , -1] == " " or temp_array[1 , -1] == " ":
        print("something wrong with last line"  + "\n {}".format(path))
        temp_array[: ,1:] = temp_array[:, :-1]
        temp_array[: , 0] = temp_array[: , 1]
        print("fixed weird format")
        print(temp_array[-5: , -1])
    return temp_array


def making_detectable_df_from_nas(arg_list):
    rank, final_folder_path, latlon_900_2d, current_time , total_model_class  = arg_list

    gk2a_using_chanels = total_model_class["gk2a_using_chanels"]
    input_order = total_model_class["input_columns_order"]
    column_length = total_model_class["column_length"]

    log = fog_logging.get_log_view(1, "platform", False, 'fog_inferencing_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_inferencing_error_log')

    full_array = np.full((900,900,column_length), -5000.0 , dtype = np.float32)

    for i in range(len(final_folder_path)):

        path = final_folder_path[i]

        try:
            file_paths = os.listdir(path)
        except:
            print("no folder at :\n {}".format(path))
            continue

        for file in file_paths:
            final_file_path = path + "/" + file

            # x= file.split("_")
            # temp_col = x[3] + "_" + x[5].split(".")[2]
            # temp_col = check_temp_col(temp_col)
            # print(temp_col)
            temp_col = justfind_feature(file , gk2a_using_chanels)
            try:
                final_pos = input_order.index(temp_col)
            except:
                continue

            try:
                temp_array = data_load(final_file_path)
                log.info('loaded success')
                print(temp_col)
                print(temp_array.shape)
                log.info('loaded : {}'.format(temp_col))
            except Exception as e:
                error_log.error("critical error - no file : {}".format(e))
                error_log.error("array  : {} - {}".format(current_time , temp_col))
                wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )

                full_array[:,:,final_pos] = wrong_array
                error_log.error("inserting wrong array")
                continue

            try:
                if '_vi' in path:
                    if "_vi006" in path:
                        try:
                            modified_array = transform_gk_3600_to_900(temp_array)
                            full_array[:,:,final_pos] = modified_array
                        except:
                            error_log.error("string data in the array")
                            error_log.error("extra preprocessing")
                            temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
                            error_log.error("flatten and numeric")
                            temp_array = np.reshape(temp_array , (3600 , 3600))
                            error_log.error("reshaping done")
                            modified_array = transform_gk_3600_to_900(temp_array)
                            error_log.error("transform done")
                            full_array[:,:,final_pos] = modified_array
                            error_log.error("inserting done")
                    else:
                        try:
                            modified_array = transform_gk_1800_to_900(temp_array)
                            full_array[:,:,final_pos] = modified_array
                        except:
                            error_log.error("string data in the array : {} - {}".format(current_time , temp_col))
                            error_log.error("extra preprocessing")
                            temp_array = pd.to_numeric(temp_array.flatten() , errors = 'coerce')
                            error_log.error("flatten numeric")
                            temp_array = np.reshape(temp_array , (1800, 1800))
                            error_log.error("reshaping done")
                            modified_array = transform_gk_1800_to_900(temp_array)
                            error_log.error("transforming done")
                            full_array[:,:,final_pos] = modified_array
                            error_log.error("inserting done")
                else:
                    try:
                        full_array[:,:,final_pos] = temp_array
                    except:
                        error_log.error("string data in the array :{} - {}".format(current_time , temp_col))
                        error_log.error("extra preprocessing")
                        temp_array = pd.to_numeric(temp_array.flatten() , errors = 'coerce' )
                        error_log.error("temp_array done")
                        temp_array = np.reshape(temp_array , (900 , 900 ))
                        error_log.error("reshaping done")
                        full_array[:,:,final_pos] = temp_array
                        error_log.error("insering done")
            except Exception as e:
                error_log.error("critical issue : {}".format(e) )
                error_log.error("array  : {} - {}".format(current_time , temp_col))
                wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )
                error_log.error("inserting wrong array")
                full_array[:,:,final_pos] = wrong_array
    root_path = external_info_path.root_path
    try:
        #!!!
        modified_real =  datetime.datetime.strptime(current_time ,"%Y%m%d%H%M" ) + datetime.timedelta(hours  = 9 )
        modified_time = datetime.datetime.strftime(modified_real ,"%Y%m%d%H%M" )

        final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform(modified_time, root_path)
        log.info("success loading hsr")
        log.info("success transform hsr shape")
        try:
            full_array[:,:,input_order.index("HSR_value") ] = temp_hsr_reshaped
            log.info("success inserting hsr")
        except:
            error_log.error("string data in the hsr array :{}".format(modified_time))
            error_log.error("extra preprocessing")
            temp_hsr_reshaped = pd.to_numeric(temp_hsr_reshaped.flatten() , errors = 'coerce' )
            error_log.error("temp_array done")
            temp_hsr_reshaped = np.reshape(temp_hsr_reshped , (900 , 900 ))
            error_log.error("reshaping done")
            full_array[:,:,input_order.index("HSR_value") ] = temp_hsr_reshaped
            error_log.error("insering done")
    except Exception as e:
        error_log.error("error loading hsr : {}".format(e))
        error_log.error("hsr array at : {}".format(modified_time))
        wrong_array = np.full((900,900), -5000.0  ,dtype = np.float32)
        error_log.error("inserting wrong array")
        full_array[:,:,input_order.index("HSR_value") ] = wrong_array
    zenith_at_time = get_zenith_900_faster(latlon_900_2d, modified_time )
    full_array[:,:,input_order.index("zenith")] = zenith_at_time
    return full_array , rank





def get_hsr_and_transform(time, root_path):
    year_month = time[0:6]
    day = time[6:8]
    # file_path = '/share/3_데이터/HSR/result/' +year_month  +"/" + day + "/result/RDR_CMP_HSR_PUB_" + time +".bin/RDR_CMP_HSR_PUB_" + time + ".txt"
    #file_path = '/share/3_데이터/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'.format(year_month,day,time,time)
    file_path = collecting.data_collection_path_nas_hsr.format(year_month,day,time,time)

    try:
        hsr_df = pd.read_csv(file_path,header = None)
    except:
        wrong_array = np.full((900,900),  -5000.0  ,dtype = np.float32 )
        wrong_array2 = wrong_array.copy()
        print("no hsr value inserting wrong arrays")
        return wrong_array , wrong_array2

    hsr_np = hsr_df.values
    # mapping_table = pd.read_csv("hsr_processing/gk2a_hsr_int_pixel.csv")
    mapping_table = pd.read_csv(external_info_path.gk2a_hsr_pixel_info )
    # gk2a 영역이 더 넓기 때문에 필요 없는 데이터 제거
    for_merge_table = mapping_table.loc[(mapping_table['hsr_y'] >= 0 ) & (mapping_table['hsr_y'] <= 2880) & (mapping_table['hsr_x'] >= 0 ) & (mapping_table['hsr_x'] <= 2304 ),: ]
    # hsr 반사도 값 갖고 오기
    for_merge_table['hsr_values'] =  list(map(lambda x,y: hsr_np[ int(y) ][ int(x) ], for_merge_table['hsr_x'], for_merge_table['hsr_y'] ))
    # hsr 반사도와 gk2a 픽셀 정보 나란히 갖고오기
    for_merge_table_modified = for_merge_table.loc[:,['gk2a', 'hsr_values']]
    # hsr과 위치정보 테이블 융합 후 hsr value 만 따로 분리
    hsr_transformed_np = pd.merge(mapping_table,for_merge_table_modified, on = 'gk2a', how = 'outer')['hsr_values'].values
    # nan 값 모드 -50000으로 변경
    hsr_transformed_np_modified =  np.nan_to_num(hsr_transformed_np, nan = -50000)
    # 900 900 크기로 변경
    final_hsr_reshaped = np.reshape(hsr_transformed_np_modified, (900,900))
    temp_hsr_reshaped = final_hsr_reshaped.copy()
    # 최소값 0으로 지정
    temp_hsr_reshaped =temp_hsr_reshaped.clip(min = 0)
    return final_hsr_reshaped , temp_hsr_reshaped



# im done with this shit. there is no fking pattern
def justfind_feature(file , gk2a_using_chanels):

    for channel in gk2a_using_chanels:
        if channel in file:
            final_chanel = channel
            break
    if "Brightness" in file:
        if "Effective" in file:
            feature = "Effective"
        else:
            feature = "Brightness"
    elif "Radiance" in file:
        feature = "Radiance"
    elif "Albedo" in file:
        feature = "Albedo"
    final_col_name = final_chanel + "_" + feature
    return final_col_name



def getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user):
    active_host = None

    ''' ambari user/pass '''
    #auth_user = 'admin'
    #auth_pass = 'admin'
    #ambari_host = "192.168.0.174"
    ''' ambari host '''
    #ambari_cluster_name = 'hacluster'


    api_url = 'http://{}:8080/api/v1/clusters/{}/host_components?HostRoles/component_name=NAMENODE&metrics/dfs/FSNamesystem/HAState=active'.format(ambari_host,
                                                                                                                                                   ambari_cluster_name)
    res = requests.get(api_url, auth=(auth_user, auth_pass))

    status_code = res.status_code
    res_json = res.json()

    '''
    - status 200 == 정상
    - status 403 == 암바리 계정명 불일치
    - status 404 == api url 재확인 필요
    - status 5xx == 암바리 서버 에러
    '''
    if status_code==200 and res_json['items']:
        active_host = res_json['items'][0]['HostRoles']['host_name']

    return status_code, active_host





def main(ticketNo_dic , env , params , log , error_log):
    start = datetime.datetime.now()
    log.info("starting")
    total_model_path = params["total_model_path"]
    amos_model_path = params["amos_model_path"]

    ########### 모델의 학습 데이터 형태 정보 읽기#################################
    total_summary_path = os.path.join(total_model_path , "feature_summary.csv")
    amos_summary_path  = os.path.join(amos_model_path , "feature_summary.csv")

    total_model_info_path = os.path.join(total_model_path , "total_model_info.json")
    with open(total_model_info_path, 'r') as fp:
        total_model_info = json.load(fp)
    fp.close()

    amos_model_info_path = os.path.join(amos_model_path , "amos_model_info.json")
    with open(amos_model_info_path , 'r') as gp:
        amos_model_info = json.load(gp)
    gp.close()
    ############################################################################

    # 전지역 모델 입력 데이터 크기 정보
    total_final_conv_shape_format = total_model_info["final_conv_shape_format"]
    log.info("total_final_conv_shape_format : {}".format(total_final_conv_shape_format) )
    total_model_sequence_len =  total_model_info["sequence_len"]
    log.info("total_model_sequence_len : {}".format(total_model_sequence_len)  )
    total_model_each_sequence_minutes  = total_model_info["each_sequence_minutes"]
    log.info("total_model_each_sequence_minutes : {}".format(total_model_each_sequence_minutes))

    # AMOS 모델 입력 데이터 크기 정보
    amos_final_conv_shape_format = amos_model_info["final_shape_format_amos"]
    log.info("amos_final_conv_shape_format : {}".format(amos_final_conv_shape_format))
    amos_model_sequence_len =  amos_model_info["sequence_len"]
    log.info("amos_model_sequence_len : {}".format(amos_model_sequence_len) )
    amos_model_each_sequence_minutes = amos_model_info["each_sequence_minutes"]
    log.info("amos_model_each_sequence_minutes : {}".format(amos_model_each_sequence_minutes))

    # 전지역 모델 특성 정보를 보유한 클래스 생성
    total_model_class = fog_main_preprocessing(total_final_conv_shape_format , total_model_sequence_len , total_model_each_sequence_minutes)
    # AMOS 모델 특성 정보를 보유한 클래스 생성
    amos_model_class = fog_main_preprocessing(amos_final_conv_shape_format , amos_model_sequence_len  , amos_model_each_sequence_minutes)

    # 전처리 필요한 메소드 실행
    total_model_class.extract_to_list()
    amos_model_class.extract_to_list()


    data_path = Config.data_platform_path
    root_path = Config.root_platform_path

    total_summary = pd.read_csv(total_summary_path  , index_col = 0)
    amos_summary = pd.read_csv(amos_summary_path , index_col = 0)

    ticketNo = ticketNo_dic["reading_ticketNo"]

    real_time = datetime.datetime.strptime(ticketNo, "%Y%m%d%H%M") - datetime.timedelta(hours = 9 )

    time_list = [real_time] * total_model_class.sequence_len
    before_list =["None"] * total_model_class.sequence_len
    rank_list = list(range(total_model_class.sequence_len-1 , -1 , -1) )


    for i in range(total_model_class.sequence_len):
        subtracting_mins = int(i * total_model_class.each_sequence_minutes)
        current_time = real_time - datetime.timedelta(minutes = subtracting_mins)
        before_time = current_time.strftime("%Y%m%d%H%M")
        time_list[i] = current_time
        before_list[i] = before_time


    latlon_900_900 = pd.read_csv('{}/LATLON_KO_2000.txt'.format(root_path),header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)
    hdfs_nas = preprocess_info.hdfs_nas
    #hdfs_nas = "nas"


    if hdfs_nas == "hdfs":
        func = get_hdfs_path_list_by_time
    elif hdfs_nas == "nas":
        func = get_nas_folder_path_by_time


    #ambari_host = hdfs_path_class.ambari_host
    #hdfs_port = hdfs_path_class.hdfs_port

    total_model_dic = {"gk2a_using_chanels" : total_model_class.gk2a_using_chanels ,
                      "input_columns_order":total_model_class.input_columns_order ,
                      "column_length" : total_model_class.column_length,
                      "ambari_host": hdfs_path_class.ambari_host,
                      "hdfs_port" : hdfs_path_class.hdfs_port,
                      "auth_pass" : hdfs_path_class.auth_pass ,
                      "ambari_cluster_name" : hdfs_path_class.ambari_cluster_name,
                      "auth_user" : hdfs_path_class.auth_user }




    final_path_total_list = [(5 , func(str(ticketNo),total_model_class.input_columns_order), latlon_900_2d, str(ticketNo) , total_model_dic)] * total_model_class.sequence_len
    for i in range(total_model_class.sequence_len):
        current_path_list = (rank_list[i] , func(before_list[i] , total_model_class.input_columns_order) , latlon_900_2d , before_list[i] , total_model_dic)
        final_path_total_list[i] = current_path_list


    number_of_cpus = total_model_class.sequence_len


    dfl = multi_process(final_path_total_list, log , hdfs_nas , number_of_cpus)

    #dfl = single_process(final_path_total_list , hdfs_nas)

    array_list = [x[0] for x in dfl]

    ranking_list = [x[1] for x in dfl ]
    sorted_array_list =  [y for x, y in sorted(zip(ranking_list, array_list))]


    concat_df_list = [ final_array_to_detectable_df(sorted_array_list[x] ,  total_model_class.input_columns_order , x ) for x in range(total_model_class.sequence_len)  ]

    whole_detect_df = pd.concat(concat_df_list  , axis = 1)

    whole_detect_df_modified = missing_data_processing(whole_detect_df , total_model_class , total_summary)

    total_normed_whole_df = norming_features_deep(whole_detect_df_modified , total_summary)

    ###
    ### filtering to save same data used by amos
    filtering_cols = list(filter(lambda x: x[:-2] in amos_model_class.input_columns_order , whole_detect_df_modified.columns.tolist() ))

    amos_whole_detect_df_modified = whole_detect_df_modified.loc[:,filtering_cols]

    if amos_model_each_sequence_minutes == 0: amos_model_each_sequence_minutes = total_model_each_sequence_minutes

    rank_convert = {}
    needed_seq = []

    amos_rank_list  =list(range(amos_model_sequence_len-1 , -1 , -1) )
    for i in range(amos_model_sequence_len):
        val =  rank_list[int(i /  (total_model_each_sequence_minutes / amos_model_each_sequence_minutes))]
        seq_val = "_" + str(val)
        needed_seq.append(seq_val)
        rank_convert[str(val)]  =  str(amos_rank_list[i])

    second_filtering_cols = list(filter(lambda x: x[-2:] in needed_seq ,  amos_whole_detect_df_modified.columns.tolist()   ))
    amos_whole_detect_df_modified = amos_whole_detect_df_modified.loc[: , second_filtering_cols]

    col_rank_changer = {}
    for i in amos_whole_detect_df_modified.columns.tolist():
        val = i[:-1] + rank_convert[i[-1]]
        col_rank_changer[i] = val


    amos_whole_detect_df_modified = amos_whole_detect_df_modified.rename(columns = col_rank_changer)
    amos_normed_whole_df = norming_features_deep(amos_whole_detect_df_modified , amos_summary)

    #amos_normed_whole_df.to_csv("amos_normed_whole_df.csv")


    total_model_class.get_input_shape()
    total_final_input_shape = total_model_class.final_input_shape

    total_final_input_shape = total_final_input_shape[:-1]
    total_final_seq_array = np.zeros((810000 , ) + total_final_input_shape  )

    total_model_class.getting_final_format()

    total_final_seq_data= make_final_seq_array(total_model_class.final_conv_shape_input , total_normed_whole_df , total_final_seq_array)

    total_final_seq_data = total_final_seq_data[..., np.newaxis]



    end = datetime.datetime.now()
    log.info("preprocessing done : " +  str(end - start) )

    #amos_normed_whole_df = amos_normed_whole_df.clip(lower = 0)  #### not clipping in training either
    #total_final_seq_data =  np.clip(total_final_seq_data , 0 , None)

    return total_final_seq_data , amos_normed_whole_df



#np.save('detect_array.npy', total_final_seq_data)

# if __name__ == '__main__':
#      tempo()






#
# #---------------------------------------------------------- for only debugging ---------------------------------------------------------------------
#
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
#
# def init_svc(im):
#     import fog_loading_model  ################
#
#     log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_log')
#     error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
#     model_path = im.model_path
#
#     log.info('model path ->{}'.format(model_path))
#     log.info('model path exists : {}'.format(os.path.exists(model_path)))
#
#
#     try:
#
#
#         params = fog_loading_model.main(model_path ,"platform",log, error_log)   #####################
#         log.info('model loaded')
#     except Exception as e:
#         error_log.error('model loading error :{}'.format(e))
#
#     # param = im.param_info
#     # params = {**param, **model_param}
#     return params
#
# class making_class:
#     def __init__(self , path , dicting ):
#         self.model_path = path
#         self.param_info = dicting
#
#
# dicting  =   {'model_training'               : '2',
#               'data_collecting'              : '2',
#               'total_train_start_ticket'     : '201910010000',
#               'total_train_end_ticket'       : '201910312350',
#               'amos_train_start_ticket'      : '201910010000',
#               'amos_train_end_ticket'        : '201910312350',
#               'custom_features'              : '2',
#               'seq_len_total'                : '3',
#               'each_seq_minutes_total'       : '10',
#               'conv1_total'                  : '32',
#               'conv2_total'                  : '32',
#               'dense1_total'                 : '64',
#               'dense2_total'                 : '32',
#               'dense1_amos'                  : '128',
#               'dense2_amos'                  : '64',
#               'batch_size'                   : '30',
#               'epoch'                        : '10',
#               'previous_model'               : '1',
#               'es_patience'                  : '50',
#               'weight_0_1000'                : '1',
#               'weight_1000_2000'             : '1',
#               'weight_2000_3000'             : '1',
#               'weight_3000_4000'             : '1',
#               'weight_4000_5000'             : '3',
#               'weight_5000_all'              : '1'}
#
#
# im = making_class( '/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/tempo_file2', dicting )
#
# params = init_svc(im)
#
#
# ticketNo_dic = {'reading_ticketNo': '201909060950', 'saving_ticketNo': '201909060950'}
#
# env = "platform"
# log = fog_logging.get_log_view(1, "platform", False, 'fog_ui_enfo_info')
# error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_ui_enfo_info_error')
# #
# #
# # #######################################################
# #import fog_preprocess
# #total_final_seq_data , amos_normed_whole_df = fog_preprocess.main(ticketNo_dic , env , params , log , error_log)
#
# def main(ticketNo_dic , env , params , log , error_log):
#
# ticketNo_dic , env , params , log , error_log = ticketNo_dic , env , params , log , error_log
