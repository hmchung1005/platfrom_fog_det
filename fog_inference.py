import tensorflow as tf
import sys
import os
import numpy as np
sys.path.append(os.getcwd())

def train(tm):
    '''
    train(tm) : 학습에 필요한 필수 함수

    labels
        tm.features.get('y_value')
    data load
        tm.load_data_for_cnn()    # cnn 데이터
        tm.load_data_for_all()    # 기타 데이터
        ex) (train_id, train_x, train_y), (test_id, test_x, test_y) = tm.load_data_for_cnn()

    param
        tm.param_info[common_params]
        tm.param_info[algo_params]

    common_params
        nn_type          NN 유형
        init_method      초기화 방법
        opt_method       최적화 방법
        learning_rate    Learning Rate
        dropout_ratio    Dropout Ratio
        random_seed      랜덤 seed
        autosave_p       자동저장 주기
        epoch            학습수행횟수
        batch_size       배치 사이즈

    algo_params
        ui 에서 정의

    save
        tm.save_result_metrics(eval_results)

    eval_results
        step
        predict_y
        actual_y
        test_id
        confusion_matrix

    '''


def init_svc(im):
    '''
    init_svc(im) : 추론 서비스 초기화

    labels
        im.features.get('y_value')
    params
        im.param_info
    nn_info
        im.nn_info
    model_path
        im.get_model_path()

    return 값을 inference 함수에서 params 으로 접근
        ex) return {"svc_config":svc_config, "estimator":estimator}
    '''

def data_input_validation(ticketNo):
    '''check data'''


def inference(df, params, batch_size):
    import fog_preprocess
    import fog_postprocess
    #import logging
    import fog_database

    #logging.info('df : {}, params : {}'.format(df,params))
    #print('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    #ticekNo type =>type=<class 'pandas.core.series.Series'>
    ticketNo = ticket_df.tolist()[0]
    #ex) ticketNo = '201909071530'
    #logging.info('ticketNo : {}'.format(ticketNo))

    result_df = fog_preprocess.main(ticketNo,'local')

    data_input_validation(ticketNo)

    final_df , watch_warn_df = fog_postprocess.main(ticketNo, result_df , 'local')
    fog_database.main(final_df ,base_result ,  watch_warn_df , 'local')

    return True

    '''
    inference : init_svc 의 return 값으로 부터 추론
        ex)
        svc_config = params['svc_config']
        estimator = params['estimator']
        test_data = df.iloc[:, 0].values.tolist()

    results 를 json 형태로 리턴한다
    '''

if __name__ == "__main__":
    inference({0: np.array(["201909071610"])} , "params" , "batch_size"      )


##
