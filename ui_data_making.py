from datetime import datetime
from datetime import timedelta
import fog_postprocess
import logging
import fog_database
import fog_logging
import tensorflow as tf
import os
import fog_preprocess


def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta




def inference_onetime(ticketNo , params , log , error_log):
    log.info('fog_preprocess start')
    result_df = fog_preprocess.main_original(ticketNo, "platform" , params["summary_path"] ,  log , error_log)
    log.info('fog_preprocess end')

    log.info('fog_postprocess start')
    final_df , watch_warn_df_list = fog_postprocess.main(ticketNo, result_df, "platform" , params ,log , error_log )
    log.info('fog_postprocess end')

    log.info('fog_database start')
    fog_database.main(final_df , watch_warn_df_list, "platform" , log, error_log )
    log.info('fog_database end')

def main():
    train_start_ticket = "201909070000"
    train_end_ticket = "201909080000"
    fmt = '%Y%m%d%H%M'
    start = datetime.strptime(str(train_start_ticket), fmt)
    end = datetime.strptime(str(train_end_ticket), fmt)
    timelist = []
    for result in perdelta(start , end , timedelta(minutes=10)):
        timelist.append(result)
    timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , timelist ))
    log = fog_logging.get_log_view(1, "platform", False, 'fog_ui_enfo_info')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_ui_enfo_info')

    the_path = os.getcwd() + "/fog_model_20200605-1411"
    saved_model =  tf.keras.models.load_model(the_path)
    summary_path = the_path + "/feature_summary.csv"
    params = {"saved_model":saved_model, "summary_path":summary_path}

    for ticketNo in timelist:
        try:
            inference_onetime(ticketNo , params , log , error_log)
            print(ticketNo)
        except Exception as e:
            log.info("\n \n something wrong at : {}".format(ticketNo))
            log.info("\n error : {} \n \n".format(e))
            continue




if __name__ == '__main__':
    main()
