import numpy as np
import pandas as pd
import datetime, os, sys, time
import cv2
from PIL import Image
from math import sin, cos, radians, pi
from fog_database import set_globvar_to_path , open_connection
from fog_config import Config , fog_img_making , external_info_path , fog_character , tb_info





def make_time_img(now_time_str , time_img_shape):
    location       = fog_img_making.location_time
    font           = cv2.FONT_HERSHEY_SIMPLEX
    fontScale      = fog_img_making.fontScale_time
    input_txt      = fog_img_making.input_txt
    w_color        = fog_img_making.w_color_time
    thickness      = fog_img_making.thickness_time

    time_img_ary = np.zeros(time_img_shape, np.uint8)
    time_img_ary[:, :, 3] = 255   # Alpha 값을 변경
    cv2.putText(time_img_ary, input_txt.format(now_time_str), location, font, fontScale, w_color, thickness)
    return time_img_ary


def draw_amos_base(bk_img , amos_info):
    bk_img_arr = np.array(bk_img)
    cross_len  = fog_img_making.base_cross_length
    for idx in range(len(amos_info)):
        cv2.line(bk_img_arr,(amos_info["lon_index_900"].iloc[idx]- cross_len ,amos_info["lat_index_900"].iloc[idx]) , (amos_info["lon_index_900"].iloc[idx]  +  cross_len, amos_info["lat_index_900"].iloc[idx]) ,fog_img_making.base_rgb, thickness= fog_img_making.base_tickness )
        cv2.line(bk_img_arr,(amos_info["lon_index_900"].iloc[idx] ,amos_info["lat_index_900"].iloc[idx]-cross_len) , (amos_info["lon_index_900"].iloc[idx], amos_info["lat_index_900"].iloc[idx] +  cross_len) ,fog_img_making.base_rgb, thickness=fog_img_making.base_tickness )

    base_loc_img = Image.fromarray(bk_img_arr).convert("RGBA")
    return base_loc_img


def get_where_db_df(table_name , cursor, ticketNo):
    col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='{}';".format(table_name)
    cursor.execute(col_sql)
    col = cursor.fetchall()
    col = list(map(lambda x: x[0] , col))
    cursor.execute("select * from {} where NOW_TIME = '{}';".format(table_name, ticketNo))
    data = cursor.fetchall()
    final_df = pd.DataFrame(data , columns = col)
    return final_df
# 9-6. 구름 셀 이동방향, 이동속도 화살표 그림
def draw_arrow(composite_img, moving_df, same_df):
    # opencv에서는 rgba가 bgra로 array를 반영함
    composite_img_ary = np.array(composite_img)

    expanding_arrow_color = fog_img_making.expanding_arrow_color
    reduction_arraw_color = fog_img_making.reduction_arraw_color
    same_arrw_color = fog_img_making.same_arrw_color
    decide_color = {"expansion" : expanding_arrow_color , "reduction" : reduction_arraw_color , "same" : same_arrw_color}
    arrow_thickness  =  fog_img_making.arrow_thickness
    arrow_tipLength = fog_img_making.arrow_tiplength
    fog_cross_len = fog_img_making.fog_cross_length
    fog_center_max = 900 - fog_cross_len
    for i in range(len(moving_df)):
        start_point_x = moving_df.FOG_CENTER_X.iloc[i]
        start_point_y  = moving_df.FOG_CENTER_Y.iloc[i]
        end_point_x = moving_df.ending_x.iloc[i]
        end_point_y = moving_df.ending_y.iloc[i]
        arrow_color = decide_color[moving_df.expansion_state.iloc[i]]

        if moving_df.MV_SPEED.iloc[i] > 0:
            cv2.arrowedLine(composite_img_ary,(start_point_x , start_point_y),(end_point_x , end_point_y) ,arrow_color,thickness = arrow_thickness ,tipLength = arrow_tipLength )

        else:
            clipping = np.array([start_point_x , start_point_y]).clip(min = fog_cross_len , max = fog_center_max )
            start_point_x = clipping[0]
            start_point_y = clipping[1]
            cv2.line(composite_img_ary ,   (start_point_x -fog_cross_len , start_point_y ) , (start_point_x + fog_cross_len , start_point_y) , arrow_color , thickness = arrow_thickness   )
            cv2.line(composite_img_ary , (start_point_x , start_point_y  - fog_cross_len) , (start_point_x , start_point_y   + fog_cross_len) , arrow_color ,  thickness = arrow_thickness )
    color_same_df = decide_color["same"]
    for i in range(len(same_df)):
        start_point_x  = same_df.FOG_CENTER_X.iloc[i]
        start_point_y = same_df.FOG_CENTER_Y.iloc[i]
        clipping = np.array([start_point_x , start_point_y]).clip(min = fog_cross_len, max =fog_center_max)
        start_point_x = clipping[0]
        start_pont_y = clipping[1]
        cv2.line(composite_img_ary , ( start_point_x - fog_cross_len , start_point_y) , (start_point_x + fog_cross_len, start_point_y  ) , color_same_df , thickness = arrow_thickness)
        cv2.line(composite_img_ary,  ( start_point_x , start_point_y - fog_cross_len) , (start_point_x , start_point_y +  fog_cross_len) , color_same_df , thickness = arrow_thickness )
    # pillow 객체로 다시 변환
    composite_img = Image.fromarray(composite_img_ary).convert('RGBA')

    return composite_img

def point_pos(x0, y0, d, theta):
    theta_rad = pi/2 - radians(theta)
    x1 = x0 + d*cos(theta_rad)
    y1 = y0 - d*sin(theta_rad)  # in numpy array the bigger the value the lower it goes down
    x1 =  int(round(x1))
    y1 = int(round(y1))
    return  y1 , x1






def modify_df(df):
    df["MV_SPEED"] = df["MV_SPEED"].clip(upper = fog_character.maximum_fog_speed )  ####@@@@ erase after sometime
    bool_list = list(map(lambda x , y : x != y , df.FOG_CURRENT_ID , df.FOG_FIRST_ID))
    reversed_bool_list = np.logical_not(bool_list).tolist()

    moving_df=  df.loc[bool_list , :]
    same_df = df.loc[reversed_bool_list , :]

    moving_df = modify_moving_df(moving_df)

    ending_points = list(map(lambda x0 , y0 , d, theta:  point_pos(x0 , y0 , d, theta)  ,  moving_df.FOG_CENTER_X , moving_df.FOG_CENTER_Y ,  moving_df.moving_distance , moving_df.MV_DIRECTION  ))

    ending_y = list(map(lambda x: x[0] , ending_points))
    ending_x = list(map(lambda x: x[1] , ending_points))

    moving_df["ending_x"] = ending_x
    moving_df["ending_y"] = ending_y
    cols = ['ending_y', 'ending_x']
    moving_df[cols] = moving_df[cols].clip(upper=900 , lower = 0 )
    moving_df["expansion_state"] =  list(map(lambda x: "expansion" if x > 0  else "reduction" if x < 0 else "same" , moving_df.AREA_DIFFERENCE   ))
    return moving_df , same_df

def modify_moving_df(df):
    max_value_class = fog_img_making.moving_speed_class
    df["moving_distance"] = None
    moving_class = list(map(lambda x: classification_speed(x, max_value_class) ,df.MV_SPEED ))
    moving_speed_distance  = fog_img_making.moving_speed_distance
    distance_result = list(map(lambda x: moving_speed_distance[x] , moving_class ))
    df["moving_distance"] = distance_result

    return df




def get_all_db_df(table_name , cursor):
    col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='{}';".format(table_name)
    cursor.execute(col_sql)
    col = cursor.fetchall()
    col = list(map(lambda x: x[0] , col))
    cursor.execute("select * from {};".format(table_name, ticketNo))
    data = cursor.fetchall()
    final_df = pd.DataFrame(data , columns = col)
    return final_df

# 9-5. 상, 하단 부분 합친 후 Pillow 이미지 객체로 만듦(RGBA)
def overlap_time_legend(composite_img, time_img_ary, time_img_h, below_legend_img_ary):
    # 상단 부분 합침
    composite_img_ary = np.array(composite_img)
    composite_img_ary[:time_img_h, :, :] = time_img_ary

    # 하단 부분 합침
    legend_h, legend_w, _ = below_legend_img_ary.shape
    composite_img_ary[-legend_h:, -legend_w:, :] = below_legend_img_ary
    composite_img = Image.fromarray(composite_img_ary).convert('RGBA')
    return composite_img


def classification_speed(num , class_list):
    for i in class_list:
        if num  <= i:
            answer = i
            return answer


def main(ticketNo):
    env = "platform"
    root_path , tempo_path , output_path = set_globvar_to_path(env)

    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port

    conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)

    # bk_img = Image.open("{}/reformed_bk_img.png".format(root_path))
    bk_img = Image.open(fog_img_making.bk_img_path )

    amos_info = pd.read_csv( external_info_path.all_amos_info_path , index_col = 0  )

    base_loc_img = draw_amos_base(bk_img , amos_info)

    yearmonth = ticketNo[:8]

    pred_img_path = fog_img_making.pred_img_path_base.format(yearmonth, ticketNo)

    pred_img = Image.open(pred_img_path)

    combined_img = Image.alpha_composite( base_loc_img, pred_img )

    df = get_where_db_df("D_FOG_DET_TRAC_RESULT" , cursor, ticketNo)

    moving_df , same_df = modify_df(df)

    composite_img = draw_arrow(combined_img , moving_df, same_df)

    time_img_shape = fog_img_making.time_img_shape
    time_img_ary = make_time_img(ticketNo , time_img_shape)

    below_legend_img = Image.open(fog_img_making.below_legend_img_path )
    below_legend_img_ary = np.array(below_legend_img)
    below_legend_img_ary[:,:,3] = 255

    final_img = overlap_time_legend(composite_img , time_img_ary, time_img_shape[0], below_legend_img_ary)
    temp_yearmonthday = ticketNo[:8]
    temp_path = "{}/handled_image/{}".format(output_path , temp_yearmonthday)
    if not os.path.exists(temp_path):
        os.makedirs(temp_path )
    final_img.save("{}/fog-det-trac_{}_handling.png".format(temp_path , ticketNo ))
