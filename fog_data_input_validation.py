# def main(ticketNo):
#     return False
import os
import json
import datetime
import numpy as np
import pandas as pd
from fog_config import collecting , fog_main_preprocessing , hdfs_path_class
from hdfs import InsecureClient
import requests
import fog_logging

#
# def init_svc(im):
#     import fog_loading_model  ################
#
#     log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_log')
#     error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
#     model_path = im.model_path
#
#     log.info('model path ->{}'.format(model_path))
#     log.info('model path exists : {}'.format(os.path.exists(model_path)))
#
#
#     try:
#     # parmas = {"total_model_path" :total_model_path ,
#     #           "total_model" : total_model ,
#     #           "total_summary_path" : total_summary_path ,
#     #           "amos_model_path" :amos_model_path,
#     #           "amos_model" : amos_model ,
#     #           "amos_summary_path" : amos_summary_path}
#
#         params = fog_loading_model.main(model_path ,"platform",log, error_log)   #####################
#         log.info('model loaded')
#     except Exception as e:
#         error_log.error('model loading error :{}'.format(e))
#
#     # param = im.param_info
#     # params = {**param, **model_param}
#     return params
#
#
# class making_class:
#     def __init__(self , path , dicting ):
#         self.model_path = path
#         self.param_info = dicting


#
#
# dicting =  {"total_train_start_ticket": "201909051600" , "total_train_end_ticket": "201909051700" ,
#             "amos_train_start_ticket" : "201909051600" , "amos_train_end_ticket" : "201909051700" ,
#             "data_collecting": "1" ,                       "model_training": "2" ,
#                 "batch_size": "10",                           "epoch":"5",
#             "previous_model":"1"  }
#
# im = making_class(os.getcwd() , dicting)
# params = init_svc(im)
# model_dic = {"total_model_path":params["total_model_path"],
#              "amos_model_path" :params["amos_model_path"] }




def get_hdfs_path_list_by_time(ticketNo, input_columns_order):

    convert_dict = {"Radiance" : "Radiance" , "Effective" : "Effective_Brightness_Temperature" , "Brightness" : "Brightness_Temperature" , "Albedo" :"Albedo"  }
    year_month = ticketNo[:6]
    day = ticketNo[6:8]
    final_path_list = []
    features = input_columns_order.copy()
    if "zenith" in features: features.remove("zenith")
    if "HSR_value" in features: features.remove("HSR_value")
    if "target" in features: features.remove("target")
    for feature in features:
        channel = feature.split("_")[0]
        type = feature.split("_")[1]
        type = convert_dict[type]
        if "vi" in channel:
            if "vi006" in channel:
                distance = "05"
            else:
                distance = "10"
        else:
            distance = "20"
        final_path_list.append(collecting.data_collection_path_hdfs_gk2a.format(channel.upper(),year_month,day,channel,distance,ticketNo,type))
    return final_path_list



def getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user):
    active_host = None

    ''' ambari user/pass '''
    #auth_user = 'admin'

    #auth_pass = 'admin'
    #ambari_host = "192.168.0.174"
    ''' ambari host '''
    #ambari_cluster_name = 'hacluster'


    api_url = 'http://{}:8080/api/v1/clusters/{}/host_components?HostRoles/component_name=NAMENODE&metrics/dfs/FSNamesystem/HAState=active'.format(ambari_host,
                                                                                                                                                   ambari_cluster_name)
    res = requests.get(api_url, auth=(auth_user, auth_pass))

    status_code = res.status_code
    res_json = res.json()

    '''
    - status 200 == 정상
    - status 403 == 암바리 계정명 불일치
    - status 404 == api url 재확인 필요
    - status 5xx == 암바리 서버 에러
    '''
    if status_code==200 and res_json['items']:
        active_host = res_json['items'][0]['HostRoles']['host_name']

    return status_code, active_host




def check_data(path , hdfs_port , ambari_host , auth_pass , ambari_cluster_name, auth_user, log , error_log ):
    try:
        hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user)
        if hdfs_status  !=  200:
            log.info("hdfs status : {}".format(hdfs_status))
            return False
        else:
            working_hdfs_path = "http://{}:{}".format(active_host , hdfs_port)
            client = InsecureClient(working_hdfs_path, user="hdfs")
            file_status = client.status(path , strict = False)
            if file_status == None:
                return False
            else:
                return True
    except Exception as e:
        error_log.error("unknow error occured in checking the data : {}".format(e))
        return False






def sub_main(ticketNo ,log, error_log,  model_dic):
    log.info("\n\n" +ticketNo +"\n\n")
    try:
        total_model_info_path = os.path.join(model_dic["total_model_path"] , "total_model_info.json")

        with open(total_model_info_path, 'r') as fp:
            total_model_info = json.load(fp)
        fp.close()

        final_conv_shape_format = total_model_info["final_conv_shape_format"]
        sequence_len = total_model_info["sequence_len"]
        each_sequence_minutes = total_model_info["each_sequence_minutes"]

        total_model_class = fog_main_preprocessing(final_conv_shape_format , sequence_len , each_sequence_minutes)
        total_model_class.getting_final_format()
        total_model_class.extract_to_list()
        total_model_class.data_check_list()

        gk2a_check_list = total_model_class.gk2a_data_check_list

        ambari_host = hdfs_path_class.ambari_host
        hdfs_port = hdfs_path_class.hdfs_port
        auth_pass = hdfs_path_class.auth_pass
        ambari_cluster_name = hdfs_path_class.ambari_cluster_name
        auth_user = hdfs_path_class.auth_user

        ticketNo_real = datetime.datetime.strptime(ticketNo , "%Y%m%d%H%M")
        data_status_df = pd.DataFrame(index = gk2a_check_list)

        for i in range(sequence_len):
            subtracting_mins =  i * each_sequence_minutes
            each_ticketNo_real = ticketNo_real - datetime.timedelta( minutes = subtracting_mins)
            gk2a_each_ticketNo_real = each_ticketNo_real - datetime.timedelta(hours = 9)
            gk2a_each_ticketNo = datetime.datetime.strftime(gk2a_each_ticketNo_real ,"%Y%m%d%H%M" )
            hdfs_path_list = get_hdfs_path_list_by_time(gk2a_each_ticketNo, gk2a_check_list)
            true_false_list = list(map(lambda x: check_data(x , hdfs_port , ambari_host , auth_pass , ambari_cluster_name, auth_user, log , error_log) , hdfs_path_list  ))
            data_status_df[str(i) + "_" + str(gk2a_each_ticketNo) ] = true_false_list

        log.info(data_status_df)
        missing_row_count = 0
        for index in data_status_df.index.tolist():
            total_counts = sum(data_status_df.loc[index , : ])
            if total_counts == 0:
                logging_status = "False"
                missing_row_count  = missing_row_count + 1
            else:
                logging_status = "True"

            log.info("{} total counts--{} -----------> {}".format(index , total_counts , logging_status) )

        if missing_row_count > int(len(gk2a_check_list) / 1.5 ) :
            log.info("data status ----------------> disqualified")
            return True
        else:
            log.info("data status ----------------> qualified")
            return False
    except Exception as e:
        error_log.error("unable to check data list ----> force processing : {}".format(e))
        return False




def main(ticketNo , model_dic = None):
    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_check_data_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_check_data_error')

    import json
    base_model_path = model_dic["base_model_path"]
    processing_path = '{}/processing_status.json'.format(base_model_path)
    with open(processing_path) as json_file:
        status = json.load(json_file)
    json_file.close()
    log.info("processing status : {}".format(status["processing"]))

    if status["processing"] == "True":
        log.info("previous process is not finished --------> stopping process")
        return True, ticketNo

    ticketNo_date = datetime.datetime.strptime(ticketNo, '%Y%m%d%H%M') - datetime.timedelta(minutes=20)
    ticketNo = ticketNo_date.strftime('%Y%m%d%H%M')

    first_try =  sub_main(ticketNo ,log , error_log ,  model_dic)

    if first_try == False:
        reading_ticketNo = ticketNo
        return first_try , reading_ticketNo
    else:
        log.info("\n\n first ticketNo failed ---------------> proceeds to check second ticketNo \n\n")
        second_real_time = datetime.datetime.strptime(ticketNo , "%Y%m%d%H%M") - datetime.timedelta(minutes = 10)
        second_ticketNo = datetime.datetime.strftime(second_real_time  , "%Y%m%d%H%M")
        second_try = sub_main(second_ticketNo ,log, error_log , model_dic)
        reading_ticketNo = second_ticketNo
        return second_try , reading_ticketNo



# ticketNo = "201909060950"
#
# params = {"total_model_path" : "/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/tempo_file2/total_model_direc" ,
#           "amos_model_path"  : "/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/tempo_file2/amos_model_direc" }
# model_dic = {"total_model_path":params["total_model_path"],
#              "amos_model_path" :params["amos_model_path"] }
#
#
#
#
# log = fog_logging.get_log_view(1, "platform", False, 'fog_det_check_data_log')
# error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_check_data_error')
#
# ##first_try =  sub_main(ticketNo ,log , error_log ,  model_dic)
#
# ticketNo ,log , error_log ,  model_dic = ticketNo ,log, error_log,  model_dic
#
