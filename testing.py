import pandas as pd
import numpy as np
from fog_config import Config
from scipy.spatial import distance
import argparse

def set_globvar_to_path(env):
    if env == "local":
        root_path = Config.root_local_path
    elif env == "platform":
        root_path = Config.root_platform_path
    elif env == "dgx_share2":
        root_path = Config.root_dgx_path
    return root_path




def get_single_index(lat ,lon , reshaped_to_3d):
    result = find_mindist(float(lat),float(lon),reshaped_to_3d)
    result_lat = int(result[0])
    result_lon = int(result[1])
    return result_lat , result_lon


def find_mindist(lat, lon, lat_lon_map):
    dist_map = []
    for i in range(lat_lon_map.shape[0]):
        temp = list(map(lambda x: distance.euclidean([lat,lon],x),lat_lon_map[i]))
        dist_map.append(temp)
    dist_map_array = np.array(dist_map)
    solution = np.argwhere(dist_map_array == dist_map_array.min())
    return (solution[0][0], solution[0][1])




if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Process amos info')
    parser.add_argument('-lat' , '--lat' , dest = 'lat'  , type = np.float64)
    parser.add_argument('-lon' , '--lon' , dest = 'lon'  , type = np.float64)
    args = parser.parse_args()

    env = "platform"

    root_path = set_globvar_to_path(env)
    latlon_2000 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_2000.txt".format(root_path) , sep = "\t" , header = None)
    latlon_1000 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_1000.txt".format(root_path) , sep = "\t", header = None)
    latlon_500 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_500.txt".format(root_path) , sep = "\t" ,header = None)

    array_gk2a_2000 = np.array(latlon_2000)
    array_gk2a_1000 = np.array(latlon_1000)
    array_gk2a_500 = np.array(latlon_500)

    reshaped_to_3d_900 = np.reshape(array_gk2a_2000,(900,900,2))
    reshaped_to_3d_1800 = np.reshape(array_gk2a_1000,(1800,1800,2))
    reshaped_to_3d_3600 = np.reshape(array_gk2a_500 , (3600,3600,2))

    lat = args.lat
    lon = args.lon

    temp_lat,temp_lon = get_single_index(lat , lon , reshaped_to_3d_900)
    gk2a_lat = temp_lat
    gk2a_lon = temp_lon

    print("900 by 900 size --> y_pixel index : {}  , x_pixel index {}".format(temp_lat, temp_lon))
    temp_lat,temp_lon = get_single_index(lat , lon , reshaped_to_3d_1800)
    print("1800 by 1800 size --> y_pixel index : {}  , x_pixel index {}".format(temp_lat, temp_lon))
    temp_lat,temp_lon = get_single_index(lat , lon , reshaped_to_3d_3600)
    print("3600 by 3600 size --> y_pixel index : {}  , x_pixel index {}".format(temp_lat, temp_lon))
    hsr_data  = pd.read_csv("gk2a_hsr_int_pixel.csv")
    hsr_y = hsr_data.loc[(hsr_data["gk2a_y"] == int(gk2a_lat)) & (hsr_data["gk2a_x"] == int(gk2a_lon)) ,"hsr_y"].values[0]
    hsr_x = hsr_data.loc[(hsr_data["gk2a_y"] == int(gk2a_lat)) & (hsr_data["gk2a_x"] == int(gk2a_lon)) ,"hsr_x"].values[0]

    print("HSR size --> y_pixel index : {}  , x_pixel index {}".format(hsr_y, hsr_x))
