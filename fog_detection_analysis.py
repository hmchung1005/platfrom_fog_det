from fog_config import Config
import fog_config
import fog_logging
import datetime
import jaydebeapi
import jpype
from fog_create_model import load_table_tibero
import pandas as pd
import os
import argparse
import numpy as np

def set_globvar_to_path(env):
    if env == "local":
        root_path = Config.root_local_path
        output_path = Config.output_local_path
    elif env == "platform":
        root_path = Config.root_platform_path
        output_path = Config.output_platform_path
    elif env == "dgx_share2":
        root_path = Config.root_dgx_path
        output_path = Config.output_dgx_path
    return root_path , output_path

def open_connection(host,db_name,user_name,password, port, root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,
                               driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor

def merge_data(gk2a_df, hsr_df):
    if "table_id" in gk2a_df.columns.tolist():
        del gk2a_df["table_id"]
    if "table_id" in hsr_df.columns.tolist():
        del hsr_df["table_id"]
    if "time" in hsr_df.columns.tolist():
        del hsr_df["time"]
    if "tower" in hsr_df.columns.tolist():
        del hsr_df["tower"]


    merged = pd.merge(gk2a_df,  hsr_df , on = "time_base" , how = "inner")
    return merged

def plateaus(A, atol=3):
    runs = np.split(A, np.where(np.abs(np.diff(A)) >= atol)[0] + 1)
    return [list(x) for x in runs if len(x) > 1]



def analyze(train_start_ticket , train_end_ticket , log_file_name):

    env = "platform"
    root_path, output_path = set_globvar_to_path(env)
    host = fog_config.tb_host
    db_name = fog_config.tb_db_name
    user_name = fog_config.tb_user_name
    password = fog_config.tb_password
    port = fog_config.tb_port
    log = fog_logging.get_log_view(1, env, False, log_file_name )
    error_log = fog_logging.get_log_view(1, env, True, log_file_name )

    try:
        train_start_ticket = int(train_start_ticket)
        train_end_ticket = int(train_end_ticket)



        conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)

        gk2a_table_name = "d_fog_det_train_gk2a"

        hsr_table_name = "d_fog_det_train_hsr"

        gk2a_df = load_table_tibero(train_start_ticket,train_end_ticket, gk2a_table_name,cursor)



        hsr_df =  load_table_tibero(train_start_ticket , train_end_ticket , hsr_table_name , cursor)


        merged_data = merge_data(gk2a_df, hsr_df)   #<------ output UT-SFR-

        merged_data = merged_data.dropna()
        merged_data.time.max()

        merged_data["hsr_value"] = list(map(lambda x: 0 if x < 0 else x , merged_data["hsr_value"]))
        if not os.path.exists(output_path + '/analysis'):
            os.makedirs(output_path + '/analysis')

        folder_name = "analysis_bw_{}_{}".format(str(train_start_ticket), str(train_end_ticket))

        if not os.path.exists(output_path + '/analysis/{}'.format(folder_name)):
            os.makedirs(output_path + '/analysis/{}'.format(folder_name))


        cor_bool_list = list(map(lambda x:  x != "time"  and x != "tower"  and x != "time_base" , merged_data.columns.tolist() ))

        merged_data.loc[:,cor_bool_list ].corr().to_csv(output_path + '/analysis/{}/{}'.format(folder_name, "fog_detection_analysis.csv" ))

        log.info("analysis saved")

        tower_list = ["107" , "111" , "118" , "120" , "124", "125" , "128" ,"142" , "153" , "158" , "161" , "147" , "149" , "134"]
        new_df = pd.DataFrame()

        for tower in tower_list:
            tempo_df =  merged_data.loc[merged_data["tower"] == int(tower) , :]
            tempo_df =  merged_data.loc[merged_data["tower"] == int(tower) , :]
            tempo_df = tempo_df.sort_values("time")
            tempo_df = tempo_df.drop_duplicates("time_base")
            tempo_df = tempo_df.reset_index(drop = True)
            col_bool_list = list(map(lambda x: x != "time_tower" and x != "tower" and x != "time" , tempo_df.columns.tolist() ))
            tempo_df2 = tempo_df.loc[: , col_bool_list]
            stat_tempo = tempo_df2.describe()

            final_col = "base_{}".format(tower)

            for col in stat_tempo.columns.tolist():
                for index in stat_tempo.index.tolist():
                    new_index = col + "_" + index
                    new_value = stat_tempo.loc[index , col]
                    new_df.loc[new_index , final_col ] = str(new_value)


            fog_time_tempo = tempo_df.loc[tempo_df["target"]  < 4800 , :  ]

            fog_indexes = fog_time_tempo.index.tolist()

            final_group = plateaus(fog_indexes , atol  = 8)
            final_group_dates = []
            for listing in final_group:
                start_time = listing[0]
                end_time = listing[-1]
                dur = str(tempo_df.loc[start_time , "time" ]) + "_"  + str(tempo_df.loc[end_time , "time" ])
                final_group_dates.append(dur)



            new_df.loc["fog_duration"] = str(final_group_dates)

        new_df.to_csv(output_path + '/analysis/{}/{}'.format(folder_name, "fog_detection_analysis2.csv" ))
        log.info("analysis2 saved")
    except Exception as e:
        error_log.error("error : {}".format(e))
        pass

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Process some info.')
    parser.add_argument('-s' , '--train_start_ticket',dest = "train_start_ticket", type = str )
    parser.add_argument('-e' , '--train_end_ticket',dest = "train_end_ticket", type = str )
    parser.add_argument('-l' , '--log_file_name',dest = "log_file_name", type = str )
    args = parser.parse_args()
    train_start_ticket = args.train_start_ticket
    train_end_ticket = args.train_end_ticket
    log_file_name = args.log_file_name
    analyze(train_start_ticket , train_end_ticket, log_file_name)
