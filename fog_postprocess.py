import pandas as pd
import numpy as np
import os
from PIL import Image
import imageio
import datetime
import tensorflow as tf
import cv2
from collections import defaultdict
import geopy.distance
from geographiclib.geodesic import Geodesic
import math

from fog_config import Config, fog_character , fog_coloring , external_info_path  , tb_info , fog_main_preprocessing , apply_amos_limit , amos_model_features_labeling  #final_conv_shape_format
import fog_logging
import logging
import json
import math
import jaydebeapi
import jpype


#model_path = Config.model_path
def set_globvar_to_path(env):
    #tempo_path = Config.tempo_file

    if env == "platform":
        total_model_path = Config.total_model_platform_path
        output_path = Config.output_platform_path
        root_path = Config.root_platform_path
        tempo_path  = Config.tempo_platform_path
        amos_model_path = Config.amos_model_platform_path

    return total_model_path , output_path , root_path , tempo_path , amos_model_path


# 모델에 맞게 슬라이싱 해주는 함수
def df_to_dataset(dataframe, shuffle=True, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop('target')
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))
    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))
    ds = ds.batch(batch_size)
    return ds

# 픽셀별 위경도 값 산출
def make_vis_distance_array(linear_classes,time , output_path):
    linear_dis = linear_classes.copy()
    vis_array = np.array(linear_dis)
    vis_array_reshaped = np.reshape(vis_array,(900,900))
    vis_array_reshaped = np.round(vis_array_reshaped , fog_character.vis_distance_digits_number)
    vis_array_reshaped = np.clip(vis_array_reshaped, 0 , fog_character.maximum_vis_distance_clipping)
    temp_yearmonth = time[:8]
    if not os.path.exists("{}/number/{}".format(output_path , temp_yearmonth)):
        os.makedirs("{}/number/{}".format(output_path , temp_yearmonth))
    pd.DataFrame(vis_array_reshaped).to_csv("{}/number/{}/fog-det-trac_{}_number-det.csv".format(output_path,temp_yearmonth,time), index  = False)
    return vis_array_reshaped



def classify_array(linear_classes):
    deep_fog_high_end = fog_character.deep_fog_high_end
    normal_fog_high_end = fog_character.normal_fog_high_end
    weak_fog_high_end = fog_character.fog_binary_level

    linear_test = linear_classes.copy()
    # 시정거리를 사용해서 각 클래스로 분류
    linear_test = list(map(lambda x: 1 if x < deep_fog_high_end  else x , linear_test))
    linear_test = list(map(lambda x: 2 if x < normal_fog_high_end and x >= deep_fog_high_end else x , linear_test))
    linear_test = list(map(lambda x: 3 if x < weak_fog_high_end and x >= normal_fog_high_end else x , linear_test))
    linear_test = list(map(lambda x: 0 if x >= weak_fog_high_end  else x , linear_test))
    linear_final_array = np.array(linear_test)
    return linear_final_array




def make_watch_warn_df(tower_info, vis_array, time):
    tower_info["pred_value"] = list(map(lambda x,y: vis_array[int(y), int(x) ] , tower_info["lon_index_900"], tower_info["lat_index_900"] ))
    tower_info = tower_info.loc[:,["place" ,"pred_value"]]
    tower_info = tower_info.rename(columns = {"place":"base"})
    tower_info["model"] = "fog_det_trac"
    tower_info["type"] = "low_vis"
    tower_info["watch"] = False
    tower_info["warning"] = True
    tower_info["now_time"] = pd.to_datetime(time, format = "%Y%m%d%H%M")
    tower_info["watch_warn_time"] = tower_info["now_time"]
    return tower_info


def make_amos_vis_df(tower_info, vis_array , time):
    tower_info["vis_distance"] = list(map(lambda x,y: vis_array[y,x]  ,tower_info["lon_index_900"], tower_info["lat_index_900"]))
    tower_info["now_time"] = time
    tower_info = tower_info.loc[:,["place",'lat','lon','now_time','vis_distance']]
    tower_info = tower_info.rename(columns = {"place":"base"})
    convert_dict = {"now_time":int , "base":int , "lat":float , "lon":float, "vis_distance":float }
    tower_info = tower_info.drop_duplicates()
    return tower_info


# 안개 이동 시뮬레이션에서의 각 안개 객체를 생성 하는 클래스
class fog_object:
    def __init__(self, target_positions, target_ids_per_position, target_non_repeating_ids):
        self.tower_pos_list =  target_positions
        self.tower_num_list = target_ids_per_position
        self.non_repeat_num_list  = target_non_repeating_ids
    def set_values_to_fog(self,moving_ver,moving_hor,coord_list, id, total_seconds):
        self.report = {}
        self.id = id
        self.coord_list = np.array(coord_list)
        self.moving_ver= moving_ver
        self.moving_hor = moving_hor
        self.total_seconds = total_seconds
    def moving_position(self):
        self.coord_list = self.coord_list  +  [ self.moving_ver, self.moving_hor]
        #print("vertical_moving:{} horizontal_moving:{}".format(self.moving_ver,self.moving_hor))
    def checking_target(self):
        current_hit_pos =  multidim_intersect(self.coord_list, np.array(self.tower_pos_list))
        current_hit_tower = []
        for hit_pos in current_hit_pos:
            temp_index = self.tower_pos_list.index(hit_pos.tolist())
            current_hit_tower.append( self.tower_num_list[temp_index])
        self.current_hit_tower = current_hit_tower
        #print("hit tower {}".format(self.current_hit_tower))
    def time_simulation(self, number_of_eachtime):
        self.checking_target()
        if len(self.current_hit_tower) > 0:
            #print("tower hit alert")
            #print(time , "\n")
            self.report[0] = self.current_hit_tower
        for time in range(1,number_of_eachtime+1):
            self.moving_position()
            self.checking_target()
            if len(self.current_hit_tower) > 0:
                #print("tower hit alert")
                #print(time , "\n")
                self.report[time] = self.current_hit_tower
    def make_result_data(self):
        self.df = pd.DataFrame( columns = ["seconds_to_reach"] , index = self.non_repeat_num_list)
        self.df["seconds_to_reach"] = np.nan
        for time, towers in self.report.items():
            for tower in towers:
                if np.isnan(self.df.loc[tower,"seconds_to_reach"]):
                    self.df.loc[tower,"seconds_to_reach"] =  int((time)*self.total_seconds)
        self.df["target_base"] = self.df.index.tolist()
        self.df["fog_current_id"] = self.id
        self.df = self.df.dropna()

# 각 클래스별 모든 픽셀의 위치정보 산출
def find_fog_index_linear(final_detected_array):
    fog_indexes_1 = np.argwhere(final_detected_array == 1)
    fog_indexes_2 = np.argwhere(final_detected_array == 2)
    fog_indexes_3 = np.argwhere(final_detected_array == 3)
    return fog_indexes_1, fog_indexes_2 , fog_indexes_3

# 각 클래스별 색깔 지정후 픽셀 위치에 rgb 값 삽입
def linear_color_fog_on_map(image , fog_indexes_1, fog_indexes_2 , fog_indexes_3):
    for i in range(len(fog_indexes_1)):
        temp_index = fog_indexes_1[i]
        y = temp_index[0]
        x = temp_index[1]
        image[y,x,:,] = np.array(fog_coloring.deep_fog_rgb )
    for j in range(len(fog_indexes_2)):
        temp_index = fog_indexes_2[j]
        y = temp_index[0]
        x = temp_index[1]
        image[y,x,:] = np.array(fog_coloring.normal_fog_rgb )
    for k in range(len(fog_indexes_3)):
        temp_index = fog_indexes_3[k]
        y = temp_index[0]
        x = temp_index[1]
        image[y,x,:] = np.array(fog_coloring.weak_fog_rgb )
    return image

#날짜로 Dataframe과 coordinate_list를 만든다
def make_numeric_data(image_array, date, reshaped_to_3d_900, connectivity, coord_count, vis_array):
    raw_classes_array = image_array.copy()
    raw_classes_array = raw_classes_array.astype(np.uint8)
    raw_distances_array = vis_array.copy()

    #읽은 csv를 img의 형태로 바꿈
    img = convert_df_to_img(raw_classes_array)
    #img를 연관성있는 coordinate의 묶음으로 변환
    coord_list, center_list = make_coordinates_list(img, connectivity, coord_count)
    #중심점을 이용해 중심점 위경도를 포함한 df를 만든다
    info_df = get_info_df(coord_list,raw_classes_array,raw_distances_array, date, reshaped_to_3d_900)
    return info_df, coord_list, img

# 10분전의 시간 출력
def define_before_ten_minutes(end_date):
    date_time_obj = datetime.datetime.strptime(end_date , '%Y%m%d%H%M')

    ten_minute = datetime.timedelta(minutes=10)

    previous_time = date_time_obj- ten_minute

    start_date = previous_time.strftime("%Y%m%d%H%M")

    return start_date


def readjson_start_date(time, tempo_path):
    previous_date = time[:8]
    previous_file_path = '{}/{}/tempo_{}.json'.format(tempo_path, previous_date, time)
    is_beforedate = os.path.isfile(previous_file_path)
    if is_beforedate:
        with open(previous_file_path , 'r') as f:
            tempo_dict = json.load(f)
        f.close()
        start_info = pd.DataFrame.from_dict(tempo_dict)
        return start_info , True
    else:
        print("\n \n \n did not find previous information \n \n \n")
        return False , False



#연관된 좌표묶음으로 교집합을 찾는다
def find_intersection_from_coordlist(start_id, start_coord_list, end_id,  end_coord_list, intersection_ratio):
    whole_list=[]
    whole_debug_start_list=[]
    whole_debug_end_list=[]
    for start_idx, start_component in zip(start_id, start_coord_list):
        start_center, start_coord = start_component
        temp_list=[]
        debug_end_list=[]
        debug_start_list=[]
        previous_count=0
        for end_idx, end_component in zip(end_id, end_coord_list):
            end_center, end_coord = end_component
            if abs(start_center[0] - end_center[0]) > 80  or abs(start_center[1] - end_center[1]) > 80:
                continue
            elif len(start_coord)  / len(end_coord)  < 0.33 or len(start_coord) / len(end_coord) > 3:
                continue
            start_tmp_coord = [int(str(x[0]) + str(x[1])) for x in start_coord]
            end_tmp_coord = [int(str(x[0]) + str(x[1])) for x in end_coord]
            #for문은 연산시간이 크므로 np로, 220개 기준 // for :24sec vs numpy : 6sec
            start_np = np.array(start_tmp_coord)
            end_np = np.array(end_tmp_coord)
            result_np = np.intersect1d(start_np, end_np)
            count = len(result_np.tolist())
            min_count = min(len(start_coord), len(end_coord))
            if count > min_count*intersection_ratio:
                res_count = count / min_count
                if res_count > previous_count:
                    temp_list = [res_count, start_idx, end_idx]
                    debug_start_list = [start_coord, start_idx]
                    debug_end_list = [end_coord, end_idx]
                    previous_count = res_count
                    if res_count == 1: break
        if temp_list != []:
            # print(temp_list)
            #한 시작객체기준으로 가장 교집합이 큰것 하나
            # temp_list = sorted(temp_list, key=lambda x: x[0], reverse=True)
            del temp_list[0]
            whole_list.append(temp_list)
            whole_debug_start_list.append(debug_start_list)
            whole_debug_end_list.append(debug_end_list)
    return whole_list, whole_debug_start_list, whole_debug_end_list


def saving_tempo_file(final_info,coord_list,time, tempo_path):
    try:
        tempo_saving_df = final_info.copy()
        tempo_saving_df["start_coord_list"] =  list(map(lambda x: np.array(x), coord_list ))
        tempo_saving_df = tempo_saving_df.loc[:,["fog_current_id","fog_first_id","start_coord_list" , "mid_y","mid_x","lat","lon","strong_fog_count","normal_fog_count","weak_fog_count","blurred_pixel_count","avg_vis_distance","std_distance","low_vis_distance","whole_count"]]
        tempo_saving_df = tempo_saving_df.rename(columns = {"fog_current_id" : "fog_previous_id"})
    except:
        tempo_saving_df = pd.DataFrame(columns =["fog_previous_id","fog_first_id","start_coord_list" , "mid_y","mid_x","lat","lon","strong_fog_count","normal_fog_count","weak_fog_count","blurred_pixel_count","avg_vis_distance","std_distance","low_vis_distance","whole_count"])
    date=  time[:8]
    final_tempo_path =  "{}/{}".format(tempo_path , date)
    if not os.path.exists(final_tempo_path):
        os.makedirs(final_tempo_path)
    tempo_saving_df.to_json(os.path.join(final_tempo_path, "tempo_"+time+".json" ))



def get_speed(final_info):
    final_info["mv_speed"] = 0
    for i in final_info.index.tolist():
        coords_1 = (final_info.loc[i,'previous_lat'] , final_info.loc[i, 'previous_lon'] )
        coords_2 = (final_info.loc[i,'lat'], final_info.loc[i, 'lon'])
        moving_speed_kmh = geopy.distance.distance(coords_1 , coords_2).km * 6
        if moving_speed_kmh > fog_character.maximum_fog_speed:
            moving_speed_kmh = fog_character.maximum_fog_speed
        moving_speed_knots =  moving_speed_kmh * 0.539957
        final_info.loc[i,'mv_speed'] = moving_speed_knots
    return final_info


# 데이터프레임에서 각 객체별 방위값 생성
def get_direction(final_info):
    final_info["mv_direction"] = 0
    for i in final_info.index.tolist():
        lat1 = final_info.loc[i,'previous_lat']
        long1  = final_info.loc[i, 'previous_lon']
        lat2 = final_info.loc[i,'lat']
        long2 = final_info.loc[i, 'lon']
        final_info.loc[i,'mv_direction'] = get_bearing(lat1, lat2, long1, long2)
    final_info["mv_direction"] = (final_info["mv_direction"] + 360) % 360
    return final_info


# 안개 넓이, 평균 시정거리 생성
def modify_final_df(final_df):
    #final_df.loc[bool_list,['previous_mean','previous_whole_count','mv_speed','mv_direction']] = None
    final_df['fog_area'] = final_df['whole_count'] * 4
    final_df['previous_whole_area'] = final_df['previous_whole_count'] * 4
    final_df['mean_difference'] = final_df['avg_vis_distance'] - final_df['previous_mean']
    final_df['area_difference'] = final_df['fog_area'] - final_df['previous_whole_area']
    return final_df

# 좌우 움직임과 상하 움직임 최대공배수 계산 후 시간 움직임 전부 최대 공배수로 나누기
def modify_simulation_time_fogspeed(y_dif, x_dif):
    common_multiple = list(map(lambda x,y : math.gcd(x,y), x_dif, y_dif ))
    each_iter_total_seconds = list(map(lambda x: int(600 / x) if x != 0 else 600, common_multiple ))
    moving_ver_list = list(map(lambda x,y: int(x/y)  if y != 0 else 0, y_dif , common_multiple  ))
    moving_hor_list = list(map(lambda x,y: int(x/y)  if y != 0 else 0, x_dif , common_multiple ))
    return moving_ver_list, moving_hor_list, each_iter_total_seconds





def modify_total_seconds(divided_y_dif , divided_x_dif , divided_total_seconds):
    from math import sqrt
    modified_divided_total_seconds = divided_total_seconds.copy()
    for i in range(len(modified_divided_total_seconds)):
        # 최대 속도 픽셀 이동량 계산
        maximum_pixel_distance  = float((fog_character.maximum_fog_speed / int(3600 / divided_total_seconds[i])) / 2)
        tempo_y_dif = divided_y_dif[i]
        tempo_x_dif = divided_x_dif[i]
        # 안개 시뮬레이션에서 한 iteration 에서 최대 픽셀 이동량을 넘어서면 각 iteration 에서 더해지는 소요 시간 정보를 수정
        # 예시 : 각 iteration 에서 pixel 이동량이 x 고 걸리는 시간이 y 라 가정
        # 이동량과 걸리는 시간정보를 사용해서 객체 이동 속도 계산
        # 이동 속도가 한계치를 넘어서면 각 iteration 에 더해지는 소요시간 y 값을 강제로 증가
        # 소요시간 y를 늘리면서 다시 객체 이동속도를 최대 이동속도 및으로 조정
        current_pixel_distance  = sqrt(abs(tempo_y_dif)**2 + abs(tempo_x_dif) **2)
        if current_pixel_distance > maximum_pixel_distance:
            multiplying_value = current_pixel_distance / maximum_pixel_distance
            new_duration_time =  divided_total_seconds[i] * multiplying_value
            modified_divided_total_seconds[i] = new_duration_time
    return modified_divided_total_seconds




# 객체별 위치와 속도/방향을 고려해서 시뮬레이션
# 시뮬레이션 도중 기지에 객체가 도달하면 기지 유입 정보 생성
def simulation_all_fogs(final_df2, end_coord_list,tower_dict):
    final_df3 = final_df2.copy()
    fog_bool_list = list(map(lambda x, y: x != y , final_df3["fog_current_id"] , final_df3["fog_previous_id"]))
    final_df3 = final_df3.loc[fog_bool_list, :]
    y_difference = list(map(lambda x,y: int(y - x) , final_df3["previous_mid_y"] ,final_df3["mid_y"]))
    x_difference = list(map(lambda x,y: int(y - x) , final_df3["previous_mid_x"], final_df3["mid_x"]))

    divided_y_dif, divided_x_dif, divided_total_seconds  =  modify_simulation_time_fogspeed(y_difference, x_difference)
    divided_total_seconds = modify_total_seconds(divided_x_dif , divided_y_dif , divided_total_seconds)
    final_epoch_num = list(map(lambda x: int(fog_character.maximum_prediction_time  / x)  , divided_total_seconds ))

    end_coord_list2 = [ end_coord_list[x] for x in range(len(fog_bool_list)) if fog_bool_list[x] == True  ]
    extracted_coord_list = list(map(lambda x: x[1], end_coord_list2 ))
    tower_pos_list, tower_num_list, non_repeat_num_list = tower_dict_modification(tower_dict)
    fog = fog_object(tower_pos_list, tower_num_list, non_repeat_num_list)
    base_df  = pd.DataFrame(columns = ["seconds_to_reach", "target_base", "fog_current_id"])
    for id ,  y_dif, x_dif, coord, total_seconds, epoch  in zip(final_df3["fog_current_id"] ,y_difference, x_difference,extracted_coord_list, divided_total_seconds , final_epoch_num):
        fog.set_values_to_fog(y_dif, x_dif, coord , id , total_seconds)
        if max(abs(y_dif),abs(x_dif)) == 0:
            print(id , "not moving--> skipping")
            continue
        fog.time_simulation( int(epoch) )

        fog.make_result_data()
        #print(fog.df)
        tempo_df = fog.df
        tempo_df =  tempo_df.loc[tempo_df["seconds_to_reach"]!= 0 , :]
        base_df =  base_df.append(tempo_df)
    base_df  = base_df.reset_index()
    del base_df["index"]
    return base_df

# 각 객체별 짙은, 보통, 얕은 안개 픽셀 개수 정보 출력
def extra_info(pixels_index_array_list, raw_classes_array,raw_distances_array):
    strong_fog_list = []
    normal_fog_list = []
    weak_fog_list = []
    mean_distance_list = []
    std_distance_list = []
    blurred_pixel_list = []
    min_distance_list = []
    for i in pixels_index_array_list:

        current_classes = list(map(lambda x: raw_classes_array[x[0]][x[1]],i))
        blurred_pixel = sum(np.array(current_classes) == 0 )
        strong_fog = sum(np.array(current_classes) == 1 )
        normal_fog = sum(np.array(current_classes) == 2 )
        weak_fog = sum(np.array(current_classes) == 3 )

        strong_fog_list.append(strong_fog)
        normal_fog_list.append(normal_fog)
        weak_fog_list.append(weak_fog)
        blurred_pixel_list.append(blurred_pixel)

        current_mean_distance = np.array([raw_distances_array[x[0]][x[1]] for x in i if raw_distances_array[x[0]][x[1]] <= 4828 ] ).mean()
        current_std_distance =  np.array([raw_distances_array[x[0]][x[1]] for x in i if raw_distances_array[x[0]][x[1]] <= 4828 ] ).std()
        try:
            # if they are all the same -> min() function does not work
            current_min_distance =  np.array([raw_distances_array[x[0]][x[1]] for x in i if raw_distances_array[x[0]][x[1]] <= 4828 ] ).min()
        except:
            current_min_distance = current_mean_distance


        mean_distance_list.append(current_mean_distance)
        std_distance_list.append(current_std_distance)
        min_distance_list.append(current_min_distance)
    return strong_fog_list, normal_fog_list, weak_fog_list, blurred_pixel_list, mean_distance_list, std_distance_list,min_distance_list

# 픽셀과 픽셀간에 각도 계산
def get_angle(p1, p2):
    return math.atan2(p1[1] - p2[1], p1[0] - p2[0]) * 180/math.pi


def display_arrow(arrow_num, whole_num):
    tmp_str = '>'
    for i in range(whole_num):
        if i > arrow_num: tmp_str += '.'
        else: tmp_str += '>'
    result_str = '[{}]'.format(tmp_str)
    return result_str

def pil_to_cv2(pil_img):
    cv_img = cv2.cvtColor(np.array(pil_img), cv2.COLOR_RGB2BGR)
    return cv_img

# binary 에서 클래스 픽셀의 rgb 값 삽입
def linear_color_fog_on_singlemap(image , fog_indexes_1):
    for i in range(len(fog_indexes_1)):
        temp_index = fog_indexes_1[i]
        y = temp_index[0]
        x = temp_index[1]
        image[y,x,:] = np.array([255,0,0])
    return image


#안개 중심지역 찾기
def get_fog_location(df,array):
    info_df = df.copy()
    reshaped_to_3d_900 = array.copy()
    for i in info_df.index.tolist():
        lon_index = info_df.loc[i,'mid_x']
        lat_index = info_df.loc[i,'mid_y']
        info_df.loc[i,'lat'] = reshaped_to_3d_900[lat_index,lon_index,:][0]
        info_df.loc[i,'lon'] = reshaped_to_3d_900[lat_index,lon_index,:][1]
    return info_df

#중심점을 이용해 중심점 위경도를 포함한 df를 만든다
def get_info_df(coord_list, raw_classes_array, raw_distances_array, date, reshaped_to_3d_900):
    fog_id_list = [int(date + "000") + x for x in range(len(coord_list))]

    center_list = [x[0] for x in coord_list]
    pixels_within_group = [x[1] for x in coord_list]
    mid_x = [x[1] for x in center_list]
    mid_y = [x[0] for x in center_list]
    info_df = pd.DataFrame({"fog_current_id":fog_id_list, 'mid_y':mid_y,'mid_x':mid_x})
    info_df = info_df.astype({"fog_current_id":int,"mid_x":int,"mid_y":int})
    #info_df = info_df.astype(int)
    #중심점을 이용해 중심점 위경도를 포함한 df를 만든다
    info_df = get_fog_location(info_df,reshaped_to_3d_900)
    pixels_index_array_list =   list(map(lambda x: np.array(x) ,pixels_within_group))
    strong_fog_list, normal_fog_list, weak_fog_list, blurred_pixel_list, mean_distance_list, std_distance_list,min_distance_list = extra_info(pixels_index_array_list,raw_classes_array,raw_distances_array)

    info_df['strong_fog_count'],info_df['normal_fog_count'],info_df['weak_fog_count'],info_df['blurred_pixel_count'],info_df['avg_vis_distance'],info_df['std_distance'],info_df['low_vis_distance'] = strong_fog_list, normal_fog_list, weak_fog_list, blurred_pixel_list, mean_distance_list, std_distance_list,min_distance_list
    info_df['whole_count'] =  info_df['strong_fog_count'] + info_df['normal_fog_count'] + info_df['weak_fog_count']
    info_info = info_df.copy()
    return info_info


#연관성있는 coordinate의 묶음으로 변환
def extract_connected_components(image, connectivity, coord_count):
    output = cv2.connectedComponentsWithStats(image, connectivity)
    ret = output[0] -1
    labels = output[1]
    stats = output[2][1:]
    centroids = output[3][1:]
    indexdim0, indexdim1 = np.array(labels).nonzero()
    points = list(zip(indexdim0, indexdim1))
    ccdict = defaultdict(list)
    for p in points:
        y_coord, x_coord = p[0], p[1]
        k = output[1][y_coord][x_coord]
        ccdict[k].append([y_coord, x_coord])
    cc_list = [ccdict[k] for k in sorted(ccdict.keys())]
    #중심점의 리스트
    center_list = [(int(cy), int(cx)) for cx, cy in centroids]
    #중심점과 그에 연관된 좌표리스트의 묶음, 리스트 갯수통제를 하면서 작은 것들은 안가져온다
    result_list = [[center_point, cc] for center_point, cc in zip(center_list, cc_list) if len(cc) > coord_count]
    new_center_list = [(int(cy), int(cx)) for (cy, cx), cc in result_list]
    # print(len(result_list), len(center_list))
    return result_list, ret, stats, centroids, new_center_list

#읽은 csv를 img의 형태로 바꿈
def convert_df_to_img(array):
    data_np = array.copy()
    data = (data_np == 1).astype(int) + (data_np == 2).astype(int) + (data_np == 3).astype(int)
    fog_indexes_1 = np.argwhere(data == 1)
    zeros = np.zeros((900,900,3), dtype='uint8')
    data = linear_color_fog_on_singlemap(zeros,fog_indexes_1)
    img = Image.fromarray(data, 'RGB')
    img = pil_to_cv2(img)
    return img

#img를 연관성있는 coordinate의 묶음으로 변환
def make_coordinates_list(img, connectivity, coord_count):
    img = cv2.blur(img, (3,3))
    kernel = np.ones((5, 5), np.uint8)
    #erosion for removing noise
    img = cv2.erode(img, kernel, iterations=1)
    #dilation for removing noise
    img = cv2.dilate(img, kernel, iterations=2)
    # convert bgr to gray
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # setting threshold using cv2
    thresh = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)[1]
    #연관성있는 coordinate의 묶음으로 변환
    coord_list, ret, stats, centroids, center_list = extract_connected_components(thresh, connectivity, coord_count)
    return coord_list, center_list

# 방위각 계산 함수
def get_bearing(lat1, lat2, long1, long2):
    brng = Geodesic.WGS84.Inverse(lat1, long1, lat2, long2)['azi1']
    return brng

# 픽셀과 픽셀간의 거리 계산 함수
def get_pixel_angle_distance(final_info):
    final_info['opposite_pixel_angle'] = list(map(lambda x,y,x1,y1: get_angle((-y,x), (-y1,x1)) , final_info['previous_mid_x'], final_info['previous_mid_y'],final_info['mid_x'], final_info['mid_y'] ))
    final_info['pixel_distance'] = list(map(lambda x,y,x1,y1: np.sqrt((x1 -x)**2 + ( y1 - y)**2), final_info['previous_mid_x'], final_info['previous_mid_y'],final_info['mid_x'], final_info['mid_y'] ))
    return final_info

# 기지 유입 시간 계산 함수
def add_time_to_tower(y_tower_pixel, x_tower_pixel, coord_list, fog_opposite_angles, fog_pixel_distances,fog_lats,fog_lons):
    time_predictions = []
    for i in range(len(coord_list)):
        if np.isnan(fog_opposite_angles[i]):
            time_predictions.append(np.nan)
            # print('no angle')
        else:
            temp_angles = []
            coord = coord_list[i][1]
            fog_opposite_angle = (fog_opposite_angles[i] + 360) % 360
            # 꼭지점만 뽑아 오기
            convex_points = cv2.convexHull(np.array(coord))
            for convex_point in convex_points:
                temp_y = int(convex_point[0][0])
                temp_x = int(convex_point[0][1])
                temp_angle = (get_angle( (temp_y, temp_x), (y_tower_pixel,x_tower_pixel)) + 360) % 360
                temp_angles.append(temp_angle)
            # print("opposite angle:", fog_opposite_angle, " tower_fog_angles:" ,temp_angles)
            # 최대 각도와 최소 각도  사이에 대각이 있으면 기지 유입 시간 계산
            if fog_opposite_angle < max(temp_angles) and fog_opposite_angle > min(temp_angles):
                fog_pixel_distance = fog_pixel_distances[i]
                fog_y = fog_lats[i]
                fog_x = fog_lons[i]
                tower_fog_distance = np.sqrt(( x_tower_pixel -fog_x)**2 + (y_tower_pixel - fog_y)**2)
                time_predicition = (tower_fog_distance / fog_pixel_distance) * 10
                time_predictions.append(time_predicition)
            else:
                time_predictions.append(np.nan)
    return time_predictions

# 데이터프레임에 기지 유입 시간 융합
def get_predition_columns_by_num(table, num, coord_list, amos, array):
    final_df = table.copy()
    amos_df = amos.copy()
    amos_df['place'] = list(map(lambda x: "0" + str(x) if len(str(x)) == 2 else str(x), amos_df['place']  ))
    fog_opposite_angles = final_df['opposite_pixel_angle']
    fog_pixel_distances = final_df['pixel_distance']
    fog_lats = final_df['mid_y']
    fog_lons = final_df['mid_x']
    for i in amos_df.index.tolist()[:num]:
        y_tower_pixel = int(amos_df.loc[i, "lat_index_900"])
        x_tower_pixel = int(amos_df.loc[i, "lon_index_900"])
        time_prediction_list = add_time_to_tower(y_tower_pixel,x_tower_pixel,coord_list,fog_opposite_angles,fog_pixel_distances,fog_lats,fog_lons)
        tower_name = "arrivalTIME_" + str(amos_df.loc[i, "place"])
        final_df[tower_name] = time_prediction_lis
    reshaped_to_3d_900 = array.copy()

    #detect_time_using_straight_line(coord_list, reshaped_to_3d_900, final_df, amos_df, num)

    return final_df

# 10분후의 시간 출력
def define_after_ten_minutes(start_date):
    date_time_obj = datetime.datetime.strptime(start_date , '%Y%m%d%H%M')
    ten_minute = datetime.timedelta(minutes=10)
    previous_time = date_time_obj +  ten_minute
    end_date = previous_time.strftime("%Y%m%d%H%M")
    return end_date

# 10분전 객체와 현재 객체에서 같은 위치에 탐지된 픽셀 찾기
def multidim_intersect(arr1, arr2):
    arr1_view = arr1.view([('',arr1.dtype)]*arr1.shape[1])
    arr2_view = arr2.view([('',arr2.dtype)]*arr2.shape[1])
    intersected = np.intersect1d(arr1_view, arr2_view)
    return intersected.view(arr1.dtype).reshape(-1, arr1.shape[1])

# 10분 전 시간에서 사용할 데이터 융합
def get_previous_lat_lon(df1,df2, whole_list):
    start_info = df1.copy()
    end_info = df2.copy()
    if len(end_info) > 0:
        for previous_id, current_id  in whole_list:
            end_info.loc[end_info["fog_current_id"]== current_id, "fog_previous_id"] = previous_id
    else:
        end_info["fog_previous_id"] = end_info["fog_current_id"]

    bool_list= list(map(lambda x: (x > 0) == False, end_info["fog_previous_id"]))
    end_info.loc[bool_list,"fog_previous_id" ] = end_info.loc[bool_list, "fog_current_id" ]
    end_info["fog_previous_id"] = end_info["fog_previous_id"].astype(int)

    start_info = start_info.loc[:,["fog_previous_id", "fog_first_id", "lat","lon","avg_vis_distance","whole_count","mid_y","mid_x"]]
    start_info = start_info.rename(columns = {"lat":"previous_lat","lon":"previous_lon", "avg_vis_distance":"previous_mean","whole_count":"previous_whole_count","mid_y":"previous_mid_y", "mid_x":"previous_mid_x"})

    end_info = end_info.merge(start_info, on = "fog_previous_id", how = "left")
    end_info.loc[bool_list, ["fog_first_id", "previous_lat", "previous_lon" , "previous_mean", "previous_whole_count", "previous_mid_y", "previous_mid_x"]] = pd.DataFrame(end_info.loc[bool_list,["fog_current_id", "lat","lon","avg_vis_distance", "whole_count","mid_y", "mid_x"]]).values
    end_info[["fog_first_id", "previous_whole_count","previous_mid_y","previous_mid_x"]] = end_info[["fog_first_id", "previous_whole_count","previous_mid_y","previous_mid_x"]].astype(int)
    return end_info

def check_base_result(base_result, final_df2, tower_info):
    final_df3 = final_df2.copy()
    tower_info2 = tower_info.copy()
    base_result2 = base_result.copy()
    base_result2 = base_result2.merge(tower_info2, left_on = "target_base", right_on = "place").loc[:,["target_base","lat_index_900","lon_index_900","fog_current_id"]]
    bool_list = list(map(lambda x: x in base_result2.fog_current_id.tolist() ,final_df3.fog_current_id.tolist()))
    final_final = final_df3.loc[bool_list, ["fog_current_id", "mid_y", "mid_x", "previous_mid_y", "previous_mid_x","mv_direction"] ]
    final_final = final_final.merge(base_result2, on = "fog_current_id", how= "outer" )
    final_final["y_dif"] = final_final["mid_y"] - final_final["previous_mid_y"]
    final_final["x_dif"] = final_final["mid_x"] - final_final["previous_mid_x"]
    del final_final["previous_mid_y"]
    del final_final["previous_mid_x"]
    return final_final

# 기지 유입 정보 계산시 스쳐 지나가는 객체도 인지 하기 위해 기지 주변 픽셀도 기지에 포함  (blur 처리)
def blurring_tower(arr):
    base  = arr.tolist()
    north =  (arr + [1,0]).tolist()
    south =  (arr + [-1,0]).tolist()
    east =  (arr + [0 , 1]).tolist()
    west =  (arr + [0 , -1]).tolist()
    north_east =  (arr + [1 , 1]).tolist()
    north_west =  (arr + [1  , -1]).tolist()
    south_east =  (arr + [-1 , 1]).tolist()
    south_west =  (arr + [-1 , -1]).tolist()
    return np.array([base, north, south, east, west , north_east, north_west, south_east,south_west ])


# 기지 주변 픽셀 까지 blur 처리 후 기지 픽셀 정보 리스트 생성
# 예제 : 기지 번호 : 108
# tower_num_list : 108 , 108 , 108 , 108 , 108 , 108 , 108 , 108, 108 --> 9개 .. 주변 픽셀 까지 합치면 9개 픽셀이 있다
# tower_pos_list : [1,1],[1,2],[1,3],[2,1],[2,2],[2,3],[3,1],[3,2],[3,3]  -> 9개 픽셀 위치 정보
# non_repeat_num_lst: 108
def tower_dict_modification(tower_dict):
    new_tower_dic ={}
    for tower, values in tower_dict.items():
        new_tower_dic[tower] = blurring_tower(values)
    tower_pos_list = np.array([x.tolist() for x in new_tower_dic.values()])
    num1 = tower_pos_list.shape[0]
    num2 = tower_pos_list.shape[1]
    tower_pos_list = np.reshape(tower_pos_list, (num1 * num2 , 2  ) ).tolist()
    tower_num_list = np.array([x for x in new_tower_dic.keys()])
    non_repeat_num_list = tower_num_list.copy().tolist()
    tower_num_list = np.repeat(tower_num_list, 9 ).tolist()
    return tower_pos_list, tower_num_list, non_repeat_num_list





# a = np.load("/share/3_데이터/GK2A/ko/result/201909/03/04/result/gk2a_ami_le1b_vi008_ko010lc_201909030420.nc/gk2a_ami_le1b_vi008_ko010lc_201909030420.nc.Radiance.txt.npz", allow_pickle = True)
# for i in a.keys():
#     print(i)
# data.shape
# data = a["arr_0"]
# data[:,1799]
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# model = tf.keras.models.load_model(model_path)
#
# detect_df['target'] = 0      ### 모델이 추론 할 수 있게 가상 y 값 설정
# detect_ds = df_to_dataset(detect_df, shuffle = False, batch_size = 32)   # 퓨쳐 슬라이싱
#
# linear_pred = model.predict(detect_ds)
# linear_classes = linear_pred.flatten()
#
# vis_array = make_vis_distance_array(linear_classes, str(time))
#
# image_array = make_picture_and_detectable_array(linear_classes=linear_classes,time = str(time))
#
# final_df , tower_info = make_result_dataframe_from_image(image_array, str(time),vis_array)
#
# watch_warn_df = make_watch_warn_df(tower_info, vis_array,time)
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

def make_result_dataframe_from_image(image_array, end_date,vis_array, root_path, tempo_path , output_path , log ,error_log):
    #connectivity에 따라 contour의 연관도를 인식하는 객체의 범위가 바뀜(4~8)
    connectivity= fog_character.connectivity
    # connected_coorinate에서 한 객체에 포함되는 좌표수를 의미, 해당 갯수보다 큰 좌표묶음만 가져온다
    coord_count= fog_character.coord_count
    #한 시작객체기준으로 교집합의 비율
    intersection_ratio = fog_character.intersection_ratio
    latlon_2000 =  pd.read_csv("{}/LATLON_KO_2000.txt".format(root_path), header = None, sep = '\t')
    array_gk2a_2000 = np.array(latlon_2000)
    reshaped_to_3d_900 = np.reshape(array_gk2a_2000,(900,900,2))

    #날짜로 Dataframe과 coordinate_list를 만든다
    end_info_df, end_coord_list, end_img = make_numeric_data(image_array, end_date, reshaped_to_3d_900, connectivity, coord_count,vis_array)
    only_coords = list(map(lambda x: x[1] , end_coord_list))
    save_picture(image_array , only_coords , end_date , output_path)


    if len(end_info_df) == 0:
        print("there is no fog at all")
        empty_cols = ['fog_current_id','mid_y','mid_x','lat','lon','strong_fog_count','normal_fog_count','weak_fog_count','blurred_pixel_count','avg_vis_distance','std_distance','low_vis_distance','whole_count']
        end_info_df = pd.DataFrame(columns = empty_cols)
        end_coord_list = [[]]
        emd_img = np.zeros((900,900,3) , dtype = "uint8")

    start_date = define_before_ten_minutes(str(end_date))
    start_info_df, booling = readjson_start_date(start_date, tempo_path)
    log.info("booling : " )
    log.info(booling)
    if booling == False:
        final_info = end_info_df.copy()
        final_info["previous_lat"], final_info["previous_lon"],final_info["fog_previous_id"],final_info["previous_mean"],final_info["previous_whole_count"],final_info["previous_mid_y"],final_info["previous_mid_x"] = end_info_df["lat"],end_info_df["lon"],end_info_df["fog_current_id"],end_info_df["avg_vis_distance"],end_info_df["whole_count"],end_info_df["mid_y"],end_info_df["mid_x"]
        final_info["fog_first_id"] = final_info["fog_current_id"]
    else:
        start_id = start_info_df.fog_previous_id.tolist()
        start_coord_list = start_info_df.start_coord_list.tolist()
        end_id = end_info_df.fog_current_id
        #연관된 좌표묶음으로 교집합을 찾는다
        whole_list, whole_debug_start_list, whole_debug_end_list = find_intersection_from_coordlist(start_id,start_coord_list,end_id,end_coord_list,intersection_ratio)
        final_info = get_previous_lat_lon(start_info_df,end_info_df, whole_list)
    if type(final_info) == pd.core.frame.DataFrame:
        if len(final_info) > 0:
            log.info("saving temp file")
            saving_tempo_file(final_info, end_coord_list, end_date, tempo_path)
    final_info = get_speed(final_info)
    final_df = get_direction(final_info)
    final_df = modify_final_df(final_df)
    final_df2 =  get_pixel_angle_distance(final_df)
    tower_info =  pd.read_csv(external_info_path.all_amos_info_path)

    tower_dict = {}
    for i in  tower_info.index.tolist():
        tower_dict[tower_info.loc[i,"place"]] = np.array([tower_info.loc[i,"lat_index_900"], tower_info.loc[i,"lon_index_900"]]  )

    start_time = datetime.datetime.now()
    base_result = simulation_all_fogs(final_df2, end_coord_list,tower_dict)
    end_time =datetime.datetime.now()
    print("fog simulation Duration :",  end_time - start_time)

    final_df3 = {"result_db" : final_df2 , "base_db" : base_result }
    return final_df3



# 노드 확률을 사용해서 시정거리로 변환
def add_linear_layer(results):
    min_results = results.min(axis = 1)
    min_results = min_results[..., np.newaxis ]
    max_results = results.max(axis=1)
    max_results = max_results[..., np.newaxis]
    max_minus_min = max_results - min_results
    min_max_results = (results - min_results) / (max_minus_min)
    classes = np.argmax(min_max_results, axis=1)
    high_percentage = sum(np.equal(classes , 5)) / len(classes)
    low_percentage = sum(np.equal(classes , 0 )) / len(classes)
    modified_classes = classes  + 1
    modified_min_max = np.insert(min_max_results , 0 , low_percentage , axis = 1)
    modified_min_max = np.insert(modified_min_max , 7 , high_percentage , axis = 1)
    modified_classes_minus_one = modified_classes - 1
    modified_classes_plus_one = modified_classes + 1
    minus_ratio = list(map(lambda x ,y : x[y] , modified_min_max , modified_classes_minus_one ))
    minus_ratio = np.array(minus_ratio)
    plus_ratio = list(map(lambda x, y : x[y] , modified_min_max , modified_classes_plus_one))
    plus_ratio = np.array(plus_ratio)
    minus_sum = minus_ratio  * -490
    #plus_sum =  np.array(list(map(lambda x, y: x * 510 if y == 4 else x *500 , plus_ratio , classes  )))
    plus_sum = plus_ratio * 500
    sum_sum = minus_sum  + plus_sum
    vis_pred_distance = classes * 1000 + 500
    vis_pred_modified = vis_pred_distance + sum_sum
    return vis_pred_modified

def save_picture(image_array , only_coords , time , output_path):
    new_array = np.zeros((900,900) , dtype= "uint8")
    for fog_object_coords in only_coords:
        for pixel_loc in fog_object_coords:
             new_array[pixel_loc[0] ,pixel_loc[1]] = image_array[pixel_loc[0] ,pixel_loc[1] ]
    fog_indexes_1, fog_indexes_2 , fog_indexes_3 = find_fog_index_linear(new_array)
    im2 = np.zeros((900,900,4))
    modified_image2 = linear_color_fog_on_map(im2, fog_indexes_1, fog_indexes_2 , fog_indexes_3)
    modified_image2 =  modified_image2.astype(np.uint8)
    picture = Image.fromarray(modified_image2)
    temp_yearmonth = time[:8]
    if not os.path.exists("{}/image/{}".format(output_path , temp_yearmonth) ):
        os.makedirs("{}/image/{}".format(output_path , temp_yearmonth))
    imageio.imwrite('{}/image/{}/fog-det-trac_{}_image-det.png'.format(output_path,temp_yearmonth , time),modified_image2)

def open_connection(host,db_name,user_name,password, port, root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor


def reshaping_array_into_picture(linear_final_array, time, output_path):
    final_detected_array = np.reshape(linear_final_array , (900,900))
    # fog_indexes_1, fog_indexes_2 , fog_indexes_3 = find_fog_index_linear(final_detected_array)
    # im2 = np.zeros((900,900,4))
    # modified_image2 = linear_color_fog_on_map(im2, fog_indexes_1, fog_indexes_2 , fog_indexes_3)
    # modified_image2 =  modified_image2.astype(np.uint8)
    # picture = Image.fromarray(modified_image2)
    # temp_yearmonth = time[:8]
    # if not os.path.exists("{}/image/{}".format(output_path , temp_yearmonth) ):
    #     os.makedirs("{}/image/{}".format(output_path , temp_yearmonth))
    # imageio.imwrite('{}/image/{}/detected_colored_result_{}.png'.format(output_path,temp_yearmonth , time),modified_image2)
    return final_detected_array

def norm_amos_vals(current_vals , amos_summary):
    for key , val in current_vals.items():
        max = amos_summary.loc["max" , key]
        min = amos_summary.loc["min" , key]
        normed_data = (val  - min) / (max - min)
        current_vals[key] = normed_data
    return current_vals


# import fog_preprocess
# # 사용할 GPU 선택
def set_gpu(gpu_num):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')   # 물리적 GPU리스트
    if physical_devices != []:
        tf.config.experimental.set_visible_devices(physical_devices[gpu_num], 'GPU')   # 3번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.experimental.set_memory_growth(physical_devices[gpu_num], True) # 현재 사용중인 GPU의 메모리를 사용하는 만큼만 메모리 적용
#     logical_devices = tf.config.experimental.list_logical_devices('GPU')


def norming_features_deep(df2 ,feature_summary):
    df = df2.copy()
    # target = df.pop("target")
    df = df.astype("float")
    normed_df = pd.DataFrame()
    columns = df.columns.tolist()
    for col in columns:
        summary_col = col
        min = feature_summary.loc["min" , summary_col]
        max = feature_summary.loc["max" , summary_col]
        normed_data = (df.loc[:,col] - min) / (max - min)
        normed_df[col] = normed_data
    return normed_df

def make_final_seq_array(final_conv_shape_input , normed_whole_df , final_seq_array):
    dimension_len = len(final_conv_shape_input.shape)
    if dimension_len == 3:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0] , indices[1] , indices[2]) )
            for loc in loc_list:
                seq_num = loc[0]
                y_num = loc[1]
                x_num = loc[2]
                final_seq_array[:, seq_num , y_num , x_num] = normed_whole_df[col]
    elif dimension_len == 2:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0] , indices[1]))
            for loc in loc_list:
                y_num = loc[0]
                x_num = loc[1]
                final_seq_array[:,y_num , x_num] = normed_whole_df[col]
    elif dimension_len ==1:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0]))
            for loc in loc_list:
                x_num = loc[0]
                final_seq_array[:, x_num] = normed_whole_df[col]
    return final_seq_array

def filter_float_and_positive(number):
    try:
        if float(number) > 0:
            return True
        else:
            return False
    except:
        return False


def apply_amos_model_independently(result_vis , amos_model , ticketNo , amos_normed_whole_df , amos_model_class , amos_summary , amos_pixel_info, log , error_log):
    amos_model_class.get_input_shape()
    final_input_shape =  amos_model_class.final_input_shape
    final_input_shape =  final_input_shape[:-1]
    amos_model_class.getting_final_format()
    final_conv_shape_input =  amos_model_class.final_conv_shape_input
    amos_normed_whole_df_columns = amos_normed_whole_df.columns.tolist()
    amos_normed_whole_np = np.reshape(amos_normed_whole_df.values , (900, 900 , len(amos_normed_whole_df_columns)  ) )
    amos_model_class.extract_to_list()
    amos_features = amos_model_class.amos_features

    db_col_dic = {"tower":"K2" , "time":"K1"}
    db_col_dic_153 = {"tower":"K2" , "time":"K1"}
    naming_rules = {v: k for k, v in amos_model_features_labeling.amos_features_naming.items()}
    naming_rules_153 = {v: k for k, v in amos_model_features_labeling.amos_153_features_naming.items()}
    for amos_feature in amos_features:
        db_col_dic[amos_feature] = naming_rules[amos_feature]
        db_col_dic_153[amos_feature] = naming_rules_153[amos_feature]

    col_db_dic = {v: k for k, v in db_col_dic.items()}
    col_db_dic_153 = {v: k for k, v in db_col_dic_153.items()}

    amos_normed_features = pd.Series(list(map(lambda x:  x[:-2] , amos_normed_whole_df_columns ))).unique().tolist()



    non_gk2a_hsr_amos_features = list(filter(lambda x: x not in amos_normed_features , amos_model_class.final_conv_shape_format ))
    non_gk2a_hsr_amos_features.append("tower")
    non_gk2a_hsr_amos_features.append("time")


    db_cols = list(map(lambda x: db_col_dic[x] , non_gk2a_hsr_amos_features ))
    db_cols_153 = list(map(lambda x: db_col_dic_153[x] , non_gk2a_hsr_amos_features))


    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port
    root_path = external_info_path.root_path
    conn, cursor = open_connection(host,db_name,user_name,password,port, root_path)
    now_ticket = ticketNo
    real_time = datetime.datetime.strptime(now_ticket, '%Y%m%d%H%M')
    time_list = [real_time] * amos_model_class.sequence_len
    before_list = ["None"] * amos_model_class.sequence_len
    rank_list = list(range(amos_model_class.sequence_len  - 1 , - 1 , -1 ))

    for i in range(amos_model_class.sequence_len):
        subtracting_mins = int(i * amos_model_class.each_sequence_minutes)
        current_time = real_time - datetime.timedelta(minutes = int(subtracting_mins + 2 ) ) ################################ changed############################
        before_time = current_time.strftime("%Y%m%d%H%M")
        time_list[i] = current_time
        before_list[i] = before_time


    time_to_rank_dic = dict(zip(before_list, rank_list))
    time_list_str = " or ".join( list(map(lambda x: "K1 = {}".format(x) , before_list  )))
    sql = "select "+  " , ".join(db_cols)+ " from D_AMOS_DETAIL where (" + time_list_str  +   ") and K2 != '153';"
    sql_153 = "select "+  " , ".join(db_cols_153)+ " from D_AMOS_DETAIL where (" + time_list_str  +   ") and K2 = '153';"
    cursor.execute(sql)
    data = cursor.fetchall()
    amos_df = pd.DataFrame(data, columns = db_cols)
    amos_df = amos_df.rename(columns = col_db_dic)

    cursor.execute(sql_153)
    data_153 = cursor.fetchall()
    amos_df_153 = pd.DataFrame(data_153 , columns = db_cols_153)
    amos_df_153 = amos_df_153.rename(columns = col_db_dic_153)

    amos_df = pd.concat([amos_df, amos_df_153] , ignore_index = True)
    filter_amos_df_bool_list = list(map(lambda x: filter_float_and_positive(x)  , amos_df["vis_distance"] ))
    amos_df =  amos_df.loc[filter_amos_df_bool_list ,  : ]

    #log.info("apply amos model df : {}".format(amos_df))

    timing = amos_df.pop("time")
    towering = amos_df.pop("tower")
    amos_df = norming_features_deep(amos_df , amos_summary)
    amos_df["time"] = timing
    amos_df["tower"]  = towering

    amos_df = amos_df.drop_duplicates(['time', 'tower'] , keep = 'last')
    #log.info("apply amos model df dropped duplicates : {}".format(amos_df))
    unique_avail_towers  = pd.Series(amos_df.tower).unique().tolist()

    unique_avail_towers = list(filter(lambda x: int(x) in  amos_pixel_info["tower"].tolist() , unique_avail_towers ))

    amos_pixel_info = amos_pixel_info.loc[ list(map(lambda x: str(x) in  unique_avail_towers, amos_pixel_info["tower"] ))  , : ]

    y_axis_limit = apply_amos_limit.y_axis_limit
    x_axis_limit = apply_amos_limit.x_axis_limit

    for current_tower in unique_avail_towers:
        try:
            #print(current_tower)
            amos_y_pixel = int(amos_pixel_info.loc[amos_pixel_info["tower"] == int(current_tower) , "lat_index_900"])
            amos_x_pixel = int(amos_pixel_info.loc[amos_pixel_info["tower"] == int(current_tower) , "lon_index_900"])
            current_gk2a_features = amos_normed_whole_np[amos_y_pixel - y_axis_limit : amos_y_pixel + y_axis_limit , amos_x_pixel - x_axis_limit : amos_x_pixel + x_axis_limit , :    ]
            reshaped_gk2a_features = np.reshape(current_gk2a_features , ((y_axis_limit*2) *  (x_axis_limit*2)  ,  len(amos_normed_whole_df_columns) )  )
            reshaped_gk2a_df = pd.DataFrame(reshaped_gk2a_features , columns = amos_normed_whole_df_columns )
            current_amos_df = amos_df.loc[amos_df["tower"] == current_tower , :]
            for feature in apply_amos_limit.tolerant_amos_feature_list:
                if feature in current_amos_df.columns.tolist():
                    log.info("applying tolerance")
                    if current_amos_df[feature].isnull().values.all(axis = 0):
                        current_amos_df[feature] = (amos_summary.loc["mean",  feature] - amos_summary.loc["min",  feature]) / (amos_summary.loc["max",  feature] - amos_summary.loc["min",  feature])
            if sum(current_amos_df.isnull().values.all(axis=0)) > 0 :
                continue
            if len(current_amos_df) != amos_model_class.sequence_len:
                continue
            total_current_amos_series = pd.Series([])
            for each_time , rank in zip(before_list , rank_list):
                current_amos_series = current_amos_df.loc[current_amos_df["time"] == each_time , :].squeeze()
                current_amos_series = current_amos_series.rename(lambda x: x + "_{}".format(rank))
                total_current_amos_series = pd.concat([total_current_amos_series  , current_amos_series ] )


            for key , val in total_current_amos_series.items():
                reshaped_gk2a_df[key]= val


            empty_seq_array = np.zeros(((y_axis_limit*2) * (x_axis_limit*2) , ) +  final_input_shape)

            final_seq_array = make_final_seq_array(final_conv_shape_input , reshaped_gk2a_df , empty_seq_array )
            predict_input_array = final_seq_array[..., np.newaxis]
            tempo_result =  amos_model.predict(predict_input_array)
            tempo_vis_array  =add_linear_layer(tempo_result)
            tempo_reshaped_vis_array = np.reshape(tempo_vis_array , (y_axis_limit * 2 , x_axis_limit * 2 ) )
            log.info(current_tower)
            log.info(tempo_reshaped_vis_array)
            result_vis[amos_y_pixel - y_axis_limit : amos_y_pixel + y_axis_limit , amos_x_pixel - x_axis_limit : amos_x_pixel + x_axis_limit  ] = tempo_reshaped_vis_array
        except Exception as e:
            error_log.error("apply amos error at tower : {}".format(current_tower))
            error_log.error("due to : {}".format(e))

    cursor.close()
    conn.close()

    return result_vis

def main(ticketNo_dic, total_final_seq_data, amos_normed_whole_df , env, params , log , error_log):

    ticketNo = ticketNo_dic["saving_ticketNo"]
    reading_ticketNo=ticketNo_dic["reading_ticketNo"]

    backup_total_model_path , output_path , root_path , tempo_path , backup_amos_model_path = set_globvar_to_path(env)
    # 모델 로드..... 방어 코드로 최적 모델 로드
    try:
        total_model = params["total_model"]
        total_summary_path = params["total_summary_path"]
        log.info("total model loaded")
        total_model_info_path = os.path.join(params["total_model_path"] , "total_model_info.json")
        with open(total_model_info_path, 'r') as fp:
            total_model_info = json.load(fp)
        fp.close()


    except Exception as e:
        error_log.error("error loading total model : {}".format(e))
        total_model = tf.keras.models.load_model(backup_total_model_path)
        total_summary_path = os.path.join(backup_total_model_path , "feature_summary.csv")
        log.info("loading previous total model")
        total_model_info_path = os.path.join(backup_total_model_path , "total_model_info.json")
        with open(total_model_info_path, 'r') as fp:
            total_model_info = json.load(fp)
        fp.close()


    try:
        amos_model = params["amos_model"]
        amos_summary_path = params["amos_summary_path"]
        log.info("amos model loaded")
        amos_model_info_path = os.path.join(params["amos_model_path"] , "amos_model_info.json")
        with open(amos_model_info_path , 'r') as gp:
            amos_model_info = json.load(gp)
        gp.close()

    except Exception as e:
        error_log.error("error loading amos model : {}".format(e))
        amos_model = tf.keras.models.load_model(backup_amos_model_path)
        amos_summary_path = os.path.join(backup_amos_model_path , "feature_summary.csv")
        log.info("loading previous amos model")
        amos_model_info_path = os.path.join(backup_amos_model_path , "amos_model_info.json")
        with open(amos_model_info_path , 'r') as gp:
            amos_model_info = json.load(gp)
        gp.close()


    try:
        prediction_start = datetime.datetime.now()
        total_linear_pred = total_model.predict(total_final_seq_data)

        prediction_end = datetime.datetime.now()
        print("prediction Duration : {}".format(prediction_end - prediction_start))
        # 카테고리 시정거리 (0~1000 , 1000~2000...) 에서 시정거리 수치로 변환
        total_linear_classes = add_linear_layer(total_linear_pred)


        log.info('model.predict')
    except Exception as e:
        error_log.error('model.predict error : {} '.format(e))

    try:
        log.info("applying amos model")
        a = datetime.datetime.now()
        # 모델 특성 정보를 보유한 클래스 생성
        amos_model_class = fog_main_preprocessing(amos_model_info["final_shape_format_amos" ] , amos_model_info["sequence_len"] , amos_model_info["each_sequence_minutes"])
        amos_summary = pd.read_csv(amos_summary_path , index_col = 0)

        amos_pixel_info = pd.read_csv(external_info_path.training_amos_info_path , index_col = 0)

        total_result_vis = np.reshape(total_linear_classes , (900 , 900 ))

        temp_array =total_result_vis.copy()
        # 추론 결과 배열에서 다시 AMOS 가 위치하는 부분을 AMOS 모델로 추론 하고 다시 추론 결과 배열에 덮어 씌우기
        whole_linear_vis = apply_amos_model_independently(temp_array  , amos_model , reading_ticketNo  , amos_normed_whole_df , amos_model_class , amos_summary , amos_pixel_info, log , error_log)

        linear_classes = np.reshape(whole_linear_vis , (810000))

        total_result_vis = np.reshape(linear_classes , (900 , 900)) ################################ changed########################################
        total_result_vis = np.clip(total_result_vis, 0 , fog_character.maximum_vis_distance_clipping)# delete these lines if you want to validate the total model not the amos model
                                                                    #################################################################################

        b = datetime.datetime.now()
        dur = b - a
        log.info("applying amos model duration : {}".format(dur))

    except Exception as e:
        error_log.error('applying amos model error : {}'.format(e))

    try:
        # 데이터 클리핑
        # gk2a 이미지 배열 크기로 변환
        # 시정거리 배열 저장
        vis_array = make_vis_distance_array(linear_classes, str(ticketNo), output_path)
        log.info('make_vis_distance_array')
    except Exception as e:
        error_log.error('make_vis_distance_array error : {}'.format(e))

    try:
        # 해무 발달 단계 정보 배열 생성
        linear_final_array = classify_array(linear_classes )
        log.info('clasify_array')

    except Exception as e:
        error_log.error('classify_array : {}'.format(e))

    try:
        # 배열 크기를 (900,900) 로 변환
        image_array = reshaping_array_into_picture(linear_final_array, str(ticketNo) , output_path)
        log.info("reshaping into picture")

    except Exception as e:
        error_log.error("make_picutre error : {}".format(e))

    try:
        # 해무 발달 단계를 이미지 배열 형태로 변환
        # 이미지 배열 형태에서 안개 객체 추적
        # 객체별 픽셀 정보 생성
        # 객체별 픽셀 정보 저장
        # 10분 전 픽셀 정보 읽기
        # 이동속도 계산
        # 이동 방향 계산
        # 객체별 안개 이동 시뮬레이션 실행
        # 기지 유입 시간 정보 생성
        postprocess_params  = make_result_dataframe_from_image(image_array, str(ticketNo),vis_array, root_path, tempo_path , output_path,log ,error_log )

        postprocess_params["ticketNo"] = ticketNo
        postprocess_params["reading_ticketNo"] = reading_ticketNo

        log.info('make_result_data_frame_from_image')
        log.info("saving picture")
    except Exception as e:
        error_log.error('make_result_dataframe_from_image error : {}'.format(e))

    training_tower_info   = pd.read_csv(external_info_path.training_amos_info_path)
    all_tower_info = pd.read_csv(external_info_path.all_amos_info_path)
    try:
        # ● 탐지 결과 시정거리 배열에서 AMOS가 위치한 데이터를 샘플링
        # ● 샘플링 데이터를 사용해 주의보 경보 데이터프레임 생성
        watch_warn_df = make_watch_warn_df(all_tower_info, vis_array,ticketNo)
        log.info('make_watch_warn_df')
    except Exception as e:
        error_log.error('make_watch_warn_df error : {}'.format(e))
    try:
        # ● 탐지 결과 시정거리 배열에서 AMOS가 위치한 데이터를 샘플링
        # ● 샘플링 데이터를 사용해 주의보 경보 데이터프레임 생성
        amos_detail_df = make_amos_vis_df(all_tower_info,vis_array , ticketNo)
        valid_detail_df = make_amos_vis_df(training_tower_info , total_result_vis , ticketNo )
        watch_warn_df_params = {"watch_warn_df":watch_warn_df ,"amos_detail_df":amos_detail_df , "valid_detail_df" : valid_detail_df  }
        log.info("valid_detail_df : {}".format(valid_detail_df))

    except Exception as e:
        error_log.error("make_amos_vis_df error : {}".format(e))

    return postprocess_params , watch_warn_df_params


#
#
#
# #---------------------------------------------------------- for only debugging ---------------------------------------------------------------------
#
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
#
# def init_svc(im):
#     import fog_loading_model  ################
#
#     log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_log')
#     error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
#     model_path = im.model_path
#
#     log.info('model path ->{}'.format(model_path))
#     log.info('model path exists : {}'.format(os.path.exists(model_path)))
#
#
#     try:
#
#
#         params = fog_loading_model.main(model_path ,"platform",log, error_log)   #####################
#         log.info('model loaded')
#     except Exception as e:
#         error_log.error('model loading error :{}'.format(e))
#
#     # param = im.param_info
#     # params = {**param, **model_param}
#     return params
#
# class making_class:
#     def __init__(self , path , dicting ):
#         self.model_path = path
#         self.param_info = dicting
#
#
# dicting  =   {'model_training'               : '2',
#               'data_collecting'              : '2',
#               'total_train_start_ticket'     : '201910010000',
#               'total_train_end_ticket'       : '201910312350',
#               'amos_train_start_ticket'      : '201910010000',
#               'amos_train_end_ticket'        : '201910312350',
#               'custom_features'              : '2',
#               'seq_len_total'                : '3',
#               'each_seq_minutes_total'       : '10',
#               'conv1_total'                  : '32',
#               'conv2_total'                  : '32',
#               'dense1_total'                 : '64',
#               'dense2_total'                 : '32',
#               'dense1_amos'                  : '128',
#               'dense2_amos'                  : '64',
#               'batch_size'                   : '30',
#               'epoch'                        : '10',
#               'previous_model'               : '1',
#               'es_patience'                  : '50',
#               'weight_0_1000'                : '1',
#               'weight_1000_2000'             : '1',
#               'weight_2000_3000'             : '1',
#               'weight_3000_4000'             : '1',
#               'weight_4000_5000'             : '3',
#               'weight_5000_all'              : '1'}
#
#
# im = making_class( '/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/tempo_file2', dicting )
#
# params = init_svc(im)
#
#
# ticketNo_dic = {'reading_ticketNo': '201909060950', 'saving_ticketNo': '201909060950'}
#
# env = "platform"
# log = fog_logging.get_log_view(1, "platform", False, 'fog_ui_enfo_info')
# error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_ui_enfo_info_error')
# #
# #
# # #######################################################
# import fog_preprocess
# total_final_seq_data , amos_normed_whole_df = fog_preprocess.main(ticketNo_dic , env , params , log , error_log)
# total_final_seq_data[0]



#
#
# ###
# postprocess_params , watch_warn_df_params = fog_postprocess.main(ticketNo_dic, total_final_seq_data, amos_normed_whole_df , "platform", params , log , error_log)
# ###
# #----------------------------------------------------------#----------------------------------------------------------
#
# ticketNo_dic, total_final_seq_data, amos_normed_whole_df , env, params , log , error_log= ticketNo_dic, total_final_seq_data, amos_normed_whole_df , "platform", params , log , error_log
# def main(ticketNo_dic, total_final_seq_data, amos_normed_whole_df , env, params , log , error_log):
