import pandas as pd
import numpy as np
import imageio
from collections import Counter
import datetime

import os
import PIL
from PIL import Image
import jaydebeapi
import jpype


from sklearn.metrics import confusion_matrix , precision_score , recall_score, f1_score , accuracy_score , precision_score
import fog_config




def open_connection(host,db_name,user_name,password, port, root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,
                               driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor




import datetime

fmt = '%Y%m%d%H%M'
start_date = "201905160400"
end_date = "201905162350"
def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta

start = datetime.datetime.strptime(str(start_date), fmt)
end = datetime.datetime.strptime(str(end_date), fmt)
timelist = []
for result in perdelta(start , end , datetime.timedelta(minutes=10)):
    timelist.append(result)
timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , timelist ))

for valid_time in timelist:
    try:
        main(valid_time)
    except Exception as e:
        print(e)
        continue


def main(valid_time):


    host = fog_config.tb_host
    db_name = fog_config.tb_db_name
    user_name = fog_config.tb_user_name
    password = fog_config.tb_password
    port = fog_config.tb_port
    root_path = os.getcwd()
    conn, cursor = open_connection(host,db_name,user_name,password,port , root_path)

    length = 448
    real_img_path = "/gisangdan/kans/data-int/fog-img-fcst/gk2a_OG_data/gk2a_ami_le2_fog_ko020lc_{}.png".format(valid_time)
    valid_time_datetime = datetime.datetime.strptime(valid_time, '%Y%m%d%H%M')
    real_img = np.array(Image.open(real_img_path).crop((0,22,900,922)).resize((600, 600)))[100:548, 100:548]
    for i in range(len(real_img)):
        for j in range(len(real_img)):
            if real_img[i,j,0] == 255 and real_img[i,j,1] == 0 and real_img[i,j,2] == 0:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 0
            elif real_img[i,j,0] == 192 and real_img[i,j,1] == 0 and real_img[i,j,2] == 0:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 42.5
            elif real_img[i,j,0] == 255 and real_img[i,j,1] == 192 and real_img[i,j,2] == 0:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 85
            elif real_img[i,j,0] == 255 and real_img[i,j,1] == 255 and real_img[i,j,2] == 0:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 127.5
            elif real_img[i,j,0] == 0 and real_img[i,j,1] == 176 and real_img[i,j,2] == 80:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 170
            elif real_img[i,j,0] == 146 and real_img[i,j,1] == 208 and real_img[i,j,2] == 80:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 212.5
            elif real_img[i,j,0] == 0 and real_img[i,j,1] == 0 and real_img[i,j,2] == 255:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 255
            else:
                real_img[i,j,0] = real_img[i,j,1] = real_img[i,j,2] = 255
            real_img[i,j,3] = 255

    real_img_reshaped = np.reshape(real_img , (length * length , 4 ) )
    real_summed = list(map(lambda x: 0 if sum(x) == (255 * 4) else 1  ,real_img_reshaped ))
    for mins in range(10 , 181 , 10):
        before_datetime = valid_time_datetime- datetime.timedelta(minutes=mins )
        before =  before_datetime.strftime("%Y%m%d%H%M")
        print(before)
        fcst_image_path = "/gisangdan/kans/data-int/fog-img-fcst/{}/{}.png".format(before, valid_time)
        fcst_img = np.array(Image.open(fcst_image_path))

        fcst_img_reshaped = np.reshape(fcst_img , (length * length , 3) )


        fcst_summed = list(map(lambda x: 0 if sum(x[:3] ) == (255 * 3) else 1  , fcst_img_reshaped ))

        precision =  precision_score(real_summed , fcst_summed)
        recall = recall_score(real_summed , fcst_summed)
        f1_value  =  f1_score(real_summed , fcst_summed)
        confusion_value = confusion_matrix(real_summed , fcst_summed)
        # confusion_dic = {"TP": confusion_value[0,0], "TN" : confusion_value[1,1] , "FN":confusion_value[0,1], "FP":confusion_value[1,0] }
        # confusion_str = str(confusion_dic)

        #temp_str = '["TP":{} , "TN":{}, "FN":{} , "FP":{} ]'.format(confusion_value[0,0] ,confusion_value[1,1] , confusion_value[0,1], confusion_value[1,0])

        temp_str = '["TN":{} , "TP":{}, "FP":{} , "FN":{} ]'.format(confusion_value[0,0] ,confusion_value[1,1] , confusion_value[0,1], confusion_value[1,0])

        temp_str = temp_str.replace("[" , "{")
        final_str=  temp_str.replace("]" , "}")

        sql = '''INSERT INTO D_SEAFOG_IMG_FCST_VALIDATION (valid_time, past_pred , precision , recall , f1 , CFM) VALUES ('{}','{}',{},{},{} ,'{}');'''.format(valid_time,before, precision , recall , f1_value , final_str  )

        cursor.execute(sql)

    cursor.close()
    conn.close()



if __name__ == "__main__":
    main("201909071530")
