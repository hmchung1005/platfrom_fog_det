import sys
import json
import os
import pandas as pd
# import sqlalchemy
# from sqlalchemy import create_engine, types, select
import numpy as np
from fog_config import Config , fog_final_db_info ,fog_character, tb_info
#
import os
import fog_logging
import datetime
import jaydebeapi
import jpype
import zipfile


#글로벌 경로 설정
def set_globvar_to_path(env):

    if env == "local":
        root_path = Config.root_local_path
        tempo_path = Config.tempo_local_path
        output_path = Config.output_local_path
    elif env == "platform":
        root_path = Config.root_platform_path
        tempo_path = Config.tempo_platform_path
        output_path = Config.output_platform_path
    elif env == "dgx_share2":
        root_path = Config.root_dgx_path
        tempo_path = Config.tempo_dgx_path
        output_path = Config.output_dgx_path
    return root_path , tempo_path , output_path

# 후처리 모듈에서 얻은 상태 정보를 데이터베이스 형태에 맞게 2개로 분리
def modify_to_db_shape(df):
    #df['mv_speed'] = list(map(lambda x: x.m  if 'm' in dir(x) else np.nan , df['mv_speed']))
    #df['fog_previous_id'] = list(map(lambda x: int(x),df['fog_previous_id'] ))
    result_df = df.loc[:,["fog_current_id","fog_first_id", "lat","lon","avg_vis_distance","low_vis_distance","mv_speed","mv_direction","fog_area","area_difference" , "mid_y" , "mid_x"]]
    # dir_range  = np.arange(22.5, 360.0, 45.0)
    # result_df["mv_direction"] = result_df["mv_direction"].apply(lambda row: degree_to_8dir(row, dir_range))
    return result_df


#
# def degree_to_8dir(degree, dir_range):
#     if degree >= dir_range[0] and degree < dir_range[1]:
#         return "NE"
#     elif degree >= dir_range[1] and degree < dir_range[2]:
#         return "E"
#     elif degree >= dir_range[2] and degree < dir_range[3]:
#         return "SE"
#     elif degree >= dir_range[3] and degree < dir_range[4]:
#         return "S"
#     elif degree >= dir_range[4] and degree < dir_range[5]:
#         return "SW"
#     elif degree >= dir_range[5] and degree < dir_range[6]:
#         return "W"
#     elif degree >= dir_range[6] and degree < dir_range[7]:
#         return"NW"
#     else:
#         return "N"

# 여러 개의 기지 유입 시간 변수들을 데이터베이스 형태에 맞게 한 개의 변수로 축소
def making_final_base(result, base):
    result2 = result.loc[:,['fog_current_id','fog_first_id']]
    final_base = pd.merge(result2,base, on = 'fog_current_id')
    final_base['string_time'] = list(map(lambda x: str(x)[:-3], final_base['fog_current_id'] ))
    final_base['current_time'] =  pd.to_datetime(final_base['string_time'], format = '%Y%m%d%H%M')
    final_base['inflow_time'] =  list(map(lambda x,y: x + pd.Timedelta(seconds = y), final_base['current_time'], final_base['seconds_to_reach'] ))
    del final_base['string_time']
    del final_base ['current_time']
    #del final_base['fog_current_id']
    del final_base['seconds_to_reach']
    return final_base
# 각 기지 영역의 안개 상태를 파악해서 주의보를 발령을 결정 하는 함수
# def check_watch_warn_df(df):
#     watch_warn_df = df.copy()
#     watch_warn_df_modified = watch_warn_df.loc[watch_warn_df["pred_value"] < fog_final_db_info.watch_warn_limit , : ]
#     return watch_warn_df_modified

# 임시 저장 파일에 일정 개수 이상 파일이 생기면 가장 오래된 파일부터 일정 개수 삭제
#def clean_tempo_file(tempo_path):
#    path, dirs, files = next(os.walk(tempo_path))
#    file_count = len(files)
#    if file_count > 20:
#        dates = list(map(lambda x:int(x.split(".")[0].split("_")[2]) , files   ))
#        low_dates =   list(set(sorted(dates)[:6]))
#        deleting_json = list(map(lambda x:  "tempo_{}.json".format(str(x) ) ,low_dates))
#        for f in deleting_json:
#            os.remove(os.path.join(tempo_path,f))

# 최초안개 와 현재안개 id가 같으면 안개 이동속도, 이동방향, 확장 축소 값 강제로 -50000 값으로 변경
def processing_nan_values(result_df):
    created_fog_list = list(map(lambda x, y: int(x)==int(y), result_df['fog_current_id'],result_df['fog_first_id']))
    result_df.loc[created_fog_list,['mv_speed', 'mv_direction', 'area_difference']] = fog_final_db_info.null_data
    return result_df

# making_final_result 내부 함수 -> 데이터베이스에 where 절 sql 문 전송 후 최초 안개 id 값 출력
def find_first_fog_from_previous_fog(result_df, db_class):
    str_listing =  ",".join(list(map(lambda x : str(x),result_df['fog_previous_id'])))
    table_name = "d_fog_det_trac_result"                                             ########### for test
    where_column = "fog_current_id"
    str_columns =  "fog_first_id,fog_current_id"
    db_class.get_where_dataframe(table_name,str_columns,where_column,str_listing)
    return db_class.df


def read_modify_amos_info():
    amos_info = pd.read_csv("amos_data_tower_info_hsr_included.csv")
    amos_info = amos_info.loc[:,["tower","lat_index_900","lon_index_900"]]
    amos_info["tower"] = amos_info["tower"].astype(str)
    return amos_info

def open_connection(host,db_name,user_name,password, port, root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,
                               driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor

# 여러 개의 기지 유입 시간 변수들을 데이터베이스 형태에 맞게 한 개의 변수로 축소
def making_final_base_tibero(result, base):
    result2 = result.loc[:,['fog_current_id','fog_first_id']]
    final_base = pd.merge(result2,base, on = 'fog_current_id')
    final_base['string_time'] = list(map(lambda x: str(x)[:-3], final_base['fog_current_id'] ))
    final_base['current_time'] =  pd.to_datetime(final_base['string_time'], format = '%Y%m%d%H%M')
    final_base['inflow_time'] =  list(map(lambda x,y: x + pd.Timedelta(seconds = y), final_base['current_time'], final_base['seconds_to_reach'] ))
    final_base["inflow_time"] = final_base.inflow_time.apply(lambda x: x.strftime('%Y%m%d%H%M'))
    del final_base['string_time']
    del final_base ['current_time']
    #del final_base['fog_current_id']
    del final_base['seconds_to_reach']
    return final_base


def insert_result_tibero(final_result,ticketNo,cursor):
    print("inserting result")
    final_result["now_time"] = ticketNo
    final_result["mv_speed"] = final_result["mv_speed"].clip(0,fog_character.maximum_fog_speed)

    for index in final_result.index.tolist():
        fog_current_id = final_result.loc[index,"fog_current_id"]
        fog_first_id =  final_result.loc[index,"fog_first_id"]
        lat = final_result.loc[index,"lat"]
        lon = final_result.loc[index,"lon"]
        avg_vis_distance = final_result.loc[index,"avg_vis_distance"]
        low_vis_distance = final_result.loc[index,"low_vis_distance"]
        mv_speed = final_result.loc[index,"mv_speed"]
        mv_direction = final_result.loc[index,"mv_direction"]
        fog_area = final_result.loc[index,"fog_area"]
        area_difference = final_result.loc[index,"area_difference"]
        fog_center_x = final_result.loc[index , "mid_x"]
        fog_center_y = final_result.loc[index , "mid_y"]
        now_time  = final_result.loc[index, "now_time"]

        if fog_current_id != fog_first_id:
            params = (fog_current_id,fog_first_id,lat,lon,avg_vis_distance,low_vis_distance,mv_speed,mv_direction,fog_area,area_difference , fog_center_x , fog_center_y , now_time)
            sql = '''INSERT INTO d_fog_det_trac_result
            (table_id , fog_current_id,fog_first_id,lat,lon,avg_vis_distance,low_vis_distance,mv_speed,mv_direction,fog_area,area_difference , fog_center_x , fog_center_y , now_time )
            VALUES
            (D_FOG_DET_TRAC_RESULT_SEQ.NEXTVAL ,?,?,?,?,?,?,?,?,?,?,?,?,?);
            '''
            cursor.execute(sql, params)
        else:
            params = (fog_current_id,fog_first_id,lat,lon,avg_vis_distance,low_vis_distance,mv_speed,fog_area,area_difference , fog_center_x , fog_center_y , now_time)
            sql = '''INSERT INTO d_fog_det_trac_result
            (table_id , fog_current_id,fog_first_id,lat,lon,avg_vis_distance,low_vis_distance,mv_speed,fog_area,area_difference , fog_center_x , fog_center_y , now_time )
            VALUES
            (D_FOG_DET_TRAC_RESULT_SEQ.NEXTVAL ,?,?,?,?,?,?,?,?,?,?,?,?);
            '''
            cursor.execute(sql, params)



def insert_base_tibero(final_base,ticketNo,cursor):
    print("inserting base")
    final_base["now_time"] = ticketNo
    for index in final_base.index.tolist():
        fog_current_id = final_base.loc[index,"fog_current_id"]
        fog_first_id = final_base.loc[index,"fog_first_id"]
        target_base = final_base.loc[index,"target_base"]
        inflow_time = final_base.loc[index,"inflow_time"]
        now_time = final_base.loc[index , "now_time"]
        params = (fog_current_id,fog_first_id,target_base,inflow_time , now_time)
        sql ='''INSERT INTO d_fog_det_trac_base
        (table_id, fog_current_id,fog_first_id,target_base,inflow_time, now_time)
        VALUES
        (D_FOG_DET_TRAC_BASE_SEQ.NEXTVAL ,?,?,?,?,? );
        '''
        cursor.execute(sql , params)


# # 각 기지 영역의 안개 상태를 파악해서 주의보를 발령을 결정 하는 함수
def check_watch_warn_df_tibero(df):
    watch_warn_df = df.copy()
    watch_warn_df = watch_warn_df.loc[watch_warn_df["pred_value"] < fog_final_db_info.watch_warn_limit , : ] #
    watch_warn_df["watch"] = 0
    watch_warn_df["warning"] = 1
    watch_warn_df["now_time"] = watch_warn_df.now_time.apply(lambda x: x.strftime('%Y%m%d%H%M'))
    watch_warn_df["watch_warn_time"] = watch_warn_df.watch_warn_time.apply(lambda x: x.strftime('%Y%m%d%H%M'))

    return watch_warn_df



def insert_watch_warn_tibero(watch_warn,cursor):
    watch_warn_df = watch_warn.copy()
    print("inserting watch_warn")
    for index in watch_warn_df.index.tolist():
        base = watch_warn_df.loc[index , "base"]
        pred_value = watch_warn_df.loc[index, "pred_value"]
        model = watch_warn_df.loc[index, "model"]
        type = watch_warn_df.loc[index, "type"]
        watch = watch_warn_df.loc[index, "watch"]
        warning = watch_warn_df.loc[index, "warning"]
        now_time = watch_warn_df.loc[index, "now_time"]
        watch_warn_time = watch_warn_df.loc[index, "watch_warn_time"]
        sql = ''' INSERT INTO watch_warn
        (table_id , target_base, pred_value, model, type, watch, warning, now_time, watch_warn_time)
        VALUES
        (WATCH_WARN_SEQ.NEXTVAL ,{},{},'{}','{}',{},{},TO_TIMESTAMP('{}','YYYYMMDDHH24MI'),TO_TIMESTAMP('{}','YYYYMMDDHH24MI'));
        '''.format(base,pred_value,model,type,watch,warning,now_time,watch_warn_time)
        cursor.execute(sql)


def whole_validation_process(fog_det_amos_df , ticketNo, cursor, log , level):
    amos_result = get_amos_data(ticketNo , cursor, log)
    predicted = fog_det_amos_df.loc[:, ["base" , "vis_distance"]]
    validataion = merge_and_validate(predicted , amos_result,ticketNo ,cursor , log, level)
    return validataion

def insert_validation_tibero(validation , cursor):
    now_time = validation.now_time
    GD = validation.GD
    MI = validation.MI
    FA = validation.FA
    CR = validation.CR
    POD = validation.POD
    POFD  = validation.POFD
    FART = validation.FART
    TS = validation.TS
    MAE = validation.MAE
    kinds = validation.kinds
    params =(now_time,GD,MI,FA,CR,POD,POFD,FART,TS,MAE , kinds)
    sql = '''
    INSERT INTO d_fog_det_trac_valid
    (table_id, valid_time, GD,MI,FA,CR,POD,POFD,FART,TS,MAE ,KINDS)
    VALUES
    (D_FOG_DET_TRAC_VALID_SEQ.NEXTVAL ,?,?,?,?,?,?,?,?,?,?,?);
    '''
    cursor.execute(sql , params)



def get_amos_data(ticketNo, cursor, log):

    param = (ticketNo,)
    data_sql = "SELECT K2 , SU7 FROM D_AMOS_DETAIL where K1 = ? and K2 != '153';"
    data_sql_153 = "SELECT K2 , SU8 FROM D_AMOS_DETAIL where K1 = ? and K2 = '153';"
    cursor.execute(data_sql , param)
    data = cursor.fetchall()
    df = pd.DataFrame(data, columns = ["tower" , "real_vis_distance"] )

    cursor.execute(data_sql_153 , param)
    data_153 = cursor.fetchall()
    df_153 = pd.DataFrame(data_153 , columns = ["tower" , "real_vis_distance"])

    df = pd.concat([df , df_153] , ignore_index = True)

    for index in df.index.tolist():
        try:
            df.loc[index , "real_vis_distance"] = float(df.loc[index , "real_vis_distance"])
        except:
            log.info("no distance fount at AMOS BASE {}".format(str(df.loc[index, "tower"])))
            df.loc[index , "real_vis_distance"] = np.nan


    df = df.dropna()
    df = df.rename(columns = {'tower':'base'})
    df['base'] = df['base'].astype(int)
    df["real_vis_distance"] =df["real_vis_distance"].astype(int)
    return df


def get_past_valid(ticketNo , cursor):
    current_time = datetime.datetime.strptime(ticketNo,'%Y%m%d%H%M')
    past_time = current_time  - datetime.timedelta(days = fog_final_db_info.past_valid_time_limit )
    future_time = current_time +  datetime.timedelta(days = fog_final_db_info.past_valid_time_limit )
    past_ticket = datetime.datetime.strftime(past_time , format ='%Y%m%d%H%M' )
    future_ticket= datetime.datetime.strftime(future_time , format = '%Y%m%d%H%M' )
    param = (past_ticket,future_ticket)
    sql = "select GD , MI , FA , CR from d_fog_det_trac_valid where valid_time > ? and valid_time < ?;"
    cursor.execute(sql,param)
    db_data = cursor.fetchall()
    db_df = pd.DataFrame(db_data , columns = ["GD" , "MI", "FA" , "CR"] )
    db_df = db_df.dropna()
    db_df =  db_df[(db_df.iloc[:, :] >= 0).all(axis=1)]
    past_GD = sum(db_df.GD)
    past_MI = sum(db_df.MI)
    past_FA = sum(db_df.FA)
    past_CR = sum(db_df.CR)
    return past_GD , past_MI , past_FA , past_CR



def merge_and_validate(amos_info_modified , amos_result, ticketNo , cursor , log , level):#fog_character.fog_binary_level

    level_to_level_num = {"shallow_fog" :  fog_character.fog_binary_level ,
                          "normal_fog" : fog_character.normal_fog_high_end,
                           "thick_fog" : fog_character.deep_fog_high_end}
    level_num = level_to_level_num[level]

    merged = amos_info_modified.merge(amos_result, on = "base")
    merged  = merged.dropna()
    merged  = merged.drop_duplicates()

    max_vis = fog_character.maximum_vis_distance_clipping
    merged = merged.loc[merged["real_vis_distance"] > 0 , :]
    log.info("merged results")
    log.info(merged)
    if merged.shape[0] > 0:
        merged["real_vis_distance"] = list(map(lambda x: max_vis if x > max_vis else x , merged["real_vis_distance"]))
        MAE = sum(abs(merged["vis_distance"] - merged["real_vis_distance"])) / merged.shape[0]

        merged["vis_distance"] = list(map(lambda x: 1 if x < level_num else 0 , merged["vis_distance"] ))
        merged["real_vis_distance"] = list(map(lambda x: 1 if x < level_num  else 0 , merged["real_vis_distance"]))
        GD = len(merged.loc[(merged["vis_distance"] ==1) & (merged["real_vis_distance"] == 1) ,:  ])
        MI = len(merged.loc[(merged["vis_distance"] ==0) & (merged["real_vis_distance"] == 1) ,:  ])
        FA = len(merged.loc[(merged["vis_distance"] ==1) & (merged["real_vis_distance"] == 0) ,:  ])
        CR = len(merged.loc[(merged["vis_distance"] ==0) & (merged["real_vis_distance"] == 0) ,:  ])
    else:
        log.info("there is no amos data --> cannot validate")
        GD , MI , FA , CR , MAE = 0,0,0,0, fog_final_db_info.null_data

    try:
        try:
            POD = GD / (GD + MI)
            POFD = FA / (FA + CR)
            FART = FA / (GD + FA)
            TS = GD / (GD + FA + MI)
        except Exception as e:
            print("validation error : {}".format(e))
            print("not enough data to validate ---> validating whole month")
            sumGD , sumMI , sumFA ,sumCR = get_past_valid(ticketNo , cursor)
            log.info("getting past valid")
            POD = sumGD / (sumGD + sumMI)
            POFD = sumFA / (sumFA + sumCR)
            FART = sumFA / (sumGD +sumFA)
            TS = sumGD / (sumGD + sumFA + sumMI)

    except Exception as e:
        print("validation unknown error : {}".format(e))
        POD = fog_final_db_info.null_data
        POFD = fog_final_db_info.null_data
        FART = fog_final_db_info.null_data
        TS = fog_final_db_info.null_data
        MAE = fog_final_db_info.null_data

    data = pd.Series({"GD":GD, "MI":MI, "FA":FA, "CR":CR,"POD":POD,"POFD":POFD,"FART":FART,"TS":TS ,"MAE":MAE , "now_time":ticketNo  , "kinds":level } )
    return data



def insert_det_amos_df_tibero(fog_det_amos_df , cursor):
    max_vis = fog_character.maximum_vis_distance_clipping
    fog_det_amos_df["vis_distance"] = list(map(lambda x: max_vis if x >= max_vis else x , fog_det_amos_df["vis_distance"]  ))
    for index in fog_det_amos_df.index.tolist():
        now_time = fog_det_amos_df.loc[index , "now_time"]
        base = fog_det_amos_df.loc[index , "base"]
        lat = fog_det_amos_df.loc[index, "lat"]
        lon = fog_det_amos_df.loc[index , "lon"]
        vis_distance = fog_det_amos_df.loc[index , "vis_distance"]
        params = (now_time,base , lat, lon, vis_distance)
        sql =  '''INSERT INTO d_fog_det_trac_amos_detail
        (table_id , valid_time , base , lat , lon , vis_distance)
        VALUES
        (D_FOG_DET_TRAC_AMOS_DETAIL_SEQ.NEXTVAL , ?,?,?,?,?);
        '''
        cursor.execute(sql , params)

def insert_det_pred_df_tibero(fog_det_amos_df , cursor):
    max_vis = fog_character.maximum_vis_distance_clipping
    fog_det_amos_df["vis_distance"] = list(map(lambda x: max_vis if x >= max_vis else x , fog_det_amos_df["vis_distance"]  ))
    for index in fog_det_amos_df.index.tolist():
        now_time = fog_det_amos_df.loc[index , "now_time"]
        base = fog_det_amos_df.loc[index , "base"]
        vis_distance = fog_det_amos_df.loc[index , "vis_distance"]
        params = (now_time, base , vis_distance)
        sql =  '''INSERT INTO D_FOG_DET_TRAC_PRED
        (table_id , now_time , target_base , pred_value)
        VALUES
        (D_FOG_DET_TRAC_PRED_SEQ.NEXTVAL,?,?,?);
        '''
        cursor.execute(sql , params)



def get_whole_db_df(table_name , cursor ):
    col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='{}';".format(table_name)
    cursor.execute(col_sql)
    col = cursor.fetchall()
    col = list(map(lambda x: x[0] , col))
    cursor.execute("select * from {};".format(table_name))
    data = cursor.fetchall()
    final_df = pd.DataFrame(data , columns = col)
    return final_df
# env = "platform"
# root_path , tempo_path , output_path = set_globvar_to_path(env)
#
# host = fog_config.tb_host
# db_name = fog_config.tb_db_name
# user_name = fog_config.tb_user_name
# password = fog_config.tb_password
# port = fog_config.tb_port
#
# conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)
#
# get_whole_db_df("D_FOG_DET_TRAC_BASE" , cursor )
#
# base = watch_warn_df.loc[index , "base"]
# base = 107
# pred_value = watch_warn_df.loc[index, "pred_value"]
# pred_value = 0.8
# model = watch_warn_df.loc[index, "model"]
# model =  "rain-det-trac"
# type = watch_warn_df.loc[index, "type"]
# type = "rain"
# watch = watch_warn_df.loc[index, "watch"]
# watch = 1
# warning = watch_warn_df.loc[index, "warning"]
# warning = 0
# now_time = watch_warn_df.loc[index, "now_time"]
# now_time = "201909051400"
# watch_warn_time = watch_warn_df.loc[index, "watch_warn_time"]
# watch_warn_time = "201909051400"
#
# sql = ''' INSERT INTO watch_warn
# (target_base, pred_value, model, type, watch, warning, now_time, watch_warn_time)
# VALUES
# ({},{},'{}','{}',{},{},TO_TIMESTAMP('{}','YYYYMMDDHH24MI'),TO_TIMESTAMP('{}','YYYYMMDDHH24MI'));
# '''.format(base,pred_value,model,type,watch,warning,now_time,watch_warn_time)
# sql
#
#
# cursor.execute(sql)



def processing_after_call_handling(ticket_tuple):
    ticketNo , log , error_log = ticket_tuple
    try:
        import fog_det_handling_img
        log.info('fog_handling start')
        fog_det_handling_img.main(ticketNo)
        log.info('fog_handling end')
    except Exception as e:
        error_log.error("handling image error : {}".format(e))



def call_threadfunc_handling(ticketNo):
    log = fog_logging.get_log_view(1, "platform", False, 'image_handling_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'image_handling_error')
    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)
    try:
        ticket_tuple = (ticketNo, log, error_log)
        future = pool.submit(processing_after_call_handling,(ticket_tuple))
    except Exception as e:
        error_log.error('image_handling_call error : {}'.format(e))
    log.info('call_threadfunc end')

# -------------------------------------------------------- for -debugging only ------------------------------------------------------------------------------------
# env = "platform"
# root_path , tempo_path , output_path = set_globvar_to_path(env)
# host = tb_info.tb_host
# db_name = tb_info.tb_db_name
# user_name = tb_info.tb_user_name
# password = tb_info.tb_password
# port = tb_info.tb_port
# conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)
# log = fog_logging.get_log_view(1, "platform", False, 'fog_ui_enfo_info')
# error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_ui_enfo_info_error')
# ticketNo = "201909051500"
# amos_result = get_amos_data(ticketNo , cursor, log)


# --
# cursor.execute("select * from d_fog_det_train_gk2a;")
# data = cursor.fetchall()
# col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='D_FOG_DET_TRAIN_GK2A';"
# cursor.execute(col_sql)
# cols = cursor.fetchall()
# cols = list(map(lambda x: x[0] , cols))
#
# whole_df = pd.DataFrame(data , columns = cols)
# whole_df.columns
#
# del whole_df["TABLE_ID"]
# whole_df2 = whole_df.drop_duplicates()
# whole_df3 = whole_df2.drop_duplicates(subset='TIME_BASE', keep="last")
# whole_df3.to_csv("whole_df3.csv")
#
# whole_df3 = pd.read_csv("whole_df3.csv", index_col = 0)
#
#
# zenith_df = whole_df3.loc[:,["TIME" , "TOWER" , "TARGET" , "ZENITH" , "TIME_BASE"] ]
#
#
# for i in ["TIME" , "TOWER" , "TARGET" , "ZENITH" , "TIME_BASE"]:
#     cols.remove(i)
# cols.remove("TABLE_ID")
# cols.append("TIME")
#
# gk2a_df = whole_df3.loc[: ,cols ]
#
# gk2a_df["real_time"] = pd.to_datetime(gk2a_df['TIME'], format="%Y%m%d%H%M")
#
# gk2a_df["real_kst_time"] =  gk2a_df["real_time"] - datetime.timedelta(hours = 9)
#
# gk2a_df["final_time"] = gk2a_df["real_kst_time"].apply(lambda x:  x.strftime("%Y%m%d%H%M") )
# gk2a_df
#
#
# del gk2a_df["TIME"]
# del gk2a_df["real_time"]
# del gk2a_df["real_kst_time"]
#
# gk2a_df2 = gk2a_df.rename(columns = {"final_time":"TIME" })
# gk2a_df2["TIME"] = gk2a_df2["TIME"].astype("int")
#
# merged_df = zenith_df.merge(gk2a_df2 , on = "TIME" , how ="inner")
#
# merged_df.to_csv("gk2a_train_dataset.csv")
#
#
#
# gk2a_train_dataset = merged_df.copy()
#
# --



#gk2a_train_dataset = pd.read_csv("gk2a_train_dataset.csv" , index_col = 0)



# cursor.execute("select * from d_fog_det_train_hsr;")
# data = cursor.fetchall()
#
# col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='D_FOG_DET_TRAIN_HSR';"
# cursor.execute(col_sql)
# cols = cursor.fetchall()
# cols = list(map(lambda x: x[0] , cols))
#
# df = pd.DataFrame(data , columns = cols)
# del df["TABLE_ID"]
#
#
# df2 =  df.drop_duplicates(subset='TIME_BASE', keep="last")
#
# df2.to_csv("hsr_train_dataset.csv")
#
#
#


#----------------------------------------------------------------------------------------------------------------------------------------------------------

def main(postprocess_params , watch_warn_df_params, env, log , error_log):

    root_path , tempo_path , output_path = set_globvar_to_path(env)

    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port

    log.info("setting done")

    try:
        conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)
        log.info("open_connection")
    except Exception as e:
        error_log.error("open_connection : {}".format(e))
    log.info("setting done")

    try:
        final_result = postprocess_params["result_db"]
        base_result = postprocess_params["base_db"]
        ticketNo = postprocess_params["ticketNo"]
        reading_ticketNo = postprocess_params["reading_ticketNo"]
        watch_warn_df = watch_warn_df_params["watch_warn_df"]
        fog_det_amos_df = watch_warn_df_params["amos_detail_df"]
        valid_det_amos_df = watch_warn_df_params["valid_detail_df"]
        # 해무 추적 결과 정보 데이터 생성
        final_result = modify_to_db_shape(final_result)
    except Exception as e:
        error_log.error("modify_to_db_shape : {}".format(e))

    try:
        # 기지 유입 정보 데이터 생성
        final_base = making_final_base_tibero(final_result,base_result)
    except Exception as e:
        error_log.error("making_final_base_tibero : {}".format(e))

    try:
        # 해무 추적 결과 정보 데이터베이스에 삽입
        insert_result_tibero(final_result, ticketNo,cursor)
        log.info("insert_result_tibero")
    except Exception as e:
        error_log.error("insert_result_tibero : {}".format(e))

    try:
        # 기지 유입 정보 데이터베이스에 삽입
        insert_base_tibero(final_base,ticketNo ,cursor)
        log.info("insert_base_tibero")
    except Exception as e:
        error_log.error("insert_base_tibero : {}".format(e))

    ####
    try:
        # 주의보 경보 데이터 생성
        watch_warn_df= check_watch_warn_df_tibero(watch_warn_df)
        log.info("check_watch_warn_df_tibero")
    except Exception as e:
        error_log.error("chekc_watch_warn_df_tibero : {}".format(e))

    try:
        if watch_warn_df.shape[0] > 0:
            # 주의보 경보 정보 데이터베이스에 삽입
            insert_watch_warn_tibero(watch_warn_df,cursor)
            log.info("inset_watch_warn_tibero")
        else:
            log.info("no warning signs"
    except Exception as e:
        error_log.error("insert_watch_warn_tibero : {}".format(e))
    ####

    # try:
    #     insert_det_amos_df_tibero(fog_det_amos_df , cursor)
    #     log.info("insert_det_amos_df_tibero")
    # except Exception as e:
    #     error_log.error("insert_det_amos_df_tibero".format(e) )


    try:
        # 기지 및 관할지점 결과 정보 데이터베이스에 삽입
        insert_det_pred_df_tibero(fog_det_amos_df , cursor)
        log.info("insert_det_pred_tibero")
    except Exception as e:
        error_log.error("insert_det_pred_tibero".format(e) )
    try:
        level_list = ["shallow_fog" , "normal_fog" , "thick_fog"]
        for level in level_list:
            # 발달 단계별로 검증
            validataion = whole_validation_process(valid_det_amos_df,reading_ticketNo , cursor,log , level) #UT-SFR-004-04-01
            # 검증 결과 데이터베이스에 삽입
            insert_validation_tibero(validataion, cursor)

        log.info("whole_validation_process")
    except Exception as e:
        error_log.error("whole_validation_process error : {}".format(e))

    cursor.close()
    conn.close()

    log.info("whole process finished")

    try:
        #call_threadfunc_handling(ticketNo)
        log.info("handling image threaded")
        import fog_det_handling_img
        log.info('fog_handling start')
        # 데이터베이스 와 추론 결과 이미지를 활용해서 핸들링 이미지 생성
        fog_det_handling_img.main(ticketNo)
        log.info('fog_handling end')
    except Exception as e:
        error_log.error("handling image thread call error : {}".format(e))








#
#
#
# if __name__ == '__main__':
#
#
#     env = "platform"
#     root_path , tempo_path , output_path = set_globvar_to_path(env)
#     host = tb_info.tb_host
#     db_name = tb_info.tb_db_name
#     user_name = tb_info.tb_user_name
#     password = tb_info.tb_password
#     port = tb_info.tb_port
#     conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)
#
#     cursor.execute("select * from d_fog_det_train_gk2a;")
#     data = cursor.fetchall()
#     col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='D_FOG_DET_TRAIN_GK2A';"
#     cursor.execute(col_sql)
#     cols = cursor.fetchall()
#     cols = list(map(lambda x: x[0] , cols))
#
#     whole_df = pd.DataFrame(data , columns = cols)
#     del whole_df["TABLE_ID"]
#     whole_df2 = whole_df.drop_duplicates()
#     whole_df3 = whole_df2.drop_duplicates(subset='TIME_BASE', keep="last")
#
#
#     # env = "platform"
#     # root_path , tempo_path , output_path = set_globvar_to_path(env)
#     # host = tb_info.tb_host
#     # db_name = tb_info.tb_db_name
#     # user_name = tb_info.tb_user_name
#     # password = tb_info.tb_password
#     # port = tb_info.tb_port
#     # conn, cursor = open_connection(host,db_name,user_name,password,port,root_path)
#     #
#     # col_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='D_FOG_DET_TRAIN_GK2A';"
#     # cursor.execute(col_sql)
#
#
#     # cols = cursor.fetchall()
#     # cols = list(map(lambda x: x[0] , cols))
#     conn.close()
#     cursor.close()
#
#
#
#     # whole_df3 = pd.read_csv("whole_df_final.csv", index_col = 0)
#     #
#     # whole_df3 = whole_df3.drop_duplicates(subset='TIME_BASE', keep="last")
#
#     whole_df3["TOWER"] = list(map(lambda x:  int(x.split("_")[1]) ,  whole_df3["TIME_BASE"]))
#     whole_df3["TIME"] = list(map(lambda x:  int(x.split("_")[0]) ,  whole_df3["TIME_BASE"]))
#     #whole_df3 = whole_df3.iloc[:10000 , :]
#
#
#     zenith_df = whole_df3.loc[:,["TIME" , "TOWER" , "TARGET" , "ZENITH" , "TIME_BASE"] ]
#
#
#     for i in ["TIME" , "TOWER" , "TARGET" , "ZENITH" , "TIME_BASE"]:
#         cols.remove(i)
#
#
#     cols.remove("TABLE_ID")
#     cols.append("TIME")
#     cols.append("TOWER")
#
#
#     gk2a_df = whole_df3.loc[: ,cols ]
#
#     gk2a_df["real_time"] = pd.to_datetime(gk2a_df['TIME'], format="%Y%m%d%H%M")
#
#     gk2a_df["real_kst_time"] =  gk2a_df["real_time"] +  datetime.timedelta(hours = 9)
#
#     gk2a_df["final_time"] = gk2a_df["real_kst_time"].apply(lambda x:  x.strftime("%Y%m%d%H%M") )
#
#
#     del gk2a_df["TIME"]
#     del gk2a_df["real_time"]
#     del gk2a_df["real_kst_time"]
#
#     gk2a_df2 = gk2a_df.rename(columns = {"final_time":"TIME" })
#
#     gk2a_df2["TIME"] = gk2a_df2["TIME"].astype("int")
#     gk2a_df2["TOWER"] = gk2a_df2["TOWER"].astype("int")
#
#     gk2a_df2["TIME_BASE"] = list(map(lambda x, y: str(x)  +  "_" + str(y) , gk2a_df2["TIME"] , gk2a_df2["TOWER"]  ))
#
#     del zenith_df["TIME"]
#     del zenith_df["TOWER"]
#
#     merged_df2 = gk2a_df2.merge(zenith_df , on = "TIME_BASE" , how ="inner")
#
#     merged_df2.to_csv("gk2a_train_dataset_final_final.csv")
#

#
#
#
#
#
# postprocess_params , watch_warn_df_params = fog_postprocess.main(ticketNo_dic, total_final_seq_data, amos_normed_whole_df , "platform", params , log , error_log)
# fog_database.main(postprocess_params , watch_warn_df_params, "platform" , log, error_log )
#
# postprocess_params , watch_warn_df_params, env, log , error_log = postprocess_params , watch_warn_df_params, "platform" , log, error_log
#
# def main(postprocess_params , watch_warn_df_params, env, log , error_log):
