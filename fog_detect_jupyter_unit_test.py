from finding_index import get_gk2a_index , get_hsr_index
import fog_logging

########### setting ################
env = "platform"
log = fog_logging.get_log_view(1, env, False, 'jupyter_unit_test')
error_log = fog_logging.get_log_view(1, env, True, 'jupyter_unit_test')
##################################   UT_SFR-004-02-01#####################################
amos_gk2a_index = get_gk2a_index(env)
amos_gk2a_index

#################################  UT-SFR-004-02-02#########################################
amos_gk2a_hsr_index = get_hsr_index(amos_gk2a_index)
amos_gk2a_hsr_index
##################################################################################


#@@@

# import sys, importlib
# importlib.reload(sys.modules['fog_create_data'])

from fog_create_data import unit_004_02_03, unit_004_02_04 , unit_004_02_05
train_start_ticket = "201909140000"
train_end_ticket = "201909140020"
#@@@

#################################  UT-SFR-004-02-03#########################################
target_df , amos_info, conn, cursor, gk2a_inserting_columns = unit_004_02_03(train_start_ticket , train_end_ticket , env , log ,error_log)
#################################  UT-SFR-004-02-04#########################################
target_df , timelist , client , client_backup , amos_info=unit_004_02_04(target_df , amos_info , train_start_ticket, train_end_ticket ,log ,error_log, conn,cursor ,gk2a_inserting_columns )
#################################  UT-SFR-004-02-05#########################################
unit_004_02_05(timelist, client, client_backup , amos_info , log ,conn, cursor)


#@@@
import os
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'


model_path = "/gisangdan/kans/ai/model-save/fog-det-trac/"
from fog_create_model import unit_test_004_02_06, unit_test_004_02_07
#@@@

#################################  UT-SFR-004-02-06#########################################
merged_data = unit_test_004_02_06(train_start_ticket ,train_end_ticket , model_path, env , log , error_log)
merged_data
#################################  UT-SFR-004-02-07#########################################
model_name_path = unit_test_004_02_07(merged_data , log , error_log, model_path)
model_name_path


############## cut the memoery #########################3
# import fog_logging
# env = "platform"
# log = fog_logging.get_log_view(1, "platform", False, 'jupyter_unit_test')
# error_log = fog_logging.get_log_view(1, "platform", True, 'jupyter_unit_test')
# import os
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# model_path = os.getcwd()
############################################################


#@@@
from fog_preprocess import unit_004_03_01 , unit_004_03_02 , unit_004_03_03 , unit_004_03_04
import tensorflow as tf
ticketNo = "201909140000"
summary_path = "/gisangdan/kans/ai/model-save/fog-det-trac/transformed_binaryoversampling_category_logit/feature_summary.csv"
saved_model = tf.keras.models.load_model("/gisangdan/kans/ai/model-save/fog-det-trac/transformed_binaryoversampling_category_logit")
params = {"saved_model": saved_model,"summary_path":summary_path }
#@@@
#################################  UT-SFR-004-03-01#########################################
final_array , client, input_columns_order, root_path, feature_summary = unit_004_03_01(ticketNo, env ,params["summary_path"] , log , error_log)
#################################  UT-SFR-004-03-02#########################################
final_array = unit_004_03_02(final_array , log , error_log , input_columns_order ,client , root_path, ticketNo)
#################################  UT-SFR-004-03-03#########################################
final_array = unit_004_03_03(final_array , ticketNo, input_columns_order , root_path)
#################################  UT-SFR-004-03-04#########################################
detect_df = unit_004_03_04(final_array , ticketNo , input_columns_order , root_path, log , error_log, feature_summary)


# import sys, importlib
# importlib.reload(sys.modules['fog_postprocess'])

#@@@
from fog_postprocess import unit_004_03_05 , unit_004_03_06 , unit_004_03_07, unit_004_03_08 , unit_004_03_09 , unit_004_03_10, unit_004_03_11
#@@@
#################################  UT-SFR-004-03-05#########################################
model_path , output_path , root_path , tempo_path ,detect_df , detect_ds, model = unit_004_03_05(ticketNo, detect_df, env, params , log , error_log)
#################################  UT-SFR-004-03-06#########################################
linear_classes = unit_004_03_06(model, log, error_log, detect_ds)
#################################  UT-SFR-004-03-07#########################################
vis_array = unit_004_03_07(linear_classes,ticketNo , output_path, log, error_log)
#################################  UT-SFR-004-03-08#########################################
linear_final_array = unit_004_03_08(linear_classes , log , error_log)
#################################  UT-SFR-004-03-09#########################################
image_array = unit_004_03_09(linear_final_array , ticketNo , output_path, log ,error_log)
#################################  UT-SFR-004-03-10#########################################
final_df , tower_info = unit_004_03_10(image_array , ticketNo, vis_array ,root_path , tempo_path, log, error_log)
#################################  UT-SFR-004-03-11#########################################
final_df , watch_warn_df_list = unit_004_03_11(tower_info ,vis_array , ticketNo, log ,error_log,final_df)



#@@@
# import sys, importlib
# importlib.reload(sys.modules['fog_database'])
from fog_database import unit_004_03_12, unit_015_03_01
#@@@
#################################  UT-SFR-004-03-12#########################################
fog_det_amos_df, cursor, conn = unit_004_03_12(final_df ,watch_warn_df_list, env, log , error_log)
#################################  UT-SFR-015-03-01#########################################
unit_015_03_01(fog_det_amos_df ,ticketNo, cursor, conn , log ,error_log)
