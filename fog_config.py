#!/usr/bin/env python

import os


######################################### customizing starts ######################################################


################## customizing features and dimensions starts ##############################
class model_features_customizing(object):
    ##### customizing all pixel model ####



    # total_model_features =[['sw038_Radiance', 'sw038_Effective', 'sw038_Brightness'],
    #                             ['ir087_Radiance', 'ir087_Effective', 'ir087_Brightness'],
    #                             ['ir096_Radiance', 'ir096_Effective', 'ir096_Brightness'],
    #                             ['ir105_Radiance', 'ir105_Effective', 'ir105_Brightness'],
    #                             ['ir112_Radiance', 'ir112_Effective', 'ir112_Brightness'],
    #                             ['ir123_Radiance', 'ir123_Effective', 'ir123_Brightness'],
    #                             ['ir133_Radiance', 'ir133_Effective', 'ir133_Brightness'],
    #                             ['nr013_Radiance', 'nr013_Radiance:nr013_Albedo','nr013_Albedo'],
    #                             ['nr016_Radiance', 'nr016_Radiance:nr016_Albedo','nr016_Albedo'],
    #                             #['vi004_Radiance', 'vi004_Radiance:vi004_Albedo','vi004_Albedo'],
    #                             #['vi005_Radiance', 'vi005_Radiance:vi005_Albedo','vi005_Albedo'],
    #                             #['vi006_Radiance', 'vi006_Radiance:vi006_Albedo','vi006_Albedo'],
    #                             #['vi008_Radiance', 'vi008_Radiance:vi008_Albedo','vi008_Albedo'],
    #                             ['wv063_Radiance', 'wv063_Effective', 'wv063_Brightness'],
    #                             ['wv069_Radiance', 'wv069_Effective', 'wv069_Brightness'],
    #                             ['wv073_Radiance', 'wv073_Effective', 'wv073_Brightness'],
    #                             ['zenith' ,           'zenith' ,          'zenith'],
    #                             ['HSR_value' ,      'HSR_value' ,        'HSR_value']]

    total_model_features =    [['ir105_Radiance', 'ir105_Brightness'],
                               ['sw038_Radiance', 'sw038_Brightness'],
                               ['ir133_Radiance', 'ir133_Brightness'],
                               ['nr013_Albedo',   'nr013_Albedo'],
                               ['nr016_Albedo',   'nr016_Albedo'],
                               ['vi005_Albedo',   'vi005_Albedo'],
                               ['vi006_Albedo',   'vi006_Albedo'],
                               ['zenith',         'zenith'],
                               ['HSR_value',      'HSR_value']]



                           # [['sw038_Radiance', 'sw038_Brightness'],
                           #  ['ir087_Radiance', 'ir087_Brightness'],
                           #  ['ir096_Radiance', 'ir096_Brightness'],
                           #  ['ir105_Radiance', 'ir105_Brightness'],
                           #  ['ir112_Radiance', 'ir112_Brightness'],
                           #  ['ir123_Radiance', 'ir123_Brightness'],
                           #  ['ir133_Radiance', 'ir133_Brightness'],
                           #  ['nr013_Albedo',   'nr013_Albedo'],
                           #  ['nr016_Albedo',   'nr016_Albedo'],
                           #  #['vi004_Radiance', 'vi004_Radiance:vi004_Albedo','vi004_Albedo'],
                           #  #['vi005_Radiance', 'vi005_Radiance:vi005_Albedo','vi005_Albedo'],
                           #  #['vi006_Radiance', 'vi006_Radiance:vi006_Albedo','vi006_Albedo'],
                           #  #['vi008_Radiance', 'vi008_Radiance:vi008_Albedo','vi008_Albedo'],
                           #  ['wv063_Radiance',  'wv063_Brightness'],
                           #  ['wv069_Radiance',  'wv069_Brightness'],
                           #  ['wv073_Radiance',  'wv073_Brightness'],
                           #  ['zenith' ,          'zenith'],
                           #  ['HSR_value' ,       'HSR_value']]
                           #




    #### customizing amos pixel model ####
    # if you wanna add features, please use names that were named in class amos_model_features_labeling
    amos_model_features =[    'ir087_Radiance',       #'ir087_Effective',
                              'ir087_Brightness',

                              'ir096_Radiance',
                              #'ir096_Effective',
                              'ir096_Brightness',

                              'ir105_Radiance',
                              #'ir105_Effective',
                              'ir105_Brightness',

                              'ir112_Radiance',
                              #'ir112_Effective',
                              'ir112_Brightness',

                              'ir123_Radiance',
                              #'ir123_Effective',
                              'ir123_Brightness',

                              'ir133_Radiance',
                              #'ir133_Effective',
                              'ir133_Brightness',

                              #'nr013_Radiance',
                              'nr013_Albedo',

                              #'nr016_Radiance',
                              'nr016_Albedo',

                              'sw038_Radiance',
                              #'sw038_Effective',
                              'sw038_Brightness',

                              #'vi004_Radiance',
                              #'vi004_Albedo',

                              #'vi005_Radiance',
                              #'vi005_Albedo',

                              #'vi006_Radiance',
                              #'vi006_Albedo',

                              #'vi008_Radiance',
                              #'vi008_Albedo',

                              'wv063_Radiance',
                              #'wv063_Effective',
                              'wv063_Brightness',

                              'wv069_Radiance',
                              #'wv069_Effective',
                              'wv069_Brightness',

                              'wv073_Radiance',
                              #'wv073_Effective',
                              'wv073_Brightness',

                              'zenith',
                              'wind_dir',
                              'wind_speed',
                              'temperature',
                              'humid',
                              'land_pressure',
                              'sea_pressure',
                              #'rain_hour',
                              'HSR_value' ,
                              'vis_distance']



    #total_sequence_len = 3
    #total_each_sequence_minutes = 10

    amos_sequence_len = 1
    amos_each_sequence_minutes = 10


class amos_model_features_labeling(object):
    amos_features_naming  = {"K1":"time" ,
                            "K2":"tower" ,
                            "SW1":"wind_dir" ,
                            "SW2":"wind_speed" ,
                            "SU1":"temperature" ,
                            "SU3" : "humid" ,
                            "SU9": "land_pressure" ,
                            "SU11":"sea_pressure" ,
                            "SU13":"rain_hour" ,
                            "SU7":"vis_distance" }

    apply_amos_153_bool = True
    amos_153_features_naming = {"K1" :"time",
                                "K2" : "tower",
                                "SW1" : "wind_dir",
                                "SW2":"wind_speed" ,
                                "SU1":"temperature" ,
                                "SU3" : "humid" ,
                                "SU12": "land_pressure" ,
                                "SU14":"sea_pressure" ,
                                "SU16":"rain_hour" ,
                                "SU8":"vis_distance" }


class previous_model_path(object):
    previous_model_path =   "/gisangdan/kans/ai/model-save/fog-det-trac"





################## customizing features and dimensions ends ##############################
##########################################################################################

class fog_coloring(object):
    deep_fog_rgb = [255,0,0,255]
    normal_fog_rgb = [255,69,0,255]
    weak_fog_rgb = [255,255,0,255]

class fog_character(object):
    maximum_prediction_time = 10800
    connectivity = 8
    coord_count = 20
    intersection_ratio  = 0.4

    deep_fog_high_end = 835
    normal_fog_high_end = 1670
    fog_binary_level = 5010
    maximum_vis_distance_clipping = 5200

    maximum_fog_speed = 80

    vis_distance_digits_number = 3

class apply_amos_limit(object):
    y_axis_limit = 3
    x_axis_limit = 3
    tolerant_amos_feature_list = ["rain_hour"]


class fog_model_training_conf(object):
    train_percent = 0.9
    test_percent = 0.5


class fog_final_db_info(object):
    watch_warn_limit = 1609
    past_valid_time_limit = 30  ## 날짜
    null_data = -50000

class fog_img_making(object):
    expanding_arrow_color = (100, 0, 190 , 255)
    reduction_arraw_color = (0, 255 , 0 , 255)
    same_arrw_color = (50 , 160 , 50 , 255)
    ### time image at top ###
    time_img_shape=  (30, 900 , 4)
    w_color_time        = (255, 255, 255, 255)
    thickness_time = 2
    fontScale_time = 0.6
    location_time = (10,23)

    input_txt = "Now time : {0} (KST)"

    ### base_info ###
    base_rgb = (0 , 225 , 150 , 255)
    base_tickness  = 2
    base_cross_length = 5

    ## arrow_info ###
    expanding_arrow_color = (100, 0, 190 , 255)
    reduction_arraw_color = (0, 255 , 0 , 255)
    same_arrw_color = (50 , 160 , 50 , 255)
    arrow_thickness = 2
    arrow_tiplength = 0.1

    ## not moving fog info ###
    fog_cross_length = 5
    ## cutting information ##
    # back image
    root_path = '/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det'
    bk_img_path = "{}/inverted_bk_img.png".format(root_path)
    pred_img_path_base = '/gisangdan/kans/data-int/fog-det-trac/image/{}/fog-det-trac_{}_image-det.png'
    below_legend_img_path = "{}/flat_info2.png".format(root_path)

    moving_speed_class = [0 , 10, 20, 30 , 40, 50 , 60 , 70 , 80]
    moving_speed_class.sort()
    moving_speed_distance = {}
    distance = 0
    for moving_class in moving_speed_class:
        moving_speed_distance[moving_class] = distance
        distance = distance + 5



def total_model_network(final_input_shape):
    if len(final_input_shape) == 4:
        func = total_model_network_3d
    elif len(final_input_shape) == 3:
        func = total_model_network_2d
    elif len(final_input_shape) == 2:
        func = total_model_network_1d
    return func



def total_model_network_3d(final_input_shape , conv1_total, conv2_total , dense1_total , dense2_total , optimizer , loss_func , learning_rate):
    from tensorflow.keras import layers, models
    from tensorflow import keras
    import tensorflow as tf
    if optimizer == "1":
        optimizer_v =tf.keras.optimizers.Adam(learning_rate=learning_rate) #'adam'
    elif optimizer == "2":
        optimizer_v = tf.keras.optimizers.RMSprop(learning_rate=learning_rate)
    if loss_func == "1":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    elif loss_func == "2":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False)

    model = models.Sequential()
    # you do the math here. If final_conv_shape_format is 2d and sequence length is bigger than 0 then final input shape is 3d so this model works fine
    # however, if you change the dimension, then 3d conv will not work
    #just remember if self.sequence_len is 0 , then it will lower one dimension amd you have to modify layers since it wont be 3d anymore
    # and if you change the final_conv_shape_format, then it also will change the input_shape and it is your responsibility  to modify the layers

    ##please look into this code from "class fog_main_preprocessing" and modify the layers according to the final input shape
    ###############################################################################################
    # if self.sequence_len == 0:
    #    self.final_input_shape = last_dim_input_shape + (1,)                 ----- > this could be any shape according to final_conv_shape_format and sequence_len
    #else:
    #    self.final_input_shape = (self.sequence_len , ) + last_dim_input_shape + (1,) ----- > this could be any shape according to final_conv_shape_format and sequence_len
    ###############################################################################################

    model.add(layers.Conv3D(int(conv1_total), (2, 2 , 2),  padding='same', activation='relu', input_shape=final_input_shape )) #32
    model.add(layers.MaxPooling3D((2, 2, 2), padding='same' ))
    model.add(layers.Conv3D(conv2_total , (2, 2, 2),  padding='same', activation='relu'))
    model.add(layers.Flatten())
    # model.add(layers.Dense(1024, activation='relu'))
    model.add(layers.Dense(dense1_total, activation='relu'))
    model.add(layers.Dense(dense2_total, activation='relu'))
    model.add(layers.Dense(16, activation='relu'))
    model.add(layers.Dense(6))
    model.compile(optimizer=optimizer_v,
                  loss=loss_func_v,
                  metrics=['accuracy'])
    return model

def total_model_network_2d(final_input_shape , conv1_total, conv2_total , dense1_total , dense2_total , optimizer , loss_func , learning_rate):
    from tensorflow.keras import layers, models
    from tensorflow import keras
    import tensorflow as tf
    if optimizer == "1":
        optimizer_v =tf.keras.optimizers.Adam(learning_rate=learning_rate) #'adam'
    elif optimizer == "2":
        optimizer_v = tf.keras.optimizers.RMSprop(learning_rate=learning_rate)
    if loss_func == "1":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    elif loss_func == "2":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False)

    model = models.Sequential()
    model.add(layers.Conv2D(int(conv1_total), (2 , 2),  padding='same', activation='relu', input_shape=final_input_shape )) #32
    model.add(layers.MaxPooling2D((2, 2), padding='same' ))
    model.add(layers.Conv2D(conv2_total , (2, 2),  padding='same', activation='relu'))
    model.add(layers.Flatten())
    # model.add(layers.Dense(1024, activation='relu'))
    model.add(layers.Dense(dense1_total, activation='relu'))
    model.add(layers.Dense(dense2_total, activation='relu'))
    model.add(layers.Dense(16, activation='relu'))
    model.add(layers.Dense(6))
    model.compile(optimizer=optimizer_v,
                  loss=loss_func_v,
                  metrics=['accuracy'])
    return model

def total_model_network_1d(final_input_shape , conv1_total , conv2_total  ,  dense1_total , dense2_total  , optimizer , loss_func, learning_rate):
    from tensorflow.keras import layers, models
    from tensorflow import keras
    import tensorflow as tf
    if optimizer == "1":
        optimizer_v =tf.keras.optimizers.Adam(learning_rate=learning_rate) #'adam'
    elif optimizer == "2":
        optimizer_v = tf.keras.optimizers.RMSprop(learning_rate=learning_rate)
    if loss_func == "1":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    elif loss_func == "2":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False)

    model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(final_input_shape)),
            tf.keras.layers.Dense(int(dense1_total), activation='relu'),  # 128
            tf.keras.layers.Dense(int(dense2_total) , activation='relu'),
            tf.keras.layers.Dense(32, activation='relu'),
            tf.keras.layers.Dense(6)])
    model.compile(optimizer=optimizer_v,
              loss=loss_func_v,
              metrics=['accuracy'])
    return model


def amos_model_network(final_input_shape , dense1_amos , dense2_amos  , optimizer , loss_func, learning_rate):
    from tensorflow.keras import layers, models
    from tensorflow import keras
    import tensorflow as tf
    if optimizer == "1":
        optimizer_v =tf.keras.optimizers.Adam(learning_rate=learning_rate) #'adam'
    elif optimizer == "2":
        optimizer_v = tf.keras.optimizers.RMSprop(learning_rate=learning_rate)
    if loss_func == "1":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)
    elif loss_func == "2":
        loss_func_v = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=False)
    model = tf.keras.models.Sequential([
            tf.keras.layers.Flatten(input_shape=(final_input_shape)),
            tf.keras.layers.Dense(int(dense1_amos), activation='relu'),  # 128
            tf.keras.layers.Dense(int(dense2_amos) , activation='relu'),
            tf.keras.layers.Dense(32, activation='relu'),
            tf.keras.layers.Dense(6)])
    model.compile(optimizer=optimizer_v,
              loss=loss_func_v,
              metrics=['accuracy'])
    return model

######################################## customizing ends #####################################
###############################################################################################

class tb_info(object):
    tb_host =  '192.168.0.18'# '172.16.200.10'
    tb_db_name = 'tibero'
    tb_user_name = 'afw'  # 'afns'
    tb_password =  'afw123'   #'Qwerty0987@'
    tb_port = 8629

class Config(object):
    root_platform_path = os.path.join(os.sep,'/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det')


    ####################back up model path@@@@@@@@@@@@@@@@@####
    total_model_platform_path = os.path.join(os.sep,'/gisangdan/kans/ai/model-save/fog-det-trac', 'total_model_direc')
    amos_model_platform_path = "/gisangdan/kans/ai/model-save/fog-det-trac/amos_model_direc"
    ####################@@@@@@@@@@@@@@@@@@@@@@@@@##############
    ####################@@@@@@@@@@@@@@@@@@@@@@@@@##############


    model_platform_dir = '/gisangdan/kans/ai/model-save/fog-det-trac'

    data_platform_path = "/gisangdan/kans/data-ext"

    output_platform_path = "/gisangdan/kans/data-int/fog-det-trac"

    tempo_platform_path = os.path.join(os.sep,'/gisangdan/kans/data-int/fog-det-trac/tempo_file')


class collecting(object):
    data_collection_path_platform = "/gisangdan/kans/data-ext"

    data_collection_path_hdfs_gk2a = "/hadoop/hdfs/data/PARSER/GK2A/{}/{}/{}/gk2a_ami_le1b_{}_ko0{}lc_{}.nc.{}"
    data_collection_path_hdfs_hsr = "/hadoop/hdfs/data/PARSER/HSR/{}/{}/RDR_CMP_HSR_PUB_{}.bin"

    #### for nas gk2a only up to folder directory since thre is no pattern in file names
    data_collection_path_nas_gk2a = '/gisangdan/kans/data-ext/GK2A/ko/result/{}/{}/{}/result/gk2a_ami_le1b_{}_ko0{}lc_{}.nc'
    data_collection_path_nas_hsr = '/gisangdan/kans/data-ext/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'

class external_info_path(object):
    root_path = '/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det'
    #amos_info_path = "/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/amos_data_tower_info_hsr_included.csv"
    gk2a_hsr_pixel_info =  "{}/gk2a_hsr_int_pixel.csv".format(root_path)
    #amos_pixel_info = "{}/amos_data_tower_info_hsr_included.csv".format(root_path)
    JDBC_DRIVER = "{}/tibero6-jdbc.jar".format(root_path)

    training_amos_info_path = "/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/training_amos_info.csv"
    all_amos_info_path      = "/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/all_amos_info.csv"


class fog_modeling(object):
    hsr_limit = -25000
    f1_score_limit = 0.5 ###~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


class hdfs_path_class(object):
    normal_hdfs_path = "http://192.168.0.179:50070" # "http://ba01.t3q.com:50070"

    backup_hdfs_path = "http://192.168.0.178:50070" # "http://ba02.t3q.com:50070"
    hdfs_port = "50070"

    ambari_host = "192.168.0.174"
    auth_pass = 'admin'
    ambari_cluster_name = 'hacluster'
    auth_user = 'admin'


##### about to be deleted #####
def making_channel_dic(all_channels):
    channel_feature_dic ={}
    for channel in all_channels:
        if "vi" in channel or "nr" in channel:
            values = ["Albedo" , "Radiance"]
        else:
            values = ["Radiance","Effective_Brightness_Temperature" ,"Brightness_Temperature" ]
        channel_feature_dic[channel] = values
    return channel_feature_dic




# class column_info(object):
#     ##@##
#     all_columns = ['ir087_Radiance',
#      'ir087_Effective',
#      'ir087_Brightness',
#      'ir096_Radiance',
#      'ir096_Effective',
#      'ir096_Brightness',
#      'ir105_Radiance',
#      'ir105_Effective',
#      'ir105_Brightness',
#      'ir112_Radiance',
#      'ir112_Effective',
#      'ir112_Brightness',
#      'ir123_Radiance',
#      'ir123_Effective',
#      'ir123_Brightness',
#      'ir133_Radiance',
#      'ir133_Effective',
#      'ir133_Brightness',
#      'nr013_Radiance',
#      'nr013_Albedo',
#      'nr016_Radiance',
#      'nr016_Albedo',
#      'sw038_Radiance',
#      'sw038_Effective',
#      'sw038_Brightness',
#      'vi004_Radiance',
#      'vi004_Albedo',
#      'vi005_Radiance',
#      'vi005_Albedo',
#      'vi008_Radiance',
#      'vi008_Albedo',
#      'wv063_Radiance',
#      'wv063_Effective',
#      'wv063_Brightness',
#      'wv069_Radiance',
#      'wv069_Effective',
#      'wv069_Brightness',
#      'wv073_Radiance',
#      'wv073_Effective',
#      'wv073_Brightness',
#      'vi006_Radiance',
#      'vi006_Albedo']
#
#
#     ##@##
#     nas_data_path_dic = {
#      'nr013': '{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_nr013_ko020lc_{}.nc/',
#      'ir112':'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir112_ko020lc_{}.nc/',
#      'ir087' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir087_ko020lc_{}.nc/',
#      'ir096' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir096_ko020lc_{}.nc/',
#      'ir123':'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir123_ko020lc_{}.nc/',
#      'ir133' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir133_ko020lc_{}.nc/',
#      'ir105' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir105_ko020lc_{}.nc/',
#      'nr016' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_nr016_ko020lc_{}.nc/',
#      'vi004' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi004_ko010lc_{}.nc/',
#      'vi005' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi005_ko010lc_{}.nc/',
#      'sw038' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_sw038_ko020lc_{}.nc/',
#      'wv063' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_wv063_ko020lc_{}.nc/',
#      'vi008'  :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi008_ko010lc_{}.nc/',
#      'wv069' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_wv069_ko020lc_{}.nc/',
#      'wv073'  :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_wv073_ko020lc_{}.nc/',
#      'vi006' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi006_ko005lc_{}.nc/'}
#
#
#
#     ##@## nas version only but channel_feature_dic
#     all_channels =  ['ir087','ir096','ir105','ir112','ir123','ir133','nr013','nr016','sw038','vi004','vi005','vi006', 'vi008','wv063','wv069','wv073']
#     ##@##
#     channel_feature_dic = making_channel_dic(all_channels)

##########

class collecting_data_info(object):
    ### gk2a ###
    gk2a_features_dic = {'ir087':['Radiance' , 'Brightness'], #, 'Effective'],
                         'ir096':['Radiance' , 'Brightness'], #, 'Effective'],
                         'ir105':['Radiance' , 'Brightness'], # , 'Effective'],
                         'ir112':['Radiance' , 'Brightness'], # , 'Effective'],
                         'ir123':['Radiance' , 'Brightness'], # , 'Effective'],
                         'ir133':['Radiance' , 'Brightness'], # , 'Effective'],
                         'nr013':['Albedo'],
                         'nr016':['Albedo'],
                         'sw038':['Radiance' , 'Brightness'], # , 'Effective'],
                         'vi004':['Albedo'],
                         'vi005':['Albedo'],
                         'vi006':['Albedo'],
                         'vi008':['Albedo'],
                         'wv063':['Radiance' , 'Brightness'], # , 'Effective'],
                         'wv069':['Radiance' , 'Brightness'], # , 'Effective'],
                         'wv073':['Radiance' , 'Brightness']} # , 'Effective']}
    gk2a_features_list = []
    for key , value in gk2a_features_dic.items():
        for type in value:
            gk2a_feature = key + "_" + type
            gk2a_features_list.append(gk2a_feature)

    ### hsr ###
    hsr_feature_collecting_bool = True
    hsr_feature_name = "HSR_value"

    ### derived ###
    derived_features = ['zenith']


# class gk2a_nas_path_dic(object):
#     nas_data_path_dic = {
#      'nr013': '{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_nr013_ko020lc_{}.nc/',
#      'ir112':'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir112_ko020lc_{}.nc/',
#      'ir087' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir087_ko020lc_{}.nc/',
#      'ir096' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir096_ko020lc_{}.nc/',
#      'ir123':'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir123_ko020lc_{}.nc/',
#      'ir133' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir133_ko020lc_{}.nc/',
#      'ir105' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_ir105_ko020lc_{}.nc/',
#      'nr016' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_nr016_ko020lc_{}.nc/',
#      'vi004' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi004_ko010lc_{}.nc/',
#      'vi005' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi005_ko010lc_{}.nc/',
#      'sw038' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_sw038_ko020lc_{}.nc/',
#      'wv063' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_wv063_ko020lc_{}.nc/',
#      'vi008'  :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi008_ko010lc_{}.nc/',
#      'wv069' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_wv069_ko020lc_{}.nc/',
#      'wv073'  :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_wv073_ko020lc_{}.nc/',
#      'vi006' :'{}/gk2a/ko/result/{}/{}/{}/result/gk2a_ami_le1b_vi006_ko005lc_{}.nc/'}
#


class model_features_fixed(object):
    total_model_features =     [['sw038_Radiance', 'sw038_Brightness'],
                                ['ir087_Radiance', 'ir087_Brightness'],
                                ['ir096_Radiance', 'ir096_Brightness'],
                                ['ir105_Radiance', 'ir105_Brightness'],
                                ['ir112_Radiance', 'ir112_Brightness'],
                                ['ir123_Radiance', 'ir123_Brightness'],
                                ['ir133_Radiance', 'ir133_Brightness'],
                                ['nr013_Albedo',   'nr013_Albedo'],
                                ['nr016_Albedo',   'nr016_Albedo'],
                                #['vi004_Radiance', 'vi004_Radiance:vi004_Albedo','vi004_Albedo'],
                                #['vi005_Radiance', 'vi005_Radiance:vi005_Albedo','vi005_Albedo'],
                                #['vi006_Radiance', 'vi006_Radiance:vi006_Albedo','vi006_Albedo'],
                                #['vi008_Radiance', 'vi008_Radiance:vi008_Albedo','vi008_Albedo'],
                                ['wv063_Radiance',  'wv063_Brightness'],
                                ['wv069_Radiance',  'wv069_Brightness'],
                                ['wv073_Radiance',  'wv073_Brightness'],
                                ['zenith' ,          'zenith'],
                                ['HSR_value' ,       'HSR_value']]

    #total_sequence_len = 3
    #total_each_sequence_minutes = 10

    #### customizing amos pixel model ####
    # if you wanna add features, please use names that were named in class amos_model_features_labeling
    amos_model_features =[    'ir087_Radiance',       #'ir087_Effective',
                              'ir087_Brightness',
                              'ir096_Radiance',
                              'ir096_Brightness',
                              'ir105_Radiance',
                              'ir105_Brightness',
                              'ir112_Radiance',
                              'ir112_Brightness',
                              'ir123_Radiance',
                              'ir123_Brightness',
                              'ir133_Radiance',
                              'ir133_Brightness',
                              'nr013_Albedo',
                              'nr016_Albedo',
                              'sw038_Radiance',
                              'sw038_Brightness',
                              'wv063_Radiance',
                              'wv063_Brightness',
                              'wv069_Radiance',
                              'wv069_Brightness',
                              'wv073_Radiance',
                              'wv073_Brightness',
                              'zenith',
                              'wind_dir',
                              'wind_speed',
                              'temperature',
                              'humid',
                              'land_pressure',
                              'sea_pressure',
                              'HSR_value' ,
                              'vis_distance']

    amos_sequence_len = 1
    amos_each_sequence_minutes = 10





class fog_total_model_fixed(object):
    final_conv_shape_format = model_features_fixed.total_model_features
    #sequence_len = model_features_fixed.total_sequence_len
    #each_sequence_minutes = model_features_fixed.total_each_sequence_minutes
    #total_model_info = {"final_conv_shape_format":final_conv_shape_format , "sequence_len" : sequence_len , "each_sequence_minutes" : each_sequence_minutes}

class fog_amos_model_fixed(object):
    final_shape_format_amos =model_features_fixed.amos_model_features
    sequence_len = model_features_fixed.amos_sequence_len
    each_sequence_minutes = model_features_fixed.amos_each_sequence_minutes
    amos_model_info = {"final_shape_format_amos" : final_shape_format_amos , "sequence_len":sequence_len , "each_sequence_minutes" : each_sequence_minutes}








class fog_total_model_creating(object):
    final_conv_shape_format = model_features_customizing.total_model_features
    #sequence_len = model_features_customizing.total_sequence_len
    #each_sequence_minutes = model_features_customizing.total_each_sequence_minutes
    # total_model_info.json
    #total_model_info = {"final_conv_shape_format":final_conv_shape_format , "sequence_len" : sequence_len , "each_sequence_minutes" : each_sequence_minutes}

class fog_amos_model_creating(object):
    # gk2a channels used in this model must be used also by total_model
    final_shape_format_amos =model_features_customizing.amos_model_features

    sequence_len = model_features_customizing.amos_sequence_len
    each_sequence_minutes = model_features_customizing.amos_each_sequence_minutes
    # needed_seq = list(map(lambda x:  (x / (fog_total_model_creating.each_sequence_minutes /fog_total_model_creating.sequence_len)) % 1 == 0 , list(range(sequence_len))    ))
    # if (each_sequence_minutes * sequence_len) > (fog_total_model_creating.each_sequence_minutes * fog_total_model_creating.sequence_len):
    #     print("considering prediction duration , this model cannot have this amount of each_sequence_minutes")
    #     each_sequence_minutes = fog_total_model_creating.each_sequence_minutes
    #     sequence_len = 1
    # if False in needed_seq:
    #     print("considering prediction duration , each sequence minutes and sequence length have to be modified")
    #     each_sequence_minutes = fog_total_model_creating.each_sequence_minutes
    #     sequence_len = 1
    amos_model_info = {"final_shape_format_amos" : final_shape_format_amos , "sequence_len":sequence_len , "each_sequence_minutes" : each_sequence_minutes}


##@##
class fog_main_preprocessing:
    def __init__(self , final_conv_shape_format , sequence_len , each_sequence_minutes):

        self.final_conv_shape_format = final_conv_shape_format

        self.sequence_len = sequence_len

        self.each_sequence_minutes = each_sequence_minutes

    def getting_final_format(self):
        import numpy as np
        if self.sequence_len > 1 :
            np_final_conv_shape_format = np.array(self.final_conv_shape_format)
            np_final_conv_shape_format_shape = np_final_conv_shape_format.shape
            self.conv_dimension= len(np_final_conv_shape_format.shape)
            self.dim_size_dic = {}
            for dim in range(self.conv_dimension):
                self.dim_size_dic[dim] = np_final_conv_shape_format.shape[dim]
            self.one_d_format = 1
            for dim in range(self.conv_dimension):
                self.one_d_format = self.one_d_format * self.dim_size_dic[dim]
            conv_one_d  = np.reshape(np_final_conv_shape_format , (self.one_d_format) )
            inserting_arrays = [self.final_conv_shape_format] * self.sequence_len
            for seq in range(self.sequence_len):
                tempo_conv_one_d = list(map(lambda x: x + "_{}".format(seq) , conv_one_d  ) )
                tempo_conv = np.reshape(np.array(tempo_conv_one_d) , np_final_conv_shape_format_shape  ).tolist()
                #print(inserting_arrays[seq])
                inserting_arrays[seq] = tempo_conv
            self.final_conv_shape_input = np.array(inserting_arrays)
        else:
            if len(np.array(self.final_conv_shape_format).shape) == 2:
                temp_array = np.array(self.final_conv_shape_format)
                y_len = temp_array.shape[0]
                x_len = temp_array.shape[1]
                temp_flat = temp_array.flatten()
                tempo_conv_one_d =  np.array(list(map(lambda x: x + "_0" , temp_flat )))
                self.final_conv_shape_input = np.reshape(tempo_conv_one_d , (y_len , x_len)  )
            else:
                tempo_conv_one_d =  list(map(lambda x: x + "_0" , self.final_conv_shape_format ))
                self.final_conv_shape_input = np.array(tempo_conv_one_d)

    def get_input_shape(self):
        import numpy as np
        np_final_conv_shape_format = np.array(self.final_conv_shape_format)
        conv_dimension = np_final_conv_shape_format.shape
        if self.sequence_len == 0 or self.sequence_len == 1:
            self.final_input_shape = conv_dimension + (1,)
        else:
            self.final_input_shape = (self.sequence_len , ) + conv_dimension + (1,)
    #### this is just needed for preprocessing, therefore it is not than important
    def extract_to_list(self):
        import pandas as pd
        import numpy as np
        np_final_conv_shape_format = np.array(self.final_conv_shape_format)
        input_columns_order = np_final_conv_shape_format.flatten().tolist()
        self.input_columns_order = pd.Series(list(filter(lambda x: ":" not in x , input_columns_order))).drop_duplicates().tolist()
        self.column_length = len(self.input_columns_order)
        self.using_channels = pd.Series(map(lambda x: x.split("_")[0] if x != "HSR_value" else x ,np.array(np_final_conv_shape_format).flatten())).drop_duplicates().tolist()
        self.gk2a_using_chanels  = self.using_channels.copy()


        self.non_gk2a_features = []
        if "zenith" in self.gk2a_using_chanels:
            self.gk2a_using_chanels.remove("zenith")
            self.non_gk2a_features.append("zenith")
        if "HSR_value" in self.gk2a_using_chanels:
            self.gk2a_using_chanels.remove("HSR_value")
            self.non_gk2a_features.append("HSR_value")

        self.amos_features = []
        for amos_feature in list(amos_model_features_labeling.amos_features_naming.values()):
            if amos_feature in self.input_columns_order:
                self.amos_features.append(amos_feature)
                self.input_columns_order.remove(amos_feature)
            if amos_feature.split("_")[0] in self.gk2a_using_chanels:
                self.gk2a_using_chanels.remove(amos_feature.split("_")[0])
    def data_check_list(self):
        self.gk2a_data_check_list = self.input_columns_order
        if "zenith" in self.gk2a_data_check_list: self.gk2a_data_check_list.remove("zenith")
        if "HSR_value" in self.gk2a_data_check_list: self.gk2a_data_check_list.remove("HSR_value")
        if "target" in self.gk2a_data_check_list: self.gk2a_data_check_list.remove("target")



class preprocess_info(object):
    create_data_cpu_count = 19
    hdfs_nas = 'hdfs'# 'hdfs' 'nas'
