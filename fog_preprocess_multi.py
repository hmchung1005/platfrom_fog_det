#!/usr/bin/env python
import sys
import pandas as pd
import numpy as np
import os
from pysolar import solar
from PIL import Image
import imageio
import datetime
import tensorflow as tf
import fog_logging

from fog_config import Config
import multiprocessing as mp
from hdfs import InsecureClient
from multiprocessing import cpu_count, current_process, Manager
import concurrent.futures


#global_path를 가져오기위한 환경구성
def set_globvar_to_path(env):
    global data_path
    global root_path
    if env == "local":
        data_path = Config.data_local_path
        root_path = Config.root_local_path
    elif env == "platform":
        data_path = Config.data_platform_path
        root_path = Config.root_platform_path

global root_path
root_path = "/home/platfrom_fog_det"

# 해당 날짜의 모든 파일을 가져오는 것
def bring_all_file_list(ticketNo):
    year_month = ticketNo[:6]
    day = ticketNo[6:8]
    hour = ticketNo[8:10]
    miniute = ticketNo[10:]

    time = year_month + day+ hour +miniute

    path = data_path + '/GK2A/ko/result/{}/{}/{}/result/'.format(year_month,day,hour)
    all_time_filelist = os.listdir(path)

    return time, all_time_filelist, path


# 모든 경로에 타겟 시간대가 포함된 경로만 출력
def getting_path_list_by_time(time, all_time_filelist,path):
    channels  = list(filter(lambda x: time in x , all_time_filelist))
    final_path_list = []
    for channel in channels:
        temp_path = path +channel + "/"
        temp_parsed_results = os.listdir(temp_path)
        for temp_parsed_result in temp_parsed_results:
            temp_final_path = temp_path  + temp_parsed_result
            final_path_list.append(temp_final_path)
    return final_path_list

# 학습할때 출력된 데이터의 평균 ,편차와 변수 순서 정보 갖고오기
def get_order_and_summary(summary_path):
    summary = pd.read_csv(summary_path , index_col=0)   ### change path
    input_columns_order = summary.columns.tolist()
    column_length = len(input_columns_order)
    return summary, input_columns_order, column_length




# 나스로 부터 데이터 전처리 모든 과정
def making_detectable_df_from_nas(final_path_list, input_order, column_length):
    # (900, 900, 변수 개수) 크기의 3차원 배열 생성
    full_array = np.zeros((900,900,column_length))
    for i in range(len(final_path_list)):
        path = final_path_list[i]
        # vi006 변수 학습 아직 못함
        if '_vi006' in path:
            continue
        # 파일이 txt 일때 변수명과, 3차원 배열에서 변수의 위치 계산
        if path.split(".")[-1] == 'txt':
            temp_df = pd.read_csv(path , header = None).iloc[:,:-1]
            temp_array = temp_df.values
            x = path
            temp_col = x.split('/')[-1].split('_')[3]+"_"+ x.split('/')[-1].split('.')[-2].split('_')[0]
            final_pos = input_order.index(temp_col)
            log.info('loaded : {}'.format(temp_col))
        # 파일이 npz 일때 변수명과, 3차원 배열에서 변수의 위치 계산
        elif path.split(".")[-1] =='npz':
            temp_df = np.load(path ,allow_pickle=True)
            temp_array = temp_df['arr_0']
            x = path
            temp_col = x.split('/')[-1].split('_')[3]+"_"+ x.split('/')[-1].split('.')[-3].split('_')[0]
            final_pos = input_order.index(temp_col)
            log.info('loaded : {}'.format(temp_col))
        try:
            # 현재 008 채널에서 마지막 열에서 이상함 감지
            if (float(temp_array[temp_array.shape[0] - 1][temp_array.shape[0] - 1])  > 0) == False:        ####### 일단 긴급 조치
                temp_array[:,temp_array.shape[0] - 1] = temp_array[:,temp_array.shape[0] - 2]
                temp_array = temp_array.astype('float')
            # vi 채널 사이즈 조정
            if '_vi' in path:
                modified_array = transform_gk__1800_to_900(temp_array)
                full_array[:,:,final_pos] = modified_array
            else:
                full_array[:,:,final_pos] = temp_array
        except:
            log.error('there must be none values : {}'.format(path))
            temp_df.to_csv("{}/something_wrong_with_{}.csv".format(root_path,temp_col))
    return full_array

# 태양천정각과 hsr반사도 값 배열에 추가
def making_detectable_df_use_zenith(final_array,ticketNo, time, input_columns_order):
    latlon_900_900 = pd.read_csv('{}/LATLON_KO_2000.txt'.format(root_path),header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)
    reshaped_to_3d_900 = np.reshape(latlon_900_2d,(900,900,2))
    zenith_at_time = get_zenith_900_faster(latlon_900_2d, str(ticketNo))
    final_array[:,:,40] = zenith_at_time

    get_hsr_start = datetime.datetime.now()
    final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform(time)
    get_hsr_end = datetime.datetime.now()
    print("loading hsr data Duration : {}".format(get_hsr_end - get_hsr_start))

    final_array[:,:,41] = temp_hsr_reshaped
    detectable_df = final_array_to_detectable_df(final_array, input_columns_order)
    return detectable_df

def get_hsr_and_transform(time):
    year_month = time[0:6]
    day = time[6:8]
    # file_path = '/share/3_데이터/HSR/result/' +year_month  +"/" + day + "/result/RDR_CMP_HSR_PUB_" + time +".bin/RDR_CMP_HSR_PUB_" + time + ".txt"
    #file_path = '/share/3_데이터/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'.format(year_month,day,time,time)
    file_path = data_path + '/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'.format(year_month,day,time,time)
    hsr_df = pd.read_csv(file_path,header = None)
    hsr_np = hsr_df.values
    # mapping_table = pd.read_csv("hsr_processing/gk2a_hsr_int_pixel.csv")
    mapping_table = pd.read_csv("{}/gk2a_hsr_int_pixel.csv".format(root_path))
    # gk2a 영역이 더 넓기 때문에 필요 없는 데이터 제거
    for_merge_table = mapping_table.loc[(mapping_table['hsr_y'] >= 0 ) & (mapping_table['hsr_y'] <= 2880) & (mapping_table['hsr_x'] >= 0 ) & (mapping_table['hsr_x'] <= 2304 ),: ]
    # hsr 반사도 값 갖고 오기
    for_merge_table['hsr_values'] =  list(map(lambda x,y: hsr_np[ int(y) ][ int(x) ], for_merge_table['hsr_x'], for_merge_table['hsr_y'] ))
    # hsr 반사도와 gk2a 픽셀 정보 나란히 갖고오기
    for_merge_table_modified = for_merge_table.loc[:,['gk2a', 'hsr_values']]
    # hsr과 위치정보 테이블 융합 후 hsr value 만 따로 분리
    hsr_transformed_np = pd.merge(mapping_table,for_merge_table_modified, on = 'gk2a', how = 'outer')['hsr_values'].values
    # nan 값 모드 -50000으로 변경
    hsr_transformed_np_modified =  np.nan_to_num(hsr_transformed_np, nan = -50000)
    # 900 900 크기로 변경
    final_hsr_reshaped = np.reshape(hsr_transformed_np_modified, (900,900))
    temp_hsr_reshaped = final_hsr_reshaped.copy()
    # 최소값 0으로 지정
    temp_hsr_reshaped =temp_hsr_reshaped.clip(min = 0)
    return final_hsr_reshaped , temp_hsr_reshaped

def final_array_to_detectable_df(final_array, input_order):
    reshaped_number_by_len = np.reshape(final_array,(810000,len(input_order)))
    detectable_df = pd.DataFrame(reshaped_number_by_len, columns = input_order)
    return detectable_df


# 표준화
def making_normalization_data(detectable_df, summary):
    detection_df = detectable_df.copy()
    cols = summary.columns.tolist()
    for i in cols:
        temp_normaliztion =  (detection_df.loc[:,i] - summary.loc["mean",i]) / summary.loc["std",i]
        detection_df.loc[:,i] = temp_normaliztion
    detect_df = detection_df.copy()

    return detect_df




# 태양 천정각 빠르게 계산
def get_zenith_900_faster(latlon_900_2d, time):
    year = int(time[:4])
    month = int(time[4:6])
    day = int(time[6:8])
    hour = int(time[8:10])
    minute = int(time[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime.datetime(year,month,day,hour,minute, tzinfo =datetime.timezone.utc)  -  datetime.timedelta(hours = 9)
    temp_array = get_altitude_faster(latlon_900_2d, dobj)
    # 900 900 2 차원 배열로 만듬
    whole_array = np.reshape(temp_array, (900,900) )
    final_array = float(90) - whole_array
    return final_array

# 태양 고도 빠르게 계산 함수
def get_altitude_faster(latlon_900_2d, when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(latlon_900_2d[:,0])
    hour_angle = solar.get_hour_angle(when, latlon_900_2d[:,1] )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))

# ------------------------------------------------------------------
# ------------------------------------------------------------------
# ------------------------------------------------------------------
# ------------------------------------------------------------------
#
# set_globvar_to_path(env)
# time, all_time_filelist, path = bring_all_file_list(str(ticketNo))
# final_path_list = getting_path_list_by_time(time, all_time_filelist,path)
# summary, input_columns_order, column_length = get_order_and_summary("{}/summary.csv".format(root_path))
# final_array = making_detectable_df_from_nas(final_path_list, input_columns_order, column_length)
# detectable_df = making_detectable_df_use_zenith(final_array, ticketNo, time, input_columns_order)
# detect_df = making_normalization_data(detectable_df, summary)
#
#
# ------------------------------------------------------------------
# ------------------------------------------------------------------
# -------------------------------------------------------------------
# ------------------------------------------------------------------



def main(ticketNo, env):
    global log
    log = fog_logging.get_log_view(1, env, False, 'fog_preprocess_info')
    error_log = fog_logging.get_log_view(1, env, True, 'fog_preprocess_error')

    # log.info('ticketNo : {}^{}'.format(ticketNo,type(ticketNo)))
    # tmp_list = ticketNo.tolist()
    # log.info('ticketNo : {}'.format(tmp_list))
    # ticketNo='201909071530'

    try:
        #global_path를 가져오기위한 환경구성
        set_globvar_to_path(env)
    except Exception as e:
        error_log.error('set_globvar_to_path error : {}'.format(e))

    try:
        # 해당 날짜의 모든 파일을 가져오는 것
        time, all_time_filelist, path = bring_all_file_list(str(ticketNo))
        log.info('bring_all_file_list')
    except Exception as e:
        error_log.error('bring_all_file_list error : {}'.format(e))

    try:
        # 어느 지점으로 부터 모든 가능한 경로 출력
        final_path_list = getting_path_list_by_time(time, all_time_filelist,path)
        log.info('getting_path_list_by_time')
    except Exception as e:
        error_log.error('getting_path_list_by_time error : {}'.format(e))

    try:
        # 학습할때 출력된 데이터의 평균 ,편차와 변수 순서 정보 갖고오기
        summary, input_columns_order, column_length = get_order_and_summary("{}/summary.csv".format(root_path))
        log.info('get_order_and_summary')
    except Exception as e:
        error_log.error('get_order_and_summary error : {}'.format(e))

    try:
        # 나스로 부터 데이터 전처리 모든 과정
        get_nas_data_start = datetime.datetime.now()
        final_array = making_detectable_df_from_nas(final_path_list, input_columns_order, column_length)
        get_nas_data_end = datetime.datetime.now()
        print("getting nas data Duration {}".format(get_nas_data_end  - get_nas_data_start))


        log.info('making_detectable_df_from_nas')
    except Exception as e:
        error_log.error('making_detectable_df_from_nas error : {}'.format(e))

    try:
        # 태양천정각과 hsr반사도 값 배열에 추가
        detectable_df = making_detectable_df_use_zenith(final_array, ticketNo, time, input_columns_order)
        log.info('making_detectable_df_use_zenith')
    except Exception as e:
        error_log.error('making_detectable_df_use_zenith error : {}'.format(e))

    try:
        #표준화
        detect_df = making_normalization_data(detectable_df, summary)
        log.info('making_normalization_data')
    except Exception as e:
        error_log.error('making_normalization_data error : {}'.format(e))

    return detect_df


################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################

# 1800 1800 크기의 이미지 강제로 900 900 크기로 만드는 함수
def transform_gk__1800_to_900(array_1800):
    a = array_1800[::2, ::2]
    b = array_1800[::2, 1::2]
    c = array_1800[1::2, ::2]
    d = array_1800[1::2, 1::2]
    sum = a +b+ c+d
    result_array = sum / 4
    return result_array

def get_summary_feature_names():
    summary  = pd.read_csv("{}/summary.csv".format(root_path), index_col=0)
    columns = summary.columns.tolist()
    columns.remove("zenith")
    columns.remove("HSR_value")
    columns = list(map(lambda x: x.replace("_","-") , columns ))
    columns = list(map(lambda x: x.replace("Brightness","Brightness_Temperature") , columns ))
    columns = list(map(lambda x: x.replace("Effective","Effective_Brightness_Temperature") , columns ))
    features = list(map(lambda x: x.split("-"), columns ))
    return summary , features



def get_hdfs_path_list_by_time(ticketNo, features):
    year_month = ticketNo[:6]
    day = ticketNo[6:8]
    final_path_list = []
    for feature in features:
        channel = feature[0]
        type = feature[1]
        if "vi" in channel:
            distance = "10"
        else:
            distance = "20"
        final_path_list.append("/hadoop/hdfs/data/PARSER/GK2A/{}/{}/{}/gk2a_ami_le1b_{}_ko0{}lc_{}.nc.{}".format(channel.upper(),year_month,day,channel,distance,ticketNo,type))
    return final_path_list







# 학습할때 출력된 데이터의 평균 ,편차와 변수 순서 정보 갖고오기
def get_order_and_summary2(summary_df):
    summary = summary_df.copy()
    input_columns_order = summary.columns.tolist()
    column_length = len(input_columns_order)
    return summary, input_columns_order, column_length



def making_detectable_df_from_nas2(final_path_list, input_order, column_length):
    client = InsecureClient("http://192.168.0.179:50070", user="hdfs")
    # (900, 900, 변수 개수) 크기의 3차원 배열 생성
    full_array = np.zeros((900,900,column_length))
    for i in range(len(final_path_list)):
        path = final_path_list[i]
        try:
            with client.read(path) as file:
                temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
        except:
            client = InsecureClient("http://192.168.0.178:50070", user="hdfs")
            with client.read(path) as file:
                temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
            client = InsecureClient("http://192.169.0.179:50070", user="hdfs")

        x= path.split("/")
        temp_col = x[6].lower() + "_" + x[9].split(".")[2].split("_")[0]
        final_pos = input_order.index(temp_col)
        print('loaded : {}'.format(temp_col))

        try:
            # 현재 008 채널에서 마지막 열에서 이상함 감지
            if (float(temp_array[temp_array.shape[0] - 1][temp_array.shape[0] - 1])  > 0) == False:        ####### 일단 긴급 조치
                temp_array[:,temp_array.shape[0] - 1] = temp_array[:,temp_array.shape[0] - 2]
                temp_array = temp_array.astype('float')
            # vi 채널 사이즈 조정
            if '_vi' in path:
                modified_array = transform_gk__1800_to_900(temp_array)
                full_array[:,:,final_pos] = modified_array
            else:
                full_array[:,:,final_pos] = temp_array
        except:
            print('there must be none values : {}'.format(path))
    return full_array, client

# 태양천정각과 hsr반사도 값 배열에 추가
def making_detectable_df_use_zenith2(final_array,ticketNo, input_columns_order, client):
    latlon_900_900 = pd.read_csv('{}/LATLON_KO_2000.txt'.format(root_path),header  = None, sep = '\t')
    latlon_900_2d = np.array(latlon_900_900)
    reshaped_to_3d_900 = np.reshape(latlon_900_2d,(900,900,2))
    zenith_at_time = get_zenith_900_faster(latlon_900_2d, str(ticketNo))
    final_array[:,:,40] = zenith_at_time

    load_hsr_start = datetime.datetime.now()
    final_hsr_reshaped, temp_hsr_reshaped = get_hsr_and_transform2( str(ticketNo), client)
    load_hsr_end = datetime.datetime.now()
    print("load hsr Duration : {}".format(load_hsr_end - load_hsr_start))

    final_array[:,:,41] = temp_hsr_reshaped
    detectable_df = final_array_to_detectable_df(final_array, input_columns_order)
    return detectable_df

def get_hsr_and_transform2(time, client):
    year_month = time[0:6]
    day = time[6:8]
    # file_path = '/share/3_데이터/HSR/result/' +year_month  +"/" + day + "/result/RDR_CMP_HSR_PUB_" + time +".bin/RDR_CMP_HSR_PUB_" + time + ".txt"
    #file_path = '/share/3_데이터/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'.format(year_month,day,time,time)
    file_path ="/hadoop/hdfs/data/PARSER/HSR/{}/{}/RDR_CMP_HSR_PUB_{}.bin".format(year_month,day,time)
    try:
        with client.read(file_path) as file:
            hsr_df = pd.read_csv(file, header = None)
    except:
        client = InsecureClient("http://192.168.0.178:50070", user="hdfs")
        with client.read(file_path) as file:
            hsr_df = pd.read_csv(file, header = None)
    hsr_np = hsr_df.values
    # mapping_table = pd.read_csv("hsr_processing/gk2a_hsr_int_pixel.csv")
    mapping_table = pd.read_csv("{}/gk2a_hsr_int_pixel.csv".format(root_path))
    # gk2a 영역이 더 넓기 때문에 필요 없는 데이터 제거
    for_merge_table = mapping_table.loc[(mapping_table['hsr_y'] >= 0 ) & (mapping_table['hsr_y'] <= 2880) & (mapping_table['hsr_x'] >= 0 ) & (mapping_table['hsr_x'] <= 2304 ),: ]
    # hsr 반사도 값 갖고 오기
    for_merge_table['hsr_values'] =  list(map(lambda x,y: hsr_np[ int(y) ][ int(x) ], for_merge_table['hsr_x'], for_merge_table['hsr_y'] ))
    # hsr 반사도와 gk2a 픽셀 정보 나란히 갖고오기
    for_merge_table_modified = for_merge_table.loc[:,['gk2a', 'hsr_values']]
    # hsr과 위치정보 테이블 융합 후 hsr value 만 따로 분리
    hsr_transformed_np = pd.merge(mapping_table,for_merge_table_modified, on = 'gk2a', how = 'outer')['hsr_values'].values
    # nan 값 모드 -50000으로 변경
    hsr_transformed_np_modified =  np.nan_to_num(hsr_transformed_np, nan = -50000)
    # 900 900 크기로 변경
    final_hsr_reshaped = np.reshape(hsr_transformed_np_modified, (900,900))
    temp_hsr_reshaped = final_hsr_reshaped.copy()
    # 최소값 0으로 지정
    temp_hsr_reshaped =temp_hsr_reshaped.clip(min = 0)
    return final_hsr_reshaped , temp_hsr_reshaped
--------------------------------------------------------



print(5)
summary , features = get_summary_feature_names()
ticketNo = "201909071530"
final_path_list = get_hdfs_path_list_by_time(str(ticketNo),features)
summary, input_columns_order,column_length  = get_order_and_summary2(summary)

final_path_list[0]
get_hdfs(final_path_list[0])

starting = datetime.datetime.now()
final_array, client = making_detectable_df_from_nas2(final_path_list, input_columns_order, column_length)
ending = datetime.datetime.now()
print(ending - starting)

def get_hdfs(file):
    file_name = file
    client = InsecureClient("http://192.168.0.179:50070", user = "hdfs")
    with client.read(file_name) as file:
        temp_array = pd.read_csv(file , header = None).iloc[:,:-1].values
    return temp_array
test_path = "/hadoop/hdfs/data/PARSER/GK2A/WV073/201909/07/gk2a_ami_le1b_wv073_ko020lc_201909071530.nc.Brightness_Temperature"
data = get_hdfs(test_path)
data.shape
data


def multiprocessing_load(res_list):
    fs = []
    final_list = []
    number_of_cpus = mp.cpu_count()
    with concurrent.futures.ProcessPoolExecutor(max_workers = int(number_of_cpus -1 )) as executor:
        for i in range(0, len(res_list) ):
            futures = executor.submit(get_hdfs, res_list[i])
            fs.append(futures)
        for one_future in concurrent.futures.as_completed(fs):
            try:
                tmp_res = one_future.result()
                final_list.append(tmp_res)
            except Exception as e:
                print("total_nego_extract_future error : {}".format(e))
    return final_list
new_start = datetime.datetime.now()
dfs3 = multiprocessing_load(final_path_list)
new_end = datetime.datetime.now()
print(new_end - new_start)


f = open("/etc/hosts", "r")     #open file for reading
contents = f.read()             #read contents
contents
f.close()

                       #close file
new_hostnames = '127.0.0.1\tlocalhost\n::1\tlocalhost ip6-localhost ip6-loopback\nfe00::0\tip6-localnet\nff00::0\tip6-mcastprefix\nff02::1\tip6-allnodes\nff02::2\tip6-allrouters\n172.17.0.3\t6a983661f4ba\n\n192.168.0.174 afw201 afw201.t3q.com\n192.168.0.175 afw202 afw202.t3q.com\n192.168.0.176 afw203 afw203.t3q.com\n192.168.0.177 afw204 afw204.t3q.com\n192.168.0.178 afw205 afw205.t3q.com\n192.168.0.179 afw206 afw206.t3q.com\n'
new_hostnames
f = open("/etc/hosts", "w")     #open file for writing
f.write(new_hostnames)               #write the altered contents
f.close()




def normal_processing(file_paths):
    fl = []
    for i in file_paths:
        try:
            f = get_hdfs(i)
            fl.append(f)
        except:
            print(i)
            continue

    return fl

normal_start = datetime.datetime.now()
df5 = normal_processing(final_path_list)
normal_end = datetime.datetime.now()
print(normal_end - normal_start)


len(df5)



dfs3[0]


---------------------------------------------------------



def main_hdfs(ticketNo, env):
    set_globvar_to_path(env)
    global log
    log = fog_logging.get_log_view(1, env, False, 'fog_preprocess_info')
    error_log = fog_logging.get_log_view(1, env, True, 'fog_preprocess_error')

    try:
        summary , features = get_summary_feature_names()
        log.info("get_summary_feature_names")
    except Exception as e:
        error_log.error('get_summary_feature_names : {}'.format(e))

    try:
        final_path_list = get_hdfs_path_list_by_time(str(ticketNo),features)
        log.info("get_hdfs_path_list_by_time")
    except Exception as e:
        error_log.error('get_hdfs_path_list_by_time :  {}'.format(e))

    try:
        summary, input_columns_order,column_length  = get_order_and_summary2(summary)
        log.info("get_order_and_summary")
    except Exception as e:
        error_log.error("get_order_and_summary : {}".format(e))

    try:
        load_gk2a_start = datetime.datetime.now()
        final_array, client = making_detectable_df_from_nas2(final_path_list, input_columns_order, column_length)
        load_gk2a_end = datetime.datetime.now()
        print("loading gk2a Duration : {}".format(load_gk2a_end - load_gk2a_start))
        log.info("making_detectable_df_from_nas2")
    except Exception as e:
        error_log.error("making_detectable_df_from_nas2 : {}".format(e))

    try:
        detectable_df = making_detectable_df_use_zenith2(final_array, ticketNo, input_columns_order, client)
        log.info("making_detectable_df_use_zenith")
    except Exception as e:
        error_log.error("making_detectable_df_use_zenith : {}".format(e))

    try:
        detect_df = making_normalization_data(detectable_df, summary)
        log.info("making_normalization_data")
    except Exception as e:
        error_log.error("making_normalization_data : {}".format(e))

    return detect_df


#df = main2(201909062210, "local")




# #
# path = '/hadoop/hdfs/data/PARSER/GK2A/VI004/201909/06/gk2a_ami_le1b_vi004_ko010lc_201909062210.nc.Albedo'
# client = InsecureClient("http://192.168.0.179:50070", user="hdfs")
# with client.read(path) as file:
#     temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
#
# temp_array.shape
