#!/usr/bin/env python
import sqlalchemy
from sqlalchemy import create_engine
import json
import pandas as pd
from sqlalchemy import create_engine, types, select
from tensorflow import feature_column
from tensorflow.keras import layers, models , regularizers
from tensorflow import keras
from sklearn.model_selection import train_test_split
import tensorflow as tf
import argparse
import numpy as np
from datetime import datetime
import os
from sklearn.metrics import confusion_matrix , precision_score , recall_score, f1_score , accuracy_score , precision_score
from collections import Counter
from imblearn.over_sampling import SMOTE
from scipy import stats
import smogn
import fog_logging

MODEL_DIR = 'fog_model'

#학습데이터 가져오는 함수
class connect_sql:
    def __init__(self,host):
        self.host = host
    def connect_db(self,db_name):
        self.db_name = db_name
        self.engine = create_engine('mysql+mysqlconnector://root:123@'+self.host +'/'+self.db_name) # rootnme:psw
        self.conn = self.engine.connect()
    def get_column_names(self,table_name):
        self.table_name = table_name
        query  = 'show columns from ' + self.table_name
        result = self.conn.execute(query).fetchall()
        self.columns = [x[0] for x in result]
    def get_data(self,columns,table_name):
        self.table_name = table_name
        query = 'select ' + columns + " from " + self.table_name
        result = self.conn.execute(query).fetchall()
        self.data = result
    def get_whole_data_frame(self, table_name):
        self.get_data("*",table_name)
        data = self.data
        self.get_column_names(table_name)
        columns = self.columns
        df = pd.DataFrame(data, columns = columns)
        self.df = df
    def get_joined_data(self,first_table_name, second_table_name):
        columns = "*"
        # self.table_name = table_name
        detail_time = 'detail_time'
        query = 'SELECT {} FROM {}, {} WHERE {}.{}={}.{};'.format(columns,first_table_name,second_table_name,first_table_name,detail_time,second_table_name,detail_time)
        result = self.conn.execute(query).fetchall()
        self.data = result
        self.get_column_names(first_table_name)
        self.first_cols = self.columns
        self.get_column_names(second_table_name)
        self.second_cols = self.columns
        final_cols = self.first_cols
        for col in self.second_cols:
            final_cols.append(col)
        self.final_cols = final_cols
        final_df = pd.DataFrame(self.data , columns = self.final_cols )
        final_df = final_df.loc[:,~final_df.columns.duplicated()]
        self.final_df = final_df
    def close_connection(self):
        self.conn.close()




def get_whole_data_frame(host, database , table_name):
    connect_db  = connect_sql(host)
    connect_db.connect_db(database)
    connect_db.get_whole_data_frame(table_name)
    df = connect_db.df
    connect_db.close_connection()
    return df

def get_whole_joined_data_frame(host, database , first_table_name ,second_table_name):
    connect_db = connect_sql(host)
    connect_db.connect_db(database)
    connect_db.get_joined_data(first_table_name , second_table_name)
    final_df = connect_db.final_df
    connect_db.close_connection()
    return final_df



#모델을 저장하는 함수
def model_dir_save(model, model_path, model_name, feature_summary):
    now_time = datetime.now().strftime("%Y%m%d-%H%M")
    #model_name = "{}_{}.hdf5".format(model_name, now_time)
    model_name = "{}_{}".format(model_name, now_time)
    model_name_path = model_path + '/' + model_name

    #model.save(model_name_path)
    # models.save_model(model, model_name_path)
    tf.keras.models.save_model(model, model_name_path)
    feature_summary.to_csv(model_name_path + "/feature_summary.csv", index = True)

    return model_name_path
    # tf.keras.experimental.export_saved_model(model, model_name_path)


#모델저장할 패스를 가져오는함수
def make_dir_for_savingmodel():
    now_dir = os.getcwd()
    model_path = os.path.join(now_dir, MODEL_DIR)
    is_dir = os.path.isdir(model_path)
    if not is_dir: os.mkdir(model_path)

    return model_path



# 사용할 GPU 선택
def set_gpu(gpu_num):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')   # 물리적 GPU리스트
    if physical_devices != []:
        tf.config.experimental.set_visible_devices(physical_devices[gpu_num], 'GPU')   # 3번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.experimental.set_memory_growth(physical_devices[gpu_num], True) # 현재 사용중인 GPU의 메모리를 사용하는 만큼만 메모리 적용
#     logical_devices = tf.config.experimental.list_logical_devices('GPU')



#모델 학습 그래프확인을 위한 함수
def plot_history(hist, path):
    import matplotlib.pyplot as plt

    fig, loss_ax = plt.subplots()

    acc_ax = loss_ax.twinx()

    loss_ax.plot(hist.history['loss'], 'y', label='train loss')
    loss_ax.plot(hist.history['val_loss'], 'r', label='val loss')

    acc_ax.plot(hist.history['mae'], 'b', label='train mae')
    acc_ax.plot(hist.history['val_mae'], 'g', label='val mae')

    loss_ax.set_xlabel('epoch')
    loss_ax.set_ylabel('loss')
    acc_ax.set_ylabel('mae')

    loss_ax.legend(loc='upper left')
    acc_ax.legend(loc='lower left')

    plt.savefig("{}/model_history.png".format(path))

def df_to_dataset(dataframe, shuffle=True, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop('target')
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))

    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))

    ds = ds.batch(batch_size)
    return ds, labels




def delete_unnessary(df):
    df_listing  = list(df.columns)
    if 'hour_rain' in df_listing: df = df.drop(['hour_rain'], axis=1)
    if 'all_rain' in df_listing: df = df.drop(['all_rain'], axis=1)
    if 'detail_time' in df_listing: df = df.drop(['detail_time'], axis=1)
    df = df.dropna()  #### if there is something else include
    return df



def preprocess(data,hsr_decision = None,target_decision = None):
    df = data.copy()
    hsr_value = df.pop("HSR_value")
    if hsr_decision == "all":
        final_hsr = list(map(lambda x: 0 if x  == -25000 or x == -200 else x , hsr_value ))
    elif hsr_decision == "-25000":
        final_hsr = list(map(lambda x: -1 if x == -25000 else (0 if x == -200 else x), hsr_value  ))
    elif hsr_decision == "-200":
        final_hsr = list(map(lambda x: -1 if x == -25000 or x == -200 else x , hsr_value))
    df.loc[:,'HSR_value'] = final_hsr
    df = df[df['target']>=0]
    df = df[df['HSR_value']>=0]
    df = df.astype(float)
    label = df.pop('target')
    if target_decision == "all":
        values = list(map(lambda x: int(x), label))
    else:
        value = int(target_decision)    ### string number
        values = list(map(lambda x: value if x > value else int(x), label))
    df["target"] = values
    df = df.dropna()
    return df

def oversampling(df):
    target_class = list(map(lambda x: 1 if x < fog_distance else 0, df.target.tolist() ))
    df["targetclass"] = target_class
    X = df.loc[:, df.columns != 'targetclass']
    y = df.targetclass
    seed = 100
    k = 1
    sm =  SMOTE(sampling_strategy='auto', k_neighbors=k, random_state=seed)
    X_res, y_res = sm.fit_resample(X, y)
    new_df = pd.concat([pd.DataFrame(X_res), pd.DataFrame(y_res)], axis=1)
    del new_df["targetclass"]
    return new_df

def stding_features(df2, features_summary):
    df = df2.copy()
    label = df.pop('target')
    normed_df = pd.DataFrame()
    columns = df.columns.tolist()
    if "target" in columns: columns.remove("target")
    for i in columns:
        normed_values =  (df.loc[:,i]  - features_summary.loc["mean", i]) / features_summary.loc["std", i]
        normed_df[i] = normed_values
    normed_df["target"] = label
    return normed_df

def delete_non_continuous_target(data):
    df = data.copy()
    bool_list = list(map(lambda x: x != 12000 and x != 15000 and x != 10000 , df.target.tolist()))
    df = df.loc[bool_list, :]
    return df

def validate(test_data ,test_ds ,  model , path , file_name , binary_fog_distance  ):
    df = test_data.copy()
    res_loss, res_mae = model.evaluate(test_ds)
    print("MAE : {}".format(res_mae))
    result = model.predict(test_ds , batch_size = None)

    true_vis_distance = df.target.tolist()
    pred_vis_distance = result.flatten().tolist()

    dist = {"true_vis_distance": true_vis_distance , "pred_vis_distance":pred_vis_distance }
    output_df = pd.DataFrame(dist)
    output_df.to_csv("{}/distribution_{}.csv".format(path, file_name))
    try:
        output_df.plot.kde().figure.savefig("{}/distribution_picture_{}.png".format(path, file_name))
    except:
        pass

    true_vis_binary = list(map(lambda x: 1 if x <= binary_fog_distance else 0 , true_vis_distance))
    pred_vis_binary = list(map(lambda x: 1 if x <= binary_fog_distance else 0 , pred_vis_distance))

    make_metrics(true_vis_binary , pred_vis_binary, path, file_name , res_mae )


def make_metrics(y_true, y_pred, path, file_name , mae ):
    confusionmatrix = confusion_matrix(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred)
    acc = accuracy_score(y_true, y_pred)
    print('res_confusion_matrix : {}'.format(confusionmatrix))
    print('res_precision : {}'.format(precision))
    print('res_recall : {}'.format(recall))
    print('res_f1 : {}'.format(f1))
    print("accuracy : {}".format(acc))
    score_df = pd.DataFrame({"MAE":[mae], "precision":[precision], "reacll":[recall] , "f1":[f1] , "matrix":[str(confusionmatrix)] })
    score_df.to_csv("{}/scores_{}.csv".format(path, file_name))


def my_main():
    host = "192.168.0.181"
    database = "fog_data"
    first_table_name = "gk2a_training_data_day_by201909"
    second_table_name = "hsr_data_appending_201909"
    merged_data = get_whole_joined_data_frame(host, database, first_table_name , second_table_name)



    first_table_name = "gk2a_training_data_day_by201910"
    second_table_name = "hsr_data_appending_201910"
    merged_data2 = get_whole_joined_data_frame(host, database, first_table_name , second_table_name)



    #extraction_date = 201910250000
    all_data = merged_data.append(merged_data2)
    all_data = all_data.reset_index(drop = True)


    all_data, saved_data = train_test_split(all_data, test_size=0.1)
    #dates = list(map(lambda x: int(x.split("_")[0]),  all_data.detail_time ))
    #whole_data = all_data.loc[ (np.array(dates) < extraction_date ).tolist() , : ]
    #saved_data = all_data.loc[ (np.array(dates) >= extraction_date ).tolist() , : ]
    whole_data = all_data.copy()


    # ------------------------- whole_data ------------------------------------
    merged_data_deleted = delete_unnessary(whole_data)  #### include dropna
    processed_df = preprocess(data = merged_data_deleted ,hsr_decision = "-25000",target_decision = "all" )


    ################   for train data only #####################
    processed_df = delete_non_continuous_target(data = processed_df)  # making target continuous
    processed_df =  processed_df[(np.abs(stats.zscore(processed_df)) < 3).all(axis=1)]  # delete outliers
    ############################################################


    train, test = train_test_split(processed_df, test_size=0.1)
    train, val = train_test_split(train, test_size=0.1)

    features_summary = train.describe()

    ###############  standardizing ######################
    normed_train = stding_features(train, features_summary)
    normed_val  = stding_features(val , features_summary)
    normed_test = stding_features(test , features_summary)
    ####################################################

    batch_size = 32
    train_ds, _ = df_to_dataset(normed_train, shuffle= True,batch_size=batch_size)
    val_ds, _ = df_to_dataset(normed_val, shuffle=True, batch_size=batch_size)
    test_ds, test_labels = df_to_dataset(normed_test, shuffle=False, batch_size=batch_size)

    # model building
    feature_columns = []
    train_columns = train.columns.tolist()
    if "target" in train_columns: train_columns.remove("target")
    for header in train_columns:
        feature_columns.append(feature_column.numeric_column(header))
    feature_layer = tf.keras.layers.DenseFeatures(feature_columns)

    loss_fuc = 'mean_squared_logarithmic_error'
    #loss_fuc = "mean_absolute_percentage_error"
    optimizer='adam'
    # optimizer=tf.keras.optimizers.RMSprop(0.001)
    # with tf.device('/device:GPU:0'):
    model = tf.keras.Sequential([
        feature_layer,
        layers.Dense(1200, activation='relu', kernel_regularizer=regularizers.l2(0.001)), # faster than sigmoid or tanh(Hyper tangen)
        layers.Dropout(0.25),
        layers.Dense(600, activation='relu', kernel_regularizer=regularizers.l2(0.001)), # fully-connected layer
        layers.Dropout(0.25),
        layers.Dense(300, activation='relu' ,kernel_regularizer=regularizers.l2(0.001)),
        layers.Dropout(0.25),
        layers.Dense(150, activation='relu'),
        layers.Dropout(0.25),
        layers.Dense(75, activation='relu'),
        layers.Dense(32, activation='relu'),
        #layers.Dense(32, activation='relu'),
        #layers.Dense(16, activation='relu'),
        layers.Dense(1)])

    model.compile(optimizer=optimizer,
                loss=loss_fuc,
                metrics=['mae'])

    model_path = make_dir_for_savingmodel()

    history = model.fit(train_ds,
              validation_data=val_ds,
              epochs=1500)

    model_name = 'fog_model'
    model_name_path = model_dir_save(model, model_path, model_name, features_summary)
    validate(test_data=normed_test,test_ds=test_ds,model=model,path=model_name_path,file_name="test_data",binary_fog_distance=4800)

    # ------------------------- saved_data ------------------------------------
    saved_data_deleted = delete_unnessary(saved_data)

    saved_processed = preprocess(data = saved_data_deleted ,hsr_decision = "-25000",target_decision = "all" )

    normed_saved = stding_features(saved_processed , features_summary)
    saved_ds, _ = df_to_dataset(normed_saved , shuffle=False, batch_size=batch_size)


    validate(test_data=normed_saved,test_ds=saved_ds,model=model,path=model_name_path,file_name="saved_data",binary_fog_distance=4800)

    plot_history(history, model_name_path)


if __name__ == '__main__':
    my_main()
