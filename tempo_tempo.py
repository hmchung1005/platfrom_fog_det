import pandas as pd
import numpy as np
from fog_config import Config
from datetime import datetime
from datetime import timedelta
import argparse
import tensorflow as tf
from fog_config import Config

import fog_logging
import logging
import os
import fog_preprocess
import fog_postprocess
import fog_database
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"] = ""

def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta


def set_globvar_to_path(env):
    #tempo_path = Config.tempo_file
    if env == "local":
        root_path = Config.root_local_path
    elif env == "platform":
        root_path = Config.root_platform_path
    elif env == "dgx_share2":
        root_path = Config.root_dgx_path
    return root_path



def sub(ticketNo, env , params , log , error_log):

    result_df = fog_preprocess.main_original(ticketNo, env , params["summary_path"] ,  log , error_log)

    final_df , watch_warn_df_list = fog_postprocess.main(ticketNo, result_df, env , params ,log , error_log )

    fog_database.main(final_df , watch_warn_df_list, env , log, error_log )


def main(start_date , end_date , env):
    log = fog_logging.get_log_view(1, "local", False, "main_log" )
    error_log = fog_logging.get_log_view(1, "local", True, "main_error_log" )

    fmt = '%Y%m%d%H%M'
    start = datetime.strptime(str(start_date), fmt)
    end = datetime.strptime(str(end_date), fmt)
    timelist = []
    for result in perdelta(start , end , timedelta(minutes=10)):
        timelist.append(result)
    timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , timelist ))
    root_path = set_globvar_to_path(env)
    dnn_model = tf.keras.models.load_model("transformed_binaryoversampling_category_logit")
    summary_path = "transformed_binaryoversampling_category_logit/feature_summary.csv"
    model_path = root_path
    params = {'model_path': model_path, 'saved_model':dnn_model , 'summary_path':summary_path }

    for ticketNo in timelist:
        print(ticketNo)

        try:
            start_clock = datetime.now()
            sub(ticketNo, env , params, log, error_log )
            end_cock = datetime.now()
            dif = end_cock -start_clock
            log.info("one time done : {} , time duration : {}".format(ticketNo , dif))

        except Exception as e:
            error_log.error("error occured : {}".format(e))
            continue



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some info.')
    parser.add_argument('-s', '--star_date',dest = "start_date",type = str)
    parser.add_argument('-d', '--end_date',dest = "end_date",type = str)
    parser.add_argument('-e', '--environment',dest = "env", type = str)
    args = parser.parse_args()
    start_date = args.start_date
    end_date = args.end_date
    env = args.env
    main(start_date , end_date , env)
