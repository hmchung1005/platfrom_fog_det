###############################
# please check ###@@###
#########################

import sys
import os

import time
import csv
import numpy as np
import pandas as pd

import argparse

from fog_config import Config , collecting , fog_model_training_conf , tb_info , fog_total_model_creating , fog_amos_model_creating , fog_main_preprocessing , amos_model_features_labeling


from sklearn.model_selection import train_test_split
import fog_logging
import jaydebeapi
import jpype
import datetime
import json
from pathlib import Path

from tensorflow import feature_column
from tensorflow.keras import layers, models
from tensorflow import keras
import tensorflow as tf

from imblearn.over_sampling import SMOTE
from sklearn.metrics import confusion_matrix , precision_score , recall_score, f1_score , accuracy_score , precision_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle

def set_globvar_to_path(env):
    if env == "local":
        data_collection_path = collecting.data_collection_path_local
        root_path = Config.root_local_path
        model_dir = Config.model_local_dir
    elif env == "dgx_share1":
        data_collection_path = collecting.data_collection_path_dgx_docker_share1
        root_path = Config.root_dgx_path
        model_dir  =  Config.model_dgx_dir
    elif env =="dgx_share2":
        data_collection_path = collecting.data_collection_path_dgx_docker_share2
        root_path = Config.root_dgx_path
        model_dir  =  Config.model_dgx_dir
    elif env == "platform":
        data_collection_path = collecting.data_collection_path_platform
        root_path = Config.root_platform_path
        model_dir  =  Config.model_platform_dir
    return data_collection_path , root_path , model_dir

def delete_cols(df):
    if "table_id" in df.columns.tolist(): del df["table_id"]
    if "tower" in df.columns.tolist(): del df["tower"]
    if "time" in df.columns.tolist(): del df["time"]
    return df


def open_connection(host,db_name,user_name,password, port, root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor

def set_gpu(gpu_num):
    physical_devices = tf.config.experimental.list_physical_devices('GPU')   # 물리적 GPU리스트
    if physical_devices != []:
        tf.config.experimental.set_visible_devices(physical_devices[gpu_num], 'GPU')   # 3번 GPU만 사용(여러개 쓸거면 슬라이싱도 됨)
        tf.config.experimental.set_memory_growth(physical_devices[gpu_num], True)

def delete_cols(df):
    if "table_id" in df.columns.tolist(): del df["table_id"]
    if "tower" in df.columns.tolist(): del df["tower"]
    if "time" in df.columns.tolist(): del df["time"]
    return df

def preprocess(df2,hsr_decision = None,target_decision = None):
    df = df2.copy()
    hsr_value = df.pop("HSR_value")
    if hsr_decision == "all":
        final_hsr = list(map(lambda x: 0 if x  == -25000 or x == -200 else x , hsr_value ))
    elif hsr_decision == "-25000":
        final_hsr = list(map(lambda x: -1 if x == -25000 else (0 if x == -200 else x), hsr_value  ))
    elif hsr_decision == "-200":
        final_hsr = list(map(lambda x: -1 if x == -25000 or x == -200 else x , hsr_value))
    df.loc[:,'HSR_value'] = final_hsr
    df = df[df['target']>=0]
    df = df[df['HSR_value']>=0]
    df = df.astype(float)
    label = df.pop('target')
    if target_decision == "all":
        values = list(map(lambda x: int(x), label))
    else:
        value = int(target_decision)    ### string number
        values = list(map(lambda x: value if x > value else int(x), label))
    df["target"] = values
    df = df.dropna()
    df =  df.astype(np.float32)
    return df

def df_to_dataset(dataframe, shuffle=True, batch_size=32):
    dataframe = dataframe.copy()
    labels = dataframe.pop('target')
    ds = tf.data.Dataset.from_tensor_slices((dict(dataframe), labels))

    if shuffle:
        ds = ds.shuffle(buffer_size=len(dataframe))

    ds = ds.batch(batch_size)
    return ds, labels
def build_model(feature_layer, n_hidden, output_feature, loss_fuc):

    optimizer='adam'
    # optimizer=tf.keras.optimizers.RMSprop(0.01)
    # with tf.device('/device:GPU:0'):
    model = tf.keras.Sequential([
    feature_layer,
    layers.Dense(n_hidden, activation='relu'), # faster than sigmoid or tanh(Hyper tangen)
    # layers.Dropout(0.25),
    layers.Dense(n_hidden/2, activation='relu'), # fully-connected layer
    # layers.Dropout(0.25),
    layers.Dense(n_hidden/4, activation='relu'),
    layers.Dense(n_hidden/8, activation='relu'),
    layers.Dense(n_hidden/16, activation='relu'),
    layers.Dense(n_hidden/32, activation='relu'),
    # layers.Dense(n_hidden/64, activation='relu'),
    layers.Dense(output_feature)])# softmax or sigmoid
    model.compile(optimizer=optimizer,
                loss=loss_fuc, # 이진분류 binary_crossentropy / 1-of-K categorical_crossentropy / mse
                metrics=['mae'])

    return model

def plot_history(hist, path):
    import matplotlib.pyplot as plt

    fig, loss_ax = plt.subplots()

    acc_ax = loss_ax.twinx()

    loss_ax.plot(hist.history['loss'], 'y', label='train loss')
    loss_ax.plot(hist.history['val_loss'], 'r', label='val loss')

    acc_ax.plot(hist.history['mae'], 'b', label='train mae')
    acc_ax.plot(hist.history['val_mae'], 'g', label='val mae')

    loss_ax.set_xlabel('epoch')
    loss_ax.set_ylabel('loss')
    acc_ax.set_ylabel('mae')

    loss_ax.legend(loc='upper left')
    acc_ax.legend(loc='lower left')

    plt.savefig("{}/model_history.png".format(path))


def oversampling(df):
    new_df = df.copy()
    X = new_df.loc[:, new_df.columns != 'targetclass']
    y = new_df.targetclass
    seed = 100 #100
    k = 1 #10
    sm =  SMOTE(sampling_strategy='auto', k_neighbors=k, random_state=seed)
    X_res, y_res = sm.fit_resample(X, y)
    new_new_df = pd.concat([pd.DataFrame(X_res), pd.DataFrame(y_res)], axis=1)
    new_df = new_new_df
    return new_df

def make_metrics(y_true, y_pred, path, file_name ):
    confusionmatrix = confusion_matrix(y_true, y_pred)
    precision = precision_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred)
    print(f1)
    score_df = pd.DataFrame({"precision":[precision], "reacll":[recall] , "f1":[f1] , "matrix":[str(confusionmatrix)] })
    score_df.to_csv("{}/scores_{}.csv".format(path, file_name))

def validate(true_vis_distance, pred_vis_distance ,path, file_name,fog_distance ):
    dist = {"true_vis_distance": true_vis_distance , "pred_vis_distance":pred_vis_distance }
    df = pd.DataFrame(dist)
    df.to_csv("{}/distribution_{}.csv".format(path, file_name))
    try:
        df.plot.kde().figure.savefig("{}/distribution_picture_{}.png".format(path, file_name))
    except:
        pass
    true_vis_binary = list(map(lambda x: 1 if x < fog_distance else 0 , true_vis_distance))
    pred_vis_binary = list(map(lambda x: 1 if x < fog_distance else 0 , pred_vis_distance))

    make_metrics(true_vis_binary , pred_vis_binary, path, file_name )



#모델저장할 패스를 가져오는함수
def model_dir_save(model, model_path, name, feature_summary):

    model_name_path = model_path + '/' + name

    tf.keras.models.save_model(model, model_name_path)
    feature_summary.to_csv(model_name_path + "/feature_summary.csv", index = True)

    return model_name_path


def norming_features(df2 ,feature_summary):
    df = df2.copy()
    if "target" in df.columns.tolist():
        target = df.pop("target")
        target_exist = True
    else:
        target_exist = False

    normed_df = pd.DataFrame()
    columns = df.columns.tolist()
    for col in columns:
        min = feature_summary.loc["min" , col]
        max = feature_summary.loc["max" , col]
        if min == max:
            normed_df[col] = 0
            continue
        normed_data = (df.loc[:,col] - min) / (max - min)
        normed_df[col] = normed_data
    if target_exist:
        normed_df["target"] = target
    return normed_df

def norming_features_amos(df2 ,feature_summary):
    df = df2.copy()
    normed_df = pd.DataFrame()
    columns = df.columns.tolist()
    for col in columns:
        min = feature_summary.loc["min" , col]
        max = feature_summary.loc["max" , col]
        if min == max:
            normed_df[col] = 0
            continue
        normed_data = (df.loc[:,col] - min) / (max - min)
        normed_df[col] = normed_data
    return normed_df


def transform(data , chanels):
    df = data.copy()
    all_chanels = chanels.copy()
    all_columns = df.columns.tolist()
    transformed_df  = pd.DataFrame()
    for channel in all_chanels:
        sub_channels = [x for x in df.columns.tolist() if channel in x]
        sub_channels.sort(reverse = True)
        for sub_channel in sub_channels: all_columns.remove(sub_channel)
        transformed_df[channel] =  df.loc[:,sub_channels].values.tolist()
    for left_column in all_columns:
        transformed_df[left_column] = df[left_column].values.tolist()
    return transformed_df

def load_table_tibero(train_start_ticket,train_end_ticket, table_name,cursor):

    column_sql = "select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='{}';".format(table_name.upper())
    cursor.execute(column_sql)
    column_names = cursor.fetchall()
    column_names = list(map(lambda x: x[0].lower()  , column_names  ))

    data_sql = "select * from {} where time between {} and {};".format(table_name, (train_start_ticket - 1) , (train_end_ticket + 1) )
    cursor.execute(data_sql)
    data = cursor.fetchall()
    df = pd.DataFrame(data, columns = column_names)
    return df

def add_linear_layer(results):
    min_results = results.min(axis = 1)
    min_results = min_results[..., np.newaxis ]
    max_results = results.max(axis=1)
    max_results = max_results[..., np.newaxis]
    max_minus_min = max_results - min_results
    min_max_results = (results - min_results) / (max_minus_min)
    classes = np.argmax(min_max_results, axis=1)
    high_percentage = sum(np.equal(classes , 5)) / len(classes)
    low_percentage = sum(np.equal(classes , 0 )) / len(classes)
    modified_classes = classes  + 1
    modified_min_max = np.insert(min_max_results , 0 , low_percentage , axis = 1)
    modified_min_max = np.insert(modified_min_max , 7 , high_percentage , axis = 1)
    modified_classes_minus_one = modified_classes - 1
    modified_classes_plus_one = modified_classes + 1
    minus_ratio = list(map(lambda x ,y : x[y] , modified_min_max , modified_classes_minus_one ))
    minus_ratio = np.array(minus_ratio)
    plus_ratio = list(map(lambda x, y : x[y] , modified_min_max , modified_classes_plus_one))
    plus_ratio = np.array(plus_ratio)
    minus_sum = minus_ratio  * -490
    #plus_sum =  np.array(list(map(lambda x, y: x * 510 if y == 4 else x *500 , plus_ratio , classes  )))
    plus_sum = plus_ratio * 500
    sum_sum = minus_sum  + plus_sum
    vis_pred_distance = classes * 1000 + 500
    vis_pred_modified = vis_pred_distance + sum_sum
    return vis_pred_modified

def preprocess2(df):
    processed_df = df.copy()
    train, test = train_test_split(processed_df, test_size=0.1)
    saving_number_test_target = test.target.tolist()


    cate_values_train = list(map(lambda x: int(x/1000) , train.target  ))
    train["target"] = cate_values_train

    cate_values_test = list(map(lambda x: int(x/1000) , test.target  ))
    test["target"] = cate_values_test

    train, val = train_test_split(train, test_size=0.1)
    return train, val , test, saving_number_test_target

def make_3_elements(data , cols):
    df = data.copy()
    columns = cols.copy()
    for col in columns:
        if type(df[col].iloc[0]) == list:
            if len(df[col].iloc[0]) == 2:
                df[col] = list(map(lambda x: [x[0] , (x[0] + x[1]) / 2, x[1] ] , df[col] ))
            elif len(df[col].iloc[0]) == 3:
                df[col]  = df[col].tolist()

        else:
            df[col] = list(map(lambda x: [x, x, x] , df[col]  ))
    return df


def create_deeper_feature_columns(data):
    df  = data.copy()
    columns = df.columns.tolist()
    if "target" in columns: columns.remove("target")
    feature_columns = []
    for col in columns:
        tempo_value = df[col].iloc[0]
        if len(np.array(tempo_value).shape) ==2:
            tempo_shape=  np.array(tempo_value).shape
            feature_columns.append(feature_column.numeric_column(col, shape = [tempo_shape[0], tempo_shape[1]]  ) )
        elif len(np.array(tempo_value).shape) == 1:
            length = len(tempo_value)
            feature_columns.append(feature_column.numeric_column(col, shape = (length,)  ) )
        elif len(np.array(tempo_value).shape) ==0 :
            feature_columns.append(feature_column.numeric_column(col))
    return feature_columns

def preprocess_target(data , target_decision):
    df = data.copy()
    df = df[df['target']>=0]
    df = df.reset_index(drop =True)
    detail_time = df.pop("detail_time")
    df = df.astype(float)
    label = df.pop("target")
    if target_decision == "all":
        values = list(map(lambda x: int(x)) , label)
    else:
        values = int(target_decision)
        values = list(map(lambda x: values if x > values else int(x) , label))
    df["target"] = values
    # df = df.astype(np.float32)
    df["detail_time"] = detail_time
    df = df.dropna()
    return df


def deeper_4d_transform(data):
    df = data.copy()
    detail_time = df.pop('detail_time')
    features_summary = df.describe()
    normed_df = norming_features(df , features_summary)
    using_chanels =  ['sw038','ir087','ir096','ir105','ir112','ir123','ir133','nr013','nr016','vi004','vi005','vi006','vi008','wv063','wv069','wv073']
    transformed_df = transform(normed_df , using_chanels)
    target = transformed_df.pop("target")
    columns = transformed_df.columns.tolist()
    transformed_df2 = make_3_elements(transformed_df , columns)
    deeper_df = transformed_df2.copy()
    time_list = list(map(lambda x: int(x.split("_")[0]) , detail_time))
    tower_list = list(map(lambda x: int(x.split("_")[1]) , detail_time))
    deeper_df = deeper_df.reset_index(drop = True)
    deeper_df["tower"] = tower_list
    deeper_df["now_time"] = time_list
    deeper_df["target"] = target
    tower_names = deeper_df.tower.unique().tolist()

    final_x_arrays = np.zeros((1,6,18,3))
    final_y_arrays = np.zeros((1,1))

    for tower in tower_names:

        tempo_df  = deeper_df.loc[deeper_df["tower"] == tower , :]
        tempo_df["real_time"] = pd.to_datetime(tempo_df['now_time'], format = "%Y%m%d%H%M")

        tempo_df = tempo_df.set_index("real_time")

        end = tempo_df.index.max()
        now  = tempo_df.index.min()
        all_times=[]
        while now < end:
            all_times.append(now)
            now+=  datetime.timedelta(minutes=10)

        counting = 0
        for each_time in all_times[5:]:
            try:
                time_00 = each_time - datetime.timedelta(minutes = 10)
                time_02 = each_time - datetime.timedelta(minutes = 8)
                time_04 = each_time - datetime.timedelta(minutes = 6)
                time_06 = each_time - datetime.timedelta(minutes = 4)
                time_08 = each_time - datetime.timedelta(minutes=2)
                current_time = [time_00 , time_02 , time_04 , time_06 , time_08 , each_time ]
                each_data = np.array([tempo_df.loc[current_time ,:'HSR_value'].values.tolist()])
            except:
                continue
            if each_data.shape == (1,6,18,3):
                counting = counting + 1

        x_arrays = np.zeros((counting,6,18,3))
        y_arrays = np.zeros((counting, 1) )
        indexing = 0
        for each_time in all_times[5:]:
            try:
                try:
                    time_00 = each_time - datetime.timedelta(minutes = 10)
                    time_02 = each_time - datetime.timedelta(minutes = 8)
                    time_04 = each_time - datetime.timedelta(minutes = 6)
                    time_06 = each_time - datetime.timedelta(minutes = 4)
                    time_08 = each_time - datetime.timedelta(minutes=2)
                    current_time = [time_00 , time_02 , time_04 , time_06 , time_08 , each_time ]
                    each_data = np.array([tempo_df.loc[current_time ,:'HSR_value'].values.tolist()])

                except:
                    continue
                if each_data.shape == (1,6,18,3):
                    result = np.array(  [ float(tempo_df.loc[each_time, "target"] )  ] )
                    x_arrays[indexing] = each_data

                    y_arrays[indexing] = result
                    indexing = indexing + 1
            except Exception as e:

                continue
        final_x_arrays = np.concatenate((final_x_arrays , x_arrays))
        final_y_arrays = np.concatenate((final_y_arrays , y_arrays))
    return final_x_arrays, final_y_arrays , features_summary

def simple_process_gk2a(gk2a_df):
    merged_data = delete_cols(gk2a_df)
    merged_data = merged_data.drop_duplicates(subset='time_base' , keep ='last')
    merged_data = merged_data.reset_index(drop =True )
    detail_time = merged_data.pop("time_base")
    for i in merged_data.columns.tolist():
        if i != "zenith" and i != "target":
            temp_name=  i.split("_")[0].lower() +  "_" +i.split("_")[1].capitalize()
            temp_dic = {i:temp_name}
            merged_data = merged_data.rename(columns = temp_dic)
    merged_data["detail_time"] = detail_time
    return merged_data


def preprocess_hsr(data , hsr_decision):
    df = data.copy()
    df = df.drop_duplicates(subset='time_base' , keep ='last')
    df = df.rename(columns= {"hsr_value": "HSR_value" , "time_base":"detail_time"})
    df = delete_cols(df)
    df = df.reset_index(drop = True)
    hsr_value = df.pop("HSR_value")
    if hsr_decision == "all":
        #final_hsr = list(map(lambda x: 0 if x  == -25000 or x == -200 else x , hsr_value ))
        final_hsr = list(map(lambda x: 0 if x < 0 else x , hsr_value))
    elif hsr_decision == "-25000":
        final_hsr = list(map(lambda x: -1 if x == -25000 else (0 if x == -200 else x), hsr_value  ))
    elif hsr_decision == "-200":
        final_hsr = list(map(lambda x: -1 if x == -25000 or x == -200 else x , hsr_value))
    df.loc[:,'HSR_value'] = final_hsr
    #df = df[df['HSR_value']>=0]
    df["HSR_value"] = df["HSR_value"].astype(float)
    #df = df.dropna()
    return df


def save006_data_modified(gk2a_df):

    df = gk2a_df.copy()

    original_columns = df.columns.tolist()
    tower_list = list(map(lambda x: int(x.split("_")[1]) , df.detail_time  ))
    time_list=  list(map(lambda x: int(x.split("_")[0]) , df.detail_time))

    df["tower"]  = tower_list
    df["now_time"] = time_list
    tower_names = df.tower.unique().tolist()

    final_df = pd.DataFrame()

    for tower in tower_names:


        tempo_df = df.loc[df["tower"] == tower  , :]
        tempo_df = tempo_df.drop_duplicates(subset = "now_time" , keep = "last")

        tempo_df= tempo_df.sort_values("now_time")

        groups = list(map(lambda x:  int(x / 10) * 10 , tempo_df.now_time ))
        tempo_df["groups"] = groups
        individuals = list(map(lambda x: int(str(x)[-1]) , tempo_df.now_time  ))
        tempo_df["individuals"] = individuals

        unique_groups = tempo_df.groups.unique()

        tempo_df["rad_start_value"] = None
        tempo_df["alb_start_value"] = None
        tempo_df["rad_slope"] = None
        tempo_df["alb_slope"] = None

        for group in unique_groups:
            try:
                begin_rad_value = tempo_df.loc[tempo_df["now_time"] == group , "vi006_Radiance"].values[0]
                begin_alb_value = tempo_df.loc[tempo_df["now_time"] == group , "vi006_Albedo"].values[0]

                tempo_df.loc[tempo_df["groups"] == group, "rad_start_value"] = begin_rad_value
                tempo_df.loc[tempo_df["groups"] == group, "alb_start_value"] = begin_alb_value

                end_time = datetime.datetime.strptime(str(group), "%Y%m%d%H%M") + datetime.timedelta(minutes=  10)
                end_now_time =  int(end_time.strftime("%Y%m%d%H%M"))
                end_rad_value = tempo_df.loc[tempo_df["now_time"] == end_now_time , "vi006_Radiance"].values[0]
                end_alb_value = tempo_df.loc[tempo_df["now_time"] == end_now_time , "vi006_Albedo"].values[0]

                rad_slope = (end_rad_value  - begin_rad_value) / 6
                alb_slope = (end_alb_value  - begin_alb_value) / 6

                tempo_df.loc[tempo_df["groups"] == group, "rad_slope"] = rad_slope
                tempo_df.loc[tempo_df["groups"] == group , "alb_slope"] = alb_slope
            except:
                continue


        new_rad_values  =   tempo_df["rad_slope"] * tempo_df["individuals"]  + tempo_df["rad_start_value"]
        new_alb_values  =  tempo_df["alb_slope"] * tempo_df["individuals"] + tempo_df["alb_start_value"]
        tempo_df["vi006_Radiance"] = new_rad_values
        tempo_df["vi006_Albedo"] = new_alb_values
        tempo_df = tempo_df[original_columns]
        final_df=  pd.concat([final_df , tempo_df])
    final_df = final_df.reset_index(drop = True)
    return final_df

def validate(true_vis_distance, pred_vis_distance ,path, file_name , class_div):
    true_vis_binary = list(map(lambda x: 1 if x < class_div else 0 , true_vis_distance))
    pred_vis_binary = list(map(lambda x: 1 if x < class_div else 0 , pred_vis_distance))
    make_metrics(true_vis_binary , pred_vis_binary, path, file_name )

# D_FOG_DET_TRAIN_GK2A 테이블로부터 데이터 로드
# D_FOG_DET_TRAIN_HSR 테이블로부터 데이터 로드
def get_whole_data(train_start_ticket , train_end_ticket, env , log , error_log):
    train_start_ticket = int(train_start_ticket)
    train_end_ticket = int(train_end_ticket) + 1
    data_collection_path , root_path , model_dir = set_globvar_to_path(env)


    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port

    conn, cursor = open_connection(host,db_name,user_name,password,port, root_path)
    gk2a_table_name = "d_fog_det_train_gk2a"
    hsr_table_name = "d_fog_det_train_hsr"

    gk2a_df = load_table_tibero(train_start_ticket,train_end_ticket, gk2a_table_name,cursor)
    gk2a_df = simple_process_gk2a(gk2a_df)
    log.info("load tibero gk2a")


    #gk2a_df_modified = save006_data_modified(gk2a_df)

    gk2a_df_modified = gk2a_df.copy()
    gk2a_df_modified2 = gk2a_df_modified.copy()
    ####@@####
    #gk2a_df_modified2 = gk2a_df_modified.dropna()
    ####@@####

    hsr_df =  load_table_tibero(train_start_ticket , train_end_ticket , hsr_table_name , cursor)

    log.info("load tibero hsr")

    hsr_df_modified =preprocess_hsr(hsr_df , "all")


    #hsr_df_modified2 = save_hsr_modified(hsr_df_modified)
    hsr_df_modified2 =  hsr_df_modified.copy()

    return gk2a_df_modified2 , hsr_df_modified2



def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta

#
# def deeper_4d_transform_using_pandas(df):
#     detail_time = df.pop('detail_time')
#     target = df.pop("target")
#     features_summary = df.describe()
#     normed_df = norming_features(df , features_summary)
#     final_cols_list = pd.Series(np.array(fog_config.final_conv_shape_format).flatten()).drop_duplicates().tolist()
#
#     new_df = pd.DataFrame()
#
#     for col in final_cols_list:
#         if ":" not in col:
#             new_df[col] = normed_df[col]
#         else:
#             temp_cols = col.split(":")
#             first_col = temp_cols[0]
#             second_col = temp_cols[1]
#             new_val = (normed_df[first_col] + normed_df[second_col])/2
#             new_df[col] = new_val
#
#
#     time_list = list(map(lambda x: int(x.split("_")[0]) , detail_time))
#     tower_list = list(map(lambda x: int(x.split("_")[1]) , detail_time))
#     deeper_df = new_df.reset_index(drop = True)
#     deeper_df["tower"] = tower_list
#     deeper_df["now_time"] = time_list
#     deeper_df["target"] = target
#
#     tower_names = deeper_df.tower.unique().tolist()
#
#     final_x_arrays = np.zeros((1,6,18,3))
#     final_y_arrays = np.zeros((1,1))
#
#     for tower in tower_names:
#         tempo_df  = deeper_df.loc[deeper_df["tower"] == tower , :]
#         tempo_df["real_time"] = pd.to_datetime(tempo_df['now_time'], format = "%Y%m%d%H%M")
#
#
#         end = tempo_df.real_time.max()
#         now  = tempo_df.real_time.min()
#
#         all_times=[]
#         while now < end:
#             all_times.append(now)
#             now+=  datetime.timedelta(minutes=2)
#
#
#         all_times_df = pd.DataFrame({"real_time":all_times})
#         tempo_df = tempo_df.merge(all_times_df , how = "outer" , on = "real_time")
#         tempo_df = tempo_df.sort_values("real_time")
#         original_df_cols = tempo_df.columns.tolist()
#         original_df_cols.remove("target")
#
#
#         for timing in fog_model_training_conf.seq_list:
#             tempo_df  = merging_past(tempo_df , timing , original_df_cols)
#
#         tempo_df = tempo_df.dropna()
#         tempo_df = tempo_df.drop(original_df_cols , axis = 1)
#         counting = len(tempo_df)
#         x_arrays = np.zeros((counting,6,18,3))
#         final_conv_shape=  np.array(fog_config.final_conv_shape_format)
#         for rank in list(range(6)):
#             for y , listing in enumerate(final_conv_shape):
#                 for x , col in enumerate(listing):
#                     current_col = col + "_{}".format(str(rank))
#                     x_arrays[:,rank , y , x ] = tempo_df[current_col].values
#
#
#         y_arrays = np.array(tempo_df.target.values.tolist())
#         if len(y_arrays) == counting:
#             y_arrays = y_arrays[..., np.newaxis ]
#         else:
#             print("different length of y and x tower : {}".format(tower))
#             continue
#         final_x_arrays = np.concatenate((final_x_arrays , x_arrays))
#         final_y_arrays = np.concatenate((final_y_arrays , y_arrays))
#     return final_x_arrays , final_y_arrays , features_summary


# def make_total_model(gk2a_df_modified2 , hsr_df_modified2 , log ,  model_path , epoch_num ,  batch_size_num , previous_model):
#     X_arr , Y_arr , features_summary = merge_make_input(gk2a_df_modified2 , hsr_df_modified2)
#     shuffled_x , shuffled_y = shuffle(X_arr[1:] , Y_arr[1:])
#     shuffled_x = shuffled_x[...,np.newaxis]
#     train_num  = int(shuffled_x.shape[0] * fog_model_training_conf.train_percent )
#     val_num = int((shuffled_x.shape[0] - train_num) *  fog_model_training_conf.test_percent + train_num)
#     train_x = shuffled_x[:train_num]
#     train_y = shuffled_y[:train_num]
#
#     val_x = shuffled_x[train_num:val_num]
#     val_y = shuffled_y[train_num:val_num]
#
#     test_x = shuffled_x[val_num: ]
#     test_y = shuffled_y[val_num:]
#
#     if previous_model !="2":
#         from fog_config import total_model_network
#         model = total_model_network()
#     elif previous_model == "2":
#         model = tf.keras.models.load_model("/gisangdan/kans/ai/model-save/fog-det-trac/fog_model_direc")
#
#     log.info("training start")
#     history = model.fit(train_x, train_y, epochs=epoch_num,
#                         validation_data=(val_x, val_y), batch_size = batch_size_num, verbose = 1)
#
#     log.info("training done")
#     model_name_path = model_dir_save(model, model_path, "fog_model_direc", features_summary)
#     final_model_path = model_path + "/fog_model_direc"
#     log.info("model saved at {}".format(final_model_path))
#
#     result = model.predict(test_x , batch_size = None)
#     pred_classes = np.argmax(result, axis=1)
#     real_classes = test_y.flatten()
#     validate(real_classes , pred_classes , model_name_path , "test_data" , 4.5 )
#
#     return final_model_path


# def make_amos_model(gk2a_df_modified2 , hsr_df_modified2, train_start_ticket , train_end_ticket, log , model_path , epoch_num , batch_size_num , previous_model , env):
#
#     X_arr , Y_arr , features_summary =  merge_make_input_amos(gk2a_df_modified2, hsr_df_modified2 , train_start_ticket , train_end_ticket , env)
#
#     shuffled_x , shuffled_y = shuffle(X_arr[1:] , Y_arr[1:])
#     train_num  = int(shuffled_x.shape[0] * fog_model_training_conf.train_percent )
#     val_num = int((shuffled_x.shape[0] - train_num) *  fog_model_training_conf.test_percent + train_num)
#     train_x = shuffled_x[:train_num]
#     train_y = shuffled_y[:train_num]
#
#     val_x = shuffled_x[train_num:val_num]
#     val_y = shuffled_y[train_num:val_num]
#
#     test_x = shuffled_x[val_num: ]
#     test_y = shuffled_y[val_num:]
#
#     train_x , train_y = shuffle(train_x , train_y)
#     val_x , val_y = shuffle(val_x , val_y)
#
#     if previous_model !="2":
#         from fog_config import amos_model_network
#         model = amos_model_network()
#     elif previous_model == "2":
#         model = tf.keras.models.load_model("/gisangdan/kans/ai/model-save/fog-det-trac/fog_model_direc")
#
#     log.info("training start")
#
#     history = model.fit(train_x, train_y, epochs=epoch_num,
#                         validation_data=(val_x, val_y), batch_size = batch_size_num, verbose = 1)
#     log.info("training done")
#     model_name_path = model_dir_save(model, model_path, "amos_model_direc", features_summary)
#     final_model_path = model_path + "/amos_model_direc"
#     log.info("model saved at {}".format(final_model_path))
#
#     result = model.predict(test_x , batch_size = None)
#     pred_classes = np.argmax(result, axis=1)
#     real_classes = test_y.flatten()
#     validate(real_classes , pred_classes , model_name_path , "test_data" , 4.5 )
#     return final_model_path


# def make_total_model(gk2a_df_modified2 , hsr_df_modified2 , log ,  model_path , epoch_num ,  batch_size_num , previous_model):



def save_model_class_info(model_info , model_name_path , total_amos):
    if total_amos == "total":
        file_name = "total_model_info.json"
    elif total_amos == "amos":
        file_name = "amos_model_info.json"
    final_saving_path = os.path.join(model_name_path , file_name)
    with open(final_saving_path , "w") as fp:
        json.dump(model_info , fp)
    fp.close()


def make_model(gk2a_df_modified2 , hsr_df_modified2, log , model_path , train_data_params ,  env , total_amos , train_start_ticket , train_end_ticket):

    custom_features = train_data_params["custom_features"]
    conv1_total  = int(train_data_params["conv1_total"])
    conv2_total = int(train_data_params["conv2_total"])
    dense1_total = int(train_data_params['dense1_total'])
    dense2_total = int(train_data_params['dense2_total'])
    dense1_amos = int(train_data_params['dense1_amos'])
    dense2_amos = int(train_data_params['dense2_amos'])
    previous_model = train_data_params["previous_model"]
    epoch_num = int(train_data_params["epoch_num"])
    batch_size_num = int(train_data_params["batch_size_num"])
    es_patience = int(train_data_params["es_patience"])
    seq_len_total = int(train_data_params['seq_len_total'])
    each_seq_minutes_total = int(train_data_params['each_seq_minutes_total'])
    class_0 = int(train_data_params['weight_0_1000'])     / 10
    class_1 = int(train_data_params['weight_1000_2000'])  / 10
    class_2 = int(train_data_params['weight_2000_3000'])  / 10
    class_3 = int(train_data_params['weight_3000_4000'])  / 10
    class_4 = int(train_data_params['weight_4000_5000'])  / 10
    class_5 = int(train_data_params['weight_5000_all'])   / 10

    optimizer = train_data_params["optimizer"]
    loss_func = train_data_params["loss_func"]
    try:
        learning_rate = float(train_data_params["learning_rate"])
    except:
        learning_rate = 0.01

    if  total_amos == "total" and previous_model != "2":
        if custom_features == "2":
            final_conv_shape_format = fog_total_model_creating.final_conv_shape_format
            sequence_len = seq_len_total
            each_sequence_minutes = each_seq_minutes_total
            model_info = {"final_conv_shape_format":final_conv_shape_format,
                          "sequence_len": sequence_len ,
                          "each_sequence_minutes" : each_sequence_minutes}
        else:
            from fog_config import fog_total_model_fixed
            final_conv_shape_format = fog_total_model_fixed.final_conv_shape_format
            sequence_len =  seq_len_total #fog_total_model_fixed.sequence_len
            each_sequence_minutes = each_seq_minutes_total #fog_total_model_fixed.each_sequence_minutes
            model_info = {"final_conv_shape_format":final_conv_shape_format,
                          "sequence_len": sequence_len ,
                          "each_sequence_minutes" : each_sequence_minutes}

        model_class = fog_main_preprocessing(final_conv_shape_format , sequence_len , each_sequence_minutes)
        #print("found")
        #print("ok1")
        model_class.get_input_shape()
        #print("ok2")
        model_class.getting_final_format()
        #print("going")
        X_arr , Y_arr , features_summary = merge_make_input(gk2a_df_modified2 , hsr_df_modified2 , model_class , log)

    elif total_amos == "amos" and previous_model != "2":
        if custom_features == "2":
            final_conv_shape_format = fog_amos_model_creating.final_shape_format_amos
            total_conv_shape_format =  fog_total_model_creating.final_conv_shape_format
            flatten_total_features  = pd.Series(np.array(total_conv_shape_format).flatten()).drop_duplicates().tolist()
            amos_features = list(amos_model_features_labeling.amos_features_naming.values())
            final_conv_shape_format = list(filter(lambda x: x in flatten_total_features or x in amos_features , final_conv_shape_format  ))

            sequence_len = fog_amos_model_creating.sequence_len
            each_sequence_minutes = fog_amos_model_creating.each_sequence_minutes
            model_info = {"final_conv_shape_format":final_conv_shape_format,
                          "sequence_len": sequence_len ,
                          "each_sequence_minutes" : each_sequence_minutes}

        else:
            from fog_config import fog_amos_model_fixed , fog_total_model_fixed
            final_conv_shape_format = fog_amos_model_fixed.final_shape_format_amos
            total_conv_shape_format = fog_total_model_fixed.final_conv_shape_format
            flatten_total_features  = pd.Series(np.array(total_conv_shape_format).flatten()).drop_duplicates().tolist()
            amos_features = list(amos_model_features_labeling.amos_features_naming.values())
            final_conv_shape_format = list(filter(lambda x: x in flatten_total_features or x in amos_features , final_conv_shape_format  ))
            sequence_len = fog_amos_model_fixed.sequence_len
            each_sequence_minutes = fog_amos_model_fixed.each_sequence_minutes
            model_info = {"final_conv_shape_format":final_conv_shape_format,
                          "sequence_len": sequence_len ,
                          "each_sequence_minutes" : each_sequence_minutes}

        model_class = fog_main_preprocessing(final_conv_shape_format , sequence_len, each_sequence_minutes)
        model_class.get_input_shape()
        model_class.getting_final_format()

        X_arr , Y_arr , features_summary =  merge_make_input_amos(gk2a_df_modified2, hsr_df_modified2 , train_start_ticket , train_end_ticket , env, model_class , log)


    elif previous_model == "2":
        log.info("------------------------warning------------------------------")
        log.info("------------------------warning------------------------------")
        log.info("---------------re-training previous model--------------------")
        log.info("---------------re-training previous model--------------------")
        from fog_config import previous_model_path
        if total_amos =="total":
            model_class_info_path = os.path.join(previous_model_path.previous_model_path ,"total_model_direc/total_model_info.json" )
            with open(model_class_info_path, 'r') as fp:
                model_info = json.load(fp)
            fp.close()

            final_conv_shape_format = model_info["final_conv_shape_format"]
            sequence_len =  model_info["sequence_len"]
            each_sequence_minutes  = model_info["each_sequence_minutes"]
            model_class = fog_main_preprocessing(final_conv_shape_format , sequence_len, each_sequence_minutes)
            model_class.get_input_shape()
            model_class.getting_final_format()

            X_arr , Y_arr , features_summary =  merge_make_input(gk2a_df_modified2 , hsr_df_modified2 , model_class , log)

        elif total_amos =="amos":
            model_class_info_path = os.path.join(previous_model_path.previous_model_path ,"amos_model_direc/amos_model_info.json")
            with open(model_class_info_path, 'r') as fp:
                model_info = json.load(fp)
            fp.close()

            final_conv_shape_format = model_info["final_shape_format_amos"]
            sequence_len = model_info["sequence_len"]
            each_sequence_minutes = model_info["each_sequence_minutes"]

            model_class = fog_main_preprocessing(final_conv_shape_format , sequence_len, each_sequence_minutes)
            model_class.get_input_shape()
            model_class.getting_final_format()

            X_arr , Y_arr , features_summary =  merge_make_input_amos(gk2a_df_modified2, hsr_df_modified2 , train_start_ticket , train_end_ticket , env, model_class , log)

    shuffled_x , shuffled_y = shuffle(X_arr[1:] , Y_arr[1:])
    shuffled_x = shuffled_x[...,np.newaxis]
    train_num  = int(shuffled_x.shape[0] * fog_model_training_conf.train_percent )
    val_num = int((shuffled_x.shape[0] - train_num) *  fog_model_training_conf.test_percent + train_num)
    train_x = shuffled_x[:train_num]
    train_y = shuffled_y[:train_num]

    val_x = shuffled_x[train_num:val_num]
    val_y = shuffled_y[train_num:val_num]

    test_x = shuffled_x[val_num: ]
    test_y = shuffled_y[val_num:]

    train_x , train_y = shuffle(train_x , train_y)
    val_x , val_y = shuffle(val_x , val_y)

    if len(train_x) == 0:
        log.info(" \n\n\n there is zero data for train set -------------> no training \n\n\n")
    elif len(val_x) == 0:
        log.info(" \n\n\n thre is zero data for valid set -------------> no training \n\n\n")
    elif len(test_x) == 0:
        log.info(" \n\n\n thre is zero data for test set -------------> no training \n\n\n")
    else:
        callback = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience= es_patience)
        custom_class_weight = {0 :class_0,
                               1 :class_1,
                               2 :class_2,
                               3 :class_3,
                               4 :class_4,
                               5 :class_5}


        if previous_model !="2":
            if total_amos == "total":
                from fog_config import total_model_network
                func = total_model_network(model_class.final_input_shape)
                model = func(model_class.final_input_shape , conv1_total , conv2_total ,dense1_total , dense2_total, optimizer , loss_func, learning_rate)
            elif total_amos =="amos":
                from fog_config import amos_model_network
                model = amos_model_network(model_class.final_input_shape , dense1_amos , dense2_amos , optimizer , loss_func , learning_rate)
        elif previous_model == "2":
            log.info("training previous model")
            if total_amos == "total":
                model = tf.keras.models.load_model(os.path.join(previous_model_path.previous_model_path , "total_model_direc") )
            elif total_amos =="amos":
                model = tf.keras.models.load_model(os.path.join(previous_model_path.previous_model_path , "amos_model_direc"))


        log.info("training start")

        history = model.fit(train_x, train_y, epochs=epoch_num,
                            validation_data=(val_x, val_y),
                            batch_size = batch_size_num,
                            callbacks=[callback],
                            verbose = 1,
                            class_weight=custom_class_weight)

        log.info("training done")
        model_name_path = model_dir_save(model, model_path, "{}_model_direc".format(total_amos), features_summary)
        final_model_path = model_path + "/{}_model_direc".format(total_amos)
        log.info("model saved at {}".format(final_model_path))

        result = model.predict(test_x , batch_size = None)
        pred_classes = np.argmax(result, axis=1)
        real_classes = test_y.flatten()
        validate(real_classes , pred_classes , model_name_path , "test_data" , 4.5 )
        save_model_class_info(model_info , model_name_path , total_amos)
        return final_model_path




def merge_make_input(gk2a_df_modified2 , hsr_df_modified2 , model_class , log):

    try:
        ###@###
        model_class.extract_to_list()
        using_all_cols = model_class.input_columns_order.copy()
        for x in ["detail_time" , "target"  , "tower" , "time"]:
            using_all_cols.append(x)
        filtered_cols = list(filter(lambda x: x in using_all_cols ,  gk2a_df_modified2.columns.tolist()  ))
        gk2a_df_modified2 = gk2a_df_modified2.loc[: , filtered_cols]


        ###@###

        if "HSR_value" in model_class.input_columns_order:
            merged_data = pd.merge(gk2a_df_modified2 , hsr_df_modified2 ,  on =  "detail_time" , how = "outer")
        else:
            merged_data = gk2a_df_modified2.copy()

        merged_data = merged_data.dropna()

        processed_df = preprocess_target(data = merged_data ,target_decision = "5999" )

        cate_values  = list(map(lambda x: int(x / 1000 ) , processed_df.target))

        processed_df["target"] = cate_values

        processed_df = processed_df.reset_index(drop = True)

        final_x_arrays , final_y_arrays, features_summary = deeper_4d_transform_using_pandas(processed_df , model_class)
        # X_arr , Y_arr , features_summary= deeper_4d_transform(deeper_processed_df)

        return final_x_arrays , final_y_arrays , features_summary
    except Exception as e:
        log.info("error : {}".format(e))
        log.info("there is no data -------------> no training")



def merge_make_input_amos(gk2a_df_modified2, hsr_df_modified2 , train_start_ticket , train_end_ticket , env, model_class , log):
    try:
        ###@###
        model_class.extract_to_list()
        using_all_cols = model_class.input_columns_order
        for x in ["detail_time" , "target" , "HSR_value" , "tower" , "time"]:
            using_all_cols.append(x)
        filtered_cols = list(filter(lambda x: x in using_all_cols ,  gk2a_df_modified2.columns.tolist()  ))
        gk2a_df_modified2 = gk2a_df_modified2.loc[: , filtered_cols]
        ###@###

        data_collection_path , root_path , model_dir = set_globvar_to_path(env)
        host = tb_info.tb_host
        db_name = tb_info.tb_db_name
        user_name = tb_info.tb_user_name
        password = tb_info.tb_password
        port = tb_info.tb_port

        conn, cursor = open_connection(host,db_name,user_name,password,port, root_path)
        params = (train_start_ticket , train_end_ticket)

        sql = "select " + ", ".join( list(amos_model_features_labeling.amos_features_naming.keys())) +  " from D_AMOS_DETAIL  where (to_number(K1) between ? and ?) and K2 != 153;"
        cursor.execute(sql , params)
        data = cursor.fetchall()
        amos_info_df = pd.DataFrame(data , columns = list(amos_model_features_labeling.amos_features_naming.keys()) )
        amos_info_df = amos_info_df.rename(columns = amos_model_features_labeling.amos_features_naming )

        sql_153 = "select " + ", ".join( list(amos_model_features_labeling.amos_153_features_naming.keys())) +  " from D_AMOS_DETAIL  where (to_number(K1) between ? and ?) and K2 = 153;"
        cursor.execute(sql_153 , params)
        data_153 = cursor.fetchall()
        amos_info_df_153 = pd.DataFrame(data_153 , columns = list(amos_model_features_labeling.amos_153_features_naming.keys()) )
        amos_info_df_153 = amos_info_df_153.rename(columns = amos_model_features_labeling.amos_153_features_naming )

        amos_info_df = amos_info_df.append(amos_info_df_153)


        detail_time = list(map(lambda x , y: str(x) + "_" + str(y) , amos_info_df.time , amos_info_df.tower   ))
        amos_info_df["detail_time"] = detail_time
        del amos_info_df["time"]
        del amos_info_df["tower"]

        #amos_info_df =  amos_info_df.dropna()

        model_amos_feature_list = model_class.amos_features
        model_amos_feature_list.append("detail_time")
        amos_info_df = amos_info_df.loc[: , model_amos_feature_list]

        first_merge = pd.merge(gk2a_df_modified2 , amos_info_df , on = "detail_time" , how = "outer")
        merged_data = pd.merge(first_merge , hsr_df_modified2 ,   on = "detail_time" , how = "outer")
        #merged_data=  merged_data.dropna()

        processed_df = preprocess_target(data = merged_data ,target_decision = "5999" )
        cate_values  = list(map(lambda x: int(x / 1000 ) , processed_df.target))
        processed_df["target"] = cate_values
        processed_df = processed_df.reset_index(drop = True)
        final_x_arrays , final_y_arrays, features_summary = deeper_4d_transform_using_pandas(processed_df , model_class)
        return final_x_arrays , final_y_arrays , features_summary
    except:
        log.info("there is no data ------------> no training")

#

def make_final_seq_array(final_conv_shape_input , normed_whole_df , final_seq_array):
    dimension_len = len(final_conv_shape_input.shape)
    if dimension_len == 3:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0] , indices[1] , indices[2]) )
            for loc in loc_list:
                seq_num = loc[0]
                y_num = loc[1]
                x_num = loc[2]
                final_seq_array[:, seq_num , y_num , x_num] = normed_whole_df[col]
    elif dimension_len == 2:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0] , indices[1]))
            for loc in loc_list:
                y_num = loc[0]
                x_num = loc[1]
                final_seq_array[:,y_num , x_num] = normed_whole_df[col]
    elif dimension_len ==1:
        for col in normed_whole_df.columns.tolist():
            indices = np.where(final_conv_shape_input == col)
            loc_list = list(zip(indices[0]))
            for loc in loc_list:
                x_num = loc[0]
                final_seq_array[:, x_num] = normed_whole_df[col]
    return final_seq_array

def merging_past(final_df , mins , rank , original_df, original_df_cols):

    new_cols = list(map(lambda x: x + "_{}".format(str(rank)) if x != "real_time" else x , original_df_cols  ))

    merging_df = pd.DataFrame(original_df.values, columns = new_cols)

    merging_df["real_time"] =  merging_df["real_time"] + datetime.timedelta(minutes = mins )

    final_df2 = final_df.merge(merging_df , how = "outer" , on = "real_time")

    #final_df = final_df.merge(target_df , how  = "outer", on = "real_time")
    return final_df2





def deeper_4d_transform_using_pandas(df, model_class):

    detail_time = df.pop('detail_time')
    #df = df.clip(lower = 0)
    target = df.pop("target")
    features_summary = df.describe()
    normed_df = norming_features(df , features_summary)
    final_conv_shape_format = model_class.final_conv_shape_format
    sequence_len = model_class.sequence_len
    each_sequence_minutes = model_class.each_sequence_minutes

    final_cols_list = pd.Series(np.array(final_conv_shape_format).flatten()).drop_duplicates().tolist()

    new_df = pd.DataFrame()
    for col in final_cols_list:
        if ":" not in col:
            new_df[col] = normed_df[col]
        else:
            temp_cols = col.split(":")
            first_col = temp_cols[0]
            second_col = temp_cols[1]
            new_val = (normed_df[first_col] + normed_df[second_col])/2
            new_df[col] = new_val

    time_list = list(map(lambda x: int(x.split("_")[0]) , detail_time))
    tower_list = list(map(lambda x: int(x.split("_")[1]) , detail_time))
    deeper_df = new_df.reset_index(drop = True)
    deeper_df["tower"] = tower_list
    deeper_df["now_time"] = time_list
    deeper_df["target"] = target
    tower_names = deeper_df.tower.unique().tolist()

    final_input_shape = model_class.final_input_shape
    final_input_shape = final_input_shape[:-1]
    final_x_arrays = np.zeros( (1,) + final_input_shape    )
    final_y_arrays = np.zeros((1,1))
    rank_list = list(range( 0  , model_class.sequence_len, 1) )
    mins_list = list(range((model_class.sequence_len - 1) * model_class.each_sequence_minutes , -1 , - model_class.each_sequence_minutes)    )


    for tower in tower_names:
        tempo_df  = deeper_df.loc[deeper_df["tower"] == tower , :]
        tempo_df["real_time"] = pd.to_datetime(tempo_df['now_time'], format = "%Y%m%d%H%M")
        end = tempo_df.real_time.max()
        now  = tempo_df.real_time.min()
        all_times=[]
        while now < end:
            all_times.append(now)
            ###@@####
            now+=  datetime.timedelta(minutes=10)
            ###@@###

        all_times_df = pd.DataFrame({"real_time":all_times})
        tempo_df = tempo_df.merge(all_times_df , how = "outer" , on = "real_time")
        tempo_df = tempo_df.sort_values("real_time")
        del tempo_df["tower"]
        del tempo_df["now_time"]
        original_df_cols = tempo_df.columns.tolist()
        original_df_cols.remove("target")
        tempo_df = tempo_df.drop_duplicates(subset = "real_time")
        tempo_df = tempo_df.dropna()
        final_df = tempo_df.copy()
        original_df = tempo_df.loc[: , original_df_cols]
        for mins , rank in zip(mins_list , rank_list):
            final_df  = merging_past(final_df , mins , rank , original_df  , original_df_cols)

        final_df = final_df.dropna()
        final_df = final_df.drop(original_df_cols , axis = 1)
        counting = len(final_df)
        x_arrays = np.zeros(((counting,) + final_input_shape ))
        y_arrays = np.zeros((counting))

        x_arrays= make_final_seq_array(model_class.final_conv_shape_input , final_df , x_arrays)

        y_arrays = np.array(final_df.target.values.tolist())
        if len(y_arrays) == counting:
            y_arrays = y_arrays[..., np.newaxis ]
        else:
            print("different length of y and x tower : {}".format(tower))
            continue

        final_x_arrays = np.concatenate((final_x_arrays , x_arrays))
        final_y_arrays = np.concatenate((final_y_arrays , y_arrays))

    return final_x_arrays , final_y_arrays , features_summary


def whole_main(train_data_params ,  model_path, env , log , error_log):

    total_train_start_ticket = train_data_params["total_train_start_ticket"]
    total_train_end_ticket   = train_data_params["total_train_end_ticket"]

    amos_train_start_ticket  = train_data_params["amos_train_start_ticket"]
    amos_train_end_ticket    = train_data_params["amos_train_end_ticket"]

    #D_FOG_DET_TRAIN_GK2A 테이블로부터 데이터 로드
    # D_FOG_DET_TRAIN_HSR 테이블로부터 데이터 로드
    total_gk2a_df_modified2 , total_hsr_df_modified2 = get_whole_data(total_train_start_ticket , total_train_end_ticket,env,  log , error_log)

    final_total_model_path =  make_model(total_gk2a_df_modified2 , total_hsr_df_modified2 , log ,  model_path, train_data_params , env , "total" , total_train_start_ticket , total_train_end_ticket)

    amos_gk2a_df_modified2 , amos_hsr_df_modified2 = get_whole_data(amos_train_start_ticket , amos_train_end_ticket  , env , log , error_log)

    final_amos_model_path =  make_model(amos_gk2a_df_modified2 , amos_hsr_df_modified2 , log ,  model_path , train_data_params, env , "amos" , amos_train_start_ticket , amos_train_end_ticket)

    return final_total_model_path , final_amos_model_path

# # ------------------------- debugging ----------------------------------
# #
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
# #
# set_gpu(1)
# #
# class making_class:
#     def __init__(self , path , dicting ):
#         self.model_path = path
#         self.param_info = dicting
# #
# dicting  =   {'model_training'               : '2',
#               'data_collecting'              : '1',
#               'total_train_start_ticket'     : '201910010000',
#               'total_train_end_ticket'       : '201910012350',
#               'amos_train_start_ticket'      : '201910010000',
#               'amos_train_end_ticket'        : '201910012350',
#               'custom_features'              : '2',
#               'seq_len_total'                : '5',
#               'each_seq_minutes_total'       : '10',
#               'conv1_total'                  : '32',
#               'conv2_total'                  : '32',
#               'dense1_total'                 : '64',
#               'dense2_total'                 : '32',
#               'dense1_amos'                  : '128',
#               'dense2_amos'                  : '64',
#               'batch_size'                   : '4048',
#               'epoch'                        : '100',
#               'previous_model'               : '1',
#               'es_patience'                  : '50',
#               'weight_0_1000'                : '1',
#               'weight_1000_2000'             : '1',
#               'weight_2000_3000'             : '1',
#               'weight_3000_4000'             : '1',
#               'weight_4000_5000'             : '1',
#               'weight_5000_all'              : '1',
#               'optimizer'                    : '2',
#               'loss_func'                    : '2',
#               'learning_rate'                : '0.001'}
#
#
# tm = making_class( '/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/tempo_file2', dicting )
#
#
#
# env = "platform"
# log = fog_logging.get_log_view(1, "platform", False, 'fog_ui_enfo_info')
# error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_ui_enfo_info_error')
#
#
# def train(tm):
#
#
# #import fog_create_data
# #import fog_create_model
#
# log = fog_logging.get_log_view(1, "platform", False, 'fog_det_train_info')
# error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_train_error')
# #inf.add_hosts()
# log.info('train starts')
# log.info('tm model path {}'.format(tm.model_path))
#
#
#
# param = tm.param_info
#
#
# log.info('prams ->{}'.format(param))
#
# data_collecting = str(param['data_collecting'])
#
# model_training = str(param["model_training"])
#
# train_data_params = {'total_train_start_ticket' :  str(param['total_train_start_ticket']),
#                      'total_train_end_ticket'   :  str(param['total_train_end_ticket']),
#                      'amos_train_start_ticket'  :  str(param['amos_train_start_ticket']),
#                      'amos_train_end_ticket'    :  str(param['amos_train_end_ticket']),
#                      'custom_features'          :  str(param['custom_features']),
#                      'seq_len_total'            :  str(param['seq_len_total']),
#                      'each_seq_minutes_total'   :  str(param['each_seq_minutes_total']),
#                      'conv1_total'              :  str(param["conv1_total"]),
#                      'conv2_total'              :  str(param['conv2_total']),
#                      'dense1_total'             :  str(param['dense1_total']),
#                      'dense2_total'             :  str(param['dense2_total']),
#                      'dense1_amos'              :  str(param['dense1_amos']),
#                      'dense2_amos'              :  str(param['dense2_amos']),
#                      'batch_size_num'           :  str(param["batch_size"]),
#                      'epoch_num'                :  str(param["epoch"]),
#                      'previous_model'           :  str(param['previous_model']),
#                      'es_patience'              :  str(param['es_patience']),
#                      'weight_0_1000'            :  str(param['weight_0_1000']),
#                      'weight_1000_2000'         :  str(param['weight_1000_2000']),
#                      'weight_2000_3000'         :  str(param['weight_2000_3000']),
#                      'weight_3000_4000'         :  str(param['weight_3000_4000']),
#                      'weight_4000_5000'         :  str(param['weight_4000_5000']),
#                      'weight_5000_all'          :  str(param['weight_5000_all']),
#                      'optimizer'                :  str(param['optimizer']),
#                      'loss_func'                :  str(param['loss_func'])
#                      }
#
# #
# #
# model_training != "1"
#
# set_gpu(1)
#
# # model_name_path , amos_model_name_path = fog_create_model.whole_main(train_data_param, tm.model_path, "platform" , log , error_log)
#
# train_data_params ,  model_path, env , log , error_log = train_data_params, tm.model_path, "platform" , log , error_log
#
# # def whole_main(train_data_params ,  model_path, env , log , error_log):
