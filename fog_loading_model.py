import os
import pandas as pd

import fog_logging
import logging
import tensorflow as tf
from fog_config import Config , fog_modeling


def main(model_path , env,log, error_log):
    fog_model_path = os.path.join(model_path , "total_model_direc")
    amos_model_path = os.path.join(model_path , "amos_model_direc")
    # fog_model_path = model_path + "/total_model_direc"
    # amos_model_path = model_path + "/amos_model_direc"
    log.info("model_path : {}".format(model_path))
    total_model_path , total_model , total_summary_path = get_fog_model_summary(fog_model_path, "platform" , log , error_log , "total" )
    amos_model_path ,  amos_model ,  amos_summary_path = get_fog_model_summary(amos_model_path , "platform" , log , error_log , "amos")

    if os.path.dirname(total_model_path) != os.path.dirname(amos_model_path):
        log.info("different locations between two models -----> loading back up models due to mismatch")
        total_model_path  = Config.total_model_platform_path
        amos_model_path = Config.amos_model_platform_path
        total_model = tf.keras.models.load_model(total_model_path)
        amos_model = tf.keras.models.load_model(amos_model_path)
        total_summary_path  = os.path.join(total_model_path , "feature_summary.csv")
        amos_summary_path = os.path.join(amos_model_path , "feature_summary.csv")

    params = {"total_model_path" :total_model_path ,
              "total_model" : total_model ,
              "total_summary_path" : total_summary_path ,
              "amos_model_path" :amos_model_path,
              "amos_model" : amos_model ,
              "amos_summary_path" : amos_summary_path,
              "base_model_path":model_path}
    log.info("------------------final params-----------------------------")
    log.info(params)
    return params



def get_fog_model_summary(final_model_path , env , log , error_log , fog_amos):
    try:
        scores = pd.read_csv("{}/scores_test_data.csv".format(final_model_path), index_col= 0 )
        if scores.f1.iloc[0] > fog_modeling.f1_score_limit:
            print(scores.f1.iloc[0])

            current_model = tf.keras.models.load_model(final_model_path)
            log.info("loading the {} model".format(fog_amos))

            summary_path = "{}/feature_summary.csv".format(final_model_path)
            log.info("summary path loaded")

        else:
            log.info("f1 score too low loading previous model")
            print(scores.f1.iloc[0])
            if fog_amos == "total":
                final_model_path = Config.total_model_platform_path
            elif fog_amos == "amos":
                final_model_path = Config.amos_model_platform_path
            current_model = tf.keras.models.load_model(final_model_path)
            summary_path = "{}/feature_summary.csv".format(final_model_path)
            log.info("summary path")

    except Exception as e:
        log.info("unknown error occured--> trying to load previous model : {}".format(e))
        if fog_amos == "total":
            final_model_path= Config.total_model_platform_path
        elif fog_amos == "amos":
            final_model_path =Config.amos_model_platform_path

        current_model = tf.keras.models.load_model(final_model_path)

        summary_path = "{}/feature_summary.csv".format(final_model_path)

        log.info("summary path")
    return final_model_path , current_model , summary_path
