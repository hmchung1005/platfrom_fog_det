import pandas as pd
import numpy as np
from fog_config import Config
from scipy.spatial import distance

def set_globvar_to_path(env):
    if env == "local":
        root_path = Config.root_local_path
    elif env == "platform":
        root_path = Config.root_platform_path
    elif env == "dgx_share2":
        root_path = Config.root_dgx_path
    return root_path





def find_mindist(lat, lon, lat_lon_map):
    dist_map = []
    for i in range(lat_lon_map.shape[0]):
        temp = list(map(lambda x: distance.euclidean([lat,lon],x),lat_lon_map[i]))
        dist_map.append(temp)
    dist_map_array = np.array(dist_map)
    solution = np.argwhere(dist_map_array == dist_map_array.min())
    return (solution[0][0], solution[0][1])





def get_index_900(amos_data ,reshaped_to_3d_900):
    for i in amos_data['place']:
        lat = amos_data.loc[amos_data['place']==i, 'lat']
        lon = amos_data.loc[amos_data['place']==i, 'lon']
        result = find_mindist(float(lat),float(lon),reshaped_to_3d_900)
        temp_lat = int(result[0])
        temp_lon = int(result[1])
        amos_data.loc[amos_data['place'] ==i, 'lat_index_900'] = temp_lat
        amos_data.loc[amos_data['place'] == i, 'lon_index_900'] = temp_lon
    return amos_data


def get_index_1800(amos_data, reshaped_to_3d_1800):
    for i in amos_data['place']:
        lat = amos_data.loc[amos_data['place']==i, 'lat']
        lon = amos_data.loc[amos_data['place']==i, 'lon']
        lat_900 =  int(amos_data.loc[amos_data['place'] == i , 'lat_index_900'])
        lon_900 =  int(amos_data.loc[amos_data['place'] == i , 'lon_index_900'])

        result = find_mindist(float(lat),float(lon),reshaped_to_3d_1800[(lat_900*2 - 15) :(lat_900*2 + 15) , (lon_900*2 - 15) : (lon_900*2 + 15) , : ])
        temp_lat = int(result[0]) + int(lat_900*2 - 15)
        temp_lon = int(result[1]) + int(lon_900*2 - 15)
        amos_data.loc[amos_data['place'] ==i, 'lat_index_1800'] = temp_lat
        amos_data.loc[amos_data['place'] == i, 'lon_index_1800'] = temp_lon
    return amos_data


def get_index_3600(amos_data, reshaped_to_3d_3600):
    for i in amos_data['place']:
        lat = amos_data.loc[amos_data['place']==i, 'lat']
        lon = amos_data.loc[amos_data['place']==i, 'lon']
        lat_1800 =  int(amos_data.loc[amos_data['place'] == i , 'lat_index_1800'])
        lon_1800 =  int(amos_data.loc[amos_data['place'] == i , 'lon_index_1800'])
        result = find_mindist(float(lat),float(lon),reshaped_to_3d_3600[(lat_1800*2 - 15) :(lat_1800*2 + 15) , (lon_1800*2 - 15) : (lon_1800*2 + 15) , : ])
        temp_lat = int(result[0]) + int(lat_1800*2 - 15)
        temp_lon = int(result[1]) + int(lon_1800*2 - 15)
        amos_data.loc[amos_data['place'] ==i, 'lat_index_3600'] = temp_lat
        amos_data.loc[amos_data['place'] == i, 'lon_index_3600'] = temp_lon
    return amos_data


 ### UT-SFR-004-02-01

def get_gk2a_index(env):       ### UT-SFR-004-02-01
    root_path = set_globvar_to_path(env)
    latlon_2000 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_2000.txt".format(root_path) , sep = "\t" , header = None)
    latlon_1000 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_1000.txt".format(root_path) , sep = "\t", header = None)
    latlon_500 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_500.txt".format(root_path) , sep = "\t" ,header = None)
    amos_data  = pd.read_excel("{}/latlon_gk2a/amos_tower_lat_lon.xlsx".format(root_path))
    amos_data = amos_data.rename(columns = {'WMO지점번호; 47xxx' : 'place' ,'북위-60진법; 99.99':'lat', '동경-60진법; 999.99':'lon' })
    amos_data['lat']  =  list(map(lambda x : float(int(x)) + (float(str(x).split('.')[1])/60 ) ,amos_data['lat']))
    amos_data['lon'] =   list(map(lambda x : float(int(x)) + (float(str(x).split('.')[1])/60 ) ,amos_data['lon']))
    #################################3
    amos_data = amos_data.iloc[:2,:]
    #################################
    array_gk2a_2000 = np.array(latlon_2000)
    array_gk2a_1000 = np.array(latlon_1000)
    array_gk2a_500 = np.array(latlon_500)

    reshaped_to_3d_900 = np.reshape(array_gk2a_2000,(900,900,2))
    reshaped_to_3d_1800 = np.reshape(array_gk2a_1000,(1800,1800,2))
    reshaped_to_3d_3600 = np.reshape(array_gk2a_500 , (3600,3600,2))
    amos_data = get_index_900(amos_data , reshaped_to_3d_900)
    amos_data = get_index_1800(amos_data , reshaped_to_3d_1800)
    amos_data = get_index_3600(amos_data, reshaped_to_3d_3600)
    return amos_data



def get_hsr_index(amos_data):    ###UT-SFR-004-02-02
    hsr_data  = pd.read_csv("gk2a_hsr_int_pixel.csv")
    amos_data["hsr_lat"]= None
    amos_data["hsr_lon"] = None
    for i in amos_data.index.tolist():
        gk2a_lat = amos_data.loc[i, "lat_index_900"]
        gk2a_lon = amos_data.loc[i, "lon_index_900"]
        hsr_y = hsr_data.loc[(hsr_data["gk2a_y"] == int(gk2a_lat)) & (hsr_data["gk2a_x"] == int(gk2a_lon)) ,"hsr_y"].values[0]
        hsr_x = hsr_data.loc[(hsr_data["gk2a_y"] == int(gk2a_lat)) & (hsr_data["gk2a_x"] == int(gk2a_lon)) ,"hsr_x"].values[0]
        amos_data.loc[i, "hsr_lat"] = hsr_y
        amos_data.loc[i, "hsr_lon"] = hsr_x
    return amos_data


def get_gk2a_index(env):       ### UT-SFR-004-02-01
    root_path = set_globvar_to_path(env)
    latlon_2000 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_2000.txt".format(root_path) , sep = "\t" , header = None)
    latlon_1000 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_1000.txt".format(root_path) , sep = "\t", header = None)
    latlon_500 = pd.read_csv("{}/latlon_gk2a/LATLON_KO_500.txt".format(root_path) , sep = "\t" ,header = None)

    amos_data  = pd.read_csv("AMOS_base.csv" , engine='python',encoding='CP949')

    amos_data = amos_data.rename(columns = {'WMO지점번호; 47xxx' : 'place' ,'북위-10진법':'lat', '동경-10진법':'lon' })

    array_gk2a_2000 = np.array(latlon_2000)
    array_gk2a_1000 = np.array(latlon_1000)
    array_gk2a_500 = np.array(latlon_500)

    reshaped_to_3d_900 = np.reshape(array_gk2a_2000,(900,900,2))
    reshaped_to_3d_1800 = np.reshape(array_gk2a_1000,(1800,1800,2))
    reshaped_to_3d_3600 = np.reshape(array_gk2a_500 , (3600,3600,2))
    amos_data = get_index_900(amos_data , reshaped_to_3d_900)
    amos_data = get_index_1800(amos_data , reshaped_to_3d_1800)
    amos_data = get_index_3600(amos_data, reshaped_to_3d_3600)
    return amos_data


#################################
array_gk2a_2000 = np.array(latlon_2000)
array_gk2a_1000 = np.array(latlon_1000)
array_gk2a_500 = np.array(latlon_500)

reshaped_to_3d_900 = np.reshape(array_gk2a_2000,(900,900,2))
reshaped_to_3d_1800 = np.reshape(array_gk2a_1000,(1800,1800,2))
reshaped_to_3d_3600 = np.reshape(array_gk2a_500 , (3600,3600,2))

amos_data = get_index_900(amos_data , reshaped_to_3d_900)
amos_data = get_index_1800(amos_data , reshaped_to_3d_1800)
amos_data = get_index_3600(amos_data, reshaped_to_3d_3600)





def main(env):
    ##################################   UT_SFR-004-02-01#####################################
    amos_gk2a_index = get_gk2a_index(env)
    ###########################################################################################

    #################################  UT-SFR-004-02-02#########################################
    amos_gk2a_hsr_index = get_hsr_index(amos_gk2a_index)
    ##################################################################################
