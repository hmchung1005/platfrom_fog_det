import sys
import os
sys.path.append('/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det')
import tensorflow as tf
import fog_logging
import numpy as np
import fog_preprocess
import fog_data_input_validation     ##################
import fog_postprocess
import logging
import fog_database
from datetime import datetime, timedelta



def train(tm):
    try:
        import fog_create_data
        import fog_create_model
        log = fog_logging.get_log_view(1, "platform", False, 'fog_det_train_info')
        error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_train_error')
        #inf.add_hosts()
        log.info('train starts')
        log.info('tm model path {}'.format(tm.model_path))


        param = tm.param_info
        log.info('prams ->{}'.format(param))
        data_collecting = str(param['data_collecting'])
        model_training = str(param["model_training"])

        train_data_params = {'total_train_start_ticket' :  str(param['total_train_start_ticket']),
                             'total_train_end_ticket'   :  str(param['total_train_end_ticket']),
                             'amos_train_start_ticket'  :  str(param['amos_train_start_ticket']),
                             'amos_train_end_ticket'    :  str(param['amos_train_end_ticket']),
                             'custom_features'          :  str(param['custom_features']),
                             'seq_len_total'            :  str(param['seq_len_total']),
                             'each_seq_minutes_total'   :  str(param['each_seq_minutes_total']),
                             'conv1_total'              :  str(param["conv1_total"]),
                             'conv2_total'              :  str(param['conv2_total']),
                             'dense1_total'             :  str(param['dense1_total']),
                             'dense2_total'             :  str(param['dense2_total']),
                             'dense1_amos'              :  str(param['dense1_amos']),
                             'dense2_amos'              :  str(param['dense2_amos']),
                             'batch_size_num'           :  str(param["batch_size"]),
                             'epoch_num'                :  str(param["epoch"]),
                             'previous_model'           :  str(param['previous_model']),
                             'es_patience'              :  str(param['es_patience']),
                             'weight_0_1000'            :  str(param['weight_0_1000']),
                             'weight_1000_2000'         :  str(param['weight_1000_2000']),
                             'weight_2000_3000'         :  str(param['weight_2000_3000']),
                             'weight_3000_4000'         :  str(param['weight_3000_4000']),
                             'weight_4000_5000'         :  str(param['weight_4000_5000']),
                             'weight_5000_all'          :  str(param['weight_5000_all']),
                             'optimizer'                :  str(param['optimizer']),
                             'loss_func'                :  str(param['loss_func'])
                             }


        if data_collecting != "1":

            try:
                fog_create_data.whole_main(train_data_params , "platform", log, error_log)
                log.info("fog_create_data")
            except Exception as e:
                error_log.error("error fog_create_data {}".format(e))
        else:
            pass

        if model_training != "1":
            try:
                model_name_path , amos_model_name_path = fog_create_model.whole_main(train_data_params, tm.model_path, "platform" , log , error_log)
                log.info("fog model at {}".format(model_name_path ))
                log.info("amos model at {}".format(amos_model_name_path))
            except Exception as e:
                error_log.error("error fog_create_model {}".format(e))
        else:
            pass

    except Exception as e:
        error_log.error('train error :{}'.format(e))



def init_svc(im):
    import fog_loading_model  ################
    import json
    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_log')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
    model_path = im.model_path

    log.info('model path ->{}'.format(model_path))
    log.info('model path exists : {}'.format(os.path.exists(model_path)))



    try:
        params = fog_loading_model.main(model_path ,"platform",log, error_log)   #####################
        log.info('model loaded')
    except Exception as e:
        error_log.error('model loading error :{}'.format(e))

    try:
        processing_path = '{}/processing_status.json'.format(model_path)
        status_dic= {"processing" : "False"}
        with open(processing_path, 'w') as f:
            json.dump(status_dic, f, indent=4)
        f.close()
    except Exception as e:
        error_log.error("writing json error : {}".format(e))


    # param = im.param_info
    # params = {**param, **model_param}

    return params


def data_input_validation(ticketNo):
    '''check data'''


def inference(df, params, batch_size):
    log = fog_logging.get_log_view(1, "platform", False, 'inferencing_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'inferencing_error_log')




    log.info('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    ticketNo = ticket_df.tolist()[0]

    import datetime
    ticketNo_data = datetime.datetime.strptime(ticketNo , "%Y%m%d%H%M") + datetime.timedelta(minutes = 540)
    ticketNo = ticketNo_data.strftime("%Y%m%d%H%M")

    log.info('ticketNo : {}'.format(ticketNo))
    model_dic = {"total_model_path":params["total_model_path"],
                 "amos_model_path" :params["amos_model_path"],
                 "base_model_path" :params["base_model_path"]}


    is_null , reading_ticketNo = fog_data_input_validation.main(ticketNo , model_dic)   ########################
    if is_null:
        return False
    else:
        log.info('call_threadfunc start')
        ticketNo_dic = {"reading_ticketNo": reading_ticketNo ,
                        "saving_ticketNo" : ticketNo}
        call_threadfunc(ticketNo_dic, log, error_log , params)
        log.info('inference end')
        return True


def processing_after_call(ticket_tuple):
    ticketNo_dic, log, error_log, params = ticket_tuple
    import json
    base_model_path = params["base_model_path"]
    processing_path = '{}/processing_status.json'.format(base_model_path)
    with open(processing_path) as json_file:
        status = json.load(json_file)
    if status["processing"] == "False":
        try:
            status["processing"] = "True"
            with open(processing_path, 'w') as f:
                json.dump(status, f, indent=4)

            log.info("processing confirmed")
            log.info("ticket : {}".format(ticketNo_dic))
            log.info('fog_preprocess start')
            total_final_seq_data , amos_normed_whole_df = fog_preprocess.main(ticketNo_dic, "platform", params,  log , error_log)
            log.info('fog_preprocess end')

            log.info('fog_postprocess start')
            postprocess_params , watch_warn_df_params = fog_postprocess.main(ticketNo_dic, total_final_seq_data, amos_normed_whole_df , "platform", params , log , error_log)
            log.info('fog_postprocess end')

            log.info('fog_database start')
            fog_database.main(postprocess_params , watch_warn_df_params, "platform" , log, error_log )
            log.info('fog_database end')
        except Exception as e:
            error_log.error(e)
        finally:
            status["processing"] = "False"
            with open(processing_path, 'w') as f:
                json.dump(status, f, indent=4)
    else:
        log.info("previous process is not finished-----> stopping process")




def call_threadfunc(ticketNo_dic, log, error_log, params):

    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)

    try:
        ticket_tuple = (ticketNo_dic, log, error_log,  params)
        future = pool.submit(processing_after_call,(ticket_tuple))
    except Exception as e:
        error_log.error('processing_after_call error : {}'.format(e))

    log.info('call_threadfunc end')
