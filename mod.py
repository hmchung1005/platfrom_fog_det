import sys
import os
sys.path.append('/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det')
#sys.path.append("/home/platfrom_fog_det")
import tensorflow as tf
import fog_logging
import numpy as np
import fog_preprocess
import fog_data_input_validation     ##################
import fog_postprocess
import logging
import fog_database


def train(tm):
    try:
        import fog_create_data
        import fog_create_model
        log = fog_logging.get_log_view(1, "platform", False, 'fog_det_train_info')
        error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_train_error')
        #inf.add_hosts()
        log.info('train starts')
        log.info('tm model path {}'.format(tm.model_path))
        param = tm.param_info
        log.info('prams ->{}'.format(param))
        train_start_ticket = str(param['train_start_ticket'])
        train_end_ticket = str(param['train_end_ticket'])
        try:
            fog_create_data.whole_main(train_start_ticket, train_end_ticket , "platform")
            log.info("fog_create_data")
        except Exception as e:
            error_log.error("error fog_create_data {}".format(e))
        try:
            model_name_path = fog_create_model.whole_main(train_start_ticket, train_end_ticket , tm.model_path, "platform")
            log.info("fog_create_model at {}".format(model_name_path ))
        except Exception as e:
            error_log.error("error fog_create_model {}".format(e))

    except Exception as e:
        error_log.error('train error :{}'.format(e))




def init_svc(im):
    import fog_loading_model  ################

    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_info')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
    model_path = im.model_path

    log.info('model path ->{}'.format(model_path))
    log.info('model path exists : {}'.format(os.path.exists(model_path)))

    try:
        model = fog_loading_model.main(model_path)   #####################
        log.info('model loaded')
    except Exception as e:
        error_log.error('model loading error :{}'.format(e))
    model_param = {'model_path': model_path, 'saved_model':model}

    param = im.param_info
    params = {**param, **model_param}
    return params


def data_input_validation(ticketNo):
    '''check data'''

def inference(df, params, batch_size):
    log = fog_logging.get_log_view(1, "platform", False, 'fog_inference_info')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_inference_error')


    log.info('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    ticketNo = ticket_df.tolist()[0]
    log.info('ticketNo : {}'.format(ticketNo))

    is_null = fog_data_input_validation.main(ticketNo)   ########################
    if is_null:
        error_log.error("Input data has null")
        return False
    else:
        log.info('call_threadfunc start')
        call_threadfunc(ticketNo, log, error_log , params)
        log.info('inference end')
        return True


def processing_after_call(ticket_tuple):
    ticketNo, log, params = ticket_tuple
    print(ticketNo)
    log.info('fog_preprocess start')
    result_df = fog_preprocess.main_original(ticketNo, "platform")
    log.info('fog_preprocess end')

    log.info('fog_postprocess start')
    final_df , watch_warn_df = fog_postprocess.main(ticketNo, result_df, "platform" , params )
    log.info('fog_postprocess end')

    log.info('fog_database start')
    fog_database.main(final_df , watch_warn_df, "platform" )
    log.info('fog_database end')


def call_threadfunc(ticketNo, log, error_log, params):

    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)

    try:
        ticket_tuple = (ticketNo, log,   params)
        future = pool.submit(processing_after_call,(ticket_tuple))
        print("done")
    except Exception as e:
        error_log.error('processing_after_call error : {}'.format(e))

    log.info('call_threadfunc end')
