import tensorflow as tf
import sys

sys.path.append('/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det')

import fog_preprocess
import fog_postprocess
# import logging
import fog_database
import fog_logging

def train(tm):
    '''
    train(tm) : 학습에 필요한 필수 함수

    labels
        tm.features.get('y_value')
    data load
        tm.load_data_for_cnn()    # cnn 데이터
        tm.load_data_for_all()    # 기타 데이터
        ex) (train_id, train_x, train_y), (test_id, test_x, test_y) = tm.load_data_for_cnn()

    param
        tm.param_info[common_params]
        tm.param_info[algo_params]

    common_params
        nn_type          NN 유형
        init_method      초기화 방법
        opt_method       최적화 방법
        learning_rate    Learning Rate
        dropout_ratio    Dropout Ratio
        random_seed      랜덤 seed
        autosave_p       자동저장 주기
        epoch            학습수행횟수
        batch_size       배치 사이즈

    algo_params
        ui 에서 정의

    save
        tm.save_result_metrics(eval_results)

    eval_results
        step
        predict_y
        actual_y
        test_id
        confusion_matrix

    '''


def init_svc(im):
    '''
    init_svc(im) : 추론 서비스 초기화

    labels
        im.features.get('y_value')
    params
        im.param_info
    nn_info
        im.nn_info
    model_path
        im.get_model_path()

    return 값을 inference 함수에서 params 으로 접근
        ex) return {"svc_config":svc_config, "estimator":estimator}
    '''

def data_input_validation(ticketNo):
    '''check data'''

def inference(df, params, batch_size):
    # import fog_preprocess
    # import fog_postprocess
    # import logging
    # import fog_database
    log = fog_logging.get_log_view(1, 'platform', False, 'fog_inference_info')
    error_log = fog_logging.get_log_view(1, 'platform', True, 'fog_inference_error')

    log.info('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    ticketNo = ticket_df.tolist()[0]
    log.info('ticketNo : {}'.format(ticketNo))

    # result_df = fog_preprocess.main(ticketNo, 'platform')

    # data_input_validation(ticketNo)

#     final_df , watch_warn_df = fog_postprocess.main(ticketNo, result_df, 'platform')
#     fog_database.main(final_df , watch_warn_df, 'platform' )

    log.info('call_threadfunc start')
    call_threadfunc(ticketNo, log, error_log)

    log.info('inference end')

    return True
    '''
    inference : init_svc 의 return 값으로 부터 추론
        ex)
        svc_config = params['svc_config']
        estimator = params['estimator']
        test_data = df.iloc[:, 0].values.tolist()

    results 를 json 형태로 리턴한다
    '''




def processing_after_call(ticket_tuple):

    ticketNo, log = ticket_tuple

    log.info('fog_preprocess start')
    result_df = fog_preprocess.main(ticketNo, 'platform')
    log.info('fog_preprocess end')

    log.info('data_input_validation start')
    data_input_validation(ticketNo)
    log.info('data_input_validation end')

    log.info('fog_postprocess start')
    final_df , watch_warn_df = fog_postprocess.main(ticketNo, result_df, 'platform')
    log.info('fog_postprocess end')

    log.info('fog_database start')
    fog_database.main(final_df , watch_warn_df, 'platform' )
    log.info('fog_database end')

def call_threadfunc(ticketNo, log, error_log):

    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)

    try:
        ticket_tuple = (ticketNo, log)
        future = pool.submit(processing_after_call,(ticket_tuple))
    except Exception as e:
        error_log.error('processing_after_call error : {}'.format(e))

    log.info('call_threadfunc end')
