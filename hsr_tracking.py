import pandas as pd
import numpy as np
import os
import argparse
import mysql.connector
from sqlalchemy import create_engine, types
from fog_config import collecting, Config
from final_gk2a_mapping_processing import get_from_18_tibero,load_amos_info
import multiprocessing as mp
import sys


def set_globvar_to_path(env):
    global data_collection_path
    global root_path
    if env == "local":
        data_collection_path = collecting.data_collection_path_local
        root_path = Config.root_local_path
    elif env == "dgx_share1":
        data_collection_path = collecting.data_collection_path_dgx_docker_share1
        root_path = Config.root_dgx_path
    elif env =="dgx_share2":
        data_collection_path = collecting.data_collection_path_dgx_docker_share2
        root_path = Config.root_dgx_path
    elif env == "platform":
        data_collection_path = collecting.data_collection_path_platform
        root_path = Config.root_platform_path
        global saving_train_data_path
        saving_train_data_path = collecting.train_data_path_platform



def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta

##############################


def main(train_start_ticket , train_end_ticket , table_name):

    set_globvar_to_path("platform")
    target_df = get_from_18_tibero(train_start_ticket , train_end_ticket, root_path)
    amos_info = load_amos_info()
    target_unique = list(target_df['tower'].unique())
    boollist = list(map(lambda x: x in target_unique , amos_info['tower'].tolist() ))

    amos_info = amos_info.loc[boollist,:]
    amos_info = amos_info.loc[:,["lat","lon", "lat_index_hsr" , "lon_index_hsr"  ,"tower"]]


    convert_dict = {'lat':float,'lon':float,'lat_index_hsr':int, 'lon_index_hsr':int,'tower':str}
    amos_info = amos_info.astype(convert_dict)
    del amos_info['lat']
    del amos_info['lon']


    from datetime import datetime, timedelta

    fmt = '%Y%m%d%H%M'
    start = datetime.strptime(str(train_start_ticket), fmt)
    end = datetime.strptime(str(train_end_ticket), fmt)
    timelist = []
    for result in perdelta(start , end , timedelta(minutes=10)):
        timelist.append(result)

    timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , timelist ))



    step_size = 5
    iter = int(len(timelist) / step_size )
    step_list = [x * step_size for x in range( iter + 1) ]
    number_of_cpus = int(mp.cpu_count() - 5)

    engine = create_engine('mysql+mysqlconnector://root:123@192.168.0.181/fog_data')
    for i in range(len(step_list)):
        try:
            if i != len(step_list) - 1:
                tempo_list = timelist[step_list[i] : step_list[i + 1] ]
            elif i == len(step_list) -1 :
                tempo_list = timelist[step_list[i]:]
            whole_list = []
            for each_time in tempo_list:
                whole_list = get_file_path_list_each_time_hsr(each_time , whole_list , data_collection_path)
            print("length whole_list : {}".format(len(whole_list)))
            res_lst = [ [x, amos_info] for x in  whole_list]

            mp.set_start_method('forkserver', force=True)
            pool = mp.Pool(number_of_cpus)
            dfl = pool.map(hsr_load_get_pixels, res_lst )
            pool.close()
            pool.join()
            final_df = all_dfs_to_one_df_hsr(dfl)
            final_df["detail_time"] = final_df.index.tolist()
            final_df.to_sql('hsr_training_data_{}'.format(table_name), con = engine, index = False, if_exists = 'append')
        except Exception as e:
            print("step iteration error : {}".format(e))
            continue




def return_amos_index_hsr(tower, amos_df):
    df = amos_df.copy()
    the_index = df.loc[df['tower'] == tower , ["lat_index_hsr" , "lon_index_hsr"] ]
    lat_index = int(the_index["lat_index_hsr" ] )
    lon_index = int(the_index["lon_index_hsr" ])
    return lat_index, lon_index



def get_file_path_list_each_time_hsr(each_time , whole_list , data_collection_path):
    yearmonth =each_time[:6]
    day  =  each_time[6:8]
    tempo_path =  '{}/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'.format(data_collection_path,yearmonth, day, each_time, each_time)
    whole_list.append(tempo_path)
    return whole_list




def all_dfs_to_one_df_hsr(dfl):
    final_df = pd.DataFrame()
    for tempo_df in dfl:
        final_df = pd.concat([final_df, tempo_df] , axis = 0 )
    return final_df



def hsr_load_get_pixels(tuple_arg):
    try:
        hsr_path = tuple_arg[0]
        amos_info = tuple_arg[1]
        current_time = hsr_path.split(".txt")[0].split("_")[-1]
        current_data = pd.read_csv(hsr_path, header = None)
        current_np = current_data.values
        current_df = pd.DataFrame()

        for tower in  amos_info['tower']:
            try:
                lat,lon = return_amos_index_hsr(tower, amos_info)
                current_df.loc[current_time+"_"+tower,'HSR_value'] = current_np[lat][lon]
            except Exception as e:
                print("finding hsr pixel error : {}".format(e))
                continue
        return current_df
    except Exception as e:
        print("loading hsr error : {}".fortmat(e))
        pass





if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Process some info.')
    parser.add_argument('-s' , '--start_date',dest = "start_date", type = str )
    parser.add_argument('-e' , '--end_date',dest = "end_date", type = str )
    parser.add_argument('-n' , '--table_name' , dest = 'table_name' , type= str )
    args = parser.parse_args()
    start_date = args.start_date
    end_date = args.end_date
    table_name = args.table_name
    end_date = str(int(end_date) + 1)
    set_globvar_to_path("platform")
    main(start_date , end_date , table_name)
