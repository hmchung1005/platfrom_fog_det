import sys
import os
import mysql.connector
import time
import csv
import numpy as np
import pandas as pd
from sqlalchemy import create_engine, types
from solzr_zenith_angle import Koreazenith
import argparse
from fog_config import Config
from fog_config import collecting
import fog_config
#data_path = Config.data_local_path
import multiprocessing as mp
from multiprocessing import cpu_count, current_process, Manager
import concurrent.futures
from pathlib import Path
import datetime
import argparse
import fog_logging
import logging
from pysolar import solar

def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta

def return_amos_index(resolution,df, tower):
    if resolution == 1799:
        temp_resolution = 1800
        the_index = df.loc[df['tower'] == tower , ['lat_index_' + str(temp_resolution), 'lon_index_' + str(temp_resolution)]]
        lat_index = int(the_index['lat_index_' + str(temp_resolution)])
        lon_index = int(the_index['lon_index_' + str(temp_resolution)]) - 1
        return lat_index, lon_index
    else:
        the_index = df.loc[df['tower'] == tower , ['lat_index_' + str(resolution), 'lon_index_' + str(resolution)] ]
        lat_index = int(the_index['lat_index_' + str(resolution)])
        lon_index = int(the_index['lon_index_' + str(resolution)])
    return lat_index, lon_index


def deleting_last_col(resolution,df):
    if resolution ==900:
        if df.shape[1] == 900:
            return df
        elif df.shape[1] ==901:
            print('deleting last column')
            del df[900]
            return df
    elif resolution == 1800:
        if df.shape[1] == 1800:
            return df
        elif df.shape[1] == 1801:
            print('deleting last column')
            del df[1800]
            return df
    elif resolution ==3600:
        if df.shape[1] == 3600:
            return df
        elif df.shape[1]==3601:
            print('deleting last column')
            del df[3600]
            return df
# db_connection = connect_sql("192.168.0.181")
# db_connection.connect_db("fog_data")
# db_connection.conn.excute("DELETE FROM fog_data.gk2a_training_data_day_by201909 WHERE detail_time LIKE '20190914%';")
# db_connection.conn.execute("DELETE FROM fog_data.gk2a_training_data_day_by201909 WHERE detail_time LIKE '20190914%';")


class connect_sql:
    def __init__(self,host):
        self.host = host
    def connect_db(self,db_name):
        self.db_name = db_name
        self.engine = create_engine('mysql+mysqlconnector://root:123@'+self.host +'/'+self.db_name) # rootnme:psw
        self.conn = self.engine.connect()
    def get_column_names(self,table_name):
        self.table_name = table_name
        query  = 'show columns from ' + self.table_name
        result = self.conn.execute(query).fetchall()
        self.columns = [x[0] for x in result]
    def get_data(self,columns,table_name):
        self.table_name = table_name
        query = 'select ' + columns + " from " + self.table_name
        result = self.conn.execute(query).fetchall()
        self.data = result
    def get_joined_data(self,columns,first_table_name, second_table_name):
        # self.table_name = table_name
        detail_time = 'detail_time'
        query = 'SELECT {} FROM {}, {} WHERE {}.{}={}.{};'.format(columns,first_table_name,second_table_name,first_table_name,detail_time,second_table_name,detail_time)
        result = self.conn.execute(query).fetchall()
        self.data = result


def adding_rain_to_final_df(final_df,target_df):
    all_rain_list = list(map(lambda x: target_df.loc[target_df['time_tower'] == x ,'all_rain'].values[0] if len(target_df.loc[target_df['time_tower'] == x ,'all_rain']) != 0 else np.nan  , final_df.index.values.tolist() ))
    hour_rain_list = list(map(lambda x: target_df.loc[target_df['time_tower'] == x ,'hour_rain'].values[0] if len(target_df.loc[target_df['time_tower'] == x ,'hour_rain']) != 0 else np.nan  , final_df.index.values.tolist() ))
    final_df['all_rain'] = all_rain_list
    final_df['hour_rain'] = hour_rain_list
    return final_df


def adding_zenith_to_final_df(final_df, amos_info):
    time_tower = final_df.index.values.tolist()
    zenith_list = list(map(lambda x: Koreazenith(lati = float(amos_info.loc[amos_info['tower'] == x.split('_')[1], 'lat']),
    long =float(amos_info.loc[amos_info['tower'] == x.split('_')[1], 'lon']),
    year = int(x[0:4]),
    date_time = x[4:12] ) , time_tower  ))
    final_df['zenith'] = zenith_list
    return final_df

#/share/3_데이터/GK2A/ko/result/201909/01/00/result
def get_zenith_900_faster(latlon_900_2d, time):
    year = int(time[:4])
    month = int(time[4:6])
    day = int(time[6:8])
    hour = int(time[8:10])
    minute = int(time[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime.datetime(year,month,day,hour,minute, tzinfo =datetime.timezone.utc)  -  datetime.timedelta(hours = 9)
    temp_array = get_altitude_faster(latlon_900_2d, dobj)
    # 900 900 2 차원 배열로 만듬
    whole_array = np.reshape(temp_array, (900,900) )
    final_array = float(90) - whole_array
    return final_array


def get_dobj(time):
    year = int(time[:4])
    month = int(time[4:6])
    day = int(time[6:8])
    hour = int(time[8:10])
    minute = int(time[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime.datetime(year,month,day,hour,minute, tzinfo =datetime.timezone.utc)  -  datetime.timedelta(hours = 9)
    return dobj


def get_lat_from_amos_numbers(amos_numbers , amos_info):
    result = list(map(lambda x: amos_info.loc[int(x), "lat"] , amos_numbers))
    return result


def get_altitude_faster(lat, when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(lat)
    hour_angle = solar.get_hour_angle(when, lat )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))




def load_amos_info():
    amos_info=  pd.read_csv("amos_data_tower_info_hsr_included.csv", index_col = 0)
    amos_info["tower"] = list(map(lambda x: str(x) if len(str(x)) == 3 else "0" + str(x) , amos_info["tower"] ))
    return amos_info

def set_globvar_to_path(env):
    global data_collection_path
    global root_path
    if env == "local":
        data_collection_path = collecting.data_collection_path_local
        root_path = Config.root_local_path
    elif env == "dgx_share1":
        data_collection_path = collecting.data_collection_path_dgx_docker_share1
        root_path = Config.root_dgx_path
    elif env =="dgx_share2":
        data_collection_path = collecting.data_collection_path_dgx_docker_share2
        root_path = Config.root_dgx_path
    elif env == "platform":
        data_collection_path = collecting.data_collection_path_platform
        root_path = Config.root_platform_path
        global saving_train_data_path
        saving_train_data_path = collecting.train_data_path_platform


def data_load(path):
    final_file_path = path
    file_name = final_file_path.split("/")[-1]
    if file_name.endswith("npz"):
        temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
    else:
        print("alright reading")
        temp_df = pd.read_csv(final_file_path ,header = None ,chunksize = 500)
        print("chunk done")
        temp_df = temp_df.read()

        print("reading done")
        temp_array = temp_df.values
    column_name = "{}_{}".format(  file_name.split("_")[3] , file_name.split("_")[5].split(".")[2])
    print("loaded : {}".format(column_name))
    return temp_array , column_name



def all_dfs_to_one_df(all_dfs):
    df = pd.DataFrame()
    for temp_df in all_dfs:
        try:
            for col in temp_df.columns.tolist():
                for index in temp_df.index.tolist():
                    df.loc[index,col] = temp_df.loc[index,col]
        except Exception as e:
            print("passing a df merging : {}".format(e))
            continue
    return df




# yearmonth = 201909, days = ["07","08"]. in args just put space in betwee. env = "local"  (dgx)


# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(description='Process some info.')
#     parser.add_argument('-y', '--yearmonth',dest = "yearmonth",type = str)
#     parser.add_argument('-d', '--days',dest = "days", nargs='+', default=[], type = str)
#     parser.add_argument('-e', '--environment',dest = "env", type = str)
#     args = parser.parse_args()
#     global all_columns
#     all_columns = fog_config.all_columns
#     set_globvar_to_path(args.env)
#     local_main(yearmonth = args.yearmonth,days = args.days, env = args.env , func  = multi_getting_gk_data)

import jaydebeapi
import jpype
def open_connection(host,db_name,user_name,password, port , root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor

def make_amos_df(train_start_ticket, train_end_ticket , cursor):
    amos_table = "D_AMOS_DETAIL"
    cursor.execute("select K1, K2, SU7 , TU4 from {} where to_number(K1) between {} and {};".format(amos_table, train_start_ticket , train_end_ticket))
    data = cursor.fetchall()
    # cursor.execute("select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='{}';".format(amos_table))
    # column_names = cursor.fetchall()
    # column_names = list(map(lambda x: x[0] , column_names ))
    amos_data = pd.DataFrame(data , columns = ["K1","K2","SU7" , "TU4"])
    # amos_data = amos_data.loc[:,["K1","K2", "TU4"]]
    amos_data = amos_data.rename(columns = {"K1":"time","K2":"tower", "SU7":"target_su7" , "TU4":"target_tu4"})
    convert_dict = { "target_su7":float, "target_tu4":float}
    amos_data = amos_data.astype(convert_dict)
    amos_data['time_tower'] = list(map(lambda x ,y : x +"_"+ y , amos_data['time'], amos_data['tower']))
    return amos_data



def get_from_18_tibero(train_start_ticket , train_end_ticket , root_path):
    host = '192.168.0.18'
    db_name = 'tibero'
    user_name = 'afw'
    password = 'afw123'
    port = 8629
    conn, cursor = open_connection(host,db_name,user_name,password,port ,root_path)
    target_df = make_amos_df(train_start_ticket, train_end_ticket , cursor)
    return target_df

def get_file_path_list_each_time(each_time, channels, whole_list , data_collection_path):
    yearmonth = each_time[:6]
    day = each_time[6:8]
    hour = each_time[8:10]
    file_path_list = []
    for channel in channels:
        if "vi" in channel:
            if channel == "vi006":
                size = "05"
            else:
                size = "10"
        else:
            size = "20"
        try:
            temp_path = f'{data_collection_path}/GK2A/ko/result/{yearmonth}/{day}/{hour}/result/gk2a_ami_le1b_{channel}_ko0{size}lc_{each_time}.nc/'
            if Path(temp_path).is_dir():
                files = list(map(lambda x: os.path.join(temp_path , x) ,  os.listdir(temp_path) ))
                for file in files:
                    file_path_list.append(file)
            else:
                print("no directory at {}".format(temp_path))
                continue
        except:
            pass
    for file_path in file_path_list:
        whole_list.append(file_path)
    return whole_list

# 태양 천정각 빠르게 계산
def get_zenith_900_faster(lat , lon , time):
    year = int(time[:4])
    month = int(time[4:6])
    day = int(time[6:8])
    hour = int(time[8:10])
    minute = int(time[10:])
    # 한국 시간은 + utc 9 이기때문에 9시간을 뺀다
    dobj = datetime.datetime(year,month,day,hour,minute, tzinfo =datetime.timezone.utc)  -  datetime.timedelta(hours = 9)
    alti = get_altitude_faster(lat, lon,  dobj)
    # 900 900 2 차원 배열로 만
    sza = float(90) - alti
    return sza

# 태양 고도 빠르게 계산 함수
def get_altitude_faster(lat, lon , when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(lat)
    hour_angle = solar.get_hour_angle(when, lon )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))



def main(train_start_ticket , train_end_ticket, table_name):
    global all_columns
    all_columns = fog_config.all_columns
    # for col in all_columns:
    #     if "vi" in col:
    #         all_columns.remove(col)
    env = "platform"
    set_globvar_to_path(env)

    target_df = get_from_18_tibero(train_start_ticket , train_end_ticket , root_path)
    amos_info = load_amos_info()
    target_unique = list(target_df['tower'].unique())
    boollist = list(map(lambda x: x in target_unique , amos_info['tower'].tolist() ))
    amos_info = amos_info.loc[boollist,:]
    amos_info = amos_info.loc[:,["lat","lon", "lat_index_900","lon_index_900","lat_index_1800","lon_index_1800","lat_index_3600","lon_index_3600","tower"]]
    convert_dict = {'lat':float,'lon':float,'lat_index_900':int, 'lon_index_900':int,'lat_index_1800':int,'lon_index_1800':int,'lat_index_3600':int,'lon_index_3600':int, 'tower':str}
    amos_info = amos_info.astype(convert_dict)
    boolist_target =  list(map(lambda x: x in amos_info['tower'].tolist() , target_df['tower'].tolist()   ))
    target_df = target_df.loc[boolist_target,:]
    target_df.set_index('time_tower', inplace = True)
    del target_df['time']
    del target_df['tower']
    final_bool_list = list(map(lambda x: int(x[10:12]) % 2 == 0 , target_df.index ))
    target_df = target_df.loc[final_bool_list,:]

    lat_dic = {}
    lon_dic = {}

    for i in range(len(amos_info)):
        key = int(amos_info.tower.iloc[i])
        lat_value = amos_info.lat.iloc[i]
        lon_value = amos_info.lon.iloc[i]
        lat_dic[key] = lat_value
        lon_dic[key] = lon_value

    tower_list = list(map(lambda x: int(x.split("_")[1]) , target_df.index.tolist() ))
    lat_list = list(map(lambda x: lat_dic[x] , tower_list ))
    lon_list = list(map(lambda x: lon_dic[x] , tower_list))
    time_list = list(map(lambda x: str(x.split("_")[0] ) , target_df.index.tolist() ))

    zenith_list=  list(map(lambda x,y,z: get_zenith_900_faster(x,y,z) , lat_list , lon_list , time_list    ))
    target_df["zenith"] = zenith_list
    print("done getting zenith")


    del amos_info['lat']
    del amos_info['lon']

    from datetime import datetime, timedelta
    channels = []
    for i in fog_config.channel_feature_dic.keys():channels.append(i)

    fmt = '%Y%m%d%H%M'
    start = datetime.strptime(str(train_start_ticket), fmt)
    end = datetime.strptime(str(train_end_ticket), fmt)
    timelist = []
    for result in perdelta(start , end , timedelta(minutes=2)):
        timelist.append(result)

    timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , timelist ))

    step_size = 5
    iter = int(len(timelist) / step_size )
    step_list = [x * step_size for x in range( iter + 1) ]

    number_of_cpus = int( (mp.cpu_count() / 3) - 2 )
    log = fog_logging.get_log_view(1, env, False, "gk2a_mapping_process" )
    error_log = fog_logging.get_log_view(1, env, True, "gk2a_mapping_process" )
    engine = create_engine('mysql+mysqlconnector://root:123@192.168.0.181/fog_data')
    for i in range(len(step_list)):
        try:
            log.info("step : {}".format(i))
            if i != len(step_list) - 1:
                tempo_list = timelist[step_list[i] : step_list[i + 1] ]
            elif i == len(step_list) -1 :
                tempo_list = timelist[step_list[i]:]
            whole_list = []
            for each_time in tempo_list:
                whole_list = get_file_path_list_each_time(each_time,channels, whole_list, data_collection_path)
            print("length whole_list : {}".format(len(whole_list)))
            res_lst = [ [x, amos_info, all_columns] for x in  whole_list]
            mp.set_start_method('forkserver', force=True)
            pool = mp.Pool(number_of_cpus)
            dfl = pool.map(data_load_get_pixels, res_lst )
            pool.close()
            pool.join()
            res_df = all_dfs_to_one_df(dfl)
            print(res_df)

            final_df = pd.merge(res_df, target_df,left_index=True, right_index=True,how='left')
            final_df['detail_time'] = final_df.index.values.tolist()
            final_df.to_sql('gk2a_data_{}'.format(table_name),con=engine,index=False,if_exists='append')
        except Exception as e:
            error_log.error("error occured : {}".format(e))
            continue





def data_load_get_pixels(tuple_arg):
    try:

        returning_df  = pd.DataFrame()
        final_file_path = tuple_arg[0]
        amos_info = tuple_arg[1]
        all_columns = tuple_arg[2]
        time_now = final_file_path.split(".nc")[1].split("_")[-1]
        file_name = final_file_path.split("/")[-1]
        feature  = final_file_path.split("/")[-1]
        column_name = "{}_{}".format(  file_name.split("_")[3] , file_name.split("_")[5].split(".")[2])

        if file_name.endswith("npz"):
            temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
        else:
            print("alright reading")
            temp_df = pd.read_csv(final_file_path ,header = None ,chunksize = 1000)
            print("chunk done")
            temp_df = temp_df.read()

            print("reading done")
            temp_array = temp_df.values
        print("loaded : {}".format(column_name))
    except Exception as e:
        print("loading error : {}".format(e))
    try:
        if column_name not in all_columns:
            if "ncAlbedo" in feature:
                if "{}_Albedo".format(feature.split("_")[3]) in all_columns:
                    column_name = "{}_Albedo".format(feature.split("_")[3])
                    print("chaning column name to {}".format(column_name))
                else:
                    print("wrong column name : {}".format(column_name))
                    print(final_file_path)
            else:
                print("wrong column name : {}".format(column_name))
                print(final_file_path)
        if "vi" in column_name:
            if "vi006" in column_name:
                resolution = 3600
            elif "vi008" in column_name:
                if temp_array.shape == (1800,1800)  and type(temp_array[0,1799]) ==str:
                    temp_array = np.array(temp_array[:,:-10])
                    resolution  = 1799
                elif temp_array.shape == (1800,1801) and type(temp_array[0,1800]) == str:
                    temp_array = np.array(temp_array[:,:-10])
                    resolution = 1800
                else:
                    resoultion = 1800
            else:
                resolution = 1800
        else:
            resolution = 900

        for amos_info_tower in amos_info['tower']:
            try:
                lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
                returning_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = float(temp_array[lat][lon])
            except Exception as e:
                returning_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = np.nan
                print("no vale at this tower {}_{}_{}".format(time_now,column_name,e))
                print(temp_array.shape)
                print(final_file_path)
                continue
        return returning_df
    except Exception as e:
        print("unknown error : {}".format(e))
        pass



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Process some info.')
    parser.add_argument('-s' , '--start_date',dest = "start_date", type = str )
    parser.add_argument('-e' , '--end_date',dest = "end_date", type = str )
    parser.add_argument('-n' , '--table_name' , dest = 'table_name' , type= str )
    args = parser.parse_args()
    start_date = args.start_date
    end_date = args.end_date
    table_name = args.table_name
    end_date = str(int(end_date))
    main(start_date , end_date, table_name)





# res_list
#
# all_dfs
# final_df
#
# print(final_df)
# final_df = pd.merge(final_df, target_df,left_index=True, right_index=True,how='left')
# final_df['detail_time'] = final_df.index.values.tolist()
# if env == "local" or "dgx" in env:
#     engine = create_engine('mysql+mysqlconnector://root:123@192.168.0.181/fog_data')
# try:
#     print("inserting to main")
#     final_df.to_sql('gk2a_data_{}'.format(yearmonth),con=engine,index=False,if_exists='append')
# except Exception as e:
#     print("sending to database error : {}".format(e))
#     with open("report_error/amos_gk2a_mapping_error.txt", "a") as f:
#         words = "\n"+ "sending to database error : {}_{}_{}".format(e,day, temp_hour)
#         f.write(words)
#     f.close()
# try:
#     print("inserting to sub")
#     final_df.to_sql('gk2a_data_{}{}'.format(yearmonth,day),con=engine,index=False,if_exists='append')
# except Exception as e:
#     print("sending to database error : {}".format(e))
#     with open("report_error/amos_gk2a_mapping_error.txt", "a") as f:
#         words = "\n"+ "sending to database error : {}_{}_{}".format(e,day, temp_hour)
#         f.write(words)
#     f.close()
#     continue
#
#
#
#






#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~




#
# host = '192.168.0.181'
# db_name = 'afdb'
# user_name = 'afadm'
# password = '123456qwer!'
# port = 5434
# db_type = 'postgres'
# check = connect_sql(host, db_name, user_name, password, port, db_type)
# check.get_whole_dataframe("amos_128")
# amos_data = check.df
#
# amos_data.columns
# amos_data = amos_data.loc[:,["k1","k2", "tu4"]]
# amos_data = amos_data.rename(columns = {"k1":"time","k2":"tower","tu4":"target"})
# convert_dict = {"target":float}
# amos_data = amos_data.astype(convert_dict)
# amos_data['time_tower'] = list(map(lambda x ,y : x +"_"+ y , amos_data['time'], amos_data['tower']))
# amos_128 = amos_data.copy()
# amos_128.loc[amos_128["time_tower"] == "201909030651_128", :]
#
# class connect_sql:
#     def __init__(self,host,db_name,user_name,password,port, db_type):
#         if db_type == "postgres":
#             self.database = 'postgresql'
#         elif db_type == "mysql":
#             self.database = 'mysql+mysqlconnector'
#         engine = create_engine(self.database +'://{}:{}@{}:{}/{}'.format(user_name, password, host,port , db_name))
#         self.engine = engine
#         self.conn = engine.connect()
#     def send_query(self,sql):
#         query_result = self.conn.execute(sql).fetchall()
#         self.query_result = query_result
#     def get_whole_dataframe(self,table_name):                           #데이터베이스에서 통채로 빼우는 함수
#         self.table_name = table_name
#         data =self.conn.execute("select * from " + self.table_name).fetchall()
#         self.data =  data
#         column_names = self.conn.execute("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '{}';".format(table_name)).fetchall()
#         self.column_names = column_names
#         column_list = []
#         for i in self.column_names:
#             temp_col = i[0]
#             column_list.append(temp_col)
#         self.column_list = column_list
#         self.df = pd.DataFrame(self.data, columns = self.column_list)
#     def get_where_dataframe(self,table_name, str_columns ,where_column, str_listing ):    #where 절이 포함된 sql 함수
#         self.table_name = table_name
#         data = self.conn.execute("select " + str_columns + " from " + table_name + " where " + where_column + " in ({})".format(str_listing)).fetchall()
#         self.data = data
#         self.df = pd.DataFrame(data, columns = str_columns.split(","))
#     def insert_df(self, df, table_name):                                                   # 데이터프레임 통채로 데이터베이스에 삽입
#         df.to_sql(table_name , con = self.engine, index = False, if_exists ='append')
