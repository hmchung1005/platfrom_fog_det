
import sys
import os
import time
import csv
import numpy as np
import pandas as pd
import requests
import argparse
from pysolar import solar
from fog_config import Config , collecting , preprocess_info  , hdfs_path_class ,tb_info , collecting_data_info , external_info_path # about to delete column_info
import multiprocessing as mp
import fog_logging
import jaydebeapi
import jpype
from datetime import date, datetime, timedelta , timezone
from pathlib import Path
from hdfs import InsecureClient
import argparse
import math


def return_amos_index(resolution,df, tower):
    if resolution == 1799:
        temp_resolution = 1800
        the_index = df.loc[df['tower'] == tower , ['lat_index_' + str(temp_resolution), 'lon_index_' + str(temp_resolution)]]
        lat_index = int(the_index['lat_index_' + str(temp_resolution)])
        lon_index = int(the_index['lon_index_' + str(temp_resolution)]) - 1
        return lat_index, lon_index
    else:
        the_index = df.loc[df['tower'] == tower , ['lat_index_' + str(resolution), 'lon_index_' + str(resolution)] ]
        lat_index = int(the_index['lat_index_' + str(resolution)])
        lon_index = int(the_index['lon_index_' + str(resolution)])
    return lat_index, lon_index


# 태양 천정각 빠르게 계산
def get_train_zenith_faster(target_df , amos_info):
    amos_info_latlon = amos_info.loc[: , ["tower" , "lat" , "lon"]]
    merged_target_df =  target_df.merge(amos_info_latlon , on = "tower" , how = "left")
    tower_list = merged_target_df["tower"].tolist()
    year_list = list(map(lambda x: int(x[:4]) ,  merged_target_df["time"].tolist() ))
    month_list = list(map(lambda x: int(x[4:6]) ,  merged_target_df["time"].tolist() ))
    day_list = list(map(lambda x: int(x[6:8]) ,  merged_target_df["time"].tolist() ))
    hour_list = list(map(lambda x: int(x[8:10]) ,  merged_target_df["time"].tolist() ))
    minute_list = list(map(lambda x: int(x[10:]) ,  merged_target_df["time"].tolist() ))
    dobj_list = list(map(lambda y , M ,  d , h , m: datetime(y , M , d, h , m  ,tzinfo =timezone.utc) - timedelta(hours = 9) , year_list , month_list , day_list , hour_list , minute_list  ))
    latlon_list = list(map(lambda y , x : (y,x) , merged_target_df["lat"] , merged_target_df["lon"]   ))
    temp_alt = list(map(lambda x , y:  get_altitude_faster(x, y ) , latlon_list , dobj_list  ))
    final_zenith = (float(90) - np.array(temp_alt))
    target_df["zenith"] = final_zenith
    target_df['time'] = target_df['time'].astype(int)
    target_df['tower']  = target_df['tower'].astype(int)
    return target_df


# 태양 고도 빠르게 계산 함수
def get_altitude_faster(latlon_900_2d, when):
    # 시간 입력
    day = solar.math.tm_yday(when)
    declination_rad = solar.math.radians(solar.get_declination(day))
    # 위도 정도를 사용해서 태양 천정각 계싼
    latitude_rad = solar.math.radians(latlon_900_2d[0] )
    hour_angle = solar.get_hour_angle(when, latlon_900_2d[1] )
    first_term = solar.math.cos(latitude_rad) * solar.math.cos(declination_rad) * solar.math.cos(solar.math.radians(hour_angle))
    second_term = solar.math.sin(latitude_rad) * solar.math.sin(declination_rad)
    return solar.math.degrees(solar.math.asin(first_term + second_term))


def load_amos_info(root_path):
    amos_info=  pd.read_csv(external_info_path.training_amos_info_path , index_col = 0)
    amos_info["tower"]  = amos_info["tower"].astype("str")
    return amos_info


def set_globvar_to_path(env):
    if env == "platform":
        data_collection_path = collecting.data_collection_path_platform
        root_path = Config.root_platform_path
        return data_collection_path , root_path


def make_amos_df(train_start_ticket, train_end_ticket , cursor , col_list_dic , where_phrase = None):
    amos_table = "D_AMOS_DETAIL"
    col_list = list(col_list_dic.keys())
    col_list_str = ", ".join(col_list)
    if where_phrase == None:
        sql = "select " + col_list_str +   " from {} where to_number(K1) between {} and {};".format(amos_table, train_start_ticket , train_end_ticket)
    else:
        sql = "select " + col_list_str +   " from {} where to_number(K1) between {} and {} and {};".format(amos_table, train_start_ticket , train_end_ticket , where_phrase)

    cursor.execute(sql)
    data = cursor.fetchall()

    # 원래 데이터베이스 테이블 컬럼 이름으로 만든 데이터 프레임
    amos_data = pd.DataFrame(data , columns = col_list )

    # 다시 보기 편한 칼럼이름으로 변경
    amos_data = amos_data.rename(columns = col_list_dic)

    convert_dict = {"target":float}
    amos_data = amos_data.astype(convert_dict)
    # '시간_타워지점' 이런 형태인 데이터 생성. 2개 칼럼 말고 1개 칼럼으로 매핑 하는데 있어서 편함
    amos_data['time_tower'] = list(map(lambda x ,y : x +"_"+ y , amos_data['time'], amos_data['tower']))
    return amos_data


# 데이터프레임 칼럼별 데이터 타입 세팅
def modify_amos_df(target_df , train_start_ticket, train_end_ticket, cursor , root_path):
    amos_info = load_amos_info(root_path)
    target_unique = list(target_df['tower'].unique())
    boollist = list(map(lambda x: x in target_unique , amos_info['tower'].tolist() ))
    amos_info = amos_info.loc[boollist,:]
    amos_info = amos_info.loc[:,["lat","lon", "lat_index_900","lon_index_900","lat_index_1800","lon_index_1800","lat_index_3600","lon_index_3600","tower", "lat_index_hsr", "lon_index_hsr" ]]
    convert_dict = {'lat':float,'lon':float,'lat_index_900':int, 'lon_index_900':int,'lat_index_1800':int,'lon_index_1800':int,'lat_index_3600':int,'lon_index_3600':int, 'tower':str , "lat_index_hsr":int, "lon_index_hsr":int}
    amos_info = amos_info.astype(convert_dict)
    boolist_target =  list(map(lambda x: x in amos_info['tower'].tolist() , target_df['tower'].tolist()   ))
    target_df = target_df.loc[boolist_target,:]
    target_df.set_index('time_tower', inplace = True,drop=False)
    target_df = target_df.rename(columns = {"time_tower":"time_base"})
    return target_df , amos_info


def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta


def open_connection(host,db_name,user_name,password, port ,root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor


def return_hsr_index(amos_info , tower):
    the_index = amos_info.loc[amos_info['tower'] == tower , ["lat_index_hsr" , "lon_index_hsr" ]]
    lat_index = int(the_index["lat_index_hsr"])
    lon_index = int(the_index["lon_index_hsr"])
    return lat_index , lon_index



def insert_hsr_train_table(final_df , cursor , log):
    log.info("inserting each")
    for i in final_df.index.tolist():
        try:
            params = (int(final_df.loc[i ,"time"]) , int(final_df.loc[i,'tower']),final_df.loc[i,'time_base'], float(final_df.loc[i, 'HSR_value']))
            sql = "INSERT into d_fog_det_train_hsr (table_id ,  time, tower, time_base, HSR_value ) VALUES (D_FOG_DET_TRAIN_HSR_SEQ.NEXTVAL  ,?,?,?,?)"

            cursor.execute(sql , params)

        except Exception as e:
            log.info("unable to insert hsr : {}".format(e))
            continue


def make_hsr_df_from_nas( hsr_path ,log, each_time , amos_info):
    final_df = pd.DataFrame()
    path = hsr_path
    booling = os.path.isfile(path)
    if booling  == False:
        log.info("no file found at : {}".format(path))
    else:
        try:

            array = pd.read_csv(path, header = None).iloc[:,:-1].values

            log.info("loaded {} hsr file".format(each_time))

            for amos_info_tower in amos_info["tower"]:
                try:

                    lat , lon = return_hsr_index(amos_info, amos_info_tower)

                    final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value" ] = float(array[lat][lon])

                    final_df.loc[each_time+ "_" + str(amos_info_tower), "tower"] = int(amos_info_tower)

                    final_df.loc[each_time+ "_" + str(amos_info_tower), "time"] = int(each_time)
                except Exception as e:
                    final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value"] = np.nan
                    log.info("no HSR value at this tower {} {}".format(each_time,e))
            final_df["time_base"] = final_df.index.tolist()
            return final_df
        except Exception as e:
            log.info("cannot read file from : {}  --> {}".format(path , e))


def make_hsr_df_from_hdfs(hsr_path , log, each_time , amos_info):
    final_df = pd.DataFrame()

    ambari_host = hdfs_path_class.ambari_host
    auth_pass = hdfs_path_class.auth_pass
    ambari_cluster_name = hdfs_path_class.ambari_cluster_name
    auth_user = hdfs_path_class.auth_user

    hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host , auth_pass ,ambari_cluster_name ,auth_user)
    if hdfs_status != 200:
        log.info("hdfs status : {}".format(hdfs_status))
    else:
        path = hsr_path

        working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)

        client = InsecureClient(working_hdfs_path, user="hdfs")

        status = client.status(path,  strict = False)

        if status  == None:
            log.info("no file found at : {}".format(path))
        else:
            try:
                with client.read(path) as file:
                    array = pd.read_csv(file, header = None).iloc[:,:-1].values
                log.info("loaded {} hsr file".format(each_time))
                for amos_info_tower in amos_info["tower"]:
                    try:
                        lat , lon = return_hsr_index(amos_info, amos_info_tower)

                        final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value" ] = float(array[lat][lon])

                        final_df.loc[each_time+ "_" + str(amos_info_tower), "tower"] = int(amos_info_tower)

                        final_df.loc[each_time+ "_" + str(amos_info_tower), "time"] = int(each_time)
                    except Exception as e:
                        final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value"] = np.nan
                        log.info("no HSR value at this tower {} {}".format(each_time,e))
                final_df["time_base"] = final_df.index.tolist()
            except:
                log.info("cannot read file from : {}".format(path))
    return final_df


def get_hdfs_path_list_by_time(ticketNo, chanels):
    convert_dict = {"Radiance" : "Radiance" , "Effective" : "Effective_Brightness_Temperature" , "Brightness" : "Brightness_Temperature" , "Albedo" :"Albedo"  }
    year_month = ticketNo[:6]
    day = ticketNo[6:8]
    final_path_list = []
    features = chanels.copy()

    for feature in features:
        channel = feature.split("_")[0]
        type = feature.split("_")[1]
        type = convert_dict[type]
        if "vi" in channel:
            if "vi006" in channel:
                distance = "05"
            else:
                distance = "10"
        else:
            distance = "20"
        final_path_list.append(collecting.data_collection_path_hdfs_gk2a.format(channel.upper(),year_month,day,channel,distance,ticketNo,type))
    return final_path_list



def insert_gk2a_train_table(final_df , cursor , log , error_log):
    for index in final_df.index.tolist():
        gk2a_inserting_columns = final_df.loc[:, final_df.columns != 'time_base'].columns.tolist()
        try:
            inserting_columns_sql = "{}".format(", ".join(map(str, gk2a_inserting_columns )))
            inserting_values = list(map(lambda x:  final_df.loc[index,x ] , gk2a_inserting_columns ))
            inserting_values_sql = "{}".format(", ".join(map(str, inserting_values )))
            string_col = "time_base"
            string_val = final_df.loc[index, "time_base"]
            ###@@@
            #final_sql = f"INSERT INTO d_fog_det_train_gk2a ({inserting_columns_sql}, {string_col}) VALUES ({inserting_values_sql}, '{string_val}');" #.format(inserting_columns_sql,string_col, inserting_values_sql,string_val)
            final_sql = 'insert into d_fog_det_train_gk2a (table_id , ' + inserting_columns_sql + ',' + string_col +") VALUES ( D_FOG_DET_TRAIN_GK2A_SEQ.nextval,  " + inserting_values_sql +", '"+string_val + "');"

            try:
                cursor.execute(final_sql)
            except Exception as e:
                #print(final_sql)
                error_log.error("nan value in target")
                error_log.error("error : {}".format(e))
                continue
        except Exception as e:
            error_log.error("something wrong")
            error_log.error("unknown error : {}".format(e))

            continue


def getActiveNameNodeHDFS(ambari_host , auth_pass , ambari_cluster_name, auth_user):
    active_host = None

    ''' ambari user/pass '''
    #auth_user = 'admin'

    #auth_pass = 'admin'
    #ambari_host = "192.168.0.174"
    ''' ambari host '''
    #ambari_cluster_name = 'hacluster'


    api_url = 'http://{}:8080/api/v1/clusters/{}/host_components?HostRoles/component_name=NAMENODE&metrics/dfs/FSNamesystem/HAState=active'.format(ambari_host,
                                                                                                                                                   ambari_cluster_name)
    res = requests.get(api_url, auth=(auth_user, auth_pass))

    status_code = res.status_code
    res_json = res.json()

    '''
    - status 200 == 정상
    - status 403 == 암바리 계정명 불일치
    - status 404 == api url 재확인 필요
    - status 5xx == 암바리 서버 에러
    '''
    if status_code==200 and res_json['items']:
        active_host = res_json['items'][0]['HostRoles']['host_name']

    return status_code, active_host



def make_df_from_hdfs(hdfs_paths , log, each_time , amos_info, size_to_resoultion, df_index, error_log):
    try:
        time_now = each_time
        final_df = pd.DataFrame(index =df_index)
        ambari_host = hdfs_path_class.ambari_host
        auth_pass = hdfs_path_class.auth_pass
        ambari_cluster_name = hdfs_path_class.ambari_cluster_name
        auth_user = hdfs_path_class.auth_user
        for path in hdfs_paths:
            hdfs_status , active_host =  getActiveNameNodeHDFS(ambari_host, auth_pass , ambari_cluster_name,auth_user )
            if hdfs_status  !=  200:
                log.info("hdfs status : {}".format(hdfs_status))
                continue
            else:
                working_hdfs_path = "http://{}:{}".format(active_host , hdfs_path_class.hdfs_port)
            client = InsecureClient(working_hdfs_path, user="hdfs")
            file_status = client.status(path,  strict = False)
            if file_status  == None:
                log.info("no file found at : {}".format(path))
                continue
            temp_df = pd.DataFrame()
            column_name = path.split("/")[6].lower() + "_"+ path.split("/")[-1].split(".")[2].split("_")[0]
            size = path.split("ko0")[1][:2]
            resolution = size_to_resoultion[size]
            try:
                with client.read(path) as file:
                    temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
                log.info("loaded : {}".format(column_name))
            except:
                log.info("cannot read file from : {}".format(path))
                continue
            for amos_info_tower in amos_info['tower']:
                try:
                    lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
                    temp_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = float(temp_array[lat][lon])
                except Exception as e:
                    temp_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = np.nan
                    error_log.error("no vale at this tower {}_{}_{}".format(time_now,column_name,e))
                    error_log.error(temp_array.shape)
                    continue
            final_df = pd.merge(final_df , temp_df , left_index = True , right_index = True)
        return final_df
    except Exception as e:
        error_log.error("make_df_from_hdfs : {}".format(e))
        pass


def whole_multi_process_gk2a(gk2a_timelist , target_df , amos_tower_list , amos_info, channels , gk2a_features):
    cpu_count = preprocess_info.create_data_cpu_count
    hdfs_nas = preprocess_info.hdfs_nas
    # 타임 시리즈 리스트를 리스트 안에 여러개 리스트로 변경 [[ '201909051730' , '201909051730'] , ['201909051730' , '201909051730']]
    total_steps =   math.ceil(len(gk2a_timelist) / cpu_count)
    mp.set_start_method('forkserver', force=True)
    pool = mp.Pool(cpu_count)
    for each_step in range(1,total_steps+1):
        try:
            print("inserting")
            temp_gk2a_timelist=  gk2a_timelist[(each_step - 1) * cpu_count : each_step * cpu_count ]
            int_times = list(map(lambda x: int(x) ,  temp_gk2a_timelist))
            max_time = max(int_times)
            min_time = min(int_times)
            temp_df = target_df.loc[(target_df["time"] >= min_time) & (target_df["time"] <= max_time) , :  ]
            if hdfs_nas =="hdfs":
                res_lst = [[x , amos_tower_list  , temp_df , amos_info , gk2a_features] for x in temp_gk2a_timelist]
                pool.map( multi_processing_gk2a_each_hdfs , res_lst)
            elif hdfs_nas == "nas":
                res_lst = [ [x  ,amos_info , gk2a_features , temp_df , channels] for x in temp_gk2a_timelist   ]
                pool.map(multi_processing_gk2a_each_nas , res_lst)
        except Exception as e:
            print(e)
            continue

    pool.close()
    pool.join()



def whole_single_process_gk2a(gk2a_timelist , target_df , amos_tower_list , amos_info , channels , gk2a_features):

    cpu_count = preprocess_info.create_data_cpu_count
    hdfs_nas = preprocess_info.hdfs_nas
    total_steps =   math.ceil(len(gk2a_timelist) / cpu_count)

    for each_step in range(1,total_steps+1):
        print("inserting")
        temp_gk2a_timelist=  gk2a_timelist[(each_step - 1) * cpu_count : each_step * cpu_count ]
        int_times = list(map(lambda x: int(x) ,  temp_gk2a_timelist))
        max_time = max(int_times)
        min_time = min(int_times)
        temp_df = target_df.loc[(target_df["time"] >= min_time) & (target_df["time"] <= max_time) , :  ]

        if hdfs_nas =="hdfs":
            res_lst = [[x , amos_tower_list  , temp_df , amos_info , gk2a_features] for x in temp_gk2a_timelist]
            for res in res_lst:
                multi_processing_gk2a_each(res)

        elif hdfs_nas == "nas":
            res_lst = [ [x  ,amos_info , gk2a_features , temp_df , channels] for x in temp_gk2a_timelist   ]
            for res in res_lst:
                multi_processing_gk2a_each_nas(res)


# (HDFS 버전) : 한 시간대에서 AMOS 지점, gk2a 값, 시정거리 값 매핑 된 데이터 프레임 생성 후 학습용 데이터베이스 테이블에 삽입 (D_FOG_DET_TRAIN_GK2A)
def multi_processing_gk2a_each_hdfs(arg_lst):
    log = fog_logging.get_log_view(1, "platform", False, 'fog_create_data_multiprocess_gk2a_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_create_data_multiprocess_gk2a_error')
    try:
        host = tb_info.tb_host
        db_name = tb_info.tb_db_name
        user_name = tb_info.tb_user_name
        password = tb_info.tb_password
        port = tb_info.tb_port
        data_collection_path , root_path  = set_globvar_to_path("platform")
        conn, cursor = open_connection(host,db_name,user_name,password,port , root_path)
        each_time = arg_lst[0]
        amos_tower_list = arg_lst[1]
        target_df = arg_lst[2]
        amos_info = arg_lst[3]
        gk2a_features = arg_lst[4]
        df_index = list(map(lambda x: each_time + "_" + x , amos_tower_list))

        gk2a_real_time = datetime.strptime(each_time , "%Y%m%d%H%M" ) - timedelta(hours = 9)
        gk2a_each_time = datetime.strftime(gk2a_real_time  ,"%Y%m%d%H%M")
        hdfs_paths     = get_hdfs_path_list_by_time(gk2a_each_time, gk2a_features )

        size_to_resoultion = {"05":"3600" , "10": "1800" , "20":"900" }

        final_df = make_df_from_hdfs(hdfs_paths , log, each_time , amos_info, size_to_resoultion, df_index , error_log)
        final_df["time_base"] = final_df.index.tolist()
        final_df = final_df.merge(target_df , on = "time_base" , how = "left")

        insert_gk2a_train_table(final_df , cursor , log ,error_log)
        conn.close()
        cursor.close()
    except Exception as e:
        error_log.error("error multi_processing_gk2a_each : {}".format(e))
        pass


def get_file_path_list_each_time(each_time, channels):
    yearmonth = each_time[:6]
    day = each_time[6:8]
    hour = each_time[8:10]
    file_path_list = []
    for channel in channels:
        if "vi" in channel:
            if channel == "vi006":
                size = "05"
            else:
                size = "10"
        else:
            size = "20"
        try:
            temp_path = collecting.data_collection_path_nas_gk2a.format(yearmonth , day , hour , channel  , size , each_time) #f'{data_collection_path}/GK2A/ko/result/{yearmonth}/{day}/{hour}/result/gk2a_ami_le1b_{channel}_ko0{size}lc_{each_time}.nc/'
            if Path(temp_path).is_dir():
                files = list(map(lambda x: os.path.join(temp_path , x) ,  os.listdir(temp_path) ))
                for file in files:
                    file_path_list.append(file)
            else:
                print("no directory at {}".format(temp_path))
                continue
        except:
            print("no data")
            continue
    return file_path_list


# 한 시간대에서 AMOS 지점, HSR 값, 매핑 된 데이터 프레임 생성 후 학습용 데이터베이스 테이블에 삽입 (D_FOG_DET_TRAIN_hsr)
def multi_processing_hsr_each(arg_lst):
    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port
    hdfs_nas = preprocess_info.hdfs_nas

    data_collection_path , root_path  = set_globvar_to_path("platform")

    conn, cursor = open_connection(host,db_name,user_name,password,port , root_path)

    each_time = arg_lst[0]

    hsr_path = arg_lst[1]

    amos_info = arg_lst[2]

    log = fog_logging.get_log_view(1, "platform", False, 'fog_create_data_multiprocess_hsr_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_create_data_multiprocess_hsr_error')

    if hdfs_nas == "hdfs":
        final_df = make_hsr_df_from_hdfs(hsr_path , log, each_time , amos_info)

    elif hdfs_nas =="nas":
        final_df = make_hsr_df_from_nas( hsr_path ,log, each_time , amos_info)


    insert_hsr_train_table(final_df , cursor , log)


    conn.close()
    cursor.close()

def whole_multi_process_hsr(hsr_timelist  , amos_info):
    cpu_count = preprocess_info.create_data_cpu_count
    # 타임 시리즈 리스트를 리스트 안에 여러개 리스트 형태로 만듬
    total_steps = math.ceil(len(hsr_timelist) / cpu_count )
    hdfs_nas = preprocess_info.hdfs_nas
    for each_step in range(1 , total_steps  + 1 ):
        print("inserting hsr")
        temp_hsr_timelist = hsr_timelist[(each_step - 1)  * cpu_count : each_step * cpu_count ]

        if hdfs_nas == "hdfs":
            base_hsr_path = collecting.data_collection_path_hdfs_hsr
            hsr_path_list = list(map(lambda x: base_hsr_path.format(x[:6],x[6:8],x  ) , temp_hsr_timelist ))

        elif hdfs_nas == 'nas':

            base_hsr_path  = collecting.data_collection_path_nas_hsr
            hsr_path_list = list(map(lambda x: base_hsr_path.format(x[:6] , x[6:8] , x , x ) , temp_hsr_timelist   ))

        res_lst = [[x , y , amos_info]  for  x , y in zip ( temp_hsr_timelist , hsr_path_list)]

        mp.set_start_method('forkserver', force=True)
        pool = mp.Pool(cpu_count)

        pool.map( multi_processing_hsr_each , res_lst)

        pool.close()
        pool.join()


def whole_single_process_hsr(hsr_timelist  , amos_info):
    cpu_count = preprocess_info.create_data_cpu_count
    total_steps = math.ceil(len(hsr_timelist) / cpu_count )
    hdfs_nas = preprocess_info.hdfs_nas
    for each_step in range(1 , total_steps  + 1 ):
        print("inserting hsr")
        temp_hsr_timelist = hsr_timelist[(each_step - 1)  * cpu_count : each_step * cpu_count ]

        if hdfs_nas == "hdfs":
            base_hsr_path = '/hadoop/hdfs/data/PARSER/HSR/{}/{}/RDR_CMP_HSR_PUB_{}.bin'
            hsr_path_list = list(map(lambda x: base_hsr_path.format(x[:6],x[6:8],x  ) , temp_hsr_timelist ))

        elif hdfs_nas == 'nas':

            base_hsr_path  = '/gisangdan/kans/data-ext/HSR/result/{}/{}/result/RDR_CMP_HSR_PUB_{}.bin/RDR_CMP_HSR_PUB_{}.txt'
            hsr_path_list = list(map(lambda x: base_hsr_path.format(x[:6] , x[6:8] , x , x ) , temp_hsr_timelist   ))

        res_lst = [[x , y , amos_info]  for  x , y in zip ( temp_hsr_timelist , hsr_path_list)]
        for res in res_lst:
            multi_processing_hsr_each(res)


# (NAS 버전): 한 시간대에서 AMOS 지점, 위성 gk2a 값, 시정거리 값 매핑 된 데이터 프레임 생성 후 학습용 데이터베이스 테이블에 삽입 (D_FOG_DET_TRAIN_GK2A)
def multi_processing_gk2a_each_nas(arg_lst):

    log = fog_logging.get_log_view(1, "platform", False, 'fog_create_data_multiprocess_gk2a_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_create_data_multiprocess_gk2a_error')

    try:
        host = tb_info.tb_host
        db_name = tb_info.tb_db_name
        user_name = tb_info.tb_user_name
        password = tb_info.tb_password
        port = tb_info.tb_port

        data_collection_path , root_path  = set_globvar_to_path("platform")
        conn, cursor = open_connection(host,db_name,user_name,password,port , root_path)

        each_time = arg_lst[0]
        amos_info = arg_lst[1]
        all_columns = arg_lst[2]
        temp_target_df = arg_lst[3]
        channels = arg_lst[4]


        gk2a_real_time = datetime.strptime(each_time , "%Y%m%d%H%M" ) - timedelta(hours = 9)
        gk2a_each_time = datetime.strftime(gk2a_real_time  ,"%Y%m%d%H%M")
        whole_list = get_file_path_list_each_time(gk2a_each_time, channels)

        current_list = [whole_list , amos_info , all_columns]
        current_final_df  = data_load_get_pixels(current_list, each_time , log , error_log)
        whole_final_df = current_final_df.merge(temp_target_df , how = "left", on = "time_base" )

        insert_gk2a_train_table(whole_final_df , cursor , log ,error_log)
        conn.close()
        cursor.close()
    except Exception as e:
        error_log.info("multi_processing_gk2a_each_nas : {}".format(e))
        pass


def data_load_get_pixels(tuple_arg, each_time , log , error_log):
    final_returning_df  = pd.DataFrame( columns = ["time_base"])

    amos_info = tuple_arg[1]
    all_columns = tuple_arg[2]
    whole_list = tuple_arg[0]

    for final_file_path in whole_list:
        returning_df  = pd.DataFrame()
        try:

            time_now = final_file_path.split(".nc")[1].split("_")[-1]
            kst_real_time = datetime.strptime(time_now , "%Y%m%d%H%M") + timedelta(hours = 9)
            kst_time = datetime.strftime(kst_real_time , "%Y%m%d%H%M")

            file_name = final_file_path.split("/")[-1]
            column_name = "{}_{}".format(  file_name.split("_")[3] , file_name.split("_")[5].split(".")[2])

            if column_name not in all_columns:
                if "ncAlbedo" in file_name:
                    if "{}_Albedo".format(file_name.split("_")[3]) in all_columns:
                        column_name = "{}_Albedo".format(file_name.split("_")[3])
                        log.info("chaning column name to {}".format(column_name))
                    else:
                        log.info("wrong column name : {}".format(column_name))
                        log.info(final_file_path)
                else:
                    log.info("wrong column name : {}".format(column_name))
                    log.info(final_file_path)
                    log.info("skipping")
                    continue
            if file_name.endswith("npz"):
                temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
            else:
                log.info("reading")
                temp_df = pd.read_csv(final_file_path ,header = None ,chunksize = 1000)
                log.info("chunk done")
                temp_df = temp_df.read()

                log.info("reading done")
                temp_array = temp_df.values
            log.info("loaded : {}".format(column_name))
        except Exception as e:
            error_log.error("loading error : {}".format(e))
            continue
        try:


            if "vi" in column_name:
                if "vi006" in column_name:
                    resolution = 3600
                elif "vi008" in column_name:
                    if temp_array.shape == (1800,1800)  and type(temp_array[0,1799]) ==str:
                        temp_array = np.array(temp_array[:,:-10])
                        resolution  = 1799
                    elif temp_array.shape == (1800,1801) and type(temp_array[0,1800]) == str:
                        temp_array = np.array(temp_array[:,:-10])
                        resolution = 1800
                    else:
                        resoultion = 1800
                else:
                    resolution = 1800
            else:
                resolution = 900

            for amos_info_tower in amos_info['tower']:
                try:
                    lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
                    the_value = temp_array[lat][lon]
                    try:
                        the_float_value = float(the_value)
                        returning_df.loc[kst_time+ "_" + str(amos_info_tower), column_name] = the_float_value
                        if ((the_float_value > 0) == False) and ((the_float_value < 0) == False):
                            log.info(" \n \n np nan found \n \n")
                            log.info("trying numeric")
                            y_length = temp_array.shape[0]
                            x_length = temp_array.shape[1]
                            temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
                            temp_array = np.reshape(temp_array  , (y_length , x_length) )
                            lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
                            the_value = temp_array[lat][lon]
                            the_float_value = float(the_value)
                            returning_df.loc[kst_time+ "_" + str(amos_info_tower), column_name] = float(temp_array[lat][lon])
                            log.info("numeric worked")
                    except:
                        log.info(" \n \n np nan found \n \n")
                        log.info("trying numeric")
                        y_length = temp_array.shape[0]
                        x_length = temp_array.shape[1]
                        temp_array  =  pd.to_numeric( temp_array.flatten() , errors='coerce')
                        temp_array = np.reshape(temp_array  , (y_length , x_length) )
                        lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
                        the_value = temp_array[lat][lon]
                        the_float_value = float(the_value)
                        returning_df.loc[kst_time+ "_" + str(amos_info_tower), column_name] = float(temp_array[lat][lon])
                        log.info("numeric worked")
                except Exception as e:
                    returning_df.loc[kst_time+ "_" + str(amos_info_tower), column_name] = np.nan
                    error_log.error("no vale at this tower {}_{}_{}".format(time_now,column_name,e))
                    error_log.error(temp_array.shape)
                    error_log.error(final_file_path)
                    continue

            #final_df = returning_df
            returning_df["time_base"] = returning_df.index.tolist()
            final_returning_df = final_returning_df.merge(returning_df , how = "outer")
        except Exception as e:
            error_log.error("unknown error : {}".format(e))
            continue
    return final_returning_df


#~~
def whole_main(train_data_params , env , log ,error_log):
    a = datetime.now()

    total_train_start_ticket = train_data_params["total_train_start_ticket"]
    total_train_end_ticket   = train_data_params["total_train_end_ticket"]

    amos_train_start_ticket  = train_data_params["amos_train_start_ticket"]
    amos_train_end_ticket    = train_data_params["amos_train_end_ticket"]

    # 데이터를 생성  할때는 학습 할때와 달리 일단 그냥 interval 가장 길게 다 긁어 온다
    train_start_ticket = min(int(total_train_start_ticket) , int(amos_train_start_ticket))
    log.info("start date : {}".format(train_start_ticket))
    train_end_ticket = max(int(total_train_end_ticket) , int(amos_train_end_ticket) )


    data_collection_path , root_path  = set_globvar_to_path(env)

    host = tb_info.tb_host
    db_name = tb_info.tb_db_name
    user_name = tb_info.tb_user_name
    password = tb_info.tb_password
    port = tb_info.tb_port

    conn, cursor = open_connection(host,db_name,user_name,password,port , root_path)

    col_list_dic = {"K1":"time","K2":"tower","SU7":"target"}
    where_phrase = 'K2 != 153'
    # 시간, 지점, 시정거리 데이터프레임 생성 (153번 타워 제외)
    target_df  = make_amos_df(train_start_ticket , train_end_ticket , cursor , col_list_dic , where_phrase)

    col_list_dic_153 = {"K1":"time","K2":"tower","SU8":"target"}
    where_phrase_153 = 'K2 = 153'
    # 시간, 지점, 시정거리 데이터프레임 생성 (153번 타워 전용)
    target_df_153  = make_amos_df(train_start_ticket , train_end_ticket , cursor , col_list_dic_153 , where_phrase_153)

    # 153번 타워 데이터프레임 과 나머지 타워 데이터프레임 합체
    target_df = pd.concat([target_df , target_df_153] , ignore_index = True)
    # 데이터프레임 칼럼별 데이터 타입 세팅
    target_df ,amos_info= modify_amos_df(target_df, train_start_ticket , train_end_ticket, cursor ,root_path)
    # 태양 천정각 빠른 방식으로 계산
    target_df =  get_train_zenith_faster(target_df , amos_info)
    # 수집 데이터 정보 딕셔너리에서 key 값만 갖고 오기
    channels =[*collecting_data_info.gk2a_features_dic]

    fmt = '%Y%m%d%H%M'
    start = datetime.strptime(str(train_start_ticket), fmt)
    end = datetime.strptime(str(train_end_ticket + 1), fmt)

    # hsr gk2a 시간이 예전에는 다른 단위로 수집 했지만 지금은 둘다 10분 단위 이다. 따라서 변수를 따로 생성 할 필요는 없지만 코드 수정은 안되어 있다
    gk2a_timelist = []
    for result in perdelta(start , end , timedelta(minutes=10)):
        gk2a_timelist.append(result)
    gk2a_timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , gk2a_timelist ))

    hsr_timelist = []
    for result in perdelta(start , end , timedelta(minutes=10)):
        hsr_timelist.append(result)
    hsr_timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , hsr_timelist ))

    gk2a_features = collecting_data_info.gk2a_features_list
    amos_tower_list = amos_info.tower.tolist()

    # 타임시리즈에서 gk2a 데이터 병렬로 읽기
    whole_multi_process_gk2a(gk2a_timelist , target_df , amos_tower_list , amos_info  , channels , gk2a_features)

    if collecting_data_info.hsr_feature_collecting_bool == True:
        # 타임시리즈에서 hsr 데이터 병렬로 읽기
        whole_multi_process_hsr(hsr_timelist  , amos_info)

    b = datetime.now()
    print(b-a)










if __name__ == '__main__':
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
    class making_class:
        def __init__(self , path , dicting ):
            self.model_path = path
            self.param_info = dicting
    dicting  =   {'data_collecting'          : '2',
                  'model_training'           : '2',
                  'total_train_start_ticket' : '201910010300' ,
                  'total_train_end_ticket'   : '201910010500',
                  'amos_train_start_ticket'  : '201910010300',
                  'amos_train_end_ticket'    : '201910010500',
                  'custom_features'          : '2',
                  'seq_len_total'            : '3',
                  'each_seq_minutes_total'   : '10',
                  'conv1_total'              : '32',
                  'conv2_total'              : '32',
                  'dense1_total'             : '64',
                  'dense2_total'             : '64',
                  'dense1_amos'              : '128',
                  'dense2_amos'              : '64',
                  'batch_size'           : '30',
                  'epoch'                : '20',
                  'previous_model'           : '1',
                  'es_patience'              : '10',
                  'weight_0_1000'            : '20',
                  'weight_1000_2000'         : '20',
                  'weight_2000_3000'         : '20',
                  'weight_3000_4000'         : '20',
                  'weight_4000_5000'         : '10',
                  'weight_5000_all'          : '10',
                  'optimizer'                : '1',
                  'loss_func'                : '1',
                  'learning_rate'            : '0.001'}

    tm = making_class( '/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det/tempo_file', dicting )
    env = "platform"
    log = fog_logging.get_log_view(1, "platform", False, 'fog_ui_enfo_info')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'fog_ui_enfo_info_error')

    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_train_info')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_train_error')

    log.info('train starts')
    log.info('tm model path {}'.format(tm.model_path))
    param = tm.param_info
    log.info('prams ->{}'.format(param))
    data_collecting = str(param['data_collecting'])
    model_training = str(param["model_training"])
    train_data_params = {'total_train_start_ticket' :  str(param['total_train_start_ticket']),
                         'total_train_end_ticket'   :  str(param['total_train_end_ticket']),
                         'amos_train_start_ticket'  :  str(param['amos_train_start_ticket']),
                         'amos_train_end_ticket'    :  str(param['amos_train_end_ticket']),
                         'custom_features'          :  str(param['custom_features']),
                         'seq_len_total'            :  str(param['seq_len_total']),
                         'each_seq_minutes_total'   :  str(param['each_seq_minutes_total']),
                         'conv1_total'              :  str(param["conv1_total"]),
                         'conv2_total'              :  str(param['conv2_total']),
                         'dense1_total'             :  str(param['dense1_total']),
                         'dense2_total'             :  str(param['dense2_total']),
                         'dense1_amos'              :  str(param['dense1_amos']),
                         'dense2_amos'              :  str(param['dense2_amos']),
                         'batch_size_num'           :  str(param["batch_size"]),
                         'epoch_num'                :  str(param["epoch"]),
                         'previous_model'           :  str(param['previous_model']),
                         'es_patience'              :  str(param['es_patience']),
                         'weight_0_1000'            :  str(param['weight_0_1000']),
                         'weight_1000_2000'         :  str(param['weight_1000_2000']),
                         'weight_2000_3000'         :  str(param['weight_2000_3000']),
                         'weight_3000_4000'         :  str(param['weight_3000_4000']),
                         'weight_4000_5000'         :  str(param['weight_4000_5000']),
                         'weight_5000_all'          :  str(param['weight_5000_all']),
                         'optimizer'                :  str(param['optimizer']),
                         'loss_func'                :  str(param['loss_func']),
                         'learning_rate'            :  str(param['learning_rate'])}

    whole_main(train_data_params , env , log ,error_log)
