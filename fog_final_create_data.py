import sys
#sys.path.append("/home/platfrom_fog_det")
import os
# import mysql.connector
import time
import csv
import numpy as np
import pandas as pd
from sqlalchemy import create_engine, types
from solzr_zenith_angle import Koreazenith
import argparse
from fog_config import Config , collecting
import fog_config
#data_path = Config.data_local_path
import multiprocessing as mp
# from multiprocessing import cpu_count, current_process, Manager
# import concurrent.futures
import fog_logging
import jaydebeapi
import jpype
from datetime import date, datetime, timedelta
from pathlib import Path
from hdfs import InsecureClient

def return_amos_index(resolution,df, tower):
    if resolution == 1799:
        temp_resolution = 1800
        the_index = df.loc[df['tower'] == tower , ['lat_index_' + str(temp_resolution), 'lon_index_' + str(temp_resolution)]]
        lat_index = int(the_index['lat_index_' + str(temp_resolution)])
        lon_index = int(the_index['lon_index_' + str(temp_resolution)]) - 1
        return lat_index, lon_index
    else:
        the_index = df.loc[df['tower'] == tower , ['lat_index_' + str(resolution), 'lon_index_' + str(resolution)] ]
        lat_index = int(the_index['lat_index_' + str(resolution)])
        lon_index = int(the_index['lon_index_' + str(resolution)])
    return lat_index, lon_index


def deleting_last_col(resolution,df):
    if resolution ==900:
        if df.shape[1] == 900:
            return df
        elif df.shape[1] ==901:
            print('deleting last column')
            del df[900]
            return df
    elif resolution == 1800:
        if df.shape[1] == 1800:
            return df
        elif df.shape[1] == 1801:
            print('deleting last column')
            del df[1800]
            return df
    elif resolution ==3600:
        if df.shape[1] == 3600:
            return df
        elif df.shape[1]==3601:
            print('deleting last column')
            del df[3600]
            return df

# db_connection = connect_sql("192.168.0.181")
# db_connection.connect_db("fog_data")
# db_connection.conn.excute("DELETE FROM fog_data.gk2a_training_data_day_by201909 WHERE detail_time LIKE '20190914%';")
# db_connection.conn.execute("DELETE FROM fog_data.gk2a_training_data_day_by201909 WHERE detail_time LIKE '20190914%';")



def data_load(path):
    final_file_path = path
    file_name = final_file_path.split("/")[-1]
    if file_name.endswith("npz"):
        temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
    else:
        temp_array = pd.read_csv(final_file_path ,header = None,low_memory=False).values
    column_name = "{}_{}".format(  file_name.split("_")[3] , file_name.split("_")[5].split(".")[2])
    print("loaded : {}".format(column_name))
    return temp_array , column_name



def adding_rain_to_final_df(final_df,target_df):
    all_rain_list = list(map(lambda x: target_df.loc[target_df['time_tower'] == x ,'all_rain'].values[0] if len(target_df.loc[target_df['time_tower'] == x ,'all_rain']) != 0 else np.nan  , final_df.index.values.tolist() ))
    hour_rain_list = list(map(lambda x: target_df.loc[target_df['time_tower'] == x ,'hour_rain'].values[0] if len(target_df.loc[target_df['time_tower'] == x ,'hour_rain']) != 0 else np.nan  , final_df.index.values.tolist() ))
    final_df['all_rain'] = all_rain_list
    final_df['hour_rain'] = hour_rain_list
    return final_df


def adding_zenith_to_final_df(final_df, amos_info):
    time_tower = final_df.index.values.tolist()
    zenith_list = list(map(lambda x: Koreazenith(lati = float(amos_info.loc[amos_info['tower'] == x.split('_')[1], 'lat']),
    long =float(amos_info.loc[amos_info['tower'] == x.split('_')[1], 'lon']),
    year = int(x[0:4]),
    date_time = x[4:12] ) , time_tower  ))
    final_df['zenith'] = zenith_list
    return final_df
#/share/3_데이터/GK2A/ko/result/201909/01/00/result


def load_amos_info(root_path):
    amos_info=  pd.read_csv(os.path.join(root_path ,"amos_data_tower_info_hsr_included.csv" ), index_col = 0)
    amos_info["tower"] = list(map(lambda x: str(x) if len(str(x)) == 3 else "0" + str(x) , amos_info["tower"] ))
    return amos_info

def set_globvar_to_path(env):
    if env == "local":
        data_collection_path = collecting.data_collection_path_local
        root_path = Config.root_local_path
    elif env == "dgx_share1":
        data_collection_path = collecting.data_collection_path_dgx_docker_share1
        root_path = Config.root_dgx_path
    elif env =="dgx_share2":
        data_collection_path = collecting.data_collection_path_dgx_docker_share2
        root_path = Config.root_dgx_path
    elif env == "platform":
        data_collection_path = collecting.data_collection_path_platform
        root_path = Config.root_platform_path
    return data_collection_path , root_path


def make_amos_df(train_start_ticket, train_end_ticket , cursor):
    amos_table = "D_AMOS_DETAIL"
    cursor.execute("select * from {} where to_number(K1) between {} and {};".format(amos_table, train_start_ticket , train_end_ticket))
    data = cursor.fetchall()
    cursor.execute("select COLUMN_NAME from ALL_TAB_COLUMNS where TABLE_NAME='{}';".format(amos_table))
    column_names = cursor.fetchall()
    column_names = list(map(lambda x: x[0] , column_names ))
    amos_data = pd.DataFrame(data , columns = column_names)
    amos_data = amos_data.loc[:,["K1","K2", "TU4"]]
    amos_data = amos_data.rename(columns = {"K1":"time","K2":"tower","TU4":"target"})
    convert_dict = {"target":float}
    amos_data = amos_data.astype(convert_dict)


    amos_data['time_tower'] = list(map(lambda x ,y : x +"_"+ y , amos_data['time'], amos_data['tower']))
    return amos_data


def modify_amos_df(target_df , train_start_ticket, train_end_ticket, cursor , root_path):
    amos_info = load_amos_info(root_path)
    target_unique = list(target_df['tower'].unique())
    boollist = list(map(lambda x: x in target_unique , amos_info['tower'].tolist() ))
    amos_info = amos_info.loc[boollist,:]
    amos_info = amos_info.loc[:,["lat","lon", "lat_index_900","lon_index_900","lat_index_1800","lon_index_1800","lat_index_3600","lon_index_3600","tower", "lat_index_hsr", "lon_index_hsr" ]]
    convert_dict = {'lat':float,'lon':float,'lat_index_900':int, 'lon_index_900':int,'lat_index_1800':int,'lon_index_1800':int,'lat_index_3600':int,'lon_index_3600':int, 'tower':str , "lat_index_hsr":int, "lon_index_hsr":int}
    amos_info = amos_info.astype(convert_dict)
    boolist_target =  list(map(lambda x: x in amos_info['tower'].tolist() , target_df['tower'].tolist()   ))
    target_df = target_df.loc[boolist_target,:]
    target_df.set_index('time_tower', inplace = True,drop=False)
    target_df = target_df.rename(columns = {"time_tower":"time_base"})
    return target_df , amos_info

def perdelta(start, end, delta):
    curr = start
    while curr < end:
        yield curr
        curr += delta

def adding_zenith_to_final_df(final_df, amos_info):
    time_tower = final_df.index.values.tolist()
    zenith_list = list(map(lambda x: Koreazenith(lati = float(amos_info.loc[amos_info['tower'] == x.split('_')[1], 'lat']),
    long =float(amos_info.loc[amos_info['tower'] == x.split('_')[1], 'lon']),
    year = int(x[0:4]),
    date_time = x[4:12] ) , time_tower  ))
    final_df['zenith'] = zenith_list
    final_df['time'] = final_df['time'].astype(int)
    final_df['tower']  = final_df['tower'].astype(int)
    return final_df


def open_connection(host,db_name,user_name,password, port ,root_path):
    JDBC_DRIVER = root_path +  '/tibero6-jdbc.jar'
    if jpype.isJVMStarted() and not jpype.isThreadAttachedToJVM():
        jpype.attachThreadToJVM()
        jpype.java.lang.Thread.currentThread().setContextClassLoader(jpype.java.lang.ClassLoader.getSystemClassLoader())
    url = 'jdbc:tibero:thin:@{}:{}:{}'.format(host, port ,db_name)
    conn = jaydebeapi.connect('com.tmax.tibero.jdbc.TbDriver',url ,driver_args={'user': user_name, 'password' : password}, jars=str(JDBC_DRIVER))
    cursor=conn.cursor()
    return conn,cursor



def insert_all_timelist_hsr(timelist, amos_info, cursor):
    for each_time in timelist:
        final_df = pd.DataFrame()
        yearmonth = each_time[:6]
        day = each_time[6:8]
        hour = each_time[8:10]
        final_path = f'/{data_collection_path}/HSR/result/{yearmonth}/{day}/result/RDR_CMP_HSR_PUB_{each_time}.bin/RDR_CMP_HSR_PUB_{each_time}.txt'
        array = data_load_hsr(final_path)
        for amos_info_tower in amos_info["tower"]:
            try:
                lat , lon = return_hsr_index(amos_info, amos_info_tower)
                final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value" ] = float(array[lat][lon])
                final_df.loc[each_time+ "_" + str(amos_info_tower), "tower"] = int(amos_info_tower)
                final_df.loc[each_time+ "_" + str(amos_info_tower), "time"] = int(each_time)
            except Exception as e:
                final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value"] = np.nan
                print("no HSR value at this tower {} {}".format(each_time,e))
        final_df["time_base"] = final_df.index.tolist()
        final_df  = final_df.fillna(-50000)
        for i in final_df.index.tolist():
            try:
                sql = "INSERT into d_fog_det_train_hsr (time,tower, time_base, HSR_value ) VALUES ({},{},'{}',{})".format(int(final_df.loc[i ,"time"]) , int(final_df.loc[i,'tower']),final_df.loc[i,'time_base'], float(final_df.loc[i, 'HSR_value']) )
                cursor.execute(sql)
            except:
                pass





def data_load_hsr(path):
    final_file_path = path
    file_name = final_file_path.split("/")[-1]
    if file_name.endswith("npz"):
        temp_array = np.load(final_file_path, allow_pickle = True,mmap_mode="r")["arr_0"]
    else:
        temp_array = pd.read_csv(final_file_path ,header = None,low_memory=False).values
    return temp_array


def return_hsr_index(amos_info , tower):
    the_index = amos_info.loc[amos_info['tower'] == tower , ["lat_index_hsr" , "lon_index_hsr" ]]
    lat_index = int(the_index["lat_index_hsr"])
    lon_index = int(the_index["lon_index_hsr"])
    return lat_index , lon_index







def make_df_each_time(each_time, channels , amos_info , all_columns):
    final_df = pd.DataFrame()
    yearmonth = each_time[:6]
    day = each_time[6:8]
    hour = each_time[8:10]
    for channel in channels:
        if "vi" in channel:
            if channel == "vi006":
                size = "05"
            else:
                size = "10"
        else:
            size = "20"
        try:
            temp_path = f'{data_collection_path}/gk2a/ko/result/{yearmonth}/{day}/{hour}/result/gk2a_ami_le1b_{channel}_ko0{size}lc_{each_time}.nc/'
            if Path(temp_path).is_dir():
                files = list(map(lambda x: os.path.join(temp_path , x) ,  os.listdir(temp_path) ))
            else:
                print("no directory at {}".format(temp_path))
                continue
        except:
            pass
        for file_path in files:
            try:
                final_df = read_files(file_path, amos_info,final_df, each_time, all_columns)
            except:
                pass
    return final_df

def read_files(file_path , amos_info, final_df, each_time, all_columns):
    time_now = each_time
    temp_array , column_name = data_load(file_path)
    if column_name not in all_columns:
        if "ncAlbedo" in feature:
            if "{}_Albedo".format(feature.split("_")[3]) in all_columns:
                column_name = "{}_Albedo".format(feature.split("_")[3])
                print("chaning column name to {}".format(column_name))
            else:
                print("wrong column name : {}".format(column_name))
        else:
            print("wrong column name : {}".format(column_name))
    if "vi" in column_name:
        if "vi006" in column_name:
            resolution = 3600
        elif "vi008" in column_name:
            if temp_array.shape == (1800,1800)  and type(temp_array[0,1799]) ==str:
                #temp_array = np.array(temp_array[:,:-10] , dtype = float)
                resolution  = 1799
            elif temp_array.shape == (1800,1801) and type(temp_array[0,1800]) == str:
                #temp_array = np.array(temp_array[:,:-10] , dtype= float )
                resolution = 1800
            else:
                resoultion = 1800
        else:
            resolution = 1800
    else:
        resolution = 900
    for amos_info_tower in amos_info['tower']:
        try:
            lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
            final_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = float(temp_array[lat][lon])
        except Exception as e:
            final_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = np.nan
            print("no vale at this tower {}_{}_{}".format(time_now,column_name,e))
            print(temp_array.shape)
            continue
    return final_df






def get_amos_vis_rain_by_yearmonth(train_start_ticket , train_end_ticket):
    connecting = connect_sql('192.168.0.181')
    connecting.connect_db('fog_data')

    query =  'select * from fog_data.amos_vis_rain_all where Column_0 >={} and Column_0 < {};'.format(train_start_ticket, train_end_ticket)
    result = connecting.conn.execute(query).fetchall()
    target_df = pd.DataFrame(result)
    target_df = target_df.rename(columns = {0:'time',1:'tower',2:'target', 3:'all_rain',4:'hour_rain' })
    target_df['time'] = list(map(lambda x: str(x) , target_df['time']))
    target_df['tower']= list(map(lambda x: str(x) , target_df['tower']))
    target_df['time_tower'] = list(map(lambda x ,y : x +"_"+ y , target_df['time'], target_df['tower']))
    return target_df


################################################################################################################
################################################################################################################
################################################################################################################






def whole_main(train_start_ticket , train_end_ticket , env , log ,error_log):

    train_start_ticket = int(train_start_ticket)
    train_end_ticket = int(train_end_ticket)
    data_collection_path , root_path  = set_globvar_to_path(env)

    gk2a_inserting_columns = fog_config.d_fog_det_train_gk2a_columns.copy()
    gk2a_inserting_columns.remove("table_id")
    gk2a_inserting_columns.remove("time_base")


    #all_columns = fog_config.all_columns

    host = fog_config.tb_host
    db_name = fog_config.tb_db_name
    user_name = fog_config.tb_user_name
    password = fog_config.tb_password
    port = fog_config.tb_port

    conn, cursor = open_connection(host,db_name,user_name,password,port , root_path)

    #################################################UT-SFR-004-02-03###############################################################

    target_df = make_amos_df(train_start_ticket, train_end_ticket , cursor)
    target_df ,amos_info= modify_amos_df(target_df, train_start_ticket , train_end_ticket, cursor ,root_path)
    target_df = adding_zenith_to_final_df(target_df, amos_info)

    channels = []
    for i in fog_config.channel_feature_dic.keys():channels.append(i)

    fmt = '%Y%m%d%H%M'
    start = datetime.strptime(str(train_start_ticket), fmt)
    end = datetime.strptime(str(train_end_ticket + 1), fmt)
    timelist = []
    for result in perdelta(start , end , timedelta(minutes=10)):
        timelist.append(result)
    timelist = list(map(lambda x: x.strftime("%Y%m%d%H%M") , timelist ))

######################################################################
    client_path = fog_config.hdfs_client
    client_backup_path = fog_config.hdfs_client_backup
    client = InsecureClient(client_path, user="hdfs")
    client_backup = InsecureClient(client_backup_path, user="hdfs")
    chanels = fog_config.all_columns
    size_to_resoultion = {"05":"3600" , "10": "1800" , "20":"900" }
    amos_tower_list = amos_info.tower.tolist()

    for each_time in timelist:
        df_index = list(map(lambda x: each_time + "_" + x , amos_tower_list))
    # each_time = timelist[0]

        hdfs_paths = get_hdfs_path_list_by_time(each_time, chanels)
        final_df = make_df_from_hdfs(client, client_backup , hdfs_paths , log, each_time , amos_info, size_to_resoultion, df_index)


        final_df["time_base"] = final_df.index.tolist()

        final_df = final_df.merge(target_df , on = "time_base" , how = "left")

        insert_gk2a_train_table(final_df , cursor, gk2a_inserting_columns)

    base_hsr_path = '/hadoop/hdfs/data/PARSER/HSR/{}/{}/RDR_CMP_HSR_PUB_{}.bin'
    hsr_path_list = list(map(lambda x: base_hsr_path.format(x[:6],x[6:8],x  ) , timelist ))

    for hsr_path ,each_time in zip(hsr_path_list,timelist) :

        final_df = make_hsr_df_from_hdfs(client, client_backup , hsr_path , log , each_time , amos_info)
        log.info("inserting hsr")
        insert_hsr_train_table(final_df , cursor)



def insert_hsr_train_table(final_df , cursor):
    for i in final_df.index.tolist():
        try:
            sql = "INSERT into d_fog_det_train_hsr (time,tower, time_base, HSR_value ) VALUES ({},{},'{}',{})".format(int(final_df.loc[i ,"time"]) , int(final_df.loc[i,'tower']),final_df.loc[i,'time_base'], float(final_df.loc[i, 'HSR_value']) )
            cursor.execute(sql)
            print("inserting")
        except:
            pass


def make_hsr_df_from_hdfs(client, client_backup , hsr_path , log, each_time , amos_info):
    final_df = pd.DataFrame()
    path = hsr_path
    try:
        try:
            with client.read(path) as file:
                array = pd.read_csv(file, header = None).iloc[:,:-1].values
        except:
            with client_backup.read(path) as file:
                array = pd.read_csv(file, header = None).iloc[:,:-1].values
        log.info("loaded {} hsr file".format(each_time))

        for amos_info_tower in amos_info["tower"]:
            try:

                lat , lon = return_hsr_index(amos_info, amos_info_tower)

                final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value" ] = float(array[lat][lon])

                final_df.loc[each_time+ "_" + str(amos_info_tower), "tower"] = int(amos_info_tower)

                final_df.loc[each_time+ "_" + str(amos_info_tower), "time"] = int(each_time)
            except Exception as e:
                final_df.loc[each_time+ "_" + str(amos_info_tower), "HSR_value"] = np.nan
                log.info("no HSR value at this tower {} {}".format(each_time,e))
        final_df["time_base"] = final_df.index.tolist()
        return final_df
    except:
        log.info("cannot read file from : {}".format(path))
        pass



def insert_gk2a_train_table(final_df , cursor, gk2a_inserting_columns):
    for index in final_df.index.tolist():
        try:
            inserting_columns_sql = "{}".format(", ".join(map(str, gk2a_inserting_columns )))
            inserting_values = list(map(lambda x:  final_df.loc[index,x ] , gk2a_inserting_columns ))
            inserting_values_sql = "{}".format(", ".join(map(str, inserting_values )))
            string_col = "time_base"
            string_val = final_df.loc[index, "time_base"]

            final_sql = "INSERT INTO d_fog_det_train_gk2a ({}, {}) VALUES ({}, '{}');".format(inserting_columns_sql,string_col, inserting_values_sql,string_val)
            cursor.execute(final_sql)
        except:
            pass




def make_df_from_hdfs(client, client_backup , hdfs_paths , log, each_time , amos_info, size_to_resoultion, df_index):
    time_now = each_time
    final_df = pd.DataFrame(index =df_index)
    for path in hdfs_paths:
        temp_df = pd.DataFrame()
        column_name = path.split("/")[6].lower() + "_"+ path.split("/")[-1].split(".")[2].split("_")[0]
        size = path.split("ko0")[1][:2]
        resolution = size_to_resoultion[size]
        try:
            try:
                with client.read(path) as file:
                    temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
            except:
                with client_backup.read(path) as file:
                    temp_array = pd.read_csv(file, header = None).iloc[:,:-1].values
            log.info("loaded : {}".format(column_name))
        except:
            log.info("cannot read file from : {}".format(path))
            continue
        for amos_info_tower in amos_info['tower']:
            try:
                lat,lon = return_amos_index(resolution, amos_info, amos_info_tower)
                temp_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = float(temp_array[lat][lon])
            except Exception as e:
                temp_df.loc[time_now+ "_" + str(amos_info_tower), column_name] = np.nan
                print("no vale at this tower {}_{}_{}".format(time_now,column_name,e))
                print(temp_array.shape)
                continue
        final_df = pd.merge(final_df , temp_df , left_index = True , right_index = True)
    return final_df





def get_hdfs_path_list_by_time(ticketNo, chanels):
    convert_dict = {"Radiance" : "Radiance" , "Effective" : "Effective_Brightness_Temperature" , "Brightness" : "Brightness_Temperature" , "Albedo" :"Albedo"  }
    year_month = ticketNo[:6]
    day = ticketNo[6:8]
    final_path_list = []
    features = chanels.copy()
    if "zenith" in features: features.remove("zenith")
    if "HSR_value" in features: features.remove("HSR_value")
    if "target" in features: features.remove("target")
    for feature in features:
        channel = feature.split("_")[0]
        type = feature.split("_")[1]
        type = convert_dict[type]
        if "vi" in channel:
            if "vi006" in channel:
                distance = "05"
            else:
                distance = "10"
        else:
            distance = "20"
        final_path_list.append("/hadoop/hdfs/data/PARSER/GK2A/{}/{}/{}/gk2a_ami_le1b_{}_ko0{}lc_{}.nc.{}".format(channel.upper(),year_month,day,channel,distance,ticketNo,type))
    return final_path_list