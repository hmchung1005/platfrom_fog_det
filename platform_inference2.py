import sys
import os
sys.path.append('/gisangdan/kans/ai/src/fog-det-trac/platfrom_fog_det')
import tensorflow as tf
import fog_logging
import numpy as np
import fog_preprocess
import fog_data_input_validation     ##################
import fog_postprocess
import logging
import fog_database


def train(tm):
    try:
        import fog_create_data
        import fog_create_model
        log = fog_logging.get_log_view(1, "platform", False, 'fog_det_train_info')
        error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_train_error')
        #inf.add_hosts()
        log.info('train starts')
        log.info('tm model path {}'.format(tm.model_path))
        param = tm.param_info
        log.info('prams ->{}'.format(param))
        train_start_ticket = str(param['train_start_ticket'])
        train_end_ticket = str(param['train_end_ticket'])
        data_collecting = str(param['data_collecting'])
        model_training = str(param["model_training"])


        if data_collecting != "1":
            try:
                fog_create_data.whole_main(train_start_ticket, train_end_ticket , "platform", log, error_log)
                log.info("fog_create_data")
            except Exception as e:
                error_log.error("error fog_create_data {}".format(e))
        else:
            pass

        if model_training != "1":
            try:
                batch_size_num = str(param["batch_size_num"])
                epoch_num = str(param["epoch_num"])
                previous_model = str(param["previous_model"])
                model_name_path = fog_create_model.whole_main(train_start_ticket,train_end_ticket, batch_size_num ,epoch_num ,previous_model , tm.model_path, "platform" , log , error_log)
                log.info("fog_create_model at {}".format(model_name_path ))
            except Exception as e:
                error_log.error("error fog_create_model {}".format(e))
        else:
            pass

    except Exception as e:
        error_log.error('train error :{}'.format(e))




def init_svc(im):
    import fog_loading_model  ################

    log = fog_logging.get_log_view(1, "platform", False, 'fog_det_init_error')
    error_log = fog_logging.get_log_view(1, "platform", True, 'fog_det_init_error')
    model_path = im.model_path

    log.info('model path ->{}'.format(model_path))
    log.info('model path exists : {}'.format(os.path.exists(model_path)))

    try:
        model , summary_path = fog_loading_model.main(model_path ,"platform",log, error_log)   #####################
        log.info('model loaded')
    except Exception as e:
        error_log.error('model loading error :{}'.format(e))
    model_param = {'model_path': model_path, 'saved_model':model , 'summary_path':summary_path }

    param = im.param_info
    params = {**param, **model_param}
    return params


def data_input_validation(ticketNo):
    '''check data'''

def inference(df, params, batch_size):
    log = fog_logging.get_log_view(1, "platform", False, 'inferencing_log')
    error_log = fog_logging.get_log_view(1, "platform" , True, 'inferencing_error_log')


    log.info('df : {}, params : {}'.format(df,params))
    ticket_df=df[0]
    ticketNo = ticket_df.tolist()[0]
    log.info('ticketNo : {}'.format(ticketNo))

    is_null = fog_data_input_validation.main(ticketNo)   ########################
    if is_null:
        error_log.error("Input data has null")
        return False
    else:
        log.info('call_threadfunc start')
        call_threadfunc(ticketNo, log, error_log , params)
        log.info('inference end')
        return True


def processing_after_call(ticket_tuple):
    ticketNo, log, error_log, params = ticket_tuple
    print(ticketNo , " : ticket")
    log.info('fog_preprocess start')
    result_df = fog_preprocess.main(ticketNo, "platform" , params["summary_path"] ,  log , error_log)
    log.info('fog_preprocess end')

    log.info('fog_postprocess start')
    final_df , watch_warn_df_list = fog_postprocess.main(ticketNo, result_df, "platform" , params ,log , error_log )
    log.info('fog_postprocess end')

    log.info('fog_database start')
    fog_database.main(final_df , watch_warn_df_list, "platform" , log, error_log )
    log.info('fog_database end')


def call_threadfunc(ticketNo, log, error_log, params):

    from concurrent.futures import ThreadPoolExecutor
    pool = ThreadPoolExecutor(max_workers=1)

    try:
        ticket_tuple = (ticketNo, log, error_log,  params)
        future = pool.submit(processing_after_call,(ticket_tuple))
    except Exception as e:
        error_log.error('processing_after_call error : {}'.format(e))

    log.info('call_threadfunc end')







class making_class:
    def __init__(self , path , start ,end , data_collecting , model_training , batch_size_num , epoch_num , previous_model ):
        self.model_path = path
        self.param_info = {"train_start_ticket":start , "train_end_ticket":end ,
                           "data_collecting":data_collecting , "model_training":model_training,
                           "batch_size_num": batch_size_num, "epoch_num":epoch_num,
                           "previous_model":previous_model }


        
import imp

log = fog_logging.get_log_view(1, "platform", False, 'ierence_info')
error_log = fog_logging.get_log_view(1, "platform" , True, 'ierence_error')
tm = making_class(os.getcwd() , "201909141300" , "201909141400" ,"1" , "2" , "1" , "5" ,"1")



train(tm)


tf.__version__




params = init_svc(im , log, error_log)


df = {0: np.array(["201909141310"])}

inference(df , params , None)



if __name__ == '__main__':
    tm = making_class("/home/platfrom_fog_det" , "201909010000" , "201909010030")
    im = making_class("/home/platfrom_fog_det" , "201909010000" , "201909010030")
    train(tm)
    params = init_svc(im)
    df = {0: np.array(["201909071640"])}
    batch_size   = None
    inference(df, params, batch_size)
